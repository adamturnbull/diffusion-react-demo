const diffusion = require('diffusion');

diffusion.connect({
    host        : 'localhost',
    port        : 8080,
    principal   : 'admin',
    credentials : 'password',
    secure: false
	}).then(function(session) {
	console.log('Connected!');
	
	var i = 0;
	
	// Create a JSON topic
	session.topics.add("foo/counter", diffusion.topics.TopicType.JSON);
	
	// Start updating the topic every second
	setInterval(function() {   
	    let toSend = { time: new Date().toJSON(), name: "name", count : i++ };
		session.topics.update("foo/counter", toSend);
	}, 1000);
});


