This is a very simple react js demo that receives data from Diffusion.

## Installation and running

If Diffusion is not installed locally with default credentials, then edit `src/index.js` and `sender.js` to match the server you are connecting to.

`npm start` - Build the project, start a browser and connect to the demo page.

`npm install diffusion` - Install Diffusion node library for the control client

`node ./sender.js` - Start sending data
