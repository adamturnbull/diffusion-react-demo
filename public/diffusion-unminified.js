(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.diffusion = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

},{}],2:[function(require,module,exports){

var util = require(42);

var pSlice = Array.prototype.slice;
var hasOwn = Object.prototype.hasOwnProperty;


var assert = module.exports = ok;


assert.AssertionError = function AssertionError(options) {
  this.name = 'AssertionError';
  this.actual = options.actual;
  this.expected = options.expected;
  this.operator = options.operator;
  if (options.message) {
    this.message = options.message;
    this.generatedMessage = false;
  } else {
    this.message = getMessage(this);
    this.generatedMessage = true;
  }
  var stackStartFunction = options.stackStartFunction || fail;

  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, stackStartFunction);
  }
  else {
    var err = new Error();
    if (err.stack) {
      var out = err.stack;

      var fn_name = stackStartFunction.name;
      var idx = out.indexOf('\n' + fn_name);
      if (idx >= 0) {
        var next_line = out.indexOf('\n', idx + 1);
        out = out.substring(next_line + 1);
      }

      this.stack = out;
    }
  }
};

util.inherits(assert.AssertionError, Error);

function replacer(key, value) {
  if (util.isUndefined(value)) {
    return '' + value;
  }
  if (util.isNumber(value) && !isFinite(value)) {
    return value.toString();
  }
  if (util.isFunction(value) || util.isRegExp(value)) {
    return value.toString();
  }
  return value;
}

function truncate(s, n) {
  if (util.isString(s)) {
    return s.length < n ? s : s.slice(0, n);
  } else {
    return s;
  }
}

function getMessage(self) {
  return truncate(JSON.stringify(self.actual, replacer), 128) + ' ' +
         self.operator + ' ' +
         truncate(JSON.stringify(self.expected, replacer), 128);
}



function fail(actual, expected, message, operator, stackStartFunction) {
  throw new assert.AssertionError({
    message: message,
    actual: actual,
    expected: expected,
    operator: operator,
    stackStartFunction: stackStartFunction
  });
}

assert.fail = fail;


function ok(value, message) {
  if (!value) fail(value, true, message, '==', assert.ok);
}
assert.ok = ok;


assert.equal = function equal(actual, expected, message) {
  if (actual != expected) fail(actual, expected, message, '==', assert.equal);
};


assert.notEqual = function notEqual(actual, expected, message) {
  if (actual == expected) {
    fail(actual, expected, message, '!=', assert.notEqual);
  }
};


assert.deepEqual = function deepEqual(actual, expected, message) {
  if (!_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'deepEqual', assert.deepEqual);
  }
};

function _deepEqual(actual, expected) {
  if (actual === expected) {
    return true;

  } else if (util.isBuffer(actual) && util.isBuffer(expected)) {
    if (actual.length != expected.length) return false;

    for (var i = 0; i < actual.length; i++) {
      if (actual[i] !== expected[i]) return false;
    }

    return true;

  } else if (util.isDate(actual) && util.isDate(expected)) {
    return actual.getTime() === expected.getTime();

  } else if (util.isRegExp(actual) && util.isRegExp(expected)) {
    return actual.source === expected.source &&
           actual.global === expected.global &&
           actual.multiline === expected.multiline &&
           actual.lastIndex === expected.lastIndex &&
           actual.ignoreCase === expected.ignoreCase;

  } else if (!util.isObject(actual) && !util.isObject(expected)) {
    return actual == expected;

  } else {
    return objEquiv(actual, expected);
  }
}

function isArguments(object) {
  return Object.prototype.toString.call(object) == '[object Arguments]';
}

function objEquiv(a, b) {
  if (util.isNullOrUndefined(a) || util.isNullOrUndefined(b))
    return false;
  if (a.prototype !== b.prototype) return false;
  if (util.isPrimitive(a) || util.isPrimitive(b)) {
    return a === b;
  }
  var aIsArgs = isArguments(a),
      bIsArgs = isArguments(b);
  if ((aIsArgs && !bIsArgs) || (!aIsArgs && bIsArgs))
    return false;
  if (aIsArgs) {
    a = pSlice.call(a);
    b = pSlice.call(b);
    return _deepEqual(a, b);
  }
  var ka = objectKeys(a),
      kb = objectKeys(b),
      key, i;
  if (ka.length != kb.length)
    return false;
  ka.sort();
  kb.sort();
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] != kb[i])
      return false;
  }
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!_deepEqual(a[key], b[key])) return false;
  }
  return true;
}


assert.notDeepEqual = function notDeepEqual(actual, expected, message) {
  if (_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'notDeepEqual', assert.notDeepEqual);
  }
};


assert.strictEqual = function strictEqual(actual, expected, message) {
  if (actual !== expected) {
    fail(actual, expected, message, '===', assert.strictEqual);
  }
};


assert.notStrictEqual = function notStrictEqual(actual, expected, message) {
  if (actual === expected) {
    fail(actual, expected, message, '!==', assert.notStrictEqual);
  }
};

function expectedException(actual, expected) {
  if (!actual || !expected) {
    return false;
  }

  if (Object.prototype.toString.call(expected) == '[object RegExp]') {
    return expected.test(actual);
  } else if (actual instanceof expected) {
    return true;
  } else if (expected.call({}, actual) === true) {
    return true;
  }

  return false;
}

function _throws(shouldThrow, block, expected, message) {
  var actual;

  if (util.isString(expected)) {
    message = expected;
    expected = null;
  }

  try {
    block();
  } catch (e) {
    actual = e;
  }

  message = (expected && expected.name ? ' (' + expected.name + ').' : '.') +
            (message ? ' ' + message : '.');

  if (shouldThrow && !actual) {
    fail(actual, expected, 'Missing expected exception' + message);
  }

  if (!shouldThrow && expectedException(actual, expected)) {
    fail(actual, expected, 'Got unwanted exception' + message);
  }

  if ((shouldThrow && actual && expected &&
      !expectedException(actual, expected)) || (!shouldThrow && actual)) {
    throw actual;
  }
}


assert.throws = function(block, error, message) {
  _throws.apply(this, [true].concat(pSlice.call(arguments)));
};

assert.doesNotThrow = function(block, message) {
  _throws.apply(this, [false].concat(pSlice.call(arguments)));
};

assert.ifError = function(err) { if (err) {throw err;}};

var objectKeys = Object.keys || function (obj) {
  var keys = [];
  for (var key in obj) {
    if (hasOwn.call(obj, key)) keys.push(key);
  }
  return keys;
};

},{"42":42}],3:[function(require,module,exports){
arguments[4][1][0].apply(exports,arguments)
},{"1":1}],4:[function(require,module,exports){
'use strict';


var TYPED_OK =  (typeof Uint8Array !== 'undefined') &&
                (typeof Uint16Array !== 'undefined') &&
                (typeof Int32Array !== 'undefined');


exports.assign = function (obj ) {
  var sources = Array.prototype.slice.call(arguments, 1);
  while (sources.length) {
    var source = sources.shift();
    if (!source) { continue; }

    if (typeof source !== 'object') {
      throw new TypeError(source + 'must be non-object');
    }

    for (var p in source) {
      if (source.hasOwnProperty(p)) {
        obj[p] = source[p];
      }
    }
  }

  return obj;
};


exports.shrinkBuf = function (buf, size) {
  if (buf.length === size) { return buf; }
  if (buf.subarray) { return buf.subarray(0, size); }
  buf.length = size;
  return buf;
};


var fnTyped = {
  arraySet: function (dest, src, src_offs, len, dest_offs) {
    if (src.subarray && dest.subarray) {
      dest.set(src.subarray(src_offs, src_offs + len), dest_offs);
      return;
    }
    for (var i = 0; i < len; i++) {
      dest[dest_offs + i] = src[src_offs + i];
    }
  },
  flattenChunks: function (chunks) {
    var i, l, len, pos, chunk, result;

    len = 0;
    for (i = 0, l = chunks.length; i < l; i++) {
      len += chunks[i].length;
    }

    result = new Uint8Array(len);
    pos = 0;
    for (i = 0, l = chunks.length; i < l; i++) {
      chunk = chunks[i];
      result.set(chunk, pos);
      pos += chunk.length;
    }

    return result;
  }
};

var fnUntyped = {
  arraySet: function (dest, src, src_offs, len, dest_offs) {
    for (var i = 0; i < len; i++) {
      dest[dest_offs + i] = src[src_offs + i];
    }
  },
  flattenChunks: function (chunks) {
    return [].concat.apply([], chunks);
  }
};


exports.setTyped = function (on) {
  if (on) {
    exports.Buf8  = Uint8Array;
    exports.Buf16 = Uint16Array;
    exports.Buf32 = Int32Array;
    exports.assign(exports, fnTyped);
  } else {
    exports.Buf8  = Array;
    exports.Buf16 = Array;
    exports.Buf32 = Array;
    exports.assign(exports, fnUntyped);
  }
};

exports.setTyped(TYPED_OK);

},{}],5:[function(require,module,exports){
'use strict';


function adler32(adler, buf, len, pos) {
  var s1 = (adler & 0xffff) |0,
      s2 = ((adler >>> 16) & 0xffff) |0,
      n = 0;

  while (len !== 0) {
    n = len > 2000 ? 2000 : len;
    len -= n;

    do {
      s1 = (s1 + buf[pos++]) |0;
      s2 = (s2 + s1) |0;
    } while (--n);

    s1 %= 65521;
    s2 %= 65521;
  }

  return (s1 | (s2 << 16)) |0;
}


module.exports = adler32;

},{}],6:[function(require,module,exports){
'use strict';


module.exports = {

  Z_NO_FLUSH:         0,
  Z_PARTIAL_FLUSH:    1,
  Z_SYNC_FLUSH:       2,
  Z_FULL_FLUSH:       3,
  Z_FINISH:           4,
  Z_BLOCK:            5,
  Z_TREES:            6,

  Z_OK:               0,
  Z_STREAM_END:       1,
  Z_NEED_DICT:        2,
  Z_ERRNO:           -1,
  Z_STREAM_ERROR:    -2,
  Z_DATA_ERROR:      -3,
  Z_BUF_ERROR:       -5,

  Z_NO_COMPRESSION:         0,
  Z_BEST_SPEED:             1,
  Z_BEST_COMPRESSION:       9,
  Z_DEFAULT_COMPRESSION:   -1,


  Z_FILTERED:               1,
  Z_HUFFMAN_ONLY:           2,
  Z_RLE:                    3,
  Z_FIXED:                  4,
  Z_DEFAULT_STRATEGY:       0,

  Z_BINARY:                 0,
  Z_TEXT:                   1,
  Z_UNKNOWN:                2,

  Z_DEFLATED:               8
};

},{}],7:[function(require,module,exports){
'use strict';



function makeTable() {
  var c, table = [];

  for (var n = 0; n < 256; n++) {
    c = n;
    for (var k = 0; k < 8; k++) {
      c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
    }
    table[n] = c;
  }

  return table;
}

var crcTable = makeTable();


function crc32(crc, buf, len, pos) {
  var t = crcTable,
      end = pos + len;

  crc ^= -1;

  for (var i = pos; i < end; i++) {
    crc = (crc >>> 8) ^ t[(crc ^ buf[i]) & 0xFF];
  }

  return (crc ^ (-1)); 
}


module.exports = crc32;

},{}],8:[function(require,module,exports){
'use strict';

var utils   = require(4);
var trees   = require(13);
var adler32 = require(5);
var crc32   = require(7);
var msg     = require(12);



var Z_NO_FLUSH      = 0;
var Z_PARTIAL_FLUSH = 1;
var Z_FULL_FLUSH    = 3;
var Z_FINISH        = 4;
var Z_BLOCK         = 5;


var Z_OK            = 0;
var Z_STREAM_END    = 1;
var Z_STREAM_ERROR  = -2;
var Z_DATA_ERROR    = -3;
var Z_BUF_ERROR     = -5;


var Z_DEFAULT_COMPRESSION = -1;


var Z_FILTERED            = 1;
var Z_HUFFMAN_ONLY        = 2;
var Z_RLE                 = 3;
var Z_FIXED               = 4;
var Z_DEFAULT_STRATEGY    = 0;

var Z_UNKNOWN             = 2;


var Z_DEFLATED  = 8;



var MAX_MEM_LEVEL = 9;
var MAX_WBITS = 15;
var DEF_MEM_LEVEL = 8;


var LENGTH_CODES  = 29;
var LITERALS      = 256;
var L_CODES       = LITERALS + 1 + LENGTH_CODES;
var D_CODES       = 30;
var BL_CODES      = 19;
var HEAP_SIZE     = 2 * L_CODES + 1;
var MAX_BITS  = 15;

var MIN_MATCH = 3;
var MAX_MATCH = 258;
var MIN_LOOKAHEAD = (MAX_MATCH + MIN_MATCH + 1);

var PRESET_DICT = 0x20;

var INIT_STATE = 42;
var EXTRA_STATE = 69;
var NAME_STATE = 73;
var COMMENT_STATE = 91;
var HCRC_STATE = 103;
var BUSY_STATE = 113;
var FINISH_STATE = 666;

var BS_NEED_MORE      = 1; 
var BS_BLOCK_DONE     = 2; 
var BS_FINISH_STARTED = 3; 
var BS_FINISH_DONE    = 4; 

var OS_CODE = 0x03; 

function err(strm, errorCode) {
  strm.msg = msg[errorCode];
  return errorCode;
}

function rank(f) {
  return ((f) << 1) - ((f) > 4 ? 9 : 0);
}

function zero(buf) { var len = buf.length; while (--len >= 0) { buf[len] = 0; } }


function flush_pending(strm) {
  var s = strm.state;

  var len = s.pending;
  if (len > strm.avail_out) {
    len = strm.avail_out;
  }
  if (len === 0) { return; }

  utils.arraySet(strm.output, s.pending_buf, s.pending_out, len, strm.next_out);
  strm.next_out += len;
  s.pending_out += len;
  strm.total_out += len;
  strm.avail_out -= len;
  s.pending -= len;
  if (s.pending === 0) {
    s.pending_out = 0;
  }
}


function flush_block_only(s, last) {
  trees._tr_flush_block(s, (s.block_start >= 0 ? s.block_start : -1), s.strstart - s.block_start, last);
  s.block_start = s.strstart;
  flush_pending(s.strm);
}


function put_byte(s, b) {
  s.pending_buf[s.pending++] = b;
}


function putShortMSB(s, b) {
  s.pending_buf[s.pending++] = (b >>> 8) & 0xff;
  s.pending_buf[s.pending++] = b & 0xff;
}


function read_buf(strm, buf, start, size) {
  var len = strm.avail_in;

  if (len > size) { len = size; }
  if (len === 0) { return 0; }

  strm.avail_in -= len;

  utils.arraySet(buf, strm.input, strm.next_in, len, start);
  if (strm.state.wrap === 1) {
    strm.adler = adler32(strm.adler, buf, len, start);
  }

  else if (strm.state.wrap === 2) {
    strm.adler = crc32(strm.adler, buf, len, start);
  }

  strm.next_in += len;
  strm.total_in += len;

  return len;
}


function longest_match(s, cur_match) {
  var chain_length = s.max_chain_length;      
  var scan = s.strstart; 
  var match;                       
  var len;                           
  var best_len = s.prev_length;              
  var nice_match = s.nice_match;             
  var limit = (s.strstart > (s.w_size - MIN_LOOKAHEAD)) ?
      s.strstart - (s.w_size - MIN_LOOKAHEAD) : 0;

  var _win = s.window; 

  var wmask = s.w_mask;
  var prev  = s.prev;


  var strend = s.strstart + MAX_MATCH;
  var scan_end1  = _win[scan + best_len - 1];
  var scan_end   = _win[scan + best_len];


  if (s.prev_length >= s.good_match) {
    chain_length >>= 2;
  }
  if (nice_match > s.lookahead) { nice_match = s.lookahead; }


  do {
    match = cur_match;


    if (_win[match + best_len]     !== scan_end  ||
        _win[match + best_len - 1] !== scan_end1 ||
        _win[match]                !== _win[scan] ||
        _win[++match]              !== _win[scan + 1]) {
      continue;
    }

    scan += 2;
    match++;

    do {
    } while (_win[++scan] === _win[++match] && _win[++scan] === _win[++match] &&
             _win[++scan] === _win[++match] && _win[++scan] === _win[++match] &&
             _win[++scan] === _win[++match] && _win[++scan] === _win[++match] &&
             _win[++scan] === _win[++match] && _win[++scan] === _win[++match] &&
             scan < strend);


    len = MAX_MATCH - (strend - scan);
    scan = strend - MAX_MATCH;

    if (len > best_len) {
      s.match_start = cur_match;
      best_len = len;
      if (len >= nice_match) {
        break;
      }
      scan_end1  = _win[scan + best_len - 1];
      scan_end   = _win[scan + best_len];
    }
  } while ((cur_match = prev[cur_match & wmask]) > limit && --chain_length !== 0);

  if (best_len <= s.lookahead) {
    return best_len;
  }
  return s.lookahead;
}


function fill_window(s) {
  var _w_size = s.w_size;
  var p, n, m, more, str;


  do {
    more = s.window_size - s.lookahead - s.strstart;



    if (s.strstart >= _w_size + (_w_size - MIN_LOOKAHEAD)) {

      utils.arraySet(s.window, s.window, _w_size, _w_size, 0);
      s.match_start -= _w_size;
      s.strstart -= _w_size;
      s.block_start -= _w_size;


      n = s.hash_size;
      p = n;
      do {
        m = s.head[--p];
        s.head[p] = (m >= _w_size ? m - _w_size : 0);
      } while (--n);

      n = _w_size;
      p = n;
      do {
        m = s.prev[--p];
        s.prev[p] = (m >= _w_size ? m - _w_size : 0);
      } while (--n);

      more += _w_size;
    }
    if (s.strm.avail_in === 0) {
      break;
    }

    n = read_buf(s.strm, s.window, s.strstart + s.lookahead, more);
    s.lookahead += n;

    if (s.lookahead + s.insert >= MIN_MATCH) {
      str = s.strstart - s.insert;
      s.ins_h = s.window[str];

      s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[str + 1]) & s.hash_mask;
      while (s.insert) {
        s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[str + MIN_MATCH - 1]) & s.hash_mask;

        s.prev[str & s.w_mask] = s.head[s.ins_h];
        s.head[s.ins_h] = str;
        str++;
        s.insert--;
        if (s.lookahead + s.insert < MIN_MATCH) {
          break;
        }
      }
    }

  } while (s.lookahead < MIN_LOOKAHEAD && s.strm.avail_in !== 0);

}

function deflate_stored(s, flush) {
  var max_block_size = 0xffff;

  if (max_block_size > s.pending_buf_size - 5) {
    max_block_size = s.pending_buf_size - 5;
  }

  for (;;) {
    if (s.lookahead <= 1) {


      fill_window(s);
      if (s.lookahead === 0 && flush === Z_NO_FLUSH) {
        return BS_NEED_MORE;
      }

      if (s.lookahead === 0) {
        break;
      }
    }

    s.strstart += s.lookahead;
    s.lookahead = 0;

    var max_start = s.block_start + max_block_size;

    if (s.strstart === 0 || s.strstart >= max_start) {
      s.lookahead = s.strstart - max_start;
      s.strstart = max_start;
      flush_block_only(s, false);
      if (s.strm.avail_out === 0) {
        return BS_NEED_MORE;
      }


    }
    if (s.strstart - s.block_start >= (s.w_size - MIN_LOOKAHEAD)) {
      flush_block_only(s, false);
      if (s.strm.avail_out === 0) {
        return BS_NEED_MORE;
      }
    }
  }

  s.insert = 0;

  if (flush === Z_FINISH) {
    flush_block_only(s, true);
    if (s.strm.avail_out === 0) {
      return BS_FINISH_STARTED;
    }
    return BS_FINISH_DONE;
  }

  if (s.strstart > s.block_start) {
    flush_block_only(s, false);
    if (s.strm.avail_out === 0) {
      return BS_NEED_MORE;
    }
  }

  return BS_NEED_MORE;
}

function deflate_fast(s, flush) {
  var hash_head;        
  var bflush;           

  for (;;) {
    if (s.lookahead < MIN_LOOKAHEAD) {
      fill_window(s);
      if (s.lookahead < MIN_LOOKAHEAD && flush === Z_NO_FLUSH) {
        return BS_NEED_MORE;
      }
      if (s.lookahead === 0) {
        break; 
      }
    }

    hash_head = 0;
    if (s.lookahead >= MIN_MATCH) {
      s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[s.strstart + MIN_MATCH - 1]) & s.hash_mask;
      hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
      s.head[s.ins_h] = s.strstart;
    }

    if (hash_head !== 0 && ((s.strstart - hash_head) <= (s.w_size - MIN_LOOKAHEAD))) {
      s.match_length = longest_match(s, hash_head);
    }
    if (s.match_length >= MIN_MATCH) {

      bflush = trees._tr_tally(s, s.strstart - s.match_start, s.match_length - MIN_MATCH);

      s.lookahead -= s.match_length;

      if (s.match_length <= s.max_lazy_match && s.lookahead >= MIN_MATCH) {
        s.match_length--; 
        do {
          s.strstart++;
          s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[s.strstart + MIN_MATCH - 1]) & s.hash_mask;
          hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
          s.head[s.ins_h] = s.strstart;
        } while (--s.match_length !== 0);
        s.strstart++;
      } else
      {
        s.strstart += s.match_length;
        s.match_length = 0;
        s.ins_h = s.window[s.strstart];
        s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[s.strstart + 1]) & s.hash_mask;

      }
    } else {
      bflush = trees._tr_tally(s, 0, s.window[s.strstart]);

      s.lookahead--;
      s.strstart++;
    }
    if (bflush) {
      flush_block_only(s, false);
      if (s.strm.avail_out === 0) {
        return BS_NEED_MORE;
      }
    }
  }
  s.insert = ((s.strstart < (MIN_MATCH - 1)) ? s.strstart : MIN_MATCH - 1);
  if (flush === Z_FINISH) {
    flush_block_only(s, true);
    if (s.strm.avail_out === 0) {
      return BS_FINISH_STARTED;
    }
    return BS_FINISH_DONE;
  }
  if (s.last_lit) {
    flush_block_only(s, false);
    if (s.strm.avail_out === 0) {
      return BS_NEED_MORE;
    }
  }
  return BS_BLOCK_DONE;
}

function deflate_slow(s, flush) {
  var hash_head;          
  var bflush;              

  var max_insert;

  for (;;) {
    if (s.lookahead < MIN_LOOKAHEAD) {
      fill_window(s);
      if (s.lookahead < MIN_LOOKAHEAD && flush === Z_NO_FLUSH) {
        return BS_NEED_MORE;
      }
      if (s.lookahead === 0) { break; } 
    }

    hash_head = 0;
    if (s.lookahead >= MIN_MATCH) {
      s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[s.strstart + MIN_MATCH - 1]) & s.hash_mask;
      hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
      s.head[s.ins_h] = s.strstart;
    }

    s.prev_length = s.match_length;
    s.prev_match = s.match_start;
    s.match_length = MIN_MATCH - 1;

    if (hash_head !== 0 && s.prev_length < s.max_lazy_match &&
        s.strstart - hash_head <= (s.w_size - MIN_LOOKAHEAD)) {
      s.match_length = longest_match(s, hash_head);

      if (s.match_length <= 5 &&
         (s.strategy === Z_FILTERED || (s.match_length === MIN_MATCH && s.strstart - s.match_start > 4096))) {

        s.match_length = MIN_MATCH - 1;
      }
    }
    if (s.prev_length >= MIN_MATCH && s.match_length <= s.prev_length) {
      max_insert = s.strstart + s.lookahead - MIN_MATCH;


      bflush = trees._tr_tally(s, s.strstart - 1 - s.prev_match, s.prev_length - MIN_MATCH);
      s.lookahead -= s.prev_length - 1;
      s.prev_length -= 2;
      do {
        if (++s.strstart <= max_insert) {
          s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[s.strstart + MIN_MATCH - 1]) & s.hash_mask;
          hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
          s.head[s.ins_h] = s.strstart;
        }
      } while (--s.prev_length !== 0);
      s.match_available = 0;
      s.match_length = MIN_MATCH - 1;
      s.strstart++;

      if (bflush) {
        flush_block_only(s, false);
        if (s.strm.avail_out === 0) {
          return BS_NEED_MORE;
        }
      }

    } else if (s.match_available) {
      bflush = trees._tr_tally(s, 0, s.window[s.strstart - 1]);

      if (bflush) {
        flush_block_only(s, false);
      }
      s.strstart++;
      s.lookahead--;
      if (s.strm.avail_out === 0) {
        return BS_NEED_MORE;
      }
    } else {
      s.match_available = 1;
      s.strstart++;
      s.lookahead--;
    }
  }
  if (s.match_available) {
    bflush = trees._tr_tally(s, 0, s.window[s.strstart - 1]);

    s.match_available = 0;
  }
  s.insert = s.strstart < MIN_MATCH - 1 ? s.strstart : MIN_MATCH - 1;
  if (flush === Z_FINISH) {
    flush_block_only(s, true);
    if (s.strm.avail_out === 0) {
      return BS_FINISH_STARTED;
    }
    return BS_FINISH_DONE;
  }
  if (s.last_lit) {
    flush_block_only(s, false);
    if (s.strm.avail_out === 0) {
      return BS_NEED_MORE;
    }
  }

  return BS_BLOCK_DONE;
}


function deflate_rle(s, flush) {
  var bflush;            
  var prev;              
  var scan, strend;      

  var _win = s.window;

  for (;;) {
    if (s.lookahead <= MAX_MATCH) {
      fill_window(s);
      if (s.lookahead <= MAX_MATCH && flush === Z_NO_FLUSH) {
        return BS_NEED_MORE;
      }
      if (s.lookahead === 0) { break; } 
    }

    s.match_length = 0;
    if (s.lookahead >= MIN_MATCH && s.strstart > 0) {
      scan = s.strstart - 1;
      prev = _win[scan];
      if (prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan]) {
        strend = s.strstart + MAX_MATCH;
        do {
        } while (prev === _win[++scan] && prev === _win[++scan] &&
                 prev === _win[++scan] && prev === _win[++scan] &&
                 prev === _win[++scan] && prev === _win[++scan] &&
                 prev === _win[++scan] && prev === _win[++scan] &&
                 scan < strend);
        s.match_length = MAX_MATCH - (strend - scan);
        if (s.match_length > s.lookahead) {
          s.match_length = s.lookahead;
        }
      }
    }

    if (s.match_length >= MIN_MATCH) {

      bflush = trees._tr_tally(s, 1, s.match_length - MIN_MATCH);

      s.lookahead -= s.match_length;
      s.strstart += s.match_length;
      s.match_length = 0;
    } else {
      bflush = trees._tr_tally(s, 0, s.window[s.strstart]);

      s.lookahead--;
      s.strstart++;
    }
    if (bflush) {
      flush_block_only(s, false);
      if (s.strm.avail_out === 0) {
        return BS_NEED_MORE;
      }
    }
  }
  s.insert = 0;
  if (flush === Z_FINISH) {
    flush_block_only(s, true);
    if (s.strm.avail_out === 0) {
      return BS_FINISH_STARTED;
    }
    return BS_FINISH_DONE;
  }
  if (s.last_lit) {
    flush_block_only(s, false);
    if (s.strm.avail_out === 0) {
      return BS_NEED_MORE;
    }
  }
  return BS_BLOCK_DONE;
}

function deflate_huff(s, flush) {
  var bflush;             

  for (;;) {
    if (s.lookahead === 0) {
      fill_window(s);
      if (s.lookahead === 0) {
        if (flush === Z_NO_FLUSH) {
          return BS_NEED_MORE;
        }
        break;      
      }
    }

    s.match_length = 0;
    bflush = trees._tr_tally(s, 0, s.window[s.strstart]);
    s.lookahead--;
    s.strstart++;
    if (bflush) {
      flush_block_only(s, false);
      if (s.strm.avail_out === 0) {
        return BS_NEED_MORE;
      }
    }
  }
  s.insert = 0;
  if (flush === Z_FINISH) {
    flush_block_only(s, true);
    if (s.strm.avail_out === 0) {
      return BS_FINISH_STARTED;
    }
    return BS_FINISH_DONE;
  }
  if (s.last_lit) {
    flush_block_only(s, false);
    if (s.strm.avail_out === 0) {
      return BS_NEED_MORE;
    }
  }
  return BS_BLOCK_DONE;
}

function Config(good_length, max_lazy, nice_length, max_chain, func) {
  this.good_length = good_length;
  this.max_lazy = max_lazy;
  this.nice_length = nice_length;
  this.max_chain = max_chain;
  this.func = func;
}

var configuration_table;

configuration_table = [
  new Config(0, 0, 0, 0, deflate_stored),          
  new Config(4, 4, 8, 4, deflate_fast),            
  new Config(4, 5, 16, 8, deflate_fast),           
  new Config(4, 6, 32, 32, deflate_fast),          

  new Config(4, 4, 16, 16, deflate_slow),          
  new Config(8, 16, 32, 32, deflate_slow),         
  new Config(8, 16, 128, 128, deflate_slow),       
  new Config(8, 32, 128, 256, deflate_slow),       
  new Config(32, 128, 258, 1024, deflate_slow),    
  new Config(32, 258, 258, 4096, deflate_slow)     
];


function lm_init(s) {
  s.window_size = 2 * s.w_size;

  zero(s.head); 

  s.max_lazy_match = configuration_table[s.level].max_lazy;
  s.good_match = configuration_table[s.level].good_length;
  s.nice_match = configuration_table[s.level].nice_length;
  s.max_chain_length = configuration_table[s.level].max_chain;

  s.strstart = 0;
  s.block_start = 0;
  s.lookahead = 0;
  s.insert = 0;
  s.match_length = s.prev_length = MIN_MATCH - 1;
  s.match_available = 0;
  s.ins_h = 0;
}


function DeflateState() {
  this.strm = null;            
  this.status = 0;            
  this.pending_buf = null;      
  this.pending_buf_size = 0;  
  this.pending_out = 0;       
  this.pending = 0;           
  this.wrap = 0;              
  this.gzhead = null;         
  this.gzindex = 0;           
  this.method = Z_DEFLATED; 
  this.last_flush = -1;   

  this.w_size = 0;  
  this.w_bits = 0;  
  this.w_mask = 0;  

  this.window = null;

  this.window_size = 0;

  this.prev = null;

  this.head = null;   

  this.ins_h = 0;       
  this.hash_size = 0;   
  this.hash_bits = 0;   
  this.hash_mask = 0;   

  this.hash_shift = 0;

  this.block_start = 0;

  this.match_length = 0;      
  this.prev_match = 0;        
  this.match_available = 0;   
  this.strstart = 0;          
  this.match_start = 0;       
  this.lookahead = 0;         

  this.prev_length = 0;

  this.max_chain_length = 0;

  this.max_lazy_match = 0;

  this.level = 0;     
  this.strategy = 0;  

  this.good_match = 0;

  this.nice_match = 0; 




  this.dyn_ltree  = new utils.Buf16(HEAP_SIZE * 2);
  this.dyn_dtree  = new utils.Buf16((2 * D_CODES + 1) * 2);
  this.bl_tree    = new utils.Buf16((2 * BL_CODES + 1) * 2);
  zero(this.dyn_ltree);
  zero(this.dyn_dtree);
  zero(this.bl_tree);

  this.l_desc   = null;         
  this.d_desc   = null;         
  this.bl_desc  = null;         

  this.bl_count = new utils.Buf16(MAX_BITS + 1);

  this.heap = new utils.Buf16(2 * L_CODES + 1);  
  zero(this.heap);

  this.heap_len = 0;               
  this.heap_max = 0;               

  this.depth = new utils.Buf16(2 * L_CODES + 1); 
  zero(this.depth);

  this.l_buf = 0;          

  this.lit_bufsize = 0;

  this.last_lit = 0;      

  this.d_buf = 0;

  this.opt_len = 0;       
  this.static_len = 0;    
  this.matches = 0;       
  this.insert = 0;        


  this.bi_buf = 0;
  this.bi_valid = 0;

}


function deflateResetKeep(strm) {
  var s;

  if (!strm || !strm.state) {
    return err(strm, Z_STREAM_ERROR);
  }

  strm.total_in = strm.total_out = 0;
  strm.data_type = Z_UNKNOWN;

  s = strm.state;
  s.pending = 0;
  s.pending_out = 0;

  if (s.wrap < 0) {
    s.wrap = -s.wrap;
  }
  s.status = (s.wrap ? INIT_STATE : BUSY_STATE);
  strm.adler = (s.wrap === 2) ?
    0  
  :
    1; 
  s.last_flush = Z_NO_FLUSH;
  trees._tr_init(s);
  return Z_OK;
}


function deflateReset(strm) {
  var ret = deflateResetKeep(strm);
  if (ret === Z_OK) {
    lm_init(strm.state);
  }
  return ret;
}


function deflateSetHeader(strm, head) {
  if (!strm || !strm.state) { return Z_STREAM_ERROR; }
  if (strm.state.wrap !== 2) { return Z_STREAM_ERROR; }
  strm.state.gzhead = head;
  return Z_OK;
}


function deflateInit2(strm, level, method, windowBits, memLevel, strategy) {
  if (!strm) { 
    return Z_STREAM_ERROR;
  }
  var wrap = 1;

  if (level === Z_DEFAULT_COMPRESSION) {
    level = 6;
  }

  if (windowBits < 0) { 
    wrap = 0;
    windowBits = -windowBits;
  }

  else if (windowBits > 15) {
    wrap = 2;           
    windowBits -= 16;
  }


  if (memLevel < 1 || memLevel > MAX_MEM_LEVEL || method !== Z_DEFLATED ||
    windowBits < 8 || windowBits > 15 || level < 0 || level > 9 ||
    strategy < 0 || strategy > Z_FIXED) {
    return err(strm, Z_STREAM_ERROR);
  }


  if (windowBits === 8) {
    windowBits = 9;
  }

  var s = new DeflateState();

  strm.state = s;
  s.strm = strm;

  s.wrap = wrap;
  s.gzhead = null;
  s.w_bits = windowBits;
  s.w_size = 1 << s.w_bits;
  s.w_mask = s.w_size - 1;

  s.hash_bits = memLevel + 7;
  s.hash_size = 1 << s.hash_bits;
  s.hash_mask = s.hash_size - 1;
  s.hash_shift = ~~((s.hash_bits + MIN_MATCH - 1) / MIN_MATCH);

  s.window = new utils.Buf8(s.w_size * 2);
  s.head = new utils.Buf16(s.hash_size);
  s.prev = new utils.Buf16(s.w_size);


  s.lit_bufsize = 1 << (memLevel + 6); 

  s.pending_buf_size = s.lit_bufsize * 4;

  s.pending_buf = new utils.Buf8(s.pending_buf_size);

  s.d_buf = 1 * s.lit_bufsize;

  s.l_buf = (1 + 2) * s.lit_bufsize;

  s.level = level;
  s.strategy = strategy;
  s.method = method;

  return deflateReset(strm);
}

function deflateInit(strm, level) {
  return deflateInit2(strm, level, Z_DEFLATED, MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY);
}


function deflate(strm, flush) {
  var old_flush, s;
  var beg, val; 

  if (!strm || !strm.state ||
    flush > Z_BLOCK || flush < 0) {
    return strm ? err(strm, Z_STREAM_ERROR) : Z_STREAM_ERROR;
  }

  s = strm.state;

  if (!strm.output ||
      (!strm.input && strm.avail_in !== 0) ||
      (s.status === FINISH_STATE && flush !== Z_FINISH)) {
    return err(strm, (strm.avail_out === 0) ? Z_BUF_ERROR : Z_STREAM_ERROR);
  }

  s.strm = strm; 
  old_flush = s.last_flush;
  s.last_flush = flush;

  if (s.status === INIT_STATE) {

    if (s.wrap === 2) { 
      strm.adler = 0;  
      put_byte(s, 31);
      put_byte(s, 139);
      put_byte(s, 8);
      if (!s.gzhead) { 
        put_byte(s, 0);
        put_byte(s, 0);
        put_byte(s, 0);
        put_byte(s, 0);
        put_byte(s, 0);
        put_byte(s, s.level === 9 ? 2 :
                    (s.strategy >= Z_HUFFMAN_ONLY || s.level < 2 ?
                     4 : 0));
        put_byte(s, OS_CODE);
        s.status = BUSY_STATE;
      }
      else {
        put_byte(s, (s.gzhead.text ? 1 : 0) +
                    (s.gzhead.hcrc ? 2 : 0) +
                    (!s.gzhead.extra ? 0 : 4) +
                    (!s.gzhead.name ? 0 : 8) +
                    (!s.gzhead.comment ? 0 : 16)
                );
        put_byte(s, s.gzhead.time & 0xff);
        put_byte(s, (s.gzhead.time >> 8) & 0xff);
        put_byte(s, (s.gzhead.time >> 16) & 0xff);
        put_byte(s, (s.gzhead.time >> 24) & 0xff);
        put_byte(s, s.level === 9 ? 2 :
                    (s.strategy >= Z_HUFFMAN_ONLY || s.level < 2 ?
                     4 : 0));
        put_byte(s, s.gzhead.os & 0xff);
        if (s.gzhead.extra && s.gzhead.extra.length) {
          put_byte(s, s.gzhead.extra.length & 0xff);
          put_byte(s, (s.gzhead.extra.length >> 8) & 0xff);
        }
        if (s.gzhead.hcrc) {
          strm.adler = crc32(strm.adler, s.pending_buf, s.pending, 0);
        }
        s.gzindex = 0;
        s.status = EXTRA_STATE;
      }
    }
    else 
    {
      var header = (Z_DEFLATED + ((s.w_bits - 8) << 4)) << 8;
      var level_flags = -1;

      if (s.strategy >= Z_HUFFMAN_ONLY || s.level < 2) {
        level_flags = 0;
      } else if (s.level < 6) {
        level_flags = 1;
      } else if (s.level === 6) {
        level_flags = 2;
      } else {
        level_flags = 3;
      }
      header |= (level_flags << 6);
      if (s.strstart !== 0) { header |= PRESET_DICT; }
      header += 31 - (header % 31);

      s.status = BUSY_STATE;
      putShortMSB(s, header);

      if (s.strstart !== 0) {
        putShortMSB(s, strm.adler >>> 16);
        putShortMSB(s, strm.adler & 0xffff);
      }
      strm.adler = 1; 
    }
  }

  if (s.status === EXTRA_STATE) {
    if (s.gzhead.extra) {
      beg = s.pending;  

      while (s.gzindex < (s.gzhead.extra.length & 0xffff)) {
        if (s.pending === s.pending_buf_size) {
          if (s.gzhead.hcrc && s.pending > beg) {
            strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
          }
          flush_pending(strm);
          beg = s.pending;
          if (s.pending === s.pending_buf_size) {
            break;
          }
        }
        put_byte(s, s.gzhead.extra[s.gzindex] & 0xff);
        s.gzindex++;
      }
      if (s.gzhead.hcrc && s.pending > beg) {
        strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
      }
      if (s.gzindex === s.gzhead.extra.length) {
        s.gzindex = 0;
        s.status = NAME_STATE;
      }
    }
    else {
      s.status = NAME_STATE;
    }
  }
  if (s.status === NAME_STATE) {
    if (s.gzhead.name) {
      beg = s.pending;  

      do {
        if (s.pending === s.pending_buf_size) {
          if (s.gzhead.hcrc && s.pending > beg) {
            strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
          }
          flush_pending(strm);
          beg = s.pending;
          if (s.pending === s.pending_buf_size) {
            val = 1;
            break;
          }
        }
        if (s.gzindex < s.gzhead.name.length) {
          val = s.gzhead.name.charCodeAt(s.gzindex++) & 0xff;
        } else {
          val = 0;
        }
        put_byte(s, val);
      } while (val !== 0);

      if (s.gzhead.hcrc && s.pending > beg) {
        strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
      }
      if (val === 0) {
        s.gzindex = 0;
        s.status = COMMENT_STATE;
      }
    }
    else {
      s.status = COMMENT_STATE;
    }
  }
  if (s.status === COMMENT_STATE) {
    if (s.gzhead.comment) {
      beg = s.pending;  

      do {
        if (s.pending === s.pending_buf_size) {
          if (s.gzhead.hcrc && s.pending > beg) {
            strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
          }
          flush_pending(strm);
          beg = s.pending;
          if (s.pending === s.pending_buf_size) {
            val = 1;
            break;
          }
        }
        if (s.gzindex < s.gzhead.comment.length) {
          val = s.gzhead.comment.charCodeAt(s.gzindex++) & 0xff;
        } else {
          val = 0;
        }
        put_byte(s, val);
      } while (val !== 0);

      if (s.gzhead.hcrc && s.pending > beg) {
        strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
      }
      if (val === 0) {
        s.status = HCRC_STATE;
      }
    }
    else {
      s.status = HCRC_STATE;
    }
  }
  if (s.status === HCRC_STATE) {
    if (s.gzhead.hcrc) {
      if (s.pending + 2 > s.pending_buf_size) {
        flush_pending(strm);
      }
      if (s.pending + 2 <= s.pending_buf_size) {
        put_byte(s, strm.adler & 0xff);
        put_byte(s, (strm.adler >> 8) & 0xff);
        strm.adler = 0; 
        s.status = BUSY_STATE;
      }
    }
    else {
      s.status = BUSY_STATE;
    }
  }

  if (s.pending !== 0) {
    flush_pending(strm);
    if (strm.avail_out === 0) {
      s.last_flush = -1;
      return Z_OK;
    }

  } else if (strm.avail_in === 0 && rank(flush) <= rank(old_flush) &&
    flush !== Z_FINISH) {
    return err(strm, Z_BUF_ERROR);
  }

  if (s.status === FINISH_STATE && strm.avail_in !== 0) {
    return err(strm, Z_BUF_ERROR);
  }

  if (strm.avail_in !== 0 || s.lookahead !== 0 ||
    (flush !== Z_NO_FLUSH && s.status !== FINISH_STATE)) {
    var bstate = (s.strategy === Z_HUFFMAN_ONLY) ? deflate_huff(s, flush) :
      (s.strategy === Z_RLE ? deflate_rle(s, flush) :
        configuration_table[s.level].func(s, flush));

    if (bstate === BS_FINISH_STARTED || bstate === BS_FINISH_DONE) {
      s.status = FINISH_STATE;
    }
    if (bstate === BS_NEED_MORE || bstate === BS_FINISH_STARTED) {
      if (strm.avail_out === 0) {
        s.last_flush = -1;
      }
      return Z_OK;
    }
    if (bstate === BS_BLOCK_DONE) {
      if (flush === Z_PARTIAL_FLUSH) {
        trees._tr_align(s);
      }
      else if (flush !== Z_BLOCK) { 

        trees._tr_stored_block(s, 0, 0, false);
        if (flush === Z_FULL_FLUSH) {
          zero(s.head); 

          if (s.lookahead === 0) {
            s.strstart = 0;
            s.block_start = 0;
            s.insert = 0;
          }
        }
      }
      flush_pending(strm);
      if (strm.avail_out === 0) {
        s.last_flush = -1; 
        return Z_OK;
      }
    }
  }

  if (flush !== Z_FINISH) { return Z_OK; }
  if (s.wrap <= 0) { return Z_STREAM_END; }

  if (s.wrap === 2) {
    put_byte(s, strm.adler & 0xff);
    put_byte(s, (strm.adler >> 8) & 0xff);
    put_byte(s, (strm.adler >> 16) & 0xff);
    put_byte(s, (strm.adler >> 24) & 0xff);
    put_byte(s, strm.total_in & 0xff);
    put_byte(s, (strm.total_in >> 8) & 0xff);
    put_byte(s, (strm.total_in >> 16) & 0xff);
    put_byte(s, (strm.total_in >> 24) & 0xff);
  }
  else
  {
    putShortMSB(s, strm.adler >>> 16);
    putShortMSB(s, strm.adler & 0xffff);
  }

  flush_pending(strm);
  if (s.wrap > 0) { s.wrap = -s.wrap; }
  return s.pending !== 0 ? Z_OK : Z_STREAM_END;
}

function deflateEnd(strm) {
  var status;

  if (!strm || !strm.state) {
    return Z_STREAM_ERROR;
  }

  status = strm.state.status;
  if (status !== INIT_STATE &&
    status !== EXTRA_STATE &&
    status !== NAME_STATE &&
    status !== COMMENT_STATE &&
    status !== HCRC_STATE &&
    status !== BUSY_STATE &&
    status !== FINISH_STATE
  ) {
    return err(strm, Z_STREAM_ERROR);
  }

  strm.state = null;

  return status === BUSY_STATE ? err(strm, Z_DATA_ERROR) : Z_OK;
}


function deflateSetDictionary(strm, dictionary) {
  var dictLength = dictionary.length;

  var s;
  var str, n;
  var wrap;
  var avail;
  var next;
  var input;
  var tmpDict;

  if (!strm || !strm.state) {
    return Z_STREAM_ERROR;
  }

  s = strm.state;
  wrap = s.wrap;

  if (wrap === 2 || (wrap === 1 && s.status !== INIT_STATE) || s.lookahead) {
    return Z_STREAM_ERROR;
  }

  if (wrap === 1) {
    strm.adler = adler32(strm.adler, dictionary, dictLength, 0);
  }

  s.wrap = 0;   

  if (dictLength >= s.w_size) {
    if (wrap === 0) {            
      zero(s.head); 
      s.strstart = 0;
      s.block_start = 0;
      s.insert = 0;
    }
    tmpDict = new utils.Buf8(s.w_size);
    utils.arraySet(tmpDict, dictionary, dictLength - s.w_size, s.w_size, 0);
    dictionary = tmpDict;
    dictLength = s.w_size;
  }
  avail = strm.avail_in;
  next = strm.next_in;
  input = strm.input;
  strm.avail_in = dictLength;
  strm.next_in = 0;
  strm.input = dictionary;
  fill_window(s);
  while (s.lookahead >= MIN_MATCH) {
    str = s.strstart;
    n = s.lookahead - (MIN_MATCH - 1);
    do {
      s.ins_h = ((s.ins_h << s.hash_shift) ^ s.window[str + MIN_MATCH - 1]) & s.hash_mask;

      s.prev[str & s.w_mask] = s.head[s.ins_h];

      s.head[s.ins_h] = str;
      str++;
    } while (--n);
    s.strstart = str;
    s.lookahead = MIN_MATCH - 1;
    fill_window(s);
  }
  s.strstart += s.lookahead;
  s.block_start = s.strstart;
  s.insert = s.lookahead;
  s.lookahead = 0;
  s.match_length = s.prev_length = MIN_MATCH - 1;
  s.match_available = 0;
  strm.next_in = next;
  strm.input = input;
  strm.avail_in = avail;
  s.wrap = wrap;
  return Z_OK;
}


exports.deflateInit = deflateInit;
exports.deflateInit2 = deflateInit2;
exports.deflateReset = deflateReset;
exports.deflateResetKeep = deflateResetKeep;
exports.deflateSetHeader = deflateSetHeader;
exports.deflate = deflate;
exports.deflateEnd = deflateEnd;
exports.deflateSetDictionary = deflateSetDictionary;
exports.deflateInfo = 'pako deflate (from Nodeca project)';


},{"12":12,"13":13,"4":4,"5":5,"7":7}],9:[function(require,module,exports){
'use strict';

var BAD = 30;       
var TYPE = 12;      

module.exports = function inflate_fast(strm, start) {
  var state;
  var _in;                    
  var last;                   
  var _out;                   
  var beg;                    
  var end;                    
  var dmax;                   
  var wsize;                  
  var whave;                  
  var wnext;                  
  var s_window;               
  var hold;                   
  var bits;                   
  var lcode;                  
  var dcode;                  
  var lmask;                  
  var dmask;                  
  var here;                   
  var op;                     
  var len;                    
  var dist;                   
  var from;                   
  var from_source;


  var input, output; 

  state = strm.state;
  _in = strm.next_in;
  input = strm.input;
  last = _in + (strm.avail_in - 5);
  _out = strm.next_out;
  output = strm.output;
  beg = _out - (start - strm.avail_out);
  end = _out + (strm.avail_out - 257);
  dmax = state.dmax;
  wsize = state.wsize;
  whave = state.whave;
  wnext = state.wnext;
  s_window = state.window;
  hold = state.hold;
  bits = state.bits;
  lcode = state.lencode;
  dcode = state.distcode;
  lmask = (1 << state.lenbits) - 1;
  dmask = (1 << state.distbits) - 1;



  top:
  do {
    if (bits < 15) {
      hold += input[_in++] << bits;
      bits += 8;
      hold += input[_in++] << bits;
      bits += 8;
    }

    here = lcode[hold & lmask];

    dolen:
    for (;;) { 
      op = here >>> 24;
      hold >>>= op;
      bits -= op;
      op = (here >>> 16) & 0xff;
      if (op === 0) {                          
        output[_out++] = here & 0xffff;
      }
      else if (op & 16) {                     
        len = here & 0xffff;
        op &= 15;                           
        if (op) {
          if (bits < op) {
            hold += input[_in++] << bits;
            bits += 8;
          }
          len += hold & ((1 << op) - 1);
          hold >>>= op;
          bits -= op;
        }
        if (bits < 15) {
          hold += input[_in++] << bits;
          bits += 8;
          hold += input[_in++] << bits;
          bits += 8;
        }
        here = dcode[hold & dmask];

        dodist:
        for (;;) { 
          op = here >>> 24;
          hold >>>= op;
          bits -= op;
          op = (here >>> 16) & 0xff;

          if (op & 16) {                      
            dist = here & 0xffff;
            op &= 15;                       
            if (bits < op) {
              hold += input[_in++] << bits;
              bits += 8;
              if (bits < op) {
                hold += input[_in++] << bits;
                bits += 8;
              }
            }
            dist += hold & ((1 << op) - 1);
            if (dist > dmax) {
              strm.msg = 'invalid distance too far back';
              state.mode = BAD;
              break top;
            }
            hold >>>= op;
            bits -= op;
            op = _out - beg;                
            if (dist > op) {                
              op = dist - op;               
              if (op > whave) {
                if (state.sane) {
                  strm.msg = 'invalid distance too far back';
                  state.mode = BAD;
                  break top;
                }

              }
              from = 0; 
              from_source = s_window;
              if (wnext === 0) {           
                from += wsize - op;
                if (op < len) {         
                  len -= op;
                  do {
                    output[_out++] = s_window[from++];
                  } while (--op);
                  from = _out - dist;  
                  from_source = output;
                }
              }
              else if (wnext < op) {      
                from += wsize + wnext - op;
                op -= wnext;
                if (op < len) {         
                  len -= op;
                  do {
                    output[_out++] = s_window[from++];
                  } while (--op);
                  from = 0;
                  if (wnext < len) {  
                    op = wnext;
                    len -= op;
                    do {
                      output[_out++] = s_window[from++];
                    } while (--op);
                    from = _out - dist;      
                    from_source = output;
                  }
                }
              }
              else {                      
                from += wnext - op;
                if (op < len) {         
                  len -= op;
                  do {
                    output[_out++] = s_window[from++];
                  } while (--op);
                  from = _out - dist;  
                  from_source = output;
                }
              }
              while (len > 2) {
                output[_out++] = from_source[from++];
                output[_out++] = from_source[from++];
                output[_out++] = from_source[from++];
                len -= 3;
              }
              if (len) {
                output[_out++] = from_source[from++];
                if (len > 1) {
                  output[_out++] = from_source[from++];
                }
              }
            }
            else {
              from = _out - dist;          
              do {                        
                output[_out++] = output[from++];
                output[_out++] = output[from++];
                output[_out++] = output[from++];
                len -= 3;
              } while (len > 2);
              if (len) {
                output[_out++] = output[from++];
                if (len > 1) {
                  output[_out++] = output[from++];
                }
              }
            }
          }
          else if ((op & 64) === 0) {          
            here = dcode[(here & 0xffff) + (hold & ((1 << op) - 1))];
            continue dodist;
          }
          else {
            strm.msg = 'invalid distance code';
            state.mode = BAD;
            break top;
          }

          break; 
        }
      }
      else if ((op & 64) === 0) {              
        here = lcode[(here & 0xffff) + (hold & ((1 << op) - 1))];
        continue dolen;
      }
      else if (op & 32) {                     
        state.mode = TYPE;
        break top;
      }
      else {
        strm.msg = 'invalid literal/length code';
        state.mode = BAD;
        break top;
      }

      break; 
    }
  } while (_in < last && _out < end);

  len = bits >> 3;
  _in -= len;
  bits -= len << 3;
  hold &= (1 << bits) - 1;

  strm.next_in = _in;
  strm.next_out = _out;
  strm.avail_in = (_in < last ? 5 + (last - _in) : 5 - (_in - last));
  strm.avail_out = (_out < end ? 257 + (end - _out) : 257 - (_out - end));
  state.hold = hold;
  state.bits = bits;
  return;
};

},{}],10:[function(require,module,exports){
'use strict';


var utils         = require(4);
var adler32       = require(5);
var crc32         = require(7);
var inflate_fast  = require(9);
var inflate_table = require(11);

var CODES = 0;
var LENS = 1;
var DISTS = 2;



var Z_FINISH        = 4;
var Z_BLOCK         = 5;
var Z_TREES         = 6;


var Z_OK            = 0;
var Z_STREAM_END    = 1;
var Z_NEED_DICT     = 2;
var Z_STREAM_ERROR  = -2;
var Z_DATA_ERROR    = -3;
var Z_MEM_ERROR     = -4;
var Z_BUF_ERROR     = -5;

var Z_DEFLATED  = 8;




var    HEAD = 1;       
var    FLAGS = 2;      
var    TIME = 3;       
var    OS = 4;         
var    EXLEN = 5;      
var    EXTRA = 6;      
var    NAME = 7;       
var    COMMENT = 8;    
var    HCRC = 9;       
var    DICTID = 10;    
var    DICT = 11;      
var        TYPE = 12;      
var        TYPEDO = 13;    
var        STORED = 14;    
var        COPY_ = 15;     
var        COPY = 16;      
var        TABLE = 17;     
var        LENLENS = 18;   
var        CODELENS = 19;  
var            LEN_ = 20;      
var            LEN = 21;       
var            LENEXT = 22;    
var            DIST = 23;      
var            DISTEXT = 24;   
var            MATCH = 25;     
var            LIT = 26;       
var    CHECK = 27;     
var    LENGTH = 28;    
var    DONE = 29;      
var    BAD = 30;       
var    MEM = 31;       
var    SYNC = 32;      




var ENOUGH_LENS = 852;
var ENOUGH_DISTS = 592;

var MAX_WBITS = 15;
var DEF_WBITS = MAX_WBITS;


function zswap32(q) {
  return  (((q >>> 24) & 0xff) +
          ((q >>> 8) & 0xff00) +
          ((q & 0xff00) << 8) +
          ((q & 0xff) << 24));
}


function InflateState() {
  this.mode = 0;             
  this.last = false;          
  this.wrap = 0;              
  this.havedict = false;      
  this.flags = 0;             
  this.dmax = 0;              
  this.check = 0;             
  this.total = 0;             
  this.head = null;           

  this.wbits = 0;             
  this.wsize = 0;             
  this.whave = 0;             
  this.wnext = 0;             
  this.window = null;         

  this.hold = 0;              
  this.bits = 0;              

  this.length = 0;            
  this.offset = 0;            

  this.extra = 0;             

  this.lencode = null;          
  this.distcode = null;         
  this.lenbits = 0;           
  this.distbits = 0;          

  this.ncode = 0;             
  this.nlen = 0;              
  this.ndist = 0;             
  this.have = 0;              
  this.next = null;              

  this.lens = new utils.Buf16(320); 
  this.work = new utils.Buf16(288); 

  this.lendyn = null;              
  this.distdyn = null;             
  this.sane = 0;                   
  this.back = 0;                   
  this.was = 0;                    
}

function inflateResetKeep(strm) {
  var state;

  if (!strm || !strm.state) { return Z_STREAM_ERROR; }
  state = strm.state;
  strm.total_in = strm.total_out = state.total = 0;
  strm.msg = ''; 
  if (state.wrap) {       
    strm.adler = state.wrap & 1;
  }
  state.mode = HEAD;
  state.last = 0;
  state.havedict = 0;
  state.dmax = 32768;
  state.head = null;
  state.hold = 0;
  state.bits = 0;
  state.lencode = state.lendyn = new utils.Buf32(ENOUGH_LENS);
  state.distcode = state.distdyn = new utils.Buf32(ENOUGH_DISTS);

  state.sane = 1;
  state.back = -1;
  return Z_OK;
}

function inflateReset(strm) {
  var state;

  if (!strm || !strm.state) { return Z_STREAM_ERROR; }
  state = strm.state;
  state.wsize = 0;
  state.whave = 0;
  state.wnext = 0;
  return inflateResetKeep(strm);

}

function inflateReset2(strm, windowBits) {
  var wrap;
  var state;

  if (!strm || !strm.state) { return Z_STREAM_ERROR; }
  state = strm.state;

  if (windowBits < 0) {
    wrap = 0;
    windowBits = -windowBits;
  }
  else {
    wrap = (windowBits >> 4) + 1;
    if (windowBits < 48) {
      windowBits &= 15;
    }
  }

  if (windowBits && (windowBits < 8 || windowBits > 15)) {
    return Z_STREAM_ERROR;
  }
  if (state.window !== null && state.wbits !== windowBits) {
    state.window = null;
  }

  state.wrap = wrap;
  state.wbits = windowBits;
  return inflateReset(strm);
}

function inflateInit2(strm, windowBits) {
  var ret;
  var state;

  if (!strm) { return Z_STREAM_ERROR; }

  state = new InflateState();

  strm.state = state;
  state.window = null;
  ret = inflateReset2(strm, windowBits);
  if (ret !== Z_OK) {
    strm.state = null;
  }
  return ret;
}

function inflateInit(strm) {
  return inflateInit2(strm, DEF_WBITS);
}


var virgin = true;

var lenfix, distfix; 

function fixedtables(state) {
  if (virgin) {
    var sym;

    lenfix = new utils.Buf32(512);
    distfix = new utils.Buf32(32);

    sym = 0;
    while (sym < 144) { state.lens[sym++] = 8; }
    while (sym < 256) { state.lens[sym++] = 9; }
    while (sym < 280) { state.lens[sym++] = 7; }
    while (sym < 288) { state.lens[sym++] = 8; }

    inflate_table(LENS,  state.lens, 0, 288, lenfix,   0, state.work, { bits: 9 });

    sym = 0;
    while (sym < 32) { state.lens[sym++] = 5; }

    inflate_table(DISTS, state.lens, 0, 32,   distfix, 0, state.work, { bits: 5 });

    virgin = false;
  }

  state.lencode = lenfix;
  state.lenbits = 9;
  state.distcode = distfix;
  state.distbits = 5;
}


function updatewindow(strm, src, end, copy) {
  var dist;
  var state = strm.state;

  if (state.window === null) {
    state.wsize = 1 << state.wbits;
    state.wnext = 0;
    state.whave = 0;

    state.window = new utils.Buf8(state.wsize);
  }

  if (copy >= state.wsize) {
    utils.arraySet(state.window, src, end - state.wsize, state.wsize, 0);
    state.wnext = 0;
    state.whave = state.wsize;
  }
  else {
    dist = state.wsize - state.wnext;
    if (dist > copy) {
      dist = copy;
    }
    utils.arraySet(state.window, src, end - copy, dist, state.wnext);
    copy -= dist;
    if (copy) {
      utils.arraySet(state.window, src, end - copy, copy, 0);
      state.wnext = copy;
      state.whave = state.wsize;
    }
    else {
      state.wnext += dist;
      if (state.wnext === state.wsize) { state.wnext = 0; }
      if (state.whave < state.wsize) { state.whave += dist; }
    }
  }
  return 0;
}

function inflate(strm, flush) {
  var state;
  var input, output;          
  var next;                   
  var put;                    
  var have, left;             
  var hold;                   
  var bits;                   
  var _in, _out;              
  var copy;                   
  var from;                   
  var from_source;
  var here = 0;               
  var here_bits, here_op, here_val; 
  var last_bits, last_op, last_val; 
  var len;                    
  var ret;                    
  var hbuf = new utils.Buf8(4);    
  var opts;

  var n; 

  var order = 
    [ 16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15 ];


  if (!strm || !strm.state || !strm.output ||
      (!strm.input && strm.avail_in !== 0)) {
    return Z_STREAM_ERROR;
  }

  state = strm.state;
  if (state.mode === TYPE) { state.mode = TYPEDO; }    


  put = strm.next_out;
  output = strm.output;
  left = strm.avail_out;
  next = strm.next_in;
  input = strm.input;
  have = strm.avail_in;
  hold = state.hold;
  bits = state.bits;

  _in = have;
  _out = left;
  ret = Z_OK;

  inf_leave: 
  for (;;) {
    switch (state.mode) {
    case HEAD:
      if (state.wrap === 0) {
        state.mode = TYPEDO;
        break;
      }
      while (bits < 16) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      if ((state.wrap & 2) && hold === 0x8b1f) {  
        state.check = 0;
        hbuf[0] = hold & 0xff;
        hbuf[1] = (hold >>> 8) & 0xff;
        state.check = crc32(state.check, hbuf, 2, 0);

        hold = 0;
        bits = 0;
        state.mode = FLAGS;
        break;
      }
      state.flags = 0;           
      if (state.head) {
        state.head.done = false;
      }
      if (!(state.wrap & 1) ||   
        (((hold & 0xff) << 8) + (hold >> 8)) % 31) {
        strm.msg = 'incorrect header check';
        state.mode = BAD;
        break;
      }
      if ((hold & 0x0f) !== Z_DEFLATED) {
        strm.msg = 'unknown compression method';
        state.mode = BAD;
        break;
      }
      hold >>>= 4;
      bits -= 4;
      len = (hold & 0x0f) + 8;
      if (state.wbits === 0) {
        state.wbits = len;
      }
      else if (len > state.wbits) {
        strm.msg = 'invalid window size';
        state.mode = BAD;
        break;
      }
      state.dmax = 1 << len;
      strm.adler = state.check = 1;
      state.mode = hold & 0x200 ? DICTID : TYPE;
      hold = 0;
      bits = 0;
      break;
    case FLAGS:
      while (bits < 16) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      state.flags = hold;
      if ((state.flags & 0xff) !== Z_DEFLATED) {
        strm.msg = 'unknown compression method';
        state.mode = BAD;
        break;
      }
      if (state.flags & 0xe000) {
        strm.msg = 'unknown header flags set';
        state.mode = BAD;
        break;
      }
      if (state.head) {
        state.head.text = ((hold >> 8) & 1);
      }
      if (state.flags & 0x0200) {
        hbuf[0] = hold & 0xff;
        hbuf[1] = (hold >>> 8) & 0xff;
        state.check = crc32(state.check, hbuf, 2, 0);
      }
      hold = 0;
      bits = 0;
      state.mode = TIME;
    case TIME:
      while (bits < 32) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      if (state.head) {
        state.head.time = hold;
      }
      if (state.flags & 0x0200) {
        hbuf[0] = hold & 0xff;
        hbuf[1] = (hold >>> 8) & 0xff;
        hbuf[2] = (hold >>> 16) & 0xff;
        hbuf[3] = (hold >>> 24) & 0xff;
        state.check = crc32(state.check, hbuf, 4, 0);
      }
      hold = 0;
      bits = 0;
      state.mode = OS;
    case OS:
      while (bits < 16) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      if (state.head) {
        state.head.xflags = (hold & 0xff);
        state.head.os = (hold >> 8);
      }
      if (state.flags & 0x0200) {
        hbuf[0] = hold & 0xff;
        hbuf[1] = (hold >>> 8) & 0xff;
        state.check = crc32(state.check, hbuf, 2, 0);
      }
      hold = 0;
      bits = 0;
      state.mode = EXLEN;
    case EXLEN:
      if (state.flags & 0x0400) {
        while (bits < 16) {
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        state.length = hold;
        if (state.head) {
          state.head.extra_len = hold;
        }
        if (state.flags & 0x0200) {
          hbuf[0] = hold & 0xff;
          hbuf[1] = (hold >>> 8) & 0xff;
          state.check = crc32(state.check, hbuf, 2, 0);
        }
        hold = 0;
        bits = 0;
      }
      else if (state.head) {
        state.head.extra = null;
      }
      state.mode = EXTRA;
    case EXTRA:
      if (state.flags & 0x0400) {
        copy = state.length;
        if (copy > have) { copy = have; }
        if (copy) {
          if (state.head) {
            len = state.head.extra_len - state.length;
            if (!state.head.extra) {
              state.head.extra = new Array(state.head.extra_len);
            }
            utils.arraySet(
              state.head.extra,
              input,
              next,
              copy,
              len
            );
          }
          if (state.flags & 0x0200) {
            state.check = crc32(state.check, input, copy, next);
          }
          have -= copy;
          next += copy;
          state.length -= copy;
        }
        if (state.length) { break inf_leave; }
      }
      state.length = 0;
      state.mode = NAME;
    case NAME:
      if (state.flags & 0x0800) {
        if (have === 0) { break inf_leave; }
        copy = 0;
        do {
          len = input[next + copy++];
          if (state.head && len &&
              (state.length < 65536 )) {
            state.head.name += String.fromCharCode(len);
          }
        } while (len && copy < have);

        if (state.flags & 0x0200) {
          state.check = crc32(state.check, input, copy, next);
        }
        have -= copy;
        next += copy;
        if (len) { break inf_leave; }
      }
      else if (state.head) {
        state.head.name = null;
      }
      state.length = 0;
      state.mode = COMMENT;
    case COMMENT:
      if (state.flags & 0x1000) {
        if (have === 0) { break inf_leave; }
        copy = 0;
        do {
          len = input[next + copy++];
          if (state.head && len &&
              (state.length < 65536 )) {
            state.head.comment += String.fromCharCode(len);
          }
        } while (len && copy < have);
        if (state.flags & 0x0200) {
          state.check = crc32(state.check, input, copy, next);
        }
        have -= copy;
        next += copy;
        if (len) { break inf_leave; }
      }
      else if (state.head) {
        state.head.comment = null;
      }
      state.mode = HCRC;
    case HCRC:
      if (state.flags & 0x0200) {
        while (bits < 16) {
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        if (hold !== (state.check & 0xffff)) {
          strm.msg = 'header crc mismatch';
          state.mode = BAD;
          break;
        }
        hold = 0;
        bits = 0;
      }
      if (state.head) {
        state.head.hcrc = ((state.flags >> 9) & 1);
        state.head.done = true;
      }
      strm.adler = state.check = 0;
      state.mode = TYPE;
      break;
    case DICTID:
      while (bits < 32) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      strm.adler = state.check = zswap32(hold);
      hold = 0;
      bits = 0;
      state.mode = DICT;
    case DICT:
      if (state.havedict === 0) {
        strm.next_out = put;
        strm.avail_out = left;
        strm.next_in = next;
        strm.avail_in = have;
        state.hold = hold;
        state.bits = bits;
        return Z_NEED_DICT;
      }
      strm.adler = state.check = 1;
      state.mode = TYPE;
    case TYPE:
      if (flush === Z_BLOCK || flush === Z_TREES) { break inf_leave; }
    case TYPEDO:
      if (state.last) {
        hold >>>= bits & 7;
        bits -= bits & 7;
        state.mode = CHECK;
        break;
      }
      while (bits < 3) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      state.last = (hold & 0x01);
      hold >>>= 1;
      bits -= 1;

      switch ((hold & 0x03)) {
      case 0:                             
        state.mode = STORED;
        break;
      case 1:                             
        fixedtables(state);
        state.mode = LEN_;             
        if (flush === Z_TREES) {
          hold >>>= 2;
          bits -= 2;
          break inf_leave;
        }
        break;
      case 2:                             
        state.mode = TABLE;
        break;
      case 3:
        strm.msg = 'invalid block type';
        state.mode = BAD;
      }
      hold >>>= 2;
      bits -= 2;
      break;
    case STORED:
      hold >>>= bits & 7;
      bits -= bits & 7;
      while (bits < 32) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      if ((hold & 0xffff) !== ((hold >>> 16) ^ 0xffff)) {
        strm.msg = 'invalid stored block lengths';
        state.mode = BAD;
        break;
      }
      state.length = hold & 0xffff;
      hold = 0;
      bits = 0;
      state.mode = COPY_;
      if (flush === Z_TREES) { break inf_leave; }
    case COPY_:
      state.mode = COPY;
    case COPY:
      copy = state.length;
      if (copy) {
        if (copy > have) { copy = have; }
        if (copy > left) { copy = left; }
        if (copy === 0) { break inf_leave; }
        utils.arraySet(output, input, next, copy, put);
        have -= copy;
        next += copy;
        left -= copy;
        put += copy;
        state.length -= copy;
        break;
      }
      state.mode = TYPE;
      break;
    case TABLE:
      while (bits < 14) {
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      state.nlen = (hold & 0x1f) + 257;
      hold >>>= 5;
      bits -= 5;
      state.ndist = (hold & 0x1f) + 1;
      hold >>>= 5;
      bits -= 5;
      state.ncode = (hold & 0x0f) + 4;
      hold >>>= 4;
      bits -= 4;
      if (state.nlen > 286 || state.ndist > 30) {
        strm.msg = 'too many length or distance symbols';
        state.mode = BAD;
        break;
      }
      state.have = 0;
      state.mode = LENLENS;
    case LENLENS:
      while (state.have < state.ncode) {
        while (bits < 3) {
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        state.lens[order[state.have++]] = (hold & 0x07);
        hold >>>= 3;
        bits -= 3;
      }
      while (state.have < 19) {
        state.lens[order[state.have++]] = 0;
      }
      state.lencode = state.lendyn;
      state.lenbits = 7;

      opts = { bits: state.lenbits };
      ret = inflate_table(CODES, state.lens, 0, 19, state.lencode, 0, state.work, opts);
      state.lenbits = opts.bits;

      if (ret) {
        strm.msg = 'invalid code lengths set';
        state.mode = BAD;
        break;
      }
      state.have = 0;
      state.mode = CODELENS;
    case CODELENS:
      while (state.have < state.nlen + state.ndist) {
        for (;;) {
          here = state.lencode[hold & ((1 << state.lenbits) - 1)];
          here_bits = here >>> 24;
          here_op = (here >>> 16) & 0xff;
          here_val = here & 0xffff;

          if ((here_bits) <= bits) { break; }
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        if (here_val < 16) {
          hold >>>= here_bits;
          bits -= here_bits;
          state.lens[state.have++] = here_val;
        }
        else {
          if (here_val === 16) {
            n = here_bits + 2;
            while (bits < n) {
              if (have === 0) { break inf_leave; }
              have--;
              hold += input[next++] << bits;
              bits += 8;
            }
            hold >>>= here_bits;
            bits -= here_bits;
            if (state.have === 0) {
              strm.msg = 'invalid bit length repeat';
              state.mode = BAD;
              break;
            }
            len = state.lens[state.have - 1];
            copy = 3 + (hold & 0x03);
            hold >>>= 2;
            bits -= 2;
          }
          else if (here_val === 17) {
            n = here_bits + 3;
            while (bits < n) {
              if (have === 0) { break inf_leave; }
              have--;
              hold += input[next++] << bits;
              bits += 8;
            }
            hold >>>= here_bits;
            bits -= here_bits;
            len = 0;
            copy = 3 + (hold & 0x07);
            hold >>>= 3;
            bits -= 3;
          }
          else {
            n = here_bits + 7;
            while (bits < n) {
              if (have === 0) { break inf_leave; }
              have--;
              hold += input[next++] << bits;
              bits += 8;
            }
            hold >>>= here_bits;
            bits -= here_bits;
            len = 0;
            copy = 11 + (hold & 0x7f);
            hold >>>= 7;
            bits -= 7;
          }
          if (state.have + copy > state.nlen + state.ndist) {
            strm.msg = 'invalid bit length repeat';
            state.mode = BAD;
            break;
          }
          while (copy--) {
            state.lens[state.have++] = len;
          }
        }
      }

      if (state.mode === BAD) { break; }

      if (state.lens[256] === 0) {
        strm.msg = 'invalid code -- missing end-of-block';
        state.mode = BAD;
        break;
      }

      state.lenbits = 9;

      opts = { bits: state.lenbits };
      ret = inflate_table(LENS, state.lens, 0, state.nlen, state.lencode, 0, state.work, opts);
      state.lenbits = opts.bits;

      if (ret) {
        strm.msg = 'invalid literal/lengths set';
        state.mode = BAD;
        break;
      }

      state.distbits = 6;
      state.distcode = state.distdyn;
      opts = { bits: state.distbits };
      ret = inflate_table(DISTS, state.lens, state.nlen, state.ndist, state.distcode, 0, state.work, opts);
      state.distbits = opts.bits;

      if (ret) {
        strm.msg = 'invalid distances set';
        state.mode = BAD;
        break;
      }
      state.mode = LEN_;
      if (flush === Z_TREES) { break inf_leave; }
    case LEN_:
      state.mode = LEN;
    case LEN:
      if (have >= 6 && left >= 258) {
        strm.next_out = put;
        strm.avail_out = left;
        strm.next_in = next;
        strm.avail_in = have;
        state.hold = hold;
        state.bits = bits;
        inflate_fast(strm, _out);
        put = strm.next_out;
        output = strm.output;
        left = strm.avail_out;
        next = strm.next_in;
        input = strm.input;
        have = strm.avail_in;
        hold = state.hold;
        bits = state.bits;

        if (state.mode === TYPE) {
          state.back = -1;
        }
        break;
      }
      state.back = 0;
      for (;;) {
        here = state.lencode[hold & ((1 << state.lenbits) - 1)];  
        here_bits = here >>> 24;
        here_op = (here >>> 16) & 0xff;
        here_val = here & 0xffff;

        if (here_bits <= bits) { break; }
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      if (here_op && (here_op & 0xf0) === 0) {
        last_bits = here_bits;
        last_op = here_op;
        last_val = here_val;
        for (;;) {
          here = state.lencode[last_val +
                  ((hold & ((1 << (last_bits + last_op)) - 1)) >> last_bits)];
          here_bits = here >>> 24;
          here_op = (here >>> 16) & 0xff;
          here_val = here & 0xffff;

          if ((last_bits + here_bits) <= bits) { break; }
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        hold >>>= last_bits;
        bits -= last_bits;
        state.back += last_bits;
      }
      hold >>>= here_bits;
      bits -= here_bits;
      state.back += here_bits;
      state.length = here_val;
      if (here_op === 0) {
        state.mode = LIT;
        break;
      }
      if (here_op & 32) {
        state.back = -1;
        state.mode = TYPE;
        break;
      }
      if (here_op & 64) {
        strm.msg = 'invalid literal/length code';
        state.mode = BAD;
        break;
      }
      state.extra = here_op & 15;
      state.mode = LENEXT;
    case LENEXT:
      if (state.extra) {
        n = state.extra;
        while (bits < n) {
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        state.length += hold & ((1 << state.extra) - 1);
        hold >>>= state.extra;
        bits -= state.extra;
        state.back += state.extra;
      }
      state.was = state.length;
      state.mode = DIST;
    case DIST:
      for (;;) {
        here = state.distcode[hold & ((1 << state.distbits) - 1)];
        here_bits = here >>> 24;
        here_op = (here >>> 16) & 0xff;
        here_val = here & 0xffff;

        if ((here_bits) <= bits) { break; }
        if (have === 0) { break inf_leave; }
        have--;
        hold += input[next++] << bits;
        bits += 8;
      }
      if ((here_op & 0xf0) === 0) {
        last_bits = here_bits;
        last_op = here_op;
        last_val = here_val;
        for (;;) {
          here = state.distcode[last_val +
                  ((hold & ((1 << (last_bits + last_op)) - 1)) >> last_bits)];
          here_bits = here >>> 24;
          here_op = (here >>> 16) & 0xff;
          here_val = here & 0xffff;

          if ((last_bits + here_bits) <= bits) { break; }
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        hold >>>= last_bits;
        bits -= last_bits;
        state.back += last_bits;
      }
      hold >>>= here_bits;
      bits -= here_bits;
      state.back += here_bits;
      if (here_op & 64) {
        strm.msg = 'invalid distance code';
        state.mode = BAD;
        break;
      }
      state.offset = here_val;
      state.extra = (here_op) & 15;
      state.mode = DISTEXT;
    case DISTEXT:
      if (state.extra) {
        n = state.extra;
        while (bits < n) {
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        state.offset += hold & ((1 << state.extra) - 1);
        hold >>>= state.extra;
        bits -= state.extra;
        state.back += state.extra;
      }
      if (state.offset > state.dmax) {
        strm.msg = 'invalid distance too far back';
        state.mode = BAD;
        break;
      }
      state.mode = MATCH;
    case MATCH:
      if (left === 0) { break inf_leave; }
      copy = _out - left;
      if (state.offset > copy) {         
        copy = state.offset - copy;
        if (copy > state.whave) {
          if (state.sane) {
            strm.msg = 'invalid distance too far back';
            state.mode = BAD;
            break;
          }
        }
        if (copy > state.wnext) {
          copy -= state.wnext;
          from = state.wsize - copy;
        }
        else {
          from = state.wnext - copy;
        }
        if (copy > state.length) { copy = state.length; }
        from_source = state.window;
      }
      else {                              
        from_source = output;
        from = put - state.offset;
        copy = state.length;
      }
      if (copy > left) { copy = left; }
      left -= copy;
      state.length -= copy;
      do {
        output[put++] = from_source[from++];
      } while (--copy);
      if (state.length === 0) { state.mode = LEN; }
      break;
    case LIT:
      if (left === 0) { break inf_leave; }
      output[put++] = state.length;
      left--;
      state.mode = LEN;
      break;
    case CHECK:
      if (state.wrap) {
        while (bits < 32) {
          if (have === 0) { break inf_leave; }
          have--;
          hold |= input[next++] << bits;
          bits += 8;
        }
        _out -= left;
        strm.total_out += _out;
        state.total += _out;
        if (_out) {
          strm.adler = state.check =
              (state.flags ? crc32(state.check, output, _out, put - _out) : adler32(state.check, output, _out, put - _out));

        }
        _out = left;
        if ((state.flags ? hold : zswap32(hold)) !== state.check) {
          strm.msg = 'incorrect data check';
          state.mode = BAD;
          break;
        }
        hold = 0;
        bits = 0;
      }
      state.mode = LENGTH;
    case LENGTH:
      if (state.wrap && state.flags) {
        while (bits < 32) {
          if (have === 0) { break inf_leave; }
          have--;
          hold += input[next++] << bits;
          bits += 8;
        }
        if (hold !== (state.total & 0xffffffff)) {
          strm.msg = 'incorrect length check';
          state.mode = BAD;
          break;
        }
        hold = 0;
        bits = 0;
      }
      state.mode = DONE;
    case DONE:
      ret = Z_STREAM_END;
      break inf_leave;
    case BAD:
      ret = Z_DATA_ERROR;
      break inf_leave;
    case MEM:
      return Z_MEM_ERROR;
    case SYNC:
    default:
      return Z_STREAM_ERROR;
    }
  }



  strm.next_out = put;
  strm.avail_out = left;
  strm.next_in = next;
  strm.avail_in = have;
  state.hold = hold;
  state.bits = bits;

  if (state.wsize || (_out !== strm.avail_out && state.mode < BAD &&
                      (state.mode < CHECK || flush !== Z_FINISH))) {
    if (updatewindow(strm, strm.output, strm.next_out, _out - strm.avail_out)) {
      state.mode = MEM;
      return Z_MEM_ERROR;
    }
  }
  _in -= strm.avail_in;
  _out -= strm.avail_out;
  strm.total_in += _in;
  strm.total_out += _out;
  state.total += _out;
  if (state.wrap && _out) {
    strm.adler = state.check = 
      (state.flags ? crc32(state.check, output, _out, strm.next_out - _out) : adler32(state.check, output, _out, strm.next_out - _out));
  }
  strm.data_type = state.bits + (state.last ? 64 : 0) +
                    (state.mode === TYPE ? 128 : 0) +
                    (state.mode === LEN_ || state.mode === COPY_ ? 256 : 0);
  if (((_in === 0 && _out === 0) || flush === Z_FINISH) && ret === Z_OK) {
    ret = Z_BUF_ERROR;
  }
  return ret;
}

function inflateEnd(strm) {

  if (!strm || !strm.state ) {
    return Z_STREAM_ERROR;
  }

  var state = strm.state;
  if (state.window) {
    state.window = null;
  }
  strm.state = null;
  return Z_OK;
}

function inflateGetHeader(strm, head) {
  var state;

  if (!strm || !strm.state) { return Z_STREAM_ERROR; }
  state = strm.state;
  if ((state.wrap & 2) === 0) { return Z_STREAM_ERROR; }

  state.head = head;
  head.done = false;
  return Z_OK;
}

function inflateSetDictionary(strm, dictionary) {
  var dictLength = dictionary.length;

  var state;
  var dictid;
  var ret;

  if (!strm  || !strm.state ) { return Z_STREAM_ERROR; }
  state = strm.state;

  if (state.wrap !== 0 && state.mode !== DICT) {
    return Z_STREAM_ERROR;
  }

  if (state.mode === DICT) {
    dictid = 1; 
    dictid = adler32(dictid, dictionary, dictLength, 0);
    if (dictid !== state.check) {
      return Z_DATA_ERROR;
    }
  }
  ret = updatewindow(strm, dictionary, dictLength, dictLength);
  if (ret) {
    state.mode = MEM;
    return Z_MEM_ERROR;
  }
  state.havedict = 1;
  return Z_OK;
}

exports.inflateReset = inflateReset;
exports.inflateReset2 = inflateReset2;
exports.inflateResetKeep = inflateResetKeep;
exports.inflateInit = inflateInit;
exports.inflateInit2 = inflateInit2;
exports.inflate = inflate;
exports.inflateEnd = inflateEnd;
exports.inflateGetHeader = inflateGetHeader;
exports.inflateSetDictionary = inflateSetDictionary;
exports.inflateInfo = 'pako inflate (from Nodeca project)';


},{"11":11,"4":4,"5":5,"7":7,"9":9}],11:[function(require,module,exports){
'use strict';


var utils = require(4);

var MAXBITS = 15;
var ENOUGH_LENS = 852;
var ENOUGH_DISTS = 592;

var CODES = 0;
var LENS = 1;
var DISTS = 2;

var lbase = [ 
  3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
  35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 0, 0
];

var lext = [ 
  16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 18, 18,
  19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 16, 72, 78
];

var dbase = [ 
  1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193,
  257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145,
  8193, 12289, 16385, 24577, 0, 0
];

var dext = [ 
  16, 16, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22,
  23, 23, 24, 24, 25, 25, 26, 26, 27, 27,
  28, 28, 29, 29, 64, 64
];

module.exports = function inflate_table(type, lens, lens_index, codes, table, table_index, work, opts)
{
  var bits = opts.bits;

  var len = 0;               
  var sym = 0;               
  var min = 0, max = 0;          
  var root = 0;              
  var curr = 0;              
  var drop = 0;              
  var left = 0;                   
  var used = 0;              
  var huff = 0;              
  var incr;              
  var fill;              
  var low;               
  var mask;              
  var next;             
  var base = null;     
  var base_index = 0;
  var end;                    
  var count = new utils.Buf16(MAXBITS + 1); 
  var offs = new utils.Buf16(MAXBITS + 1); 
  var extra = null;
  var extra_index = 0;

  var here_bits, here_op, here_val;


  for (len = 0; len <= MAXBITS; len++) {
    count[len] = 0;
  }
  for (sym = 0; sym < codes; sym++) {
    count[lens[lens_index + sym]]++;
  }

  root = bits;
  for (max = MAXBITS; max >= 1; max--) {
    if (count[max] !== 0) { break; }
  }
  if (root > max) {
    root = max;
  }
  if (max === 0) {                     
    table[table_index++] = (1 << 24) | (64 << 16) | 0;


    table[table_index++] = (1 << 24) | (64 << 16) | 0;

    opts.bits = 1;
    return 0;     
  }
  for (min = 1; min < max; min++) {
    if (count[min] !== 0) { break; }
  }
  if (root < min) {
    root = min;
  }

  left = 1;
  for (len = 1; len <= MAXBITS; len++) {
    left <<= 1;
    left -= count[len];
    if (left < 0) {
      return -1;
    }        
  }
  if (left > 0 && (type === CODES || max !== 1)) {
    return -1;                      
  }

  offs[1] = 0;
  for (len = 1; len < MAXBITS; len++) {
    offs[len + 1] = offs[len] + count[len];
  }

  for (sym = 0; sym < codes; sym++) {
    if (lens[lens_index + sym] !== 0) {
      work[offs[lens[lens_index + sym]]++] = sym;
    }
  }


  if (type === CODES) {
    base = extra = work;    
    end = 19;

  } else if (type === LENS) {
    base = lbase;
    base_index -= 257;
    extra = lext;
    extra_index -= 257;
    end = 256;

  } else {                    
    base = dbase;
    extra = dext;
    end = -1;
  }

  huff = 0;                   
  sym = 0;                    
  len = min;                  
  next = table_index;              
  curr = root;                
  drop = 0;                   
  low = -1;                   
  used = 1 << root;          
  mask = used - 1;            

  if ((type === LENS && used > ENOUGH_LENS) ||
    (type === DISTS && used > ENOUGH_DISTS)) {
    return 1;
  }

  var i = 0;
  for (;;) {
    i++;
    here_bits = len - drop;
    if (work[sym] < end) {
      here_op = 0;
      here_val = work[sym];
    }
    else if (work[sym] > end) {
      here_op = extra[extra_index + work[sym]];
      here_val = base[base_index + work[sym]];
    }
    else {
      here_op = 32 + 64;         
      here_val = 0;
    }

    incr = 1 << (len - drop);
    fill = 1 << curr;
    min = fill;                 
    do {
      fill -= incr;
      table[next + (huff >> drop) + fill] = (here_bits << 24) | (here_op << 16) | here_val |0;
    } while (fill !== 0);

    incr = 1 << (len - 1);
    while (huff & incr) {
      incr >>= 1;
    }
    if (incr !== 0) {
      huff &= incr - 1;
      huff += incr;
    } else {
      huff = 0;
    }

    sym++;
    if (--count[len] === 0) {
      if (len === max) { break; }
      len = lens[lens_index + work[sym]];
    }

    if (len > root && (huff & mask) !== low) {
      if (drop === 0) {
        drop = root;
      }

      next += min;            

      curr = len - drop;
      left = 1 << curr;
      while (curr + drop < max) {
        left -= count[curr + drop];
        if (left <= 0) { break; }
        curr++;
        left <<= 1;
      }

      used += 1 << curr;
      if ((type === LENS && used > ENOUGH_LENS) ||
        (type === DISTS && used > ENOUGH_DISTS)) {
        return 1;
      }

      low = huff & mask;
      table[low] = (root << 24) | (curr << 16) | (next - table_index) |0;
    }
  }

  if (huff !== 0) {
    table[next + huff] = ((len - drop) << 24) | (64 << 16) |0;
  }

  opts.bits = root;
  return 0;
};

},{"4":4}],12:[function(require,module,exports){
'use strict';

module.exports = {
  2:      'need dictionary',     
  1:      'stream end',          
  0:      '',                    
  '-1':   'file error',          
  '-2':   'stream error',        
  '-3':   'data error',          
  '-4':   'insufficient memory', 
  '-5':   'buffer error',        
  '-6':   'incompatible version' 
};

},{}],13:[function(require,module,exports){
'use strict';


var utils = require(4);



var Z_FIXED               = 4;

var Z_BINARY              = 0;
var Z_TEXT                = 1;
var Z_UNKNOWN             = 2;



function zero(buf) { var len = buf.length; while (--len >= 0) { buf[len] = 0; } }


var STORED_BLOCK = 0;
var STATIC_TREES = 1;
var DYN_TREES    = 2;

var MIN_MATCH    = 3;
var MAX_MATCH    = 258;


var LENGTH_CODES  = 29;

var LITERALS      = 256;

var L_CODES       = LITERALS + 1 + LENGTH_CODES;

var D_CODES       = 30;

var BL_CODES      = 19;

var HEAP_SIZE     = 2 * L_CODES + 1;

var MAX_BITS      = 15;

var Buf_size      = 16;



var MAX_BL_BITS = 7;

var END_BLOCK   = 256;

var REP_3_6     = 16;

var REPZ_3_10   = 17;

var REPZ_11_138 = 18;

var extra_lbits =   
  [0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,0];

var extra_dbits =   
  [0,0,0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13];

var extra_blbits =  
  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,3,7];

var bl_order =
  [16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15];




var DIST_CODE_LEN = 512; 

var static_ltree  = new Array((L_CODES + 2) * 2);
zero(static_ltree);

var static_dtree  = new Array(D_CODES * 2);
zero(static_dtree);

var _dist_code    = new Array(DIST_CODE_LEN);
zero(_dist_code);

var _length_code  = new Array(MAX_MATCH - MIN_MATCH + 1);
zero(_length_code);

var base_length   = new Array(LENGTH_CODES);
zero(base_length);

var base_dist     = new Array(D_CODES);
zero(base_dist);


function StaticTreeDesc(static_tree, extra_bits, extra_base, elems, max_length) {

  this.static_tree  = static_tree;  
  this.extra_bits   = extra_bits;   
  this.extra_base   = extra_base;   
  this.elems        = elems;        
  this.max_length   = max_length;   

  this.has_stree    = static_tree && static_tree.length;
}


var static_l_desc;
var static_d_desc;
var static_bl_desc;


function TreeDesc(dyn_tree, stat_desc) {
  this.dyn_tree = dyn_tree;     
  this.max_code = 0;            
  this.stat_desc = stat_desc;   
}



function d_code(dist) {
  return dist < 256 ? _dist_code[dist] : _dist_code[256 + (dist >>> 7)];
}


function put_short(s, w) {
  s.pending_buf[s.pending++] = (w) & 0xff;
  s.pending_buf[s.pending++] = (w >>> 8) & 0xff;
}


function send_bits(s, value, length) {
  if (s.bi_valid > (Buf_size - length)) {
    s.bi_buf |= (value << s.bi_valid) & 0xffff;
    put_short(s, s.bi_buf);
    s.bi_buf = value >> (Buf_size - s.bi_valid);
    s.bi_valid += length - Buf_size;
  } else {
    s.bi_buf |= (value << s.bi_valid) & 0xffff;
    s.bi_valid += length;
  }
}


function send_code(s, c, tree) {
  send_bits(s, tree[c * 2], tree[c * 2 + 1]);
}


function bi_reverse(code, len) {
  var res = 0;
  do {
    res |= code & 1;
    code >>>= 1;
    res <<= 1;
  } while (--len > 0);
  return res >>> 1;
}


function bi_flush(s) {
  if (s.bi_valid === 16) {
    put_short(s, s.bi_buf);
    s.bi_buf = 0;
    s.bi_valid = 0;

  } else if (s.bi_valid >= 8) {
    s.pending_buf[s.pending++] = s.bi_buf & 0xff;
    s.bi_buf >>= 8;
    s.bi_valid -= 8;
  }
}


function gen_bitlen(s, desc)
{
  var tree            = desc.dyn_tree;
  var max_code        = desc.max_code;
  var stree           = desc.stat_desc.static_tree;
  var has_stree       = desc.stat_desc.has_stree;
  var extra           = desc.stat_desc.extra_bits;
  var base            = desc.stat_desc.extra_base;
  var max_length      = desc.stat_desc.max_length;
  var h;              
  var n, m;           
  var bits;           
  var xbits;          
  var f;              
  var overflow = 0;   

  for (bits = 0; bits <= MAX_BITS; bits++) {
    s.bl_count[bits] = 0;
  }

  tree[s.heap[s.heap_max] * 2 + 1] = 0; 

  for (h = s.heap_max + 1; h < HEAP_SIZE; h++) {
    n = s.heap[h];
    bits = tree[tree[n * 2 + 1] * 2 + 1] + 1;
    if (bits > max_length) {
      bits = max_length;
      overflow++;
    }
    tree[n * 2 + 1] = bits;

    if (n > max_code) { continue; } 

    s.bl_count[bits]++;
    xbits = 0;
    if (n >= base) {
      xbits = extra[n - base];
    }
    f = tree[n * 2];
    s.opt_len += f * (bits + xbits);
    if (has_stree) {
      s.static_len += f * (stree[n * 2 + 1] + xbits);
    }
  }
  if (overflow === 0) { return; }


  do {
    bits = max_length - 1;
    while (s.bl_count[bits] === 0) { bits--; }
    s.bl_count[bits]--;      
    s.bl_count[bits + 1] += 2; 
    s.bl_count[max_length]--;
    overflow -= 2;
  } while (overflow > 0);

  for (bits = max_length; bits !== 0; bits--) {
    n = s.bl_count[bits];
    while (n !== 0) {
      m = s.heap[--h];
      if (m > max_code) { continue; }
      if (tree[m * 2 + 1] !== bits) {
        s.opt_len += (bits - tree[m * 2 + 1]) * tree[m * 2];
        tree[m * 2 + 1] = bits;
      }
      n--;
    }
  }
}


function gen_codes(tree, max_code, bl_count)
{
  var next_code = new Array(MAX_BITS + 1); 
  var code = 0;              
  var bits;                  
  var n;                     

  for (bits = 1; bits <= MAX_BITS; bits++) {
    next_code[bits] = code = (code + bl_count[bits - 1]) << 1;
  }

  for (n = 0;  n <= max_code; n++) {
    var len = tree[n * 2 + 1];
    if (len === 0) { continue; }
    tree[n * 2] = bi_reverse(next_code[len]++, len);

  }
}


function tr_static_init() {
  var n;        
  var bits;     
  var length;   
  var code;     
  var dist;     
  var bl_count = new Array(MAX_BITS + 1);



  length = 0;
  for (code = 0; code < LENGTH_CODES - 1; code++) {
    base_length[code] = length;
    for (n = 0; n < (1 << extra_lbits[code]); n++) {
      _length_code[length++] = code;
    }
  }
  _length_code[length - 1] = code;

  dist = 0;
  for (code = 0; code < 16; code++) {
    base_dist[code] = dist;
    for (n = 0; n < (1 << extra_dbits[code]); n++) {
      _dist_code[dist++] = code;
    }
  }
  dist >>= 7; 
  for (; code < D_CODES; code++) {
    base_dist[code] = dist << 7;
    for (n = 0; n < (1 << (extra_dbits[code] - 7)); n++) {
      _dist_code[256 + dist++] = code;
    }
  }

  for (bits = 0; bits <= MAX_BITS; bits++) {
    bl_count[bits] = 0;
  }

  n = 0;
  while (n <= 143) {
    static_ltree[n * 2 + 1] = 8;
    n++;
    bl_count[8]++;
  }
  while (n <= 255) {
    static_ltree[n * 2 + 1] = 9;
    n++;
    bl_count[9]++;
  }
  while (n <= 279) {
    static_ltree[n * 2 + 1] = 7;
    n++;
    bl_count[7]++;
  }
  while (n <= 287) {
    static_ltree[n * 2 + 1] = 8;
    n++;
    bl_count[8]++;
  }
  gen_codes(static_ltree, L_CODES + 1, bl_count);

  for (n = 0; n < D_CODES; n++) {
    static_dtree[n * 2 + 1] = 5;
    static_dtree[n * 2] = bi_reverse(n, 5);
  }

  static_l_desc = new StaticTreeDesc(static_ltree, extra_lbits, LITERALS + 1, L_CODES, MAX_BITS);
  static_d_desc = new StaticTreeDesc(static_dtree, extra_dbits, 0,          D_CODES, MAX_BITS);
  static_bl_desc = new StaticTreeDesc(new Array(0), extra_blbits, 0,         BL_CODES, MAX_BL_BITS);

}


function init_block(s) {
  var n; 

  for (n = 0; n < L_CODES;  n++) { s.dyn_ltree[n * 2] = 0; }
  for (n = 0; n < D_CODES;  n++) { s.dyn_dtree[n * 2] = 0; }
  for (n = 0; n < BL_CODES; n++) { s.bl_tree[n * 2] = 0; }

  s.dyn_ltree[END_BLOCK * 2] = 1;
  s.opt_len = s.static_len = 0;
  s.last_lit = s.matches = 0;
}


function bi_windup(s)
{
  if (s.bi_valid > 8) {
    put_short(s, s.bi_buf);
  } else if (s.bi_valid > 0) {
    s.pending_buf[s.pending++] = s.bi_buf;
  }
  s.bi_buf = 0;
  s.bi_valid = 0;
}

function copy_block(s, buf, len, header)
{
  bi_windup(s);        

  if (header) {
    put_short(s, len);
    put_short(s, ~len);
  }
  utils.arraySet(s.pending_buf, s.window, buf, len, s.pending);
  s.pending += len;
}

function smaller(tree, n, m, depth) {
  var _n2 = n * 2;
  var _m2 = m * 2;
  return (tree[_n2] < tree[_m2] ||
         (tree[_n2] === tree[_m2] && depth[n] <= depth[m]));
}

function pqdownheap(s, tree, k)
{
  var v = s.heap[k];
  var j = k << 1;  
  while (j <= s.heap_len) {
    if (j < s.heap_len &&
      smaller(tree, s.heap[j + 1], s.heap[j], s.depth)) {
      j++;
    }
    if (smaller(tree, v, s.heap[j], s.depth)) { break; }

    s.heap[k] = s.heap[j];
    k = j;

    j <<= 1;
  }
  s.heap[k] = v;
}



function compress_block(s, ltree, dtree)
{
  var dist;           
  var lc;             
  var lx = 0;         
  var code;           
  var extra;          

  if (s.last_lit !== 0) {
    do {
      dist = (s.pending_buf[s.d_buf + lx * 2] << 8) | (s.pending_buf[s.d_buf + lx * 2 + 1]);
      lc = s.pending_buf[s.l_buf + lx];
      lx++;

      if (dist === 0) {
        send_code(s, lc, ltree); 
      } else {
        code = _length_code[lc];
        send_code(s, code + LITERALS + 1, ltree); 
        extra = extra_lbits[code];
        if (extra !== 0) {
          lc -= base_length[code];
          send_bits(s, lc, extra);       
        }
        dist--; 
        code = d_code(dist);

        send_code(s, code, dtree);       
        extra = extra_dbits[code];
        if (extra !== 0) {
          dist -= base_dist[code];
          send_bits(s, dist, extra);   
        }
      } 


    } while (lx < s.last_lit);
  }

  send_code(s, END_BLOCK, ltree);
}


function build_tree(s, desc)
{
  var tree     = desc.dyn_tree;
  var stree    = desc.stat_desc.static_tree;
  var has_stree = desc.stat_desc.has_stree;
  var elems    = desc.stat_desc.elems;
  var n, m;          
  var max_code = -1; 
  var node;          

  s.heap_len = 0;
  s.heap_max = HEAP_SIZE;

  for (n = 0; n < elems; n++) {
    if (tree[n * 2] !== 0) {
      s.heap[++s.heap_len] = max_code = n;
      s.depth[n] = 0;

    } else {
      tree[n * 2 + 1] = 0;
    }
  }

  while (s.heap_len < 2) {
    node = s.heap[++s.heap_len] = (max_code < 2 ? ++max_code : 0);
    tree[node * 2] = 1;
    s.depth[node] = 0;
    s.opt_len--;

    if (has_stree) {
      s.static_len -= stree[node * 2 + 1];
    }
  }
  desc.max_code = max_code;

  for (n = (s.heap_len >> 1); n >= 1; n--) { pqdownheap(s, tree, n); }

  node = elems;              
  do {
    n = s.heap[1];
    s.heap[1] = s.heap[s.heap_len--];
    pqdownheap(s, tree, 1);

    m = s.heap[1]; 

    s.heap[--s.heap_max] = n; 
    s.heap[--s.heap_max] = m;

    tree[node * 2] = tree[n * 2] + tree[m * 2];
    s.depth[node] = (s.depth[n] >= s.depth[m] ? s.depth[n] : s.depth[m]) + 1;
    tree[n * 2 + 1] = tree[m * 2 + 1] = node;

    s.heap[1] = node++;
    pqdownheap(s, tree, 1);

  } while (s.heap_len >= 2);

  s.heap[--s.heap_max] = s.heap[1];

  gen_bitlen(s, desc);

  gen_codes(tree, max_code, s.bl_count);
}


function scan_tree(s, tree, max_code)
{
  var n;                     
  var prevlen = -1;          
  var curlen;                

  var nextlen = tree[0 * 2 + 1]; 

  var count = 0;             
  var max_count = 7;         
  var min_count = 4;         

  if (nextlen === 0) {
    max_count = 138;
    min_count = 3;
  }
  tree[(max_code + 1) * 2 + 1] = 0xffff; 

  for (n = 0; n <= max_code; n++) {
    curlen = nextlen;
    nextlen = tree[(n + 1) * 2 + 1];

    if (++count < max_count && curlen === nextlen) {
      continue;

    } else if (count < min_count) {
      s.bl_tree[curlen * 2] += count;

    } else if (curlen !== 0) {

      if (curlen !== prevlen) { s.bl_tree[curlen * 2]++; }
      s.bl_tree[REP_3_6 * 2]++;

    } else if (count <= 10) {
      s.bl_tree[REPZ_3_10 * 2]++;

    } else {
      s.bl_tree[REPZ_11_138 * 2]++;
    }

    count = 0;
    prevlen = curlen;

    if (nextlen === 0) {
      max_count = 138;
      min_count = 3;

    } else if (curlen === nextlen) {
      max_count = 6;
      min_count = 3;

    } else {
      max_count = 7;
      min_count = 4;
    }
  }
}


function send_tree(s, tree, max_code)
{
  var n;                     
  var prevlen = -1;          
  var curlen;                

  var nextlen = tree[0 * 2 + 1]; 

  var count = 0;             
  var max_count = 7;         
  var min_count = 4;         

  if (nextlen === 0) {
    max_count = 138;
    min_count = 3;
  }

  for (n = 0; n <= max_code; n++) {
    curlen = nextlen;
    nextlen = tree[(n + 1) * 2 + 1];

    if (++count < max_count && curlen === nextlen) {
      continue;

    } else if (count < min_count) {
      do { send_code(s, curlen, s.bl_tree); } while (--count !== 0);

    } else if (curlen !== 0) {
      if (curlen !== prevlen) {
        send_code(s, curlen, s.bl_tree);
        count--;
      }
      send_code(s, REP_3_6, s.bl_tree);
      send_bits(s, count - 3, 2);

    } else if (count <= 10) {
      send_code(s, REPZ_3_10, s.bl_tree);
      send_bits(s, count - 3, 3);

    } else {
      send_code(s, REPZ_11_138, s.bl_tree);
      send_bits(s, count - 11, 7);
    }

    count = 0;
    prevlen = curlen;
    if (nextlen === 0) {
      max_count = 138;
      min_count = 3;

    } else if (curlen === nextlen) {
      max_count = 6;
      min_count = 3;

    } else {
      max_count = 7;
      min_count = 4;
    }
  }
}


function build_bl_tree(s) {
  var max_blindex;  

  scan_tree(s, s.dyn_ltree, s.l_desc.max_code);
  scan_tree(s, s.dyn_dtree, s.d_desc.max_code);

  build_tree(s, s.bl_desc);

  for (max_blindex = BL_CODES - 1; max_blindex >= 3; max_blindex--) {
    if (s.bl_tree[bl_order[max_blindex] * 2 + 1] !== 0) {
      break;
    }
  }
  s.opt_len += 3 * (max_blindex + 1) + 5 + 5 + 4;

  return max_blindex;
}


function send_all_trees(s, lcodes, dcodes, blcodes)
{
  var rank;                    

  send_bits(s, lcodes - 257, 5); 
  send_bits(s, dcodes - 1,   5);
  send_bits(s, blcodes - 4,  4); 
  for (rank = 0; rank < blcodes; rank++) {
    send_bits(s, s.bl_tree[bl_order[rank] * 2 + 1], 3);
  }

  send_tree(s, s.dyn_ltree, lcodes - 1); 

  send_tree(s, s.dyn_dtree, dcodes - 1); 
}


function detect_data_type(s) {
  var black_mask = 0xf3ffc07f;
  var n;

  for (n = 0; n <= 31; n++, black_mask >>>= 1) {
    if ((black_mask & 1) && (s.dyn_ltree[n * 2] !== 0)) {
      return Z_BINARY;
    }
  }

  if (s.dyn_ltree[9 * 2] !== 0 || s.dyn_ltree[10 * 2] !== 0 ||
      s.dyn_ltree[13 * 2] !== 0) {
    return Z_TEXT;
  }
  for (n = 32; n < LITERALS; n++) {
    if (s.dyn_ltree[n * 2] !== 0) {
      return Z_TEXT;
    }
  }

  return Z_BINARY;
}


var static_init_done = false;

function _tr_init(s)
{

  if (!static_init_done) {
    tr_static_init();
    static_init_done = true;
  }

  s.l_desc  = new TreeDesc(s.dyn_ltree, static_l_desc);
  s.d_desc  = new TreeDesc(s.dyn_dtree, static_d_desc);
  s.bl_desc = new TreeDesc(s.bl_tree, static_bl_desc);

  s.bi_buf = 0;
  s.bi_valid = 0;

  init_block(s);
}


function _tr_stored_block(s, buf, stored_len, last)
{
  send_bits(s, (STORED_BLOCK << 1) + (last ? 1 : 0), 3);    
  copy_block(s, buf, stored_len, true); 
}


function _tr_align(s) {
  send_bits(s, STATIC_TREES << 1, 3);
  send_code(s, END_BLOCK, static_ltree);
  bi_flush(s);
}


function _tr_flush_block(s, buf, stored_len, last)
{
  var opt_lenb, static_lenb;  
  var max_blindex = 0;        

  if (s.level > 0) {

    if (s.strm.data_type === Z_UNKNOWN) {
      s.strm.data_type = detect_data_type(s);
    }

    build_tree(s, s.l_desc);

    build_tree(s, s.d_desc);

    max_blindex = build_bl_tree(s);

    opt_lenb = (s.opt_len + 3 + 7) >>> 3;
    static_lenb = (s.static_len + 3 + 7) >>> 3;


    if (static_lenb <= opt_lenb) { opt_lenb = static_lenb; }

  } else {
    opt_lenb = static_lenb = stored_len + 5; 
  }

  if ((stored_len + 4 <= opt_lenb) && (buf !== -1)) {

    _tr_stored_block(s, buf, stored_len, last);

  } else if (s.strategy === Z_FIXED || static_lenb === opt_lenb) {

    send_bits(s, (STATIC_TREES << 1) + (last ? 1 : 0), 3);
    compress_block(s, static_ltree, static_dtree);

  } else {
    send_bits(s, (DYN_TREES << 1) + (last ? 1 : 0), 3);
    send_all_trees(s, s.l_desc.max_code + 1, s.d_desc.max_code + 1, max_blindex + 1);
    compress_block(s, s.dyn_ltree, s.dyn_dtree);
  }
  init_block(s);

  if (last) {
    bi_windup(s);
  }
}

function _tr_tally(s, dist, lc)
{

  s.pending_buf[s.d_buf + s.last_lit * 2]     = (dist >>> 8) & 0xff;
  s.pending_buf[s.d_buf + s.last_lit * 2 + 1] = dist & 0xff;

  s.pending_buf[s.l_buf + s.last_lit] = lc & 0xff;
  s.last_lit++;

  if (dist === 0) {
    s.dyn_ltree[lc * 2]++;
  } else {
    s.matches++;
    dist--;             

    s.dyn_ltree[(_length_code[lc] + LITERALS + 1) * 2]++;
    s.dyn_dtree[d_code(dist) * 2]++;
  }



  return (s.last_lit === s.lit_bufsize - 1);
}

exports._tr_init  = _tr_init;
exports._tr_stored_block = _tr_stored_block;
exports._tr_flush_block  = _tr_flush_block;
exports._tr_tally = _tr_tally;
exports._tr_align = _tr_align;

},{"4":4}],14:[function(require,module,exports){
'use strict';


function ZStream() {
  this.input = null; 
  this.next_in = 0;
  this.avail_in = 0;
  this.total_in = 0;
  this.output = null; 
  this.next_out = 0;
  this.avail_out = 0;
  this.total_out = 0;
  this.msg = '';
  this.state = null;
  this.data_type = 2;
  this.adler = 0;
}

module.exports = ZStream;

},{}],15:[function(require,module,exports){
(function (process,Buffer){
var msg = require(12);
var zstream = require(14);
var zlib_deflate = require(8);
var zlib_inflate = require(10);
var constants = require(6);

for (var key in constants) {
  exports[key] = constants[key];
}

exports.NONE = 0;
exports.DEFLATE = 1;
exports.INFLATE = 2;
exports.GZIP = 3;
exports.GUNZIP = 4;
exports.DEFLATERAW = 5;
exports.INFLATERAW = 6;
exports.UNZIP = 7;

function Zlib(mode) {
  if (mode < exports.DEFLATE || mode > exports.UNZIP)
    throw new TypeError("Bad argument");

      this.mode = mode;
  this.init_done = false;
  this.write_in_progress = false;
  this.pending_close = false;
  this.windowBits = 0;
  this.level = 0;
  this.memLevel = 0;
  this.strategy = 0;
  this.dictionary = null;
}

Zlib.prototype.init = function(windowBits, level, memLevel, strategy, dictionary) {
  this.windowBits = windowBits;
  this.level = level;
  this.memLevel = memLevel;
  this.strategy = strategy;

    if (this.mode === exports.GZIP || this.mode === exports.GUNZIP)
    this.windowBits += 16;

      if (this.mode === exports.UNZIP)
    this.windowBits += 32;

      if (this.mode === exports.DEFLATERAW || this.mode === exports.INFLATERAW)
    this.windowBits = -this.windowBits;

      this.strm = new zstream();

    switch (this.mode) {
    case exports.DEFLATE:
    case exports.GZIP:
    case exports.DEFLATERAW:
      var status = zlib_deflate.deflateInit2(
        this.strm,
        this.level,
        exports.Z_DEFLATED,
        this.windowBits,
        this.memLevel,
        this.strategy
      );
      break;
    case exports.INFLATE:
    case exports.GUNZIP:
    case exports.INFLATERAW:
    case exports.UNZIP:
      var status  = zlib_inflate.inflateInit2(
        this.strm,
        this.windowBits
      );
      break;
    default:
      throw new Error("Unknown mode " + this.mode);
  }

    if (status !== exports.Z_OK) {
    this._error(status);
    return;
  }

    this.write_in_progress = false;
  this.init_done = true;
};

Zlib.prototype.params = function() {
  throw new Error("deflateParams Not supported");
};

Zlib.prototype._writeCheck = function() {
  if (!this.init_done)
    throw new Error("write before init");

      if (this.mode === exports.NONE)
    throw new Error("already finalized");

      if (this.write_in_progress)
    throw new Error("write already in progress");

      if (this.pending_close)
    throw new Error("close is pending");
};

Zlib.prototype.write = function(flush, input, in_off, in_len, out, out_off, out_len) {    
  this._writeCheck();
  this.write_in_progress = true;

    var self = this;
  process.nextTick(function() {
    self.write_in_progress = false;
    var res = self._write(flush, input, in_off, in_len, out, out_off, out_len);
    self.callback(res[0], res[1]);

        if (self.pending_close)
      self.close();
  });

    return this;
};

function bufferSet(data, offset) {
  for (var i = 0; i < data.length; i++) {
    this[offset + i] = data[i];
  }
}

Zlib.prototype.writeSync = function(flush, input, in_off, in_len, out, out_off, out_len) {
  this._writeCheck();
  return this._write(flush, input, in_off, in_len, out, out_off, out_len);
};

Zlib.prototype._write = function(flush, input, in_off, in_len, out, out_off, out_len) {
  this.write_in_progress = true;

    if (flush !== exports.Z_NO_FLUSH &&
      flush !== exports.Z_PARTIAL_FLUSH &&
      flush !== exports.Z_SYNC_FLUSH &&
      flush !== exports.Z_FULL_FLUSH &&
      flush !== exports.Z_FINISH &&
      flush !== exports.Z_BLOCK) {
    throw new Error("Invalid flush value");
  }

    if (input == null) {
    input = new Buffer(0);
    in_len = 0;
    in_off = 0;
  }

    if (out._set)
    out.set = out._set;
  else
    out.set = bufferSet;

    var strm = this.strm;
  strm.avail_in = in_len;
  strm.input = input;
  strm.next_in = in_off;
  strm.avail_out = out_len;
  strm.output = out;
  strm.next_out = out_off;

    switch (this.mode) {
    case exports.DEFLATE:
    case exports.GZIP:
    case exports.DEFLATERAW:
      var status = zlib_deflate.deflate(strm, flush);
      break;
    case exports.UNZIP:
    case exports.INFLATE:
    case exports.GUNZIP:
    case exports.INFLATERAW:
      var status = zlib_inflate.inflate(strm, flush);
      break;
    default:
      throw new Error("Unknown mode " + this.mode);
  }

    if (status !== exports.Z_STREAM_END && status !== exports.Z_OK) {
    this._error(status);
  }

    this.write_in_progress = false;
  return [strm.avail_in, strm.avail_out];
};

Zlib.prototype.close = function() {
  if (this.write_in_progress) {
    this.pending_close = true;
    return;
  }

    this.pending_close = false;

    if (this.mode === exports.DEFLATE || this.mode === exports.GZIP || this.mode === exports.DEFLATERAW) {
    zlib_deflate.deflateEnd(this.strm);
  } else {
    zlib_inflate.inflateEnd(this.strm);
  }

    this.mode = exports.NONE;
};

Zlib.prototype.reset = function() {
  switch (this.mode) {
    case exports.DEFLATE:
    case exports.DEFLATERAW:
      var status = zlib_deflate.deflateReset(this.strm);
      break;
    case exports.INFLATE:
    case exports.INFLATERAW:
      var status = zlib_inflate.inflateReset(this.strm);
      break;
  }

    if (status !== exports.Z_OK) {
    this._error(status);
  }
};

Zlib.prototype._error = function(status) {
  this.onerror(msg[status] + ': ' + this.strm.msg, status);

    this.write_in_progress = false;
  if (this.pending_close)
    this.close();
};

exports.Zlib = Zlib;

}).call(this,require(23),require(17).Buffer)
},{"10":10,"12":12,"14":14,"17":17,"23":23,"6":6,"8":8}],16:[function(require,module,exports){
(function (process,Buffer){

var Transform = require(40);

var binding = require(15);
var util = require(42);
var assert = require(2).ok;

binding.Z_MIN_WINDOWBITS = 8;
binding.Z_MAX_WINDOWBITS = 15;
binding.Z_DEFAULT_WINDOWBITS = 15;

binding.Z_MIN_CHUNK = 64;
binding.Z_MAX_CHUNK = Infinity;
binding.Z_DEFAULT_CHUNK = (16 * 1024);

binding.Z_MIN_MEMLEVEL = 1;
binding.Z_MAX_MEMLEVEL = 9;
binding.Z_DEFAULT_MEMLEVEL = 8;

binding.Z_MIN_LEVEL = -1;
binding.Z_MAX_LEVEL = 9;
binding.Z_DEFAULT_LEVEL = binding.Z_DEFAULT_COMPRESSION;

Object.keys(binding).forEach(function(k) {
  if (k.match(/^Z/)) exports[k] = binding[k];
});

exports.codes = {
  Z_OK: binding.Z_OK,
  Z_STREAM_END: binding.Z_STREAM_END,
  Z_NEED_DICT: binding.Z_NEED_DICT,
  Z_ERRNO: binding.Z_ERRNO,
  Z_STREAM_ERROR: binding.Z_STREAM_ERROR,
  Z_DATA_ERROR: binding.Z_DATA_ERROR,
  Z_MEM_ERROR: binding.Z_MEM_ERROR,
  Z_BUF_ERROR: binding.Z_BUF_ERROR,
  Z_VERSION_ERROR: binding.Z_VERSION_ERROR
};

Object.keys(exports.codes).forEach(function(k) {
  exports.codes[exports.codes[k]] = k;
});

exports.Deflate = Deflate;
exports.Inflate = Inflate;
exports.Gzip = Gzip;
exports.Gunzip = Gunzip;
exports.DeflateRaw = DeflateRaw;
exports.InflateRaw = InflateRaw;
exports.Unzip = Unzip;

exports.createDeflate = function(o) {
  return new Deflate(o);
};

exports.createInflate = function(o) {
  return new Inflate(o);
};

exports.createDeflateRaw = function(o) {
  return new DeflateRaw(o);
};

exports.createInflateRaw = function(o) {
  return new InflateRaw(o);
};

exports.createGzip = function(o) {
  return new Gzip(o);
};

exports.createGunzip = function(o) {
  return new Gunzip(o);
};

exports.createUnzip = function(o) {
  return new Unzip(o);
};


exports.deflate = function(buffer, opts, callback) {
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  return zlibBuffer(new Deflate(opts), buffer, callback);
};

exports.deflateSync = function(buffer, opts) {
  return zlibBufferSync(new Deflate(opts), buffer);
};

exports.gzip = function(buffer, opts, callback) {
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  return zlibBuffer(new Gzip(opts), buffer, callback);
};

exports.gzipSync = function(buffer, opts) {
  return zlibBufferSync(new Gzip(opts), buffer);
};

exports.deflateRaw = function(buffer, opts, callback) {
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  return zlibBuffer(new DeflateRaw(opts), buffer, callback);
};

exports.deflateRawSync = function(buffer, opts) {
  return zlibBufferSync(new DeflateRaw(opts), buffer);
};

exports.unzip = function(buffer, opts, callback) {
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  return zlibBuffer(new Unzip(opts), buffer, callback);
};

exports.unzipSync = function(buffer, opts) {
  return zlibBufferSync(new Unzip(opts), buffer);
};

exports.inflate = function(buffer, opts, callback) {
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  return zlibBuffer(new Inflate(opts), buffer, callback);
};

exports.inflateSync = function(buffer, opts) {
  return zlibBufferSync(new Inflate(opts), buffer);
};

exports.gunzip = function(buffer, opts, callback) {
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  return zlibBuffer(new Gunzip(opts), buffer, callback);
};

exports.gunzipSync = function(buffer, opts) {
  return zlibBufferSync(new Gunzip(opts), buffer);
};

exports.inflateRaw = function(buffer, opts, callback) {
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  return zlibBuffer(new InflateRaw(opts), buffer, callback);
};

exports.inflateRawSync = function(buffer, opts) {
  return zlibBufferSync(new InflateRaw(opts), buffer);
};

function zlibBuffer(engine, buffer, callback) {
  var buffers = [];
  var nread = 0;

  engine.on('error', onError);
  engine.on('end', onEnd);

  engine.end(buffer);
  flow();

  function flow() {
    var chunk;
    while (null !== (chunk = engine.read())) {
      buffers.push(chunk);
      nread += chunk.length;
    }
    engine.once('readable', flow);
  }

  function onError(err) {
    engine.removeListener('end', onEnd);
    engine.removeListener('readable', flow);
    callback(err);
  }

  function onEnd() {
    var buf = Buffer.concat(buffers, nread);
    buffers = [];
    callback(null, buf);
    engine.close();
  }
}

function zlibBufferSync(engine, buffer) {
  if (typeof buffer === 'string')
    buffer = new Buffer(buffer);
  if (!Buffer.isBuffer(buffer))
    throw new TypeError('Not a string or buffer');

  var flushFlag = binding.Z_FINISH;

  return engine._processChunk(buffer, flushFlag);
}

function Deflate(opts) {
  if (!(this instanceof Deflate)) return new Deflate(opts);
  Zlib.call(this, opts, binding.DEFLATE);
}

function Inflate(opts) {
  if (!(this instanceof Inflate)) return new Inflate(opts);
  Zlib.call(this, opts, binding.INFLATE);
}



function Gzip(opts) {
  if (!(this instanceof Gzip)) return new Gzip(opts);
  Zlib.call(this, opts, binding.GZIP);
}

function Gunzip(opts) {
  if (!(this instanceof Gunzip)) return new Gunzip(opts);
  Zlib.call(this, opts, binding.GUNZIP);
}



function DeflateRaw(opts) {
  if (!(this instanceof DeflateRaw)) return new DeflateRaw(opts);
  Zlib.call(this, opts, binding.DEFLATERAW);
}

function InflateRaw(opts) {
  if (!(this instanceof InflateRaw)) return new InflateRaw(opts);
  Zlib.call(this, opts, binding.INFLATERAW);
}


function Unzip(opts) {
  if (!(this instanceof Unzip)) return new Unzip(opts);
  Zlib.call(this, opts, binding.UNZIP);
}



function Zlib(opts, mode) {
  this._opts = opts = opts || {};
  this._chunkSize = opts.chunkSize || exports.Z_DEFAULT_CHUNK;

  Transform.call(this, opts);

  if (opts.flush) {
    if (opts.flush !== binding.Z_NO_FLUSH &&
        opts.flush !== binding.Z_PARTIAL_FLUSH &&
        opts.flush !== binding.Z_SYNC_FLUSH &&
        opts.flush !== binding.Z_FULL_FLUSH &&
        opts.flush !== binding.Z_FINISH &&
        opts.flush !== binding.Z_BLOCK) {
      throw new Error('Invalid flush flag: ' + opts.flush);
    }
  }
  this._flushFlag = opts.flush || binding.Z_NO_FLUSH;

  if (opts.chunkSize) {
    if (opts.chunkSize < exports.Z_MIN_CHUNK ||
        opts.chunkSize > exports.Z_MAX_CHUNK) {
      throw new Error('Invalid chunk size: ' + opts.chunkSize);
    }
  }

  if (opts.windowBits) {
    if (opts.windowBits < exports.Z_MIN_WINDOWBITS ||
        opts.windowBits > exports.Z_MAX_WINDOWBITS) {
      throw new Error('Invalid windowBits: ' + opts.windowBits);
    }
  }

  if (opts.level) {
    if (opts.level < exports.Z_MIN_LEVEL ||
        opts.level > exports.Z_MAX_LEVEL) {
      throw new Error('Invalid compression level: ' + opts.level);
    }
  }

  if (opts.memLevel) {
    if (opts.memLevel < exports.Z_MIN_MEMLEVEL ||
        opts.memLevel > exports.Z_MAX_MEMLEVEL) {
      throw new Error('Invalid memLevel: ' + opts.memLevel);
    }
  }

  if (opts.strategy) {
    if (opts.strategy != exports.Z_FILTERED &&
        opts.strategy != exports.Z_HUFFMAN_ONLY &&
        opts.strategy != exports.Z_RLE &&
        opts.strategy != exports.Z_FIXED &&
        opts.strategy != exports.Z_DEFAULT_STRATEGY) {
      throw new Error('Invalid strategy: ' + opts.strategy);
    }
  }

  if (opts.dictionary) {
    if (!Buffer.isBuffer(opts.dictionary)) {
      throw new Error('Invalid dictionary: it should be a Buffer instance');
    }
  }

  this._binding = new binding.Zlib(mode);

  var self = this;
  this._hadError = false;
  this._binding.onerror = function(message, errno) {
    self._binding = null;
    self._hadError = true;

    var error = new Error(message);
    error.errno = errno;
    error.code = exports.codes[errno];
    self.emit('error', error);
  };

  var level = exports.Z_DEFAULT_COMPRESSION;
  if (typeof opts.level === 'number') level = opts.level;

  var strategy = exports.Z_DEFAULT_STRATEGY;
  if (typeof opts.strategy === 'number') strategy = opts.strategy;

  this._binding.init(opts.windowBits || exports.Z_DEFAULT_WINDOWBITS,
                     level,
                     opts.memLevel || exports.Z_DEFAULT_MEMLEVEL,
                     strategy,
                     opts.dictionary);

  this._buffer = new Buffer(this._chunkSize);
  this._offset = 0;
  this._closed = false;
  this._level = level;
  this._strategy = strategy;

  this.once('end', this.close);
}

util.inherits(Zlib, Transform);

Zlib.prototype.params = function(level, strategy, callback) {
  if (level < exports.Z_MIN_LEVEL ||
      level > exports.Z_MAX_LEVEL) {
    throw new RangeError('Invalid compression level: ' + level);
  }
  if (strategy != exports.Z_FILTERED &&
      strategy != exports.Z_HUFFMAN_ONLY &&
      strategy != exports.Z_RLE &&
      strategy != exports.Z_FIXED &&
      strategy != exports.Z_DEFAULT_STRATEGY) {
    throw new TypeError('Invalid strategy: ' + strategy);
  }

  if (this._level !== level || this._strategy !== strategy) {
    var self = this;
    this.flush(binding.Z_SYNC_FLUSH, function() {
      self._binding.params(level, strategy);
      if (!self._hadError) {
        self._level = level;
        self._strategy = strategy;
        if (callback) callback();
      }
    });
  } else {
    process.nextTick(callback);
  }
};

Zlib.prototype.reset = function() {
  return this._binding.reset();
};

Zlib.prototype._flush = function(callback) {
  this._transform(new Buffer(0), '', callback);
};

Zlib.prototype.flush = function(kind, callback) {
  var ws = this._writableState;

  if (typeof kind === 'function' || (kind === void 0 && !callback)) {
    callback = kind;
    kind = binding.Z_FULL_FLUSH;
  }

  if (ws.ended) {
    if (callback)
      process.nextTick(callback);
  } else if (ws.ending) {
    if (callback)
      this.once('end', callback);
  } else if (ws.needDrain) {
    var self = this;
    this.once('drain', function() {
      self.flush(callback);
    });
  } else {
    this._flushFlag = kind;
    this.write(new Buffer(0), '', callback);
  }
};

Zlib.prototype.close = function(callback) {
  if (callback)
    process.nextTick(callback);

  if (this._closed)
    return;

  this._closed = true;

  this._binding.close();

  var self = this;
  process.nextTick(function() {
    self.emit('close');
  });
};

Zlib.prototype._transform = function(chunk, encoding, cb) {
  var flushFlag;
  var ws = this._writableState;
  var ending = ws.ending || ws.ended;
  var last = ending && (!chunk || ws.length === chunk.length);

  if (!chunk === null && !Buffer.isBuffer(chunk))
    return cb(new Error('invalid input'));

  if (last)
    flushFlag = binding.Z_FINISH;
  else {
    flushFlag = this._flushFlag;
    if (chunk.length >= ws.length) {
      this._flushFlag = this._opts.flush || binding.Z_NO_FLUSH;
    }
  }

  var self = this;
  this._processChunk(chunk, flushFlag, cb);
};

Zlib.prototype._processChunk = function(chunk, flushFlag, cb) {
  var availInBefore = chunk && chunk.length;
  var availOutBefore = this._chunkSize - this._offset;
  var inOff = 0;

  var self = this;

  var async = typeof cb === 'function';

  if (!async) {
    var buffers = [];
    var nread = 0;

    var error;
    this.on('error', function(er) {
      error = er;
    });

    do {
      var res = this._binding.writeSync(flushFlag,
                                        chunk, 
                                        inOff, 
                                        availInBefore, 
                                        this._buffer, 
                                        this._offset, 
                                        availOutBefore); 
    } while (!this._hadError && callback(res[0], res[1]));

    if (this._hadError) {
      throw error;
    }

    var buf = Buffer.concat(buffers, nread);
    this.close();

    return buf;
  }

  var req = this._binding.write(flushFlag,
                                chunk, 
                                inOff, 
                                availInBefore, 
                                this._buffer, 
                                this._offset, 
                                availOutBefore); 

  req.buffer = chunk;
  req.callback = callback;

  function callback(availInAfter, availOutAfter) {
    if (self._hadError)
      return;

    var have = availOutBefore - availOutAfter;
    assert(have >= 0, 'have should not go down');

    if (have > 0) {
      var out = self._buffer.slice(self._offset, self._offset + have);
      self._offset += have;
      if (async) {
        self.push(out);
      } else {
        buffers.push(out);
        nread += out.length;
      }
    }

    if (availOutAfter === 0 || self._offset >= self._chunkSize) {
      availOutBefore = self._chunkSize;
      self._offset = 0;
      self._buffer = new Buffer(self._chunkSize);
    }

    if (availOutAfter === 0) {
      inOff += (availInBefore - availInAfter);
      availInBefore = availInAfter;

      if (!async)
        return true;

      var newReq = self._binding.write(flushFlag,
                                       chunk,
                                       inOff,
                                       availInBefore,
                                       self._buffer,
                                       self._offset,
                                       self._chunkSize);
      newReq.callback = callback; 
      newReq.buffer = chunk;
      return;
    }

    if (!async)
      return false;

    cb();
  }
};

util.inherits(Deflate, Zlib);
util.inherits(Inflate, Zlib);
util.inherits(Gzip, Zlib);
util.inherits(Gunzip, Zlib);
util.inherits(DeflateRaw, Zlib);
util.inherits(InflateRaw, Zlib);
util.inherits(Unzip, Zlib);

}).call(this,require(23),require(17).Buffer)
},{"15":15,"17":17,"2":2,"23":23,"40":40,"42":42}],17:[function(require,module,exports){
(function (global){

'use strict'

var base64 = require(18)
var ieee754 = require(19)
var isArray = require(20)

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50

Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
  ? global.TYPED_ARRAY_SUPPORT
  : typedArraySupport()

exports.kMaxLength = kMaxLength()

function typedArraySupport () {
  try {
    var arr = new Uint8Array(1)
    arr.__proto__ = {__proto__: Uint8Array.prototype, foo: function () { return 42 }}
    return arr.foo() === 42 && 
        typeof arr.subarray === 'function' && 
        arr.subarray(1, 1).byteLength === 0 
  } catch (e) {
    return false
  }
}

function kMaxLength () {
  return Buffer.TYPED_ARRAY_SUPPORT
    ? 0x7fffffff
    : 0x3fffffff
}

function createBuffer (that, length) {
  if (kMaxLength() < length) {
    throw new RangeError('Invalid typed array length')
  }
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    that = new Uint8Array(length)
    that.__proto__ = Buffer.prototype
  } else {
    if (that === null) {
      that = new Buffer(length)
    }
    that.length = length
  }

  return that
}


function Buffer (arg, encodingOrOffset, length) {
  if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
    return new Buffer(arg, encodingOrOffset, length)
  }

  if (typeof arg === 'number') {
    if (typeof encodingOrOffset === 'string') {
      throw new Error(
        'If encoding is specified then the first argument must be a string'
      )
    }
    return allocUnsafe(this, arg)
  }
  return from(this, arg, encodingOrOffset, length)
}

Buffer.poolSize = 8192 

Buffer._augment = function (arr) {
  arr.__proto__ = Buffer.prototype
  return arr
}

function from (that, value, encodingOrOffset, length) {
  if (typeof value === 'number') {
    throw new TypeError('"value" argument must not be a number')
  }

  if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
    return fromArrayBuffer(that, value, encodingOrOffset, length)
  }

  if (typeof value === 'string') {
    return fromString(that, value, encodingOrOffset)
  }

  return fromObject(that, value)
}

Buffer.from = function (value, encodingOrOffset, length) {
  return from(null, value, encodingOrOffset, length)
}

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype
  Buffer.__proto__ = Uint8Array
  if (typeof Symbol !== 'undefined' && Symbol.species &&
      Buffer[Symbol.species] === Buffer) {
    Object.defineProperty(Buffer, Symbol.species, {
      value: null,
      configurable: true
    })
  }
}

function assertSize (size) {
  if (typeof size !== 'number') {
    throw new TypeError('"size" argument must be a number')
  } else if (size < 0) {
    throw new RangeError('"size" argument must not be negative')
  }
}

function alloc (that, size, fill, encoding) {
  assertSize(size)
  if (size <= 0) {
    return createBuffer(that, size)
  }
  if (fill !== undefined) {
    return typeof encoding === 'string'
      ? createBuffer(that, size).fill(fill, encoding)
      : createBuffer(that, size).fill(fill)
  }
  return createBuffer(that, size)
}

Buffer.alloc = function (size, fill, encoding) {
  return alloc(null, size, fill, encoding)
}

function allocUnsafe (that, size) {
  assertSize(size)
  that = createBuffer(that, size < 0 ? 0 : checked(size) | 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < size; ++i) {
      that[i] = 0
    }
  }
  return that
}

Buffer.allocUnsafe = function (size) {
  return allocUnsafe(null, size)
}
Buffer.allocUnsafeSlow = function (size) {
  return allocUnsafe(null, size)
}

function fromString (that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') {
    encoding = 'utf8'
  }

  if (!Buffer.isEncoding(encoding)) {
    throw new TypeError('"encoding" must be a valid string encoding')
  }

  var length = byteLength(string, encoding) | 0
  that = createBuffer(that, length)

  var actual = that.write(string, encoding)

  if (actual !== length) {
    that = that.slice(0, actual)
  }

  return that
}

function fromArrayLike (that, array) {
  var length = array.length < 0 ? 0 : checked(array.length) | 0
  that = createBuffer(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

function fromArrayBuffer (that, array, byteOffset, length) {
  array.byteLength 

  if (byteOffset < 0 || array.byteLength < byteOffset) {
    throw new RangeError('\'offset\' is out of bounds')
  }

  if (array.byteLength < byteOffset + (length || 0)) {
    throw new RangeError('\'length\' is out of bounds')
  }

  if (byteOffset === undefined && length === undefined) {
    array = new Uint8Array(array)
  } else if (length === undefined) {
    array = new Uint8Array(array, byteOffset)
  } else {
    array = new Uint8Array(array, byteOffset, length)
  }

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    that = array
    that.__proto__ = Buffer.prototype
  } else {
    that = fromArrayLike(that, array)
  }
  return that
}

function fromObject (that, obj) {
  if (Buffer.isBuffer(obj)) {
    var len = checked(obj.length) | 0
    that = createBuffer(that, len)

    if (that.length === 0) {
      return that
    }

    obj.copy(that, 0, 0, len)
    return that
  }

  if (obj) {
    if ((typeof ArrayBuffer !== 'undefined' &&
        obj.buffer instanceof ArrayBuffer) || 'length' in obj) {
      if (typeof obj.length !== 'number' || isnan(obj.length)) {
        return createBuffer(that, 0)
      }
      return fromArrayLike(that, obj)
    }

    if (obj.type === 'Buffer' && isArray(obj.data)) {
      return fromArrayLike(that, obj.data)
    }
  }

  throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.')
}

function checked (length) {
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (length) {
  if (+length != length) { 
    length = 0
  }
  return Buffer.alloc(+length)
}

Buffer.isBuffer = function isBuffer (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function compare (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers')
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i]
      y = b[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'latin1':
    case 'binary':
    case 'base64':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!isArray(list)) {
    throw new TypeError('"list" argument must be an Array of Buffers')
  }

  if (list.length === 0) {
    return Buffer.alloc(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; ++i) {
      length += list[i].length
    }
  }

  var buffer = Buffer.allocUnsafe(length)
  var pos = 0
  for (i = 0; i < list.length; ++i) {
    var buf = list[i]
    if (!Buffer.isBuffer(buf)) {
      throw new TypeError('"list" argument must be an Array of Buffers')
    }
    buf.copy(buffer, pos)
    pos += buf.length
  }
  return buffer
}

function byteLength (string, encoding) {
  if (Buffer.isBuffer(string)) {
    return string.length
  }
  if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' &&
      (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
    return string.byteLength
  }
  if (typeof string !== 'string') {
    string = '' + string
  }

  var len = string.length
  if (len === 0) return 0

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'latin1':
      case 'binary':
        return len
      case 'utf8':
      case 'utf-8':
      case undefined:
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) return utf8ToBytes(string).length 
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

function slowToString (encoding, start, end) {
  var loweredCase = false


  if (start === undefined || start < 0) {
    start = 0
  }
  if (start > this.length) {
    return ''
  }

  if (end === undefined || end > this.length) {
    end = this.length
  }

  if (end <= 0) {
    return ''
  }

  end >>>= 0
  start >>>= 0

  if (end <= start) {
    return ''
  }

  if (!encoding) encoding = 'utf8'

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'latin1':
      case 'binary':
        return latin1Slice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype._isBuffer = true

function swap (b, n, m) {
  var i = b[n]
  b[n] = b[m]
  b[m] = i
}

Buffer.prototype.swap16 = function swap16 () {
  var len = this.length
  if (len % 2 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 16-bits')
  }
  for (var i = 0; i < len; i += 2) {
    swap(this, i, i + 1)
  }
  return this
}

Buffer.prototype.swap32 = function swap32 () {
  var len = this.length
  if (len % 4 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 32-bits')
  }
  for (var i = 0; i < len; i += 4) {
    swap(this, i, i + 3)
    swap(this, i + 1, i + 2)
  }
  return this
}

Buffer.prototype.swap64 = function swap64 () {
  var len = this.length
  if (len % 8 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 64-bits')
  }
  for (var i = 0; i < len; i += 8) {
    swap(this, i, i + 7)
    swap(this, i + 1, i + 6)
    swap(this, i + 2, i + 5)
    swap(this, i + 3, i + 4)
  }
  return this
}

Buffer.prototype.toString = function toString () {
  var length = this.length | 0
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max) str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
  if (!Buffer.isBuffer(target)) {
    throw new TypeError('Argument must be a Buffer')
  }

  if (start === undefined) {
    start = 0
  }
  if (end === undefined) {
    end = target ? target.length : 0
  }
  if (thisStart === undefined) {
    thisStart = 0
  }
  if (thisEnd === undefined) {
    thisEnd = this.length
  }

  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
    throw new RangeError('out of range index')
  }

  if (thisStart >= thisEnd && start >= end) {
    return 0
  }
  if (thisStart >= thisEnd) {
    return -1
  }
  if (start >= end) {
    return 1
  }

  start >>>= 0
  end >>>= 0
  thisStart >>>= 0
  thisEnd >>>= 0

  if (this === target) return 0

  var x = thisEnd - thisStart
  var y = end - start
  var len = Math.min(x, y)

  var thisCopy = this.slice(thisStart, thisEnd)
  var targetCopy = target.slice(start, end)

  for (var i = 0; i < len; ++i) {
    if (thisCopy[i] !== targetCopy[i]) {
      x = thisCopy[i]
      y = targetCopy[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
  if (buffer.length === 0) return -1

  if (typeof byteOffset === 'string') {
    encoding = byteOffset
    byteOffset = 0
  } else if (byteOffset > 0x7fffffff) {
    byteOffset = 0x7fffffff
  } else if (byteOffset < -0x80000000) {
    byteOffset = -0x80000000
  }
  byteOffset = +byteOffset  
  if (isNaN(byteOffset)) {
    byteOffset = dir ? 0 : (buffer.length - 1)
  }

  if (byteOffset < 0) byteOffset = buffer.length + byteOffset
  if (byteOffset >= buffer.length) {
    if (dir) return -1
    else byteOffset = buffer.length - 1
  } else if (byteOffset < 0) {
    if (dir) byteOffset = 0
    else return -1
  }

  if (typeof val === 'string') {
    val = Buffer.from(val, encoding)
  }

  if (Buffer.isBuffer(val)) {
    if (val.length === 0) {
      return -1
    }
    return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
  } else if (typeof val === 'number') {
    val = val & 0xFF 
    if (Buffer.TYPED_ARRAY_SUPPORT &&
        typeof Uint8Array.prototype.indexOf === 'function') {
      if (dir) {
        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
      } else {
        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
      }
    }
    return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
  }

  throw new TypeError('val must be string, number or Buffer')
}

function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
  var indexSize = 1
  var arrLength = arr.length
  var valLength = val.length

  if (encoding !== undefined) {
    encoding = String(encoding).toLowerCase()
    if (encoding === 'ucs2' || encoding === 'ucs-2' ||
        encoding === 'utf16le' || encoding === 'utf-16le') {
      if (arr.length < 2 || val.length < 2) {
        return -1
      }
      indexSize = 2
      arrLength /= 2
      valLength /= 2
      byteOffset /= 2
    }
  }

  function read (buf, i) {
    if (indexSize === 1) {
      return buf[i]
    } else {
      return buf.readUInt16BE(i * indexSize)
    }
  }

  var i
  if (dir) {
    var foundIndex = -1
    for (i = byteOffset; i < arrLength; i++) {
      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize
      } else {
        if (foundIndex !== -1) i -= i - foundIndex
        foundIndex = -1
      }
    }
  } else {
    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength
    for (i = byteOffset; i >= 0; i--) {
      var found = true
      for (var j = 0; j < valLength; j++) {
        if (read(arr, i + j) !== read(val, j)) {
          found = false
          break
        }
      }
      if (found) return i
    }
  }

  return -1
}

Buffer.prototype.includes = function includes (val, byteOffset, encoding) {
  return this.indexOf(val, byteOffset, encoding) !== -1
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
}

Buffer.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  var strLen = string.length
  if (strLen % 2 !== 0) throw new TypeError('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; ++i) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(parsed)) return i
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function latin1Write (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  } else if (isFinite(offset)) {
    offset = offset | 0
    if (isFinite(length)) {
      length = length | 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  } else {
    throw new Error(
      'Buffer.write(string, encoding, offset[, length]) is no longer supported'
    )
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('Attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'latin1':
      case 'binary':
        return latin1Write(this, string, offset, length)

      case 'base64':
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
      : (firstByte > 0xBF) ? 2
      : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) 
  }

  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function latin1Slice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; ++i) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = this.subarray(start, end)
    newBuf.__proto__ = Buffer.prototype
  } else {
    var sliceLen = end - start
    newBuf = new Buffer(sliceLen, undefined)
    for (var i = 0; i < sliceLen; ++i) {
      newBuf[i] = this[i + start]
    }
  }

  return newBuf
}

function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = (value & 0xff)
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
  if (offset < 0) throw new RangeError('Index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start
  var i

  if (this === target && start < targetStart && targetStart < end) {
    for (i = len - 1; i >= 0; --i) {
      target[i + targetStart] = this[i + start]
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    for (i = 0; i < len; ++i) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    Uint8Array.prototype.set.call(
      target,
      this.subarray(start, start + len),
      targetStart
    )
  }

  return len
}

Buffer.prototype.fill = function fill (val, start, end, encoding) {
  if (typeof val === 'string') {
    if (typeof start === 'string') {
      encoding = start
      start = 0
      end = this.length
    } else if (typeof end === 'string') {
      encoding = end
      end = this.length
    }
    if (val.length === 1) {
      var code = val.charCodeAt(0)
      if (code < 256) {
        val = code
      }
    }
    if (encoding !== undefined && typeof encoding !== 'string') {
      throw new TypeError('encoding must be a string')
    }
    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
      throw new TypeError('Unknown encoding: ' + encoding)
    }
  } else if (typeof val === 'number') {
    val = val & 255
  }

  if (start < 0 || this.length < start || this.length < end) {
    throw new RangeError('Out of range index')
  }

  if (end <= start) {
    return this
  }

  start = start >>> 0
  end = end === undefined ? this.length : end >>> 0

  if (!val) val = 0

  var i
  if (typeof val === 'number') {
    for (i = start; i < end; ++i) {
      this[i] = val
    }
  } else {
    var bytes = Buffer.isBuffer(val)
      ? val
      : utf8ToBytes(new Buffer(val, encoding).toString())
    var len = bytes.length
    for (i = 0; i < end - start; ++i) {
      this[i + start] = bytes[i % len]
    }
  }

  return this
}


var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g

function base64clean (str) {
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  if (str.length < 2) return ''
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; ++i) {
    codePoint = string.charCodeAt(i)

    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      if (!leadSurrogate) {
        if (codePoint > 0xDBFF) {
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        leadSurrogate = codePoint

        continue
      }

      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
    } else if (leadSurrogate) {
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; ++i) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

function isnan (val) {
  return val !== val 
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"18":18,"19":19,"20":20}],18:[function(require,module,exports){
'use strict'

exports.byteLength = byteLength
exports.toByteArray = toByteArray
exports.fromByteArray = fromByteArray

var lookup = []
var revLookup = []
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array

var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i]
  revLookup[code.charCodeAt(i)] = i
}

revLookup['-'.charCodeAt(0)] = 62
revLookup['_'.charCodeAt(0)] = 63

function placeHoldersCount (b64) {
  var len = b64.length
  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4')
  }

  return b64[len - 2] === '=' ? 2 : b64[len - 1] === '=' ? 1 : 0
}

function byteLength (b64) {
  return (b64.length * 3 / 4) - placeHoldersCount(b64)
}

function toByteArray (b64) {
  var i, l, tmp, placeHolders, arr
  var len = b64.length
  placeHolders = placeHoldersCount(b64)

  arr = new Arr((len * 3 / 4) - placeHolders)

  l = placeHolders > 0 ? len - 4 : len

  var L = 0

  for (i = 0; i < l; i += 4) {
    tmp = (revLookup[b64.charCodeAt(i)] << 18) | (revLookup[b64.charCodeAt(i + 1)] << 12) | (revLookup[b64.charCodeAt(i + 2)] << 6) | revLookup[b64.charCodeAt(i + 3)]
    arr[L++] = (tmp >> 16) & 0xFF
    arr[L++] = (tmp >> 8) & 0xFF
    arr[L++] = tmp & 0xFF
  }

  if (placeHolders === 2) {
    tmp = (revLookup[b64.charCodeAt(i)] << 2) | (revLookup[b64.charCodeAt(i + 1)] >> 4)
    arr[L++] = tmp & 0xFF
  } else if (placeHolders === 1) {
    tmp = (revLookup[b64.charCodeAt(i)] << 10) | (revLookup[b64.charCodeAt(i + 1)] << 4) | (revLookup[b64.charCodeAt(i + 2)] >> 2)
    arr[L++] = (tmp >> 8) & 0xFF
    arr[L++] = tmp & 0xFF
  }

  return arr
}

function tripletToBase64 (num) {
  return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F]
}

function encodeChunk (uint8, start, end) {
  var tmp
  var output = []
  for (var i = start; i < end; i += 3) {
    tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2])
    output.push(tripletToBase64(tmp))
  }
  return output.join('')
}

function fromByteArray (uint8) {
  var tmp
  var len = uint8.length
  var extraBytes = len % 3 
  var output = ''
  var parts = []
  var maxChunkLength = 16383 

  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)))
  }

  if (extraBytes === 1) {
    tmp = uint8[len - 1]
    output += lookup[tmp >> 2]
    output += lookup[(tmp << 4) & 0x3F]
    output += '=='
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + (uint8[len - 1])
    output += lookup[tmp >> 10]
    output += lookup[(tmp >> 4) & 0x3F]
    output += lookup[(tmp << 2) & 0x3F]
    output += '='
  }

  parts.push(output)

  return parts.join('')
}

},{}],19:[function(require,module,exports){
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}

},{}],20:[function(require,module,exports){
var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};

},{}],21:[function(require,module,exports){

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

EventEmitter.defaultMaxListeners = 10;

EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; 
      } else {
        var err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
        err.context = er;
        throw err;
      }
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      default:
        args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    args = Array.prototype.slice.call(arguments, 1);
    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    this._events[type].push(listener);
  else
    this._events[type] = [this._events[type], listener];

  if (isObject(this._events[type]) && !this._events[type].warned) {
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else if (listeners) {
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.prototype.listenerCount = function(type) {
  if (this._events) {
    var evlistener = this._events[type];

    if (isFunction(evlistener))
      return 1;
    else if (evlistener)
      return evlistener.length;
  }
  return 0;
};

EventEmitter.listenerCount = function(emitter, type) {
  return emitter.listenerCount(type);
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}],22:[function(require,module,exports){

module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}

},{}],23:[function(require,module,exports){
var process = module.exports = {};


var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        return setTimeout(fun, 0);
    }
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        return clearTimeout(marker);
    }
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; 
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],24:[function(require,module,exports){


'use strict';


var processNextTick = require(35);

var objectKeys = Object.keys || function (obj) {
  var keys = [];
  for (var key in obj) {
    keys.push(key);
  }return keys;
};

module.exports = Duplex;

var util = require(32);
util.inherits = require(33);

var Readable = require(26);
var Writable = require(28);

util.inherits(Duplex, Readable);

var keys = objectKeys(Writable.prototype);
for (var v = 0; v < keys.length; v++) {
  var method = keys[v];
  if (!Duplex.prototype[method]) Duplex.prototype[method] = Writable.prototype[method];
}

function Duplex(options) {
  if (!(this instanceof Duplex)) return new Duplex(options);

  Readable.call(this, options);
  Writable.call(this, options);

  if (options && options.readable === false) this.readable = false;

  if (options && options.writable === false) this.writable = false;

  this.allowHalfOpen = true;
  if (options && options.allowHalfOpen === false) this.allowHalfOpen = false;

  this.once('end', onend);
}

function onend() {
  if (this.allowHalfOpen || this._writableState.ended) return;

  processNextTick(onEndNT, this);
}

function onEndNT(self) {
  self.end();
}

Object.defineProperty(Duplex.prototype, 'destroyed', {
  get: function () {
    if (this._readableState === undefined || this._writableState === undefined) {
      return false;
    }
    return this._readableState.destroyed && this._writableState.destroyed;
  },
  set: function (value) {
    if (this._readableState === undefined || this._writableState === undefined) {
      return;
    }

    this._readableState.destroyed = value;
    this._writableState.destroyed = value;
  }
});

Duplex.prototype._destroy = function (err, cb) {
  this.push(null);
  this.end();

  processNextTick(cb, err);
};

function forEach(xs, f) {
  for (var i = 0, l = xs.length; i < l; i++) {
    f(xs[i], i);
  }
}
},{"26":26,"28":28,"32":32,"33":33,"35":35}],25:[function(require,module,exports){


'use strict';

module.exports = PassThrough;

var Transform = require(27);

var util = require(32);
util.inherits = require(33);

util.inherits(PassThrough, Transform);

function PassThrough(options) {
  if (!(this instanceof PassThrough)) return new PassThrough(options);

  Transform.call(this, options);
}

PassThrough.prototype._transform = function (chunk, encoding, cb) {
  cb(null, chunk);
};
},{"27":27,"32":32,"33":33}],26:[function(require,module,exports){
(function (process,global){

'use strict';


var processNextTick = require(35);

module.exports = Readable;

var isArray = require(34);

var Duplex;

Readable.ReadableState = ReadableState;

var EE = require(21).EventEmitter;

var EElistenerCount = function (emitter, type) {
  return emitter.listeners(type).length;
};

var Stream = require(31);

var Buffer = require(36).Buffer;
var OurUint8Array = global.Uint8Array || function () {};
function _uint8ArrayToBuffer(chunk) {
  return Buffer.from(chunk);
}
function _isUint8Array(obj) {
  return Buffer.isBuffer(obj) || obj instanceof OurUint8Array;
}

var util = require(32);
util.inherits = require(33);

var debugUtil = require(3);
var debug = void 0;
if (debugUtil && debugUtil.debuglog) {
  debug = debugUtil.debuglog('stream');
} else {
  debug = function () {};
}

var BufferList = require(29);
var destroyImpl = require(30);
var StringDecoder;

util.inherits(Readable, Stream);

var kProxyEvents = ['error', 'close', 'destroy', 'pause', 'resume'];

function prependListener(emitter, event, fn) {
  if (typeof emitter.prependListener === 'function') {
    return emitter.prependListener(event, fn);
  } else {
    if (!emitter._events || !emitter._events[event]) emitter.on(event, fn);else if (isArray(emitter._events[event])) emitter._events[event].unshift(fn);else emitter._events[event] = [fn, emitter._events[event]];
  }
}

function ReadableState(options, stream) {
  Duplex = Duplex || require(24);

  options = options || {};

  this.objectMode = !!options.objectMode;

  if (stream instanceof Duplex) this.objectMode = this.objectMode || !!options.readableObjectMode;

  var hwm = options.highWaterMark;
  var defaultHwm = this.objectMode ? 16 : 16 * 1024;
  this.highWaterMark = hwm || hwm === 0 ? hwm : defaultHwm;

  this.highWaterMark = Math.floor(this.highWaterMark);

  this.buffer = new BufferList();
  this.length = 0;
  this.pipes = null;
  this.pipesCount = 0;
  this.flowing = null;
  this.ended = false;
  this.endEmitted = false;
  this.reading = false;

  this.sync = true;

  this.needReadable = false;
  this.emittedReadable = false;
  this.readableListening = false;
  this.resumeScheduled = false;

  this.destroyed = false;

  this.defaultEncoding = options.defaultEncoding || 'utf8';

  this.awaitDrain = 0;

  this.readingMore = false;

  this.decoder = null;
  this.encoding = null;
  if (options.encoding) {
    if (!StringDecoder) StringDecoder = require(37).StringDecoder;
    this.decoder = new StringDecoder(options.encoding);
    this.encoding = options.encoding;
  }
}

function Readable(options) {
  Duplex = Duplex || require(24);

  if (!(this instanceof Readable)) return new Readable(options);

  this._readableState = new ReadableState(options, this);

  this.readable = true;

  if (options) {
    if (typeof options.read === 'function') this._read = options.read;

    if (typeof options.destroy === 'function') this._destroy = options.destroy;
  }

  Stream.call(this);
}

Object.defineProperty(Readable.prototype, 'destroyed', {
  get: function () {
    if (this._readableState === undefined) {
      return false;
    }
    return this._readableState.destroyed;
  },
  set: function (value) {
    if (!this._readableState) {
      return;
    }

    this._readableState.destroyed = value;
  }
});

Readable.prototype.destroy = destroyImpl.destroy;
Readable.prototype._undestroy = destroyImpl.undestroy;
Readable.prototype._destroy = function (err, cb) {
  this.push(null);
  cb(err);
};

Readable.prototype.push = function (chunk, encoding) {
  var state = this._readableState;
  var skipChunkCheck;

  if (!state.objectMode) {
    if (typeof chunk === 'string') {
      encoding = encoding || state.defaultEncoding;
      if (encoding !== state.encoding) {
        chunk = Buffer.from(chunk, encoding);
        encoding = '';
      }
      skipChunkCheck = true;
    }
  } else {
    skipChunkCheck = true;
  }

  return readableAddChunk(this, chunk, encoding, false, skipChunkCheck);
};

Readable.prototype.unshift = function (chunk) {
  return readableAddChunk(this, chunk, null, true, false);
};

function readableAddChunk(stream, chunk, encoding, addToFront, skipChunkCheck) {
  var state = stream._readableState;
  if (chunk === null) {
    state.reading = false;
    onEofChunk(stream, state);
  } else {
    var er;
    if (!skipChunkCheck) er = chunkInvalid(state, chunk);
    if (er) {
      stream.emit('error', er);
    } else if (state.objectMode || chunk && chunk.length > 0) {
      if (typeof chunk !== 'string' && !state.objectMode && Object.getPrototypeOf(chunk) !== Buffer.prototype) {
        chunk = _uint8ArrayToBuffer(chunk);
      }

      if (addToFront) {
        if (state.endEmitted) stream.emit('error', new Error('stream.unshift() after end event'));else addChunk(stream, state, chunk, true);
      } else if (state.ended) {
        stream.emit('error', new Error('stream.push() after EOF'));
      } else {
        state.reading = false;
        if (state.decoder && !encoding) {
          chunk = state.decoder.write(chunk);
          if (state.objectMode || chunk.length !== 0) addChunk(stream, state, chunk, false);else maybeReadMore(stream, state);
        } else {
          addChunk(stream, state, chunk, false);
        }
      }
    } else if (!addToFront) {
      state.reading = false;
    }
  }

  return needMoreData(state);
}

function addChunk(stream, state, chunk, addToFront) {
  if (state.flowing && state.length === 0 && !state.sync) {
    stream.emit('data', chunk);
    stream.read(0);
  } else {
    state.length += state.objectMode ? 1 : chunk.length;
    if (addToFront) state.buffer.unshift(chunk);else state.buffer.push(chunk);

    if (state.needReadable) emitReadable(stream);
  }
  maybeReadMore(stream, state);
}

function chunkInvalid(state, chunk) {
  var er;
  if (!_isUint8Array(chunk) && typeof chunk !== 'string' && chunk !== undefined && !state.objectMode) {
    er = new TypeError('Invalid non-string/buffer chunk');
  }
  return er;
}

function needMoreData(state) {
  return !state.ended && (state.needReadable || state.length < state.highWaterMark || state.length === 0);
}

Readable.prototype.isPaused = function () {
  return this._readableState.flowing === false;
};

Readable.prototype.setEncoding = function (enc) {
  if (!StringDecoder) StringDecoder = require(37).StringDecoder;
  this._readableState.decoder = new StringDecoder(enc);
  this._readableState.encoding = enc;
  return this;
};

var MAX_HWM = 0x800000;
function computeNewHighWaterMark(n) {
  if (n >= MAX_HWM) {
    n = MAX_HWM;
  } else {
    n--;
    n |= n >>> 1;
    n |= n >>> 2;
    n |= n >>> 4;
    n |= n >>> 8;
    n |= n >>> 16;
    n++;
  }
  return n;
}

function howMuchToRead(n, state) {
  if (n <= 0 || state.length === 0 && state.ended) return 0;
  if (state.objectMode) return 1;
  if (n !== n) {
    if (state.flowing && state.length) return state.buffer.head.data.length;else return state.length;
  }
  if (n > state.highWaterMark) state.highWaterMark = computeNewHighWaterMark(n);
  if (n <= state.length) return n;
  if (!state.ended) {
    state.needReadable = true;
    return 0;
  }
  return state.length;
}

Readable.prototype.read = function (n) {
  debug('read', n);
  n = parseInt(n, 10);
  var state = this._readableState;
  var nOrig = n;

  if (n !== 0) state.emittedReadable = false;

  if (n === 0 && state.needReadable && (state.length >= state.highWaterMark || state.ended)) {
    debug('read: emitReadable', state.length, state.ended);
    if (state.length === 0 && state.ended) endReadable(this);else emitReadable(this);
    return null;
  }

  n = howMuchToRead(n, state);

  if (n === 0 && state.ended) {
    if (state.length === 0) endReadable(this);
    return null;
  }


  var doRead = state.needReadable;
  debug('need readable', doRead);

  if (state.length === 0 || state.length - n < state.highWaterMark) {
    doRead = true;
    debug('length less than watermark', doRead);
  }

  if (state.ended || state.reading) {
    doRead = false;
    debug('reading or ended', doRead);
  } else if (doRead) {
    debug('do read');
    state.reading = true;
    state.sync = true;
    if (state.length === 0) state.needReadable = true;
    this._read(state.highWaterMark);
    state.sync = false;
    if (!state.reading) n = howMuchToRead(nOrig, state);
  }

  var ret;
  if (n > 0) ret = fromList(n, state);else ret = null;

  if (ret === null) {
    state.needReadable = true;
    n = 0;
  } else {
    state.length -= n;
  }

  if (state.length === 0) {
    if (!state.ended) state.needReadable = true;

    if (nOrig !== n && state.ended) endReadable(this);
  }

  if (ret !== null) this.emit('data', ret);

  return ret;
};

function onEofChunk(stream, state) {
  if (state.ended) return;
  if (state.decoder) {
    var chunk = state.decoder.end();
    if (chunk && chunk.length) {
      state.buffer.push(chunk);
      state.length += state.objectMode ? 1 : chunk.length;
    }
  }
  state.ended = true;

  emitReadable(stream);
}

function emitReadable(stream) {
  var state = stream._readableState;
  state.needReadable = false;
  if (!state.emittedReadable) {
    debug('emitReadable', state.flowing);
    state.emittedReadable = true;
    if (state.sync) processNextTick(emitReadable_, stream);else emitReadable_(stream);
  }
}

function emitReadable_(stream) {
  debug('emit readable');
  stream.emit('readable');
  flow(stream);
}

function maybeReadMore(stream, state) {
  if (!state.readingMore) {
    state.readingMore = true;
    processNextTick(maybeReadMore_, stream, state);
  }
}

function maybeReadMore_(stream, state) {
  var len = state.length;
  while (!state.reading && !state.flowing && !state.ended && state.length < state.highWaterMark) {
    debug('maybeReadMore read 0');
    stream.read(0);
    if (len === state.length)
      break;else len = state.length;
  }
  state.readingMore = false;
}

Readable.prototype._read = function (n) {
  this.emit('error', new Error('_read() is not implemented'));
};

Readable.prototype.pipe = function (dest, pipeOpts) {
  var src = this;
  var state = this._readableState;

  switch (state.pipesCount) {
    case 0:
      state.pipes = dest;
      break;
    case 1:
      state.pipes = [state.pipes, dest];
      break;
    default:
      state.pipes.push(dest);
      break;
  }
  state.pipesCount += 1;
  debug('pipe count=%d opts=%j', state.pipesCount, pipeOpts);

  var doEnd = (!pipeOpts || pipeOpts.end !== false) && dest !== process.stdout && dest !== process.stderr;

  var endFn = doEnd ? onend : unpipe;
  if (state.endEmitted) processNextTick(endFn);else src.once('end', endFn);

  dest.on('unpipe', onunpipe);
  function onunpipe(readable, unpipeInfo) {
    debug('onunpipe');
    if (readable === src) {
      if (unpipeInfo && unpipeInfo.hasUnpiped === false) {
        unpipeInfo.hasUnpiped = true;
        cleanup();
      }
    }
  }

  function onend() {
    debug('onend');
    dest.end();
  }

  var ondrain = pipeOnDrain(src);
  dest.on('drain', ondrain);

  var cleanedUp = false;
  function cleanup() {
    debug('cleanup');
    dest.removeListener('close', onclose);
    dest.removeListener('finish', onfinish);
    dest.removeListener('drain', ondrain);
    dest.removeListener('error', onerror);
    dest.removeListener('unpipe', onunpipe);
    src.removeListener('end', onend);
    src.removeListener('end', unpipe);
    src.removeListener('data', ondata);

    cleanedUp = true;

    if (state.awaitDrain && (!dest._writableState || dest._writableState.needDrain)) ondrain();
  }

  var increasedAwaitDrain = false;
  src.on('data', ondata);
  function ondata(chunk) {
    debug('ondata');
    increasedAwaitDrain = false;
    var ret = dest.write(chunk);
    if (false === ret && !increasedAwaitDrain) {
      if ((state.pipesCount === 1 && state.pipes === dest || state.pipesCount > 1 && indexOf(state.pipes, dest) !== -1) && !cleanedUp) {
        debug('false write response, pause', src._readableState.awaitDrain);
        src._readableState.awaitDrain++;
        increasedAwaitDrain = true;
      }
      src.pause();
    }
  }

  function onerror(er) {
    debug('onerror', er);
    unpipe();
    dest.removeListener('error', onerror);
    if (EElistenerCount(dest, 'error') === 0) dest.emit('error', er);
  }

  prependListener(dest, 'error', onerror);

  function onclose() {
    dest.removeListener('finish', onfinish);
    unpipe();
  }
  dest.once('close', onclose);
  function onfinish() {
    debug('onfinish');
    dest.removeListener('close', onclose);
    unpipe();
  }
  dest.once('finish', onfinish);

  function unpipe() {
    debug('unpipe');
    src.unpipe(dest);
  }

  dest.emit('pipe', src);

  if (!state.flowing) {
    debug('pipe resume');
    src.resume();
  }

  return dest;
};

function pipeOnDrain(src) {
  return function () {
    var state = src._readableState;
    debug('pipeOnDrain', state.awaitDrain);
    if (state.awaitDrain) state.awaitDrain--;
    if (state.awaitDrain === 0 && EElistenerCount(src, 'data')) {
      state.flowing = true;
      flow(src);
    }
  };
}

Readable.prototype.unpipe = function (dest) {
  var state = this._readableState;
  var unpipeInfo = { hasUnpiped: false };

  if (state.pipesCount === 0) return this;

  if (state.pipesCount === 1) {
    if (dest && dest !== state.pipes) return this;

    if (!dest) dest = state.pipes;

    state.pipes = null;
    state.pipesCount = 0;
    state.flowing = false;
    if (dest) dest.emit('unpipe', this, unpipeInfo);
    return this;
  }


  if (!dest) {
    var dests = state.pipes;
    var len = state.pipesCount;
    state.pipes = null;
    state.pipesCount = 0;
    state.flowing = false;

    for (var i = 0; i < len; i++) {
      dests[i].emit('unpipe', this, unpipeInfo);
    }return this;
  }

  var index = indexOf(state.pipes, dest);
  if (index === -1) return this;

  state.pipes.splice(index, 1);
  state.pipesCount -= 1;
  if (state.pipesCount === 1) state.pipes = state.pipes[0];

  dest.emit('unpipe', this, unpipeInfo);

  return this;
};

Readable.prototype.on = function (ev, fn) {
  var res = Stream.prototype.on.call(this, ev, fn);

  if (ev === 'data') {
    if (this._readableState.flowing !== false) this.resume();
  } else if (ev === 'readable') {
    var state = this._readableState;
    if (!state.endEmitted && !state.readableListening) {
      state.readableListening = state.needReadable = true;
      state.emittedReadable = false;
      if (!state.reading) {
        processNextTick(nReadingNextTick, this);
      } else if (state.length) {
        emitReadable(this);
      }
    }
  }

  return res;
};
Readable.prototype.addListener = Readable.prototype.on;

function nReadingNextTick(self) {
  debug('readable nexttick read 0');
  self.read(0);
}

Readable.prototype.resume = function () {
  var state = this._readableState;
  if (!state.flowing) {
    debug('resume');
    state.flowing = true;
    resume(this, state);
  }
  return this;
};

function resume(stream, state) {
  if (!state.resumeScheduled) {
    state.resumeScheduled = true;
    processNextTick(resume_, stream, state);
  }
}

function resume_(stream, state) {
  if (!state.reading) {
    debug('resume read 0');
    stream.read(0);
  }

  state.resumeScheduled = false;
  state.awaitDrain = 0;
  stream.emit('resume');
  flow(stream);
  if (state.flowing && !state.reading) stream.read(0);
}

Readable.prototype.pause = function () {
  debug('call pause flowing=%j', this._readableState.flowing);
  if (false !== this._readableState.flowing) {
    debug('pause');
    this._readableState.flowing = false;
    this.emit('pause');
  }
  return this;
};

function flow(stream) {
  var state = stream._readableState;
  debug('flow', state.flowing);
  while (state.flowing && stream.read() !== null) {}
}

Readable.prototype.wrap = function (stream) {
  var state = this._readableState;
  var paused = false;

  var self = this;
  stream.on('end', function () {
    debug('wrapped end');
    if (state.decoder && !state.ended) {
      var chunk = state.decoder.end();
      if (chunk && chunk.length) self.push(chunk);
    }

    self.push(null);
  });

  stream.on('data', function (chunk) {
    debug('wrapped data');
    if (state.decoder) chunk = state.decoder.write(chunk);

    if (state.objectMode && (chunk === null || chunk === undefined)) return;else if (!state.objectMode && (!chunk || !chunk.length)) return;

    var ret = self.push(chunk);
    if (!ret) {
      paused = true;
      stream.pause();
    }
  });

  for (var i in stream) {
    if (this[i] === undefined && typeof stream[i] === 'function') {
      this[i] = function (method) {
        return function () {
          return stream[method].apply(stream, arguments);
        };
      }(i);
    }
  }

  for (var n = 0; n < kProxyEvents.length; n++) {
    stream.on(kProxyEvents[n], self.emit.bind(self, kProxyEvents[n]));
  }

  self._read = function (n) {
    debug('wrapped _read', n);
    if (paused) {
      paused = false;
      stream.resume();
    }
  };

  return self;
};

Readable._fromList = fromList;

function fromList(n, state) {
  if (state.length === 0) return null;

  var ret;
  if (state.objectMode) ret = state.buffer.shift();else if (!n || n >= state.length) {
    if (state.decoder) ret = state.buffer.join('');else if (state.buffer.length === 1) ret = state.buffer.head.data;else ret = state.buffer.concat(state.length);
    state.buffer.clear();
  } else {
    ret = fromListPartial(n, state.buffer, state.decoder);
  }

  return ret;
}

function fromListPartial(n, list, hasStrings) {
  var ret;
  if (n < list.head.data.length) {
    ret = list.head.data.slice(0, n);
    list.head.data = list.head.data.slice(n);
  } else if (n === list.head.data.length) {
    ret = list.shift();
  } else {
    ret = hasStrings ? copyFromBufferString(n, list) : copyFromBuffer(n, list);
  }
  return ret;
}

function copyFromBufferString(n, list) {
  var p = list.head;
  var c = 1;
  var ret = p.data;
  n -= ret.length;
  while (p = p.next) {
    var str = p.data;
    var nb = n > str.length ? str.length : n;
    if (nb === str.length) ret += str;else ret += str.slice(0, n);
    n -= nb;
    if (n === 0) {
      if (nb === str.length) {
        ++c;
        if (p.next) list.head = p.next;else list.head = list.tail = null;
      } else {
        list.head = p;
        p.data = str.slice(nb);
      }
      break;
    }
    ++c;
  }
  list.length -= c;
  return ret;
}

function copyFromBuffer(n, list) {
  var ret = Buffer.allocUnsafe(n);
  var p = list.head;
  var c = 1;
  p.data.copy(ret);
  n -= p.data.length;
  while (p = p.next) {
    var buf = p.data;
    var nb = n > buf.length ? buf.length : n;
    buf.copy(ret, ret.length - n, 0, nb);
    n -= nb;
    if (n === 0) {
      if (nb === buf.length) {
        ++c;
        if (p.next) list.head = p.next;else list.head = list.tail = null;
      } else {
        list.head = p;
        p.data = buf.slice(nb);
      }
      break;
    }
    ++c;
  }
  list.length -= c;
  return ret;
}

function endReadable(stream) {
  var state = stream._readableState;

  if (state.length > 0) throw new Error('"endReadable()" called on non-empty stream');

  if (!state.endEmitted) {
    state.ended = true;
    processNextTick(endReadableNT, state, stream);
  }
}

function endReadableNT(state, stream) {
  if (!state.endEmitted && state.length === 0) {
    state.endEmitted = true;
    stream.readable = false;
    stream.emit('end');
  }
}

function forEach(xs, f) {
  for (var i = 0, l = xs.length; i < l; i++) {
    f(xs[i], i);
  }
}

function indexOf(xs, x) {
  for (var i = 0, l = xs.length; i < l; i++) {
    if (xs[i] === x) return i;
  }
  return -1;
}
}).call(this,require(23),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"21":21,"23":23,"24":24,"29":29,"3":3,"30":30,"31":31,"32":32,"33":33,"34":34,"35":35,"36":36,"37":37}],27:[function(require,module,exports){


'use strict';

module.exports = Transform;

var Duplex = require(24);

var util = require(32);
util.inherits = require(33);

util.inherits(Transform, Duplex);

function TransformState(stream) {
  this.afterTransform = function (er, data) {
    return afterTransform(stream, er, data);
  };

  this.needTransform = false;
  this.transforming = false;
  this.writecb = null;
  this.writechunk = null;
  this.writeencoding = null;
}

function afterTransform(stream, er, data) {
  var ts = stream._transformState;
  ts.transforming = false;

  var cb = ts.writecb;

  if (!cb) {
    return stream.emit('error', new Error('write callback called multiple times'));
  }

  ts.writechunk = null;
  ts.writecb = null;

  if (data !== null && data !== undefined) stream.push(data);

  cb(er);

  var rs = stream._readableState;
  rs.reading = false;
  if (rs.needReadable || rs.length < rs.highWaterMark) {
    stream._read(rs.highWaterMark);
  }
}

function Transform(options) {
  if (!(this instanceof Transform)) return new Transform(options);

  Duplex.call(this, options);

  this._transformState = new TransformState(this);

  var stream = this;

  this._readableState.needReadable = true;

  this._readableState.sync = false;

  if (options) {
    if (typeof options.transform === 'function') this._transform = options.transform;

    if (typeof options.flush === 'function') this._flush = options.flush;
  }

  this.once('prefinish', function () {
    if (typeof this._flush === 'function') this._flush(function (er, data) {
      done(stream, er, data);
    });else done(stream);
  });
}

Transform.prototype.push = function (chunk, encoding) {
  this._transformState.needTransform = false;
  return Duplex.prototype.push.call(this, chunk, encoding);
};

Transform.prototype._transform = function (chunk, encoding, cb) {
  throw new Error('_transform() is not implemented');
};

Transform.prototype._write = function (chunk, encoding, cb) {
  var ts = this._transformState;
  ts.writecb = cb;
  ts.writechunk = chunk;
  ts.writeencoding = encoding;
  if (!ts.transforming) {
    var rs = this._readableState;
    if (ts.needTransform || rs.needReadable || rs.length < rs.highWaterMark) this._read(rs.highWaterMark);
  }
};

Transform.prototype._read = function (n) {
  var ts = this._transformState;

  if (ts.writechunk !== null && ts.writecb && !ts.transforming) {
    ts.transforming = true;
    this._transform(ts.writechunk, ts.writeencoding, ts.afterTransform);
  } else {
    ts.needTransform = true;
  }
};

Transform.prototype._destroy = function (err, cb) {
  var _this = this;

  Duplex.prototype._destroy.call(this, err, function (err2) {
    cb(err2);
    _this.emit('close');
  });
};

function done(stream, er, data) {
  if (er) return stream.emit('error', er);

  if (data !== null && data !== undefined) stream.push(data);

  var ws = stream._writableState;
  var ts = stream._transformState;

  if (ws.length) throw new Error('Calling transform done when ws.length != 0');

  if (ts.transforming) throw new Error('Calling transform done when still transforming');

  return stream.push(null);
}
},{"24":24,"32":32,"33":33}],28:[function(require,module,exports){
(function (process,global){


'use strict';


var processNextTick = require(35);

module.exports = Writable;

function WriteReq(chunk, encoding, cb) {
  this.chunk = chunk;
  this.encoding = encoding;
  this.callback = cb;
  this.next = null;
}

function CorkedRequest(state) {
  var _this = this;

  this.next = null;
  this.entry = null;
  this.finish = function () {
    onCorkedFinish(_this, state);
  };
}

var asyncWrite = !process.browser && ['v0.10', 'v0.9.'].indexOf(process.version.slice(0, 5)) > -1 ? setImmediate : processNextTick;

var Duplex;

Writable.WritableState = WritableState;

var util = require(32);
util.inherits = require(33);

var internalUtil = {
  deprecate: require(38)
};

var Stream = require(31);

var Buffer = require(36).Buffer;
var OurUint8Array = global.Uint8Array || function () {};
function _uint8ArrayToBuffer(chunk) {
  return Buffer.from(chunk);
}
function _isUint8Array(obj) {
  return Buffer.isBuffer(obj) || obj instanceof OurUint8Array;
}

var destroyImpl = require(30);

util.inherits(Writable, Stream);

function nop() {}

function WritableState(options, stream) {
  Duplex = Duplex || require(24);

  options = options || {};

  this.objectMode = !!options.objectMode;

  if (stream instanceof Duplex) this.objectMode = this.objectMode || !!options.writableObjectMode;

  var hwm = options.highWaterMark;
  var defaultHwm = this.objectMode ? 16 : 16 * 1024;
  this.highWaterMark = hwm || hwm === 0 ? hwm : defaultHwm;

  this.highWaterMark = Math.floor(this.highWaterMark);

  this.finalCalled = false;

  this.needDrain = false;
  this.ending = false;
  this.ended = false;
  this.finished = false;

  this.destroyed = false;

  var noDecode = options.decodeStrings === false;
  this.decodeStrings = !noDecode;

  this.defaultEncoding = options.defaultEncoding || 'utf8';

  this.length = 0;

  this.writing = false;

  this.corked = 0;

  this.sync = true;

  this.bufferProcessing = false;

  this.onwrite = function (er) {
    onwrite(stream, er);
  };

  this.writecb = null;

  this.writelen = 0;

  this.bufferedRequest = null;
  this.lastBufferedRequest = null;

  this.pendingcb = 0;

  this.prefinished = false;

  this.errorEmitted = false;

  this.bufferedRequestCount = 0;

  this.corkedRequestsFree = new CorkedRequest(this);
}

WritableState.prototype.getBuffer = function getBuffer() {
  var current = this.bufferedRequest;
  var out = [];
  while (current) {
    out.push(current);
    current = current.next;
  }
  return out;
};

(function () {
  try {
    Object.defineProperty(WritableState.prototype, 'buffer', {
      get: internalUtil.deprecate(function () {
        return this.getBuffer();
      }, '_writableState.buffer is deprecated. Use _writableState.getBuffer ' + 'instead.', 'DEP0003')
    });
  } catch (_) {}
})();

var realHasInstance;
if (typeof Symbol === 'function' && Symbol.hasInstance && typeof Function.prototype[Symbol.hasInstance] === 'function') {
  realHasInstance = Function.prototype[Symbol.hasInstance];
  Object.defineProperty(Writable, Symbol.hasInstance, {
    value: function (object) {
      if (realHasInstance.call(this, object)) return true;

      return object && object._writableState instanceof WritableState;
    }
  });
} else {
  realHasInstance = function (object) {
    return object instanceof this;
  };
}

function Writable(options) {
  Duplex = Duplex || require(24);


  if (!realHasInstance.call(Writable, this) && !(this instanceof Duplex)) {
    return new Writable(options);
  }

  this._writableState = new WritableState(options, this);

  this.writable = true;

  if (options) {
    if (typeof options.write === 'function') this._write = options.write;

    if (typeof options.writev === 'function') this._writev = options.writev;

    if (typeof options.destroy === 'function') this._destroy = options.destroy;

    if (typeof options.final === 'function') this._final = options.final;
  }

  Stream.call(this);
}

Writable.prototype.pipe = function () {
  this.emit('error', new Error('Cannot pipe, not readable'));
};

function writeAfterEnd(stream, cb) {
  var er = new Error('write after end');
  stream.emit('error', er);
  processNextTick(cb, er);
}

function validChunk(stream, state, chunk, cb) {
  var valid = true;
  var er = false;

  if (chunk === null) {
    er = new TypeError('May not write null values to stream');
  } else if (typeof chunk !== 'string' && chunk !== undefined && !state.objectMode) {
    er = new TypeError('Invalid non-string/buffer chunk');
  }
  if (er) {
    stream.emit('error', er);
    processNextTick(cb, er);
    valid = false;
  }
  return valid;
}

Writable.prototype.write = function (chunk, encoding, cb) {
  var state = this._writableState;
  var ret = false;
  var isBuf = _isUint8Array(chunk) && !state.objectMode;

  if (isBuf && !Buffer.isBuffer(chunk)) {
    chunk = _uint8ArrayToBuffer(chunk);
  }

  if (typeof encoding === 'function') {
    cb = encoding;
    encoding = null;
  }

  if (isBuf) encoding = 'buffer';else if (!encoding) encoding = state.defaultEncoding;

  if (typeof cb !== 'function') cb = nop;

  if (state.ended) writeAfterEnd(this, cb);else if (isBuf || validChunk(this, state, chunk, cb)) {
    state.pendingcb++;
    ret = writeOrBuffer(this, state, isBuf, chunk, encoding, cb);
  }

  return ret;
};

Writable.prototype.cork = function () {
  var state = this._writableState;

  state.corked++;
};

Writable.prototype.uncork = function () {
  var state = this._writableState;

  if (state.corked) {
    state.corked--;

    if (!state.writing && !state.corked && !state.finished && !state.bufferProcessing && state.bufferedRequest) clearBuffer(this, state);
  }
};

Writable.prototype.setDefaultEncoding = function setDefaultEncoding(encoding) {
  if (typeof encoding === 'string') encoding = encoding.toLowerCase();
  if (!(['hex', 'utf8', 'utf-8', 'ascii', 'binary', 'base64', 'ucs2', 'ucs-2', 'utf16le', 'utf-16le', 'raw'].indexOf((encoding + '').toLowerCase()) > -1)) throw new TypeError('Unknown encoding: ' + encoding);
  this._writableState.defaultEncoding = encoding;
  return this;
};

function decodeChunk(state, chunk, encoding) {
  if (!state.objectMode && state.decodeStrings !== false && typeof chunk === 'string') {
    chunk = Buffer.from(chunk, encoding);
  }
  return chunk;
}

function writeOrBuffer(stream, state, isBuf, chunk, encoding, cb) {
  if (!isBuf) {
    var newChunk = decodeChunk(state, chunk, encoding);
    if (chunk !== newChunk) {
      isBuf = true;
      encoding = 'buffer';
      chunk = newChunk;
    }
  }
  var len = state.objectMode ? 1 : chunk.length;

  state.length += len;

  var ret = state.length < state.highWaterMark;
  if (!ret) state.needDrain = true;

  if (state.writing || state.corked) {
    var last = state.lastBufferedRequest;
    state.lastBufferedRequest = {
      chunk: chunk,
      encoding: encoding,
      isBuf: isBuf,
      callback: cb,
      next: null
    };
    if (last) {
      last.next = state.lastBufferedRequest;
    } else {
      state.bufferedRequest = state.lastBufferedRequest;
    }
    state.bufferedRequestCount += 1;
  } else {
    doWrite(stream, state, false, len, chunk, encoding, cb);
  }

  return ret;
}

function doWrite(stream, state, writev, len, chunk, encoding, cb) {
  state.writelen = len;
  state.writecb = cb;
  state.writing = true;
  state.sync = true;
  if (writev) stream._writev(chunk, state.onwrite);else stream._write(chunk, encoding, state.onwrite);
  state.sync = false;
}

function onwriteError(stream, state, sync, er, cb) {
  --state.pendingcb;

  if (sync) {
    processNextTick(cb, er);
    processNextTick(finishMaybe, stream, state);
    stream._writableState.errorEmitted = true;
    stream.emit('error', er);
  } else {
    cb(er);
    stream._writableState.errorEmitted = true;
    stream.emit('error', er);
    finishMaybe(stream, state);
  }
}

function onwriteStateUpdate(state) {
  state.writing = false;
  state.writecb = null;
  state.length -= state.writelen;
  state.writelen = 0;
}

function onwrite(stream, er) {
  var state = stream._writableState;
  var sync = state.sync;
  var cb = state.writecb;

  onwriteStateUpdate(state);

  if (er) onwriteError(stream, state, sync, er, cb);else {
    var finished = needFinish(state);

    if (!finished && !state.corked && !state.bufferProcessing && state.bufferedRequest) {
      clearBuffer(stream, state);
    }

    if (sync) {
      asyncWrite(afterWrite, stream, state, finished, cb);
    } else {
      afterWrite(stream, state, finished, cb);
    }
  }
}

function afterWrite(stream, state, finished, cb) {
  if (!finished) onwriteDrain(stream, state);
  state.pendingcb--;
  cb();
  finishMaybe(stream, state);
}

function onwriteDrain(stream, state) {
  if (state.length === 0 && state.needDrain) {
    state.needDrain = false;
    stream.emit('drain');
  }
}

function clearBuffer(stream, state) {
  state.bufferProcessing = true;
  var entry = state.bufferedRequest;

  if (stream._writev && entry && entry.next) {
    var l = state.bufferedRequestCount;
    var buffer = new Array(l);
    var holder = state.corkedRequestsFree;
    holder.entry = entry;

    var count = 0;
    var allBuffers = true;
    while (entry) {
      buffer[count] = entry;
      if (!entry.isBuf) allBuffers = false;
      entry = entry.next;
      count += 1;
    }
    buffer.allBuffers = allBuffers;

    doWrite(stream, state, true, state.length, buffer, '', holder.finish);

    state.pendingcb++;
    state.lastBufferedRequest = null;
    if (holder.next) {
      state.corkedRequestsFree = holder.next;
      holder.next = null;
    } else {
      state.corkedRequestsFree = new CorkedRequest(state);
    }
  } else {
    while (entry) {
      var chunk = entry.chunk;
      var encoding = entry.encoding;
      var cb = entry.callback;
      var len = state.objectMode ? 1 : chunk.length;

      doWrite(stream, state, false, len, chunk, encoding, cb);
      entry = entry.next;
      if (state.writing) {
        break;
      }
    }

    if (entry === null) state.lastBufferedRequest = null;
  }

  state.bufferedRequestCount = 0;
  state.bufferedRequest = entry;
  state.bufferProcessing = false;
}

Writable.prototype._write = function (chunk, encoding, cb) {
  cb(new Error('_write() is not implemented'));
};

Writable.prototype._writev = null;

Writable.prototype.end = function (chunk, encoding, cb) {
  var state = this._writableState;

  if (typeof chunk === 'function') {
    cb = chunk;
    chunk = null;
    encoding = null;
  } else if (typeof encoding === 'function') {
    cb = encoding;
    encoding = null;
  }

  if (chunk !== null && chunk !== undefined) this.write(chunk, encoding);

  if (state.corked) {
    state.corked = 1;
    this.uncork();
  }

  if (!state.ending && !state.finished) endWritable(this, state, cb);
};

function needFinish(state) {
  return state.ending && state.length === 0 && state.bufferedRequest === null && !state.finished && !state.writing;
}
function callFinal(stream, state) {
  stream._final(function (err) {
    state.pendingcb--;
    if (err) {
      stream.emit('error', err);
    }
    state.prefinished = true;
    stream.emit('prefinish');
    finishMaybe(stream, state);
  });
}
function prefinish(stream, state) {
  if (!state.prefinished && !state.finalCalled) {
    if (typeof stream._final === 'function') {
      state.pendingcb++;
      state.finalCalled = true;
      processNextTick(callFinal, stream, state);
    } else {
      state.prefinished = true;
      stream.emit('prefinish');
    }
  }
}

function finishMaybe(stream, state) {
  var need = needFinish(state);
  if (need) {
    prefinish(stream, state);
    if (state.pendingcb === 0) {
      state.finished = true;
      stream.emit('finish');
    }
  }
  return need;
}

function endWritable(stream, state, cb) {
  state.ending = true;
  finishMaybe(stream, state);
  if (cb) {
    if (state.finished) processNextTick(cb);else stream.once('finish', cb);
  }
  state.ended = true;
  stream.writable = false;
}

function onCorkedFinish(corkReq, state, err) {
  var entry = corkReq.entry;
  corkReq.entry = null;
  while (entry) {
    var cb = entry.callback;
    state.pendingcb--;
    cb(err);
    entry = entry.next;
  }
  if (state.corkedRequestsFree) {
    state.corkedRequestsFree.next = corkReq;
  } else {
    state.corkedRequestsFree = corkReq;
  }
}

Object.defineProperty(Writable.prototype, 'destroyed', {
  get: function () {
    if (this._writableState === undefined) {
      return false;
    }
    return this._writableState.destroyed;
  },
  set: function (value) {
    if (!this._writableState) {
      return;
    }

    this._writableState.destroyed = value;
  }
});

Writable.prototype.destroy = destroyImpl.destroy;
Writable.prototype._undestroy = destroyImpl.undestroy;
Writable.prototype._destroy = function (err, cb) {
  this.end();
  cb(err);
};
}).call(this,require(23),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"23":23,"24":24,"30":30,"31":31,"32":32,"33":33,"35":35,"36":36,"38":38}],29:[function(require,module,exports){
'use strict';


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Buffer = require(36).Buffer;

function copyBuffer(src, target, offset) {
  src.copy(target, offset);
}

module.exports = function () {
  function BufferList() {
    _classCallCheck(this, BufferList);

    this.head = null;
    this.tail = null;
    this.length = 0;
  }

  BufferList.prototype.push = function push(v) {
    var entry = { data: v, next: null };
    if (this.length > 0) this.tail.next = entry;else this.head = entry;
    this.tail = entry;
    ++this.length;
  };

  BufferList.prototype.unshift = function unshift(v) {
    var entry = { data: v, next: this.head };
    if (this.length === 0) this.tail = entry;
    this.head = entry;
    ++this.length;
  };

  BufferList.prototype.shift = function shift() {
    if (this.length === 0) return;
    var ret = this.head.data;
    if (this.length === 1) this.head = this.tail = null;else this.head = this.head.next;
    --this.length;
    return ret;
  };

  BufferList.prototype.clear = function clear() {
    this.head = this.tail = null;
    this.length = 0;
  };

  BufferList.prototype.join = function join(s) {
    if (this.length === 0) return '';
    var p = this.head;
    var ret = '' + p.data;
    while (p = p.next) {
      ret += s + p.data;
    }return ret;
  };

  BufferList.prototype.concat = function concat(n) {
    if (this.length === 0) return Buffer.alloc(0);
    if (this.length === 1) return this.head.data;
    var ret = Buffer.allocUnsafe(n >>> 0);
    var p = this.head;
    var i = 0;
    while (p) {
      copyBuffer(p.data, ret, i);
      i += p.data.length;
      p = p.next;
    }
    return ret;
  };

  return BufferList;
}();
},{"36":36}],30:[function(require,module,exports){
'use strict';


var processNextTick = require(35);

function destroy(err, cb) {
  var _this = this;

  var readableDestroyed = this._readableState && this._readableState.destroyed;
  var writableDestroyed = this._writableState && this._writableState.destroyed;

  if (readableDestroyed || writableDestroyed) {
    if (cb) {
      cb(err);
    } else if (err && (!this._writableState || !this._writableState.errorEmitted)) {
      processNextTick(emitErrorNT, this, err);
    }
    return;
  }


  if (this._readableState) {
    this._readableState.destroyed = true;
  }

  if (this._writableState) {
    this._writableState.destroyed = true;
  }

  this._destroy(err || null, function (err) {
    if (!cb && err) {
      processNextTick(emitErrorNT, _this, err);
      if (_this._writableState) {
        _this._writableState.errorEmitted = true;
      }
    } else if (cb) {
      cb(err);
    }
  });
}

function undestroy() {
  if (this._readableState) {
    this._readableState.destroyed = false;
    this._readableState.reading = false;
    this._readableState.ended = false;
    this._readableState.endEmitted = false;
  }

  if (this._writableState) {
    this._writableState.destroyed = false;
    this._writableState.ended = false;
    this._writableState.ending = false;
    this._writableState.finished = false;
    this._writableState.errorEmitted = false;
  }
}

function emitErrorNT(self, err) {
  self.emit('error', err);
}

module.exports = {
  destroy: destroy,
  undestroy: undestroy
};
},{"35":35}],31:[function(require,module,exports){
module.exports = require(21).EventEmitter;

},{"21":21}],32:[function(require,module,exports){
(function (Buffer){


function isArray(arg) {
  if (Array.isArray) {
    return Array.isArray(arg);
  }
  return objectToString(arg) === '[object Array]';
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = Buffer.isBuffer;

function objectToString(o) {
  return Object.prototype.toString.call(o);
}

}).call(this,{"isBuffer":require(22)})
},{"22":22}],33:[function(require,module,exports){
if (typeof Object.create === 'function') {
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],34:[function(require,module,exports){
arguments[4][20][0].apply(exports,arguments)
},{"20":20}],35:[function(require,module,exports){
(function (process){
'use strict';

if (!process.version ||
    process.version.indexOf('v0.') === 0 ||
    process.version.indexOf('v1.') === 0 && process.version.indexOf('v1.8.') !== 0) {
  module.exports = nextTick;
} else {
  module.exports = process.nextTick;
}

function nextTick(fn, arg1, arg2, arg3) {
  if (typeof fn !== 'function') {
    throw new TypeError('"callback" argument must be a function');
  }
  var len = arguments.length;
  var args, i;
  switch (len) {
  case 0:
  case 1:
    return process.nextTick(fn);
  case 2:
    return process.nextTick(function afterTickOne() {
      fn.call(null, arg1);
    });
  case 3:
    return process.nextTick(function afterTickTwo() {
      fn.call(null, arg1, arg2);
    });
  case 4:
    return process.nextTick(function afterTickThree() {
      fn.call(null, arg1, arg2, arg3);
    });
  default:
    args = new Array(len - 1);
    i = 0;
    while (i < args.length) {
      args[i++] = arguments[i];
    }
    return process.nextTick(function afterTick() {
      fn.apply(null, args);
    });
  }
}

}).call(this,require(23))
},{"23":23}],36:[function(require,module,exports){
var buffer = require(17)
var Buffer = buffer.Buffer

function copyProps (src, dst) {
  for (var key in src) {
    dst[key] = src[key]
  }
}
if (Buffer.from && Buffer.alloc && Buffer.allocUnsafe && Buffer.allocUnsafeSlow) {
  module.exports = buffer
} else {
  copyProps(buffer, exports)
  exports.Buffer = SafeBuffer
}

function SafeBuffer (arg, encodingOrOffset, length) {
  return Buffer(arg, encodingOrOffset, length)
}

copyProps(Buffer, SafeBuffer)

SafeBuffer.from = function (arg, encodingOrOffset, length) {
  if (typeof arg === 'number') {
    throw new TypeError('Argument must not be a number')
  }
  return Buffer(arg, encodingOrOffset, length)
}

SafeBuffer.alloc = function (size, fill, encoding) {
  if (typeof size !== 'number') {
    throw new TypeError('Argument must be a number')
  }
  var buf = Buffer(size)
  if (fill !== undefined) {
    if (typeof encoding === 'string') {
      buf.fill(fill, encoding)
    } else {
      buf.fill(fill)
    }
  } else {
    buf.fill(0)
  }
  return buf
}

SafeBuffer.allocUnsafe = function (size) {
  if (typeof size !== 'number') {
    throw new TypeError('Argument must be a number')
  }
  return Buffer(size)
}

SafeBuffer.allocUnsafeSlow = function (size) {
  if (typeof size !== 'number') {
    throw new TypeError('Argument must be a number')
  }
  return buffer.SlowBuffer(size)
}

},{"17":17}],37:[function(require,module,exports){
'use strict';

var Buffer = require(36).Buffer;

var isEncoding = Buffer.isEncoding || function (encoding) {
  encoding = '' + encoding;
  switch (encoding && encoding.toLowerCase()) {
    case 'hex':case 'utf8':case 'utf-8':case 'ascii':case 'binary':case 'base64':case 'ucs2':case 'ucs-2':case 'utf16le':case 'utf-16le':case 'raw':
      return true;
    default:
      return false;
  }
};

function _normalizeEncoding(enc) {
  if (!enc) return 'utf8';
  var retried;
  while (true) {
    switch (enc) {
      case 'utf8':
      case 'utf-8':
        return 'utf8';
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return 'utf16le';
      case 'latin1':
      case 'binary':
        return 'latin1';
      case 'base64':
      case 'ascii':
      case 'hex':
        return enc;
      default:
        if (retried) return; 
        enc = ('' + enc).toLowerCase();
        retried = true;
    }
  }
};

function normalizeEncoding(enc) {
  var nenc = _normalizeEncoding(enc);
  if (typeof nenc !== 'string' && (Buffer.isEncoding === isEncoding || !isEncoding(enc))) throw new Error('Unknown encoding: ' + enc);
  return nenc || enc;
}

exports.StringDecoder = StringDecoder;
function StringDecoder(encoding) {
  this.encoding = normalizeEncoding(encoding);
  var nb;
  switch (this.encoding) {
    case 'utf16le':
      this.text = utf16Text;
      this.end = utf16End;
      nb = 4;
      break;
    case 'utf8':
      this.fillLast = utf8FillLast;
      nb = 4;
      break;
    case 'base64':
      this.text = base64Text;
      this.end = base64End;
      nb = 3;
      break;
    default:
      this.write = simpleWrite;
      this.end = simpleEnd;
      return;
  }
  this.lastNeed = 0;
  this.lastTotal = 0;
  this.lastChar = Buffer.allocUnsafe(nb);
}

StringDecoder.prototype.write = function (buf) {
  if (buf.length === 0) return '';
  var r;
  var i;
  if (this.lastNeed) {
    r = this.fillLast(buf);
    if (r === undefined) return '';
    i = this.lastNeed;
    this.lastNeed = 0;
  } else {
    i = 0;
  }
  if (i < buf.length) return r ? r + this.text(buf, i) : this.text(buf, i);
  return r || '';
};

StringDecoder.prototype.end = utf8End;

StringDecoder.prototype.text = utf8Text;

StringDecoder.prototype.fillLast = function (buf) {
  if (this.lastNeed <= buf.length) {
    buf.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, this.lastNeed);
    return this.lastChar.toString(this.encoding, 0, this.lastTotal);
  }
  buf.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, buf.length);
  this.lastNeed -= buf.length;
};

function utf8CheckByte(byte) {
  if (byte <= 0x7F) return 0;else if (byte >> 5 === 0x06) return 2;else if (byte >> 4 === 0x0E) return 3;else if (byte >> 3 === 0x1E) return 4;
  return -1;
}

function utf8CheckIncomplete(self, buf, i) {
  var j = buf.length - 1;
  if (j < i) return 0;
  var nb = utf8CheckByte(buf[j]);
  if (nb >= 0) {
    if (nb > 0) self.lastNeed = nb - 1;
    return nb;
  }
  if (--j < i) return 0;
  nb = utf8CheckByte(buf[j]);
  if (nb >= 0) {
    if (nb > 0) self.lastNeed = nb - 2;
    return nb;
  }
  if (--j < i) return 0;
  nb = utf8CheckByte(buf[j]);
  if (nb >= 0) {
    if (nb > 0) {
      if (nb === 2) nb = 0;else self.lastNeed = nb - 3;
    }
    return nb;
  }
  return 0;
}

function utf8CheckExtraBytes(self, buf, p) {
  if ((buf[0] & 0xC0) !== 0x80) {
    self.lastNeed = 0;
    return '\ufffd'.repeat(p);
  }
  if (self.lastNeed > 1 && buf.length > 1) {
    if ((buf[1] & 0xC0) !== 0x80) {
      self.lastNeed = 1;
      return '\ufffd'.repeat(p + 1);
    }
    if (self.lastNeed > 2 && buf.length > 2) {
      if ((buf[2] & 0xC0) !== 0x80) {
        self.lastNeed = 2;
        return '\ufffd'.repeat(p + 2);
      }
    }
  }
}

function utf8FillLast(buf) {
  var p = this.lastTotal - this.lastNeed;
  var r = utf8CheckExtraBytes(this, buf, p);
  if (r !== undefined) return r;
  if (this.lastNeed <= buf.length) {
    buf.copy(this.lastChar, p, 0, this.lastNeed);
    return this.lastChar.toString(this.encoding, 0, this.lastTotal);
  }
  buf.copy(this.lastChar, p, 0, buf.length);
  this.lastNeed -= buf.length;
}

function utf8Text(buf, i) {
  var total = utf8CheckIncomplete(this, buf, i);
  if (!this.lastNeed) return buf.toString('utf8', i);
  this.lastTotal = total;
  var end = buf.length - (total - this.lastNeed);
  buf.copy(this.lastChar, 0, end);
  return buf.toString('utf8', i, end);
}

function utf8End(buf) {
  var r = buf && buf.length ? this.write(buf) : '';
  if (this.lastNeed) return r + '\ufffd'.repeat(this.lastTotal - this.lastNeed);
  return r;
}

function utf16Text(buf, i) {
  if ((buf.length - i) % 2 === 0) {
    var r = buf.toString('utf16le', i);
    if (r) {
      var c = r.charCodeAt(r.length - 1);
      if (c >= 0xD800 && c <= 0xDBFF) {
        this.lastNeed = 2;
        this.lastTotal = 4;
        this.lastChar[0] = buf[buf.length - 2];
        this.lastChar[1] = buf[buf.length - 1];
        return r.slice(0, -1);
      }
    }
    return r;
  }
  this.lastNeed = 1;
  this.lastTotal = 2;
  this.lastChar[0] = buf[buf.length - 1];
  return buf.toString('utf16le', i, buf.length - 1);
}

function utf16End(buf) {
  var r = buf && buf.length ? this.write(buf) : '';
  if (this.lastNeed) {
    var end = this.lastTotal - this.lastNeed;
    return r + this.lastChar.toString('utf16le', 0, end);
  }
  return r;
}

function base64Text(buf, i) {
  var n = (buf.length - i) % 3;
  if (n === 0) return buf.toString('base64', i);
  this.lastNeed = 3 - n;
  this.lastTotal = 3;
  if (n === 1) {
    this.lastChar[0] = buf[buf.length - 1];
  } else {
    this.lastChar[0] = buf[buf.length - 2];
    this.lastChar[1] = buf[buf.length - 1];
  }
  return buf.toString('base64', i, buf.length - n);
}

function base64End(buf) {
  var r = buf && buf.length ? this.write(buf) : '';
  if (this.lastNeed) return r + this.lastChar.toString('base64', 0, 3 - this.lastNeed);
  return r;
}

function simpleWrite(buf) {
  return buf.toString(this.encoding);
}

function simpleEnd(buf) {
  return buf && buf.length ? this.write(buf) : '';
}
},{"36":36}],38:[function(require,module,exports){
(function (global){


module.exports = deprecate;


function deprecate (fn, msg) {
  if (config('noDeprecation')) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (config('throwDeprecation')) {
        throw new Error(msg);
      } else if (config('traceDeprecation')) {
        console.trace(msg);
      } else {
        console.warn(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
}


function config (name) {
  try {
    if (!global.localStorage) return false;
  } catch (_) {
    return false;
  }
  var val = global.localStorage[name];
  if (null == val) return false;
  return String(val).toLowerCase() === 'true';
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],39:[function(require,module,exports){
exports = module.exports = require(26);
exports.Stream = exports;
exports.Readable = exports;
exports.Writable = require(28);
exports.Duplex = require(24);
exports.Transform = require(27);
exports.PassThrough = require(25);

},{"24":24,"25":25,"26":26,"27":27,"28":28}],40:[function(require,module,exports){
module.exports = require(39).Transform

},{"39":39}],41:[function(require,module,exports){
module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}
},{}],42:[function(require,module,exports){
(function (process,global){

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


exports.deprecate = function(fn, msg) {
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


function inspect(obj, opts) {
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    ctx.showHidden = opts;
  } else if (opts) {
    exports._extend(ctx, opts);
  }
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      value.inspect !== exports.inspect &&
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = require(41);

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


exports.inherits = require(44);

exports._extend = function(origin, add) {
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

}).call(this,require(23),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"23":23,"41":41,"44":44}],43:[function(require,module,exports){


(function(factory) {

	if (typeof define === 'function' && define.amd) {
		define([], factory);
	} else if (typeof module === 'object') {
		var HashMap = module.exports = factory();
		HashMap.HashMap = HashMap;
	} else {
		this.HashMap = factory();
	}
}(function() {

	function HashMap(other) {
		this.clear();
		switch (arguments.length) {
			case 0: break;
			case 1: this.copy(other); break;
			default: multi(this, arguments); break;
		}
	}

	var proto = HashMap.prototype = {
		constructor:HashMap,

		get:function(key) {
			var data = this._data[this.hash(key)];
			return data && data[1];
		},

		set:function(key, value) {
			var hash = this.hash(key);
			if ( !(hash in this._data) ) {
				this._count++;
			}
			this._data[hash] = [key, value];
		},

		multi:function() {
			multi(this, arguments);
		},

		copy:function(other) {
			for (var hash in other._data) {
				if ( !(hash in this._data) ) {
					this._count++;
				}
				this._data[hash] = other._data[hash];
			}
		},

		has:function(key) {
			return this.hash(key) in this._data;
		},

		search:function(value) {
			for (var key in this._data) {
				if (this._data[key][1] === value) {
					return this._data[key][0];
				}
			}

			return null;
		},

		remove:function(key) {
			var hash = this.hash(key);
			if ( hash in this._data ) {
				this._count--;
				delete this._data[hash];
			}
		},

		type:function(key) {
			var str = Object.prototype.toString.call(key);
			var type = str.slice(8, -1).toLowerCase();
			if (type === 'domwindow' && !key) {
				return key + '';
			}
			return type;
		},

		keys:function() {
			var keys = [];
			this.forEach(function(_, key) { keys.push(key); });
			return keys;
		},

		values:function() {
			var values = [];
			this.forEach(function(value) { values.push(value); });
			return values;
		},

		count:function() {
			return this._count;
		},

		clear:function() {
			this._data = {};
			this._count = 0;
		},

		clone:function() {
			return new HashMap(this);
		},

		hash:function(key) {
			switch (this.type(key)) {
				case 'undefined':
				case 'null':
				case 'boolean':
				case 'number':
				case 'regexp':
					return key + '';

				case 'date':
					return '♣' + key.getTime();

				case 'string':
					return '♠' + key;

				case 'array':
					var hashes = [];
					for (var i = 0; i < key.length; i++) {
						hashes[i] = this.hash(key[i]);
					}
					return '♥' + hashes.join('⁞');

				default:
					if (!key.hasOwnProperty('_hmuid_')) {
						key._hmuid_ = ++HashMap.uid;
						hide(key, '_hmuid_');
					}

					return '♦' + key._hmuid_;
			}
		},

		forEach:function(func, ctx) {
			for (var key in this._data) {
				var data = this._data[key];
				func.call(ctx || this, data[1], data[0]);
			}
		}
	};

	HashMap.uid = 0;


	['set','multi','copy','remove','clear','forEach'].forEach(function(method) {
		var fn = proto[method];
		proto[method] = function() {
			fn.apply(this, arguments);
			return this;
		};
	});


	function multi(map, args) {
		for (var i = 0; i < args.length; i += 2) {
			map.set(args[i], args[i+1]);
		}
	}

	function hide(obj, prop) {
		if (Object.defineProperty) {
			Object.defineProperty(obj, prop, {enumerable:false});
		}
	}

	return HashMap;
}));

},{}],44:[function(require,module,exports){
arguments[4][33][0].apply(exports,arguments)
},{"33":33}],45:[function(require,module,exports){

(function (root, definition) {
    "use strict";
    if (typeof module === 'object' && module.exports && typeof require === 'function') {
        module.exports = definition();
    } else if (typeof define === 'function' && typeof define.amd === 'object') {
        define(definition);
    } else {
        root.log = definition();
    }
}(this, function () {
    "use strict";
    var noop = function() {};
    var undefinedType = "undefined";

    function realMethod(methodName) {
        if (typeof console === undefinedType) {
            return false; 
        } else if (console[methodName] !== undefined) {
            return bindMethod(console, methodName);
        } else if (console.log !== undefined) {
            return bindMethod(console, 'log');
        } else {
            return noop;
        }
    }

    function bindMethod(obj, methodName) {
        var method = obj[methodName];
        if (typeof method.bind === 'function') {
            return method.bind(obj);
        } else {
            try {
                return Function.prototype.bind.call(method, obj);
            } catch (e) {
                return function() {
                    return Function.prototype.apply.apply(method, [obj, arguments]);
                };
            }
        }
    }


    function enableLoggingWhenConsoleArrives(methodName, level, loggerName) {
        return function () {
            if (typeof console !== undefinedType) {
                replaceLoggingMethods.call(this, level, loggerName);
                this[methodName].apply(this, arguments);
            }
        };
    }

    function replaceLoggingMethods(level, loggerName) {

        for (var i = 0; i < logMethods.length; i++) {
            var methodName = logMethods[i];
            this[methodName] = (i < level) ?
                noop :
                this.methodFactory(methodName, level, loggerName);
        }
    }

    function defaultMethodFactory(methodName, level, loggerName) {

        return realMethod(methodName) ||
               enableLoggingWhenConsoleArrives.apply(this, arguments);
    }

    var logMethods = [
        "trace",
        "debug",
        "info",
        "warn",
        "error"
    ];

    function Logger(name, defaultLevel, factory) {
      var self = this;
      var currentLevel;
      var storageKey = "loglevel";
      if (name) {
        storageKey += ":" + name;
      }

      function persistLevelIfPossible(levelNum) {
          var levelName = (logMethods[levelNum] || 'silent').toUpperCase();

          try {
              window.localStorage[storageKey] = levelName;
              return;
          } catch (ignore) {}

          try {
              window.document.cookie =
                encodeURIComponent(storageKey) + "=" + levelName + ";";
          } catch (ignore) {}
      }

      function getPersistedLevel() {
          var storedLevel;

          try {
              storedLevel = window.localStorage[storageKey];
          } catch (ignore) {}

          if (typeof storedLevel === undefinedType) {
              try {
                  var cookie = window.document.cookie;
                  var location = cookie.indexOf(
                      encodeURIComponent(storageKey) + "=");
                  if (location) {
                      storedLevel = /^([^;]+)/.exec(cookie.slice(location))[1];
                  }
              } catch (ignore) {}
          }

          if (self.levels[storedLevel] === undefined) {
              storedLevel = undefined;
          }

          return storedLevel;
      }



      self.levels = { "TRACE": 0, "DEBUG": 1, "INFO": 2, "WARN": 3,
          "ERROR": 4, "SILENT": 5};

      self.methodFactory = factory || defaultMethodFactory;

      self.getLevel = function () {
          return currentLevel;
      };

      self.setLevel = function (level, persist) {
          if (typeof level === "string" && self.levels[level.toUpperCase()] !== undefined) {
              level = self.levels[level.toUpperCase()];
          }
          if (typeof level === "number" && level >= 0 && level <= self.levels.SILENT) {
              currentLevel = level;
              if (persist !== false) {  
                  persistLevelIfPossible(level);
              }
              replaceLoggingMethods.call(self, level, name);
              if (typeof console === undefinedType && level < self.levels.SILENT) {
                  return "No console available for logging";
              }
          } else {
              throw "log.setLevel() called with invalid level: " + level;
          }
      };

      self.setDefaultLevel = function (level) {
          if (!getPersistedLevel()) {
              self.setLevel(level, false);
          }
      };

      self.enableAll = function(persist) {
          self.setLevel(self.levels.TRACE, persist);
      };

      self.disableAll = function(persist) {
          self.setLevel(self.levels.SILENT, persist);
      };

      var initialLevel = getPersistedLevel();
      if (initialLevel == null) {
          initialLevel = defaultLevel == null ? "WARN" : defaultLevel;
      }
      self.setLevel(initialLevel, false);
    }



    var defaultLogger = new Logger();

    var _loggersByName = {};
    defaultLogger.getLogger = function getLogger(name) {
        if (typeof name !== "string" || name === "") {
          throw new TypeError("You must supply a name when creating a logger.");
        }

        var logger = _loggersByName[name];
        if (!logger) {
          logger = _loggersByName[name] = new Logger(
            name, defaultLogger.getLevel(), defaultLogger.methodFactory);
        }
        return logger;
    };

    var _log = (typeof window !== undefinedType) ? window.log : undefined;
    defaultLogger.noConflict = function() {
        if (typeof window !== undefinedType &&
               window.log === defaultLogger) {
            window.log = _log;
        }

        return defaultLogger;
    };

    return defaultLogger;
}));

},{}],46:[function(require,module,exports){



(function(global) {
    "use strict";


    var Long = function(low, high, unsigned) {


        this.low = low|0;


        this.high = high|0;


        this.unsigned = !!unsigned;
    };



    Long.isLong = function(obj) {
        return (obj && obj instanceof Long) === true;
    };


    var INT_CACHE = {};


    var UINT_CACHE = {};


    Long.fromInt = function(value, unsigned) {
        var obj, cachedObj;
        if (!unsigned) {
            value = value | 0;
            if (-128 <= value && value < 128) {
                cachedObj = INT_CACHE[value];
                if (cachedObj)
                    return cachedObj;
            }
            obj = new Long(value, value < 0 ? -1 : 0, false);
            if (-128 <= value && value < 128)
                INT_CACHE[value] = obj;
            return obj;
        } else {
            value = value >>> 0;
            if (0 <= value && value < 256) {
                cachedObj = UINT_CACHE[value];
                if (cachedObj)
                    return cachedObj;
            }
            obj = new Long(value, (value | 0) < 0 ? -1 : 0, true);
            if (0 <= value && value < 256)
                UINT_CACHE[value] = obj;
            return obj;
        }
    };


    Long.fromNumber = function(value, unsigned) {
        unsigned = !!unsigned;
        if (isNaN(value) || !isFinite(value))
            return Long.ZERO;
        if (!unsigned && value <= -TWO_PWR_63_DBL)
            return Long.MIN_VALUE;
        if (!unsigned && value + 1 >= TWO_PWR_63_DBL)
            return Long.MAX_VALUE;
        if (unsigned && value >= TWO_PWR_64_DBL)
            return Long.MAX_UNSIGNED_VALUE;
        if (value < 0)
            return Long.fromNumber(-value, unsigned).negate();
        return new Long((value % TWO_PWR_32_DBL) | 0, (value / TWO_PWR_32_DBL) | 0, unsigned);
    };


    Long.fromBits = function(lowBits, highBits, unsigned) {
        return new Long(lowBits, highBits, unsigned);
    };


    Long.fromString = function(str, unsigned, radix) {
        if (str.length === 0)
            throw Error('number format error: empty string');
        if (str === "NaN" || str === "Infinity" || str === "+Infinity" || str === "-Infinity")
            return Long.ZERO;
        if (typeof unsigned === 'number') 
            radix = unsigned,
            unsigned = false;
        radix = radix || 10;
        if (radix < 2 || 36 < radix)
            throw Error('radix out of range: ' + radix);

        var p;
        if ((p = str.indexOf('-')) > 0)
            throw Error('number format error: interior "-" character: ' + str);
        else if (p === 0)
            return Long.fromString(str.substring(1), unsigned, radix).negate();

        var radixToPower = Long.fromNumber(Math.pow(radix, 8));

        var result = Long.ZERO;
        for (var i = 0; i < str.length; i += 8) {
            var size = Math.min(8, str.length - i);
            var value = parseInt(str.substring(i, i + size), radix);
            if (size < 8) {
                var power = Long.fromNumber(Math.pow(radix, size));
                result = result.multiply(power).add(Long.fromNumber(value));
            } else {
                result = result.multiply(radixToPower);
                result = result.add(Long.fromNumber(value));
            }
        }
        result.unsigned = unsigned;
        return result;
    };


    Long.fromValue = function(val) {
        if (typeof val === 'number')
            return Long.fromNumber(val);
        if (typeof val === 'string')
            return Long.fromString(val);
        if (Long.isLong(val))
            return val;
        return new Long(val.low, val.high, val.unsigned);
    };



    var TWO_PWR_16_DBL = 1 << 16;


    var TWO_PWR_24_DBL = 1 << 24;


    var TWO_PWR_32_DBL = TWO_PWR_16_DBL * TWO_PWR_16_DBL;


    var TWO_PWR_64_DBL = TWO_PWR_32_DBL * TWO_PWR_32_DBL;


    var TWO_PWR_63_DBL = TWO_PWR_64_DBL / 2;


    var TWO_PWR_24 = Long.fromInt(TWO_PWR_24_DBL);


    Long.ZERO = Long.fromInt(0);


    Long.UZERO = Long.fromInt(0, true);


    Long.ONE = Long.fromInt(1);


    Long.UONE = Long.fromInt(1, true);


    Long.NEG_ONE = Long.fromInt(-1);


    Long.MAX_VALUE = Long.fromBits(0xFFFFFFFF|0, 0x7FFFFFFF|0, false);


    Long.MAX_UNSIGNED_VALUE = Long.fromBits(0xFFFFFFFF|0, 0xFFFFFFFF|0, true);


    Long.MIN_VALUE = Long.fromBits(0, 0x80000000|0, false);


    Long.prototype.toInt = function() {
        return this.unsigned ? this.low >>> 0 : this.low;
    };


    Long.prototype.toNumber = function() {
        if (this.unsigned) {
            return ((this.high >>> 0) * TWO_PWR_32_DBL) + (this.low >>> 0);
        }
        return this.high * TWO_PWR_32_DBL + (this.low >>> 0);
    };


    Long.prototype.toString = function(radix) {
        radix = radix || 10;
        if (radix < 2 || 36 < radix)
            throw RangeError('radix out of range: ' + radix);
        if (this.isZero())
            return '0';
        var rem;
        if (this.isNegative()) { 
            if (this.equals(Long.MIN_VALUE)) {
                var radixLong = Long.fromNumber(radix);
                var div = this.div(radixLong);
                rem = div.multiply(radixLong).subtract(this);
                return div.toString(radix) + rem.toInt().toString(radix);
            } else
                return '-' + this.negate().toString(radix);
        }

        var radixToPower = Long.fromNumber(Math.pow(radix, 6), this.unsigned);
        rem = this;
        var result = '';
        while (true) {
            var remDiv = rem.div(radixToPower),
                intval = rem.subtract(remDiv.multiply(radixToPower)).toInt() >>> 0,
                digits = intval.toString(radix);
            rem = remDiv;
            if (rem.isZero())
                return digits + result;
            else {
                while (digits.length < 6)
                    digits = '0' + digits;
                result = '' + digits + result;
            }
        }
    };


    Long.prototype.getHighBits = function() {
        return this.high;
    };


    Long.prototype.getHighBitsUnsigned = function() {
        return this.high >>> 0;
    };


    Long.prototype.getLowBits = function() {
        return this.low;
    };


    Long.prototype.getLowBitsUnsigned = function() {
        return this.low >>> 0;
    };


    Long.prototype.getNumBitsAbs = function() {
        if (this.isNegative()) 
            return this.equals(Long.MIN_VALUE) ? 64 : this.negate().getNumBitsAbs();
        var val = this.high != 0 ? this.high : this.low;
        for (var bit = 31; bit > 0; bit--)
            if ((val & (1 << bit)) != 0)
                break;
        return this.high != 0 ? bit + 33 : bit + 1;
    };


    Long.prototype.isZero = function() {
        return this.high === 0 && this.low === 0;
    };


    Long.prototype.isNegative = function() {
        return !this.unsigned && this.high < 0;
    };


    Long.prototype.isPositive = function() {
        return this.unsigned || this.high >= 0;
    };


    Long.prototype.isOdd = function() {
        return (this.low & 1) === 1;
    };


    Long.prototype.isEven = function() {
        return (this.low & 1) === 0;
    };


    Long.prototype.equals = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        if (this.unsigned !== other.unsigned && (this.high >>> 31) === 1 && (other.high >>> 31) === 1)
            return false;
        return this.high === other.high && this.low === other.low;
    };


    Long.prototype.notEquals = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return !this.equals(other);
    };


    Long.prototype.lessThan = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) < 0;
    };


    Long.prototype.lessThanOrEqual = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) <= 0;
    };


    Long.prototype.greaterThan = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) > 0;
    };


    Long.prototype.greaterThanOrEqual = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return this.compare(other) >= 0;
    };


    Long.prototype.compare = function(other) {
        if (this.equals(other))
            return 0;
        var thisNeg = this.isNegative(),
            otherNeg = other.isNegative();
        if (thisNeg && !otherNeg)
            return -1;
        if (!thisNeg && otherNeg)
            return 1;
        if (!this.unsigned)
            return this.subtract(other).isNegative() ? -1 : 1;
        return (other.high >>> 0) > (this.high >>> 0) || (other.high === this.high && (other.low >>> 0) > (this.low >>> 0)) ? -1 : 1;
    };


    Long.prototype.negate = function() {
        if (!this.unsigned && this.equals(Long.MIN_VALUE))
            return Long.MIN_VALUE;
        return this.not().add(Long.ONE);
    };


    Long.prototype.add = function(addend) {
        if (!Long.isLong(addend))
            addend = Long.fromValue(addend);


        var a48 = this.high >>> 16;
        var a32 = this.high & 0xFFFF;
        var a16 = this.low >>> 16;
        var a00 = this.low & 0xFFFF;

        var b48 = addend.high >>> 16;
        var b32 = addend.high & 0xFFFF;
        var b16 = addend.low >>> 16;
        var b00 = addend.low & 0xFFFF;

        var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
        c00 += a00 + b00;
        c16 += c00 >>> 16;
        c00 &= 0xFFFF;
        c16 += a16 + b16;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c32 += a32 + b32;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c48 += a48 + b48;
        c48 &= 0xFFFF;
        return Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32, this.unsigned);
    };


    Long.prototype.subtract = function(subtrahend) {
        if (!Long.isLong(subtrahend))
            subtrahend = Long.fromValue(subtrahend);
        return this.add(subtrahend.negate());
    };


    Long.prototype.multiply = function(multiplier) {
        if (this.isZero())
            return Long.ZERO;
        if (!Long.isLong(multiplier))
            multiplier = Long.fromValue(multiplier);
        if (multiplier.isZero())
            return Long.ZERO;
        if (this.equals(Long.MIN_VALUE))
            return multiplier.isOdd() ? Long.MIN_VALUE : Long.ZERO;
        if (multiplier.equals(Long.MIN_VALUE))
            return this.isOdd() ? Long.MIN_VALUE : Long.ZERO;

        if (this.isNegative()) {
            if (multiplier.isNegative())
                return this.negate().multiply(multiplier.negate());
            else
                return this.negate().multiply(multiplier).negate();
        } else if (multiplier.isNegative())
            return this.multiply(multiplier.negate()).negate();

        if (this.lessThan(TWO_PWR_24) && multiplier.lessThan(TWO_PWR_24))
            return Long.fromNumber(this.toNumber() * multiplier.toNumber(), this.unsigned);


        var a48 = this.high >>> 16;
        var a32 = this.high & 0xFFFF;
        var a16 = this.low >>> 16;
        var a00 = this.low & 0xFFFF;

        var b48 = multiplier.high >>> 16;
        var b32 = multiplier.high & 0xFFFF;
        var b16 = multiplier.low >>> 16;
        var b00 = multiplier.low & 0xFFFF;

        var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
        c00 += a00 * b00;
        c16 += c00 >>> 16;
        c00 &= 0xFFFF;
        c16 += a16 * b00;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c16 += a00 * b16;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c32 += a32 * b00;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c32 += a16 * b16;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c32 += a00 * b32;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c48 += a48 * b00 + a32 * b16 + a16 * b32 + a00 * b48;
        c48 &= 0xFFFF;
        return Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32, this.unsigned);
    };


    Long.prototype.div = function(divisor) {
        if (!Long.isLong(divisor))
            divisor = Long.fromValue(divisor);
        if (divisor.isZero())
            throw(new Error('division by zero'));
        if (this.isZero())
            return this.unsigned ? Long.UZERO : Long.ZERO;
        var approx, rem, res;
        if (this.equals(Long.MIN_VALUE)) {
            if (divisor.equals(Long.ONE) || divisor.equals(Long.NEG_ONE))
                return Long.MIN_VALUE;  
            else if (divisor.equals(Long.MIN_VALUE))
                return Long.ONE;
            else {
                var halfThis = this.shiftRight(1);
                approx = halfThis.div(divisor).shiftLeft(1);
                if (approx.equals(Long.ZERO)) {
                    return divisor.isNegative() ? Long.ONE : Long.NEG_ONE;
                } else {
                    rem = this.subtract(divisor.multiply(approx));
                    res = approx.add(rem.div(divisor));
                    return res;
                }
            }
        } else if (divisor.equals(Long.MIN_VALUE))
            return this.unsigned ? Long.UZERO : Long.ZERO;
        if (this.isNegative()) {
            if (divisor.isNegative())
                return this.negate().div(divisor.negate());
            return this.negate().div(divisor).negate();
        } else if (divisor.isNegative())
            return this.div(divisor.negate()).negate();

        res = Long.ZERO;
        rem = this;
        while (rem.greaterThanOrEqual(divisor)) {
            approx = Math.max(1, Math.floor(rem.toNumber() / divisor.toNumber()));

            var log2 = Math.ceil(Math.log(approx) / Math.LN2),
                delta = (log2 <= 48) ? 1 : Math.pow(2, log2 - 48),

                approxRes = Long.fromNumber(approx),
                approxRem = approxRes.multiply(divisor);
            while (approxRem.isNegative() || approxRem.greaterThan(rem)) {
                approx -= delta;
                approxRes = Long.fromNumber(approx, this.unsigned);
                approxRem = approxRes.multiply(divisor);
            }

            if (approxRes.isZero())
                approxRes = Long.ONE;

            res = res.add(approxRes);
            rem = rem.subtract(approxRem);
        }
        return res;
    };


    Long.prototype.modulo = function(divisor) {
        if (!Long.isLong(divisor))
            divisor = Long.fromValue(divisor);
        return this.subtract(this.div(divisor).multiply(divisor));
    };


    Long.prototype.not = function() {
        return Long.fromBits(~this.low, ~this.high, this.unsigned);
    };


    Long.prototype.and = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return Long.fromBits(this.low & other.low, this.high & other.high, this.unsigned);
    };


    Long.prototype.or = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return Long.fromBits(this.low | other.low, this.high | other.high, this.unsigned);
    };


    Long.prototype.xor = function(other) {
        if (!Long.isLong(other))
            other = Long.fromValue(other);
        return Long.fromBits(this.low ^ other.low, this.high ^ other.high, this.unsigned);
    };


    Long.prototype.shiftLeft = function(numBits) {
        if (Long.isLong(numBits))
            numBits = numBits.toInt();
        if ((numBits &= 63) === 0)
            return this;
        else if (numBits < 32)
            return Long.fromBits(this.low << numBits, (this.high << numBits) | (this.low >>> (32 - numBits)), this.unsigned);
        else
            return Long.fromBits(0, this.low << (numBits - 32), this.unsigned);
    };


    Long.prototype.shiftRight = function(numBits) {
        if (Long.isLong(numBits))
            numBits = numBits.toInt();
        if ((numBits &= 63) === 0)
            return this;
        else if (numBits < 32)
            return Long.fromBits((this.low >>> numBits) | (this.high << (32 - numBits)), this.high >> numBits, this.unsigned);
        else
            return Long.fromBits(this.high >> (numBits - 32), this.high >= 0 ? 0 : -1, this.unsigned);
    };


    Long.prototype.shiftRightUnsigned = function(numBits) {
        if (Long.isLong(numBits))
            numBits = numBits.toInt();
        numBits &= 63;
        if (numBits === 0)
            return this;
        else {
            var high = this.high;
            if (numBits < 32) {
                var low = this.low;
                return Long.fromBits((low >>> numBits) | (high << (32 - numBits)), high >>> numBits, this.unsigned);
            } else if (numBits === 32)
                return Long.fromBits(high, 0, this.unsigned);
            else
                return Long.fromBits(high >>> (numBits - 32), 0, this.unsigned);
        }
    };


    Long.prototype.toSigned = function() {
        if (!this.unsigned)
            return this;
        return new Long(this.low, this.high, false);
    };


    Long.prototype.toUnsigned = function() {
        if (this.unsigned)
            return this;
        return new Long(this.low, this.high, true);
    };

 if (typeof require === 'function' && typeof module === 'object' && module && typeof exports === 'object' && exports)
        module["exports"] = Long;
 else if (typeof define === 'function' && define["amd"])
        define(function() { return Long; });
 else
        (global["dcodeIO"] = global["dcodeIO"] || {})["Long"] = Long;

})(this);

},{}],47:[function(require,module,exports){

(function(define) { 'use strict';
define(function (require) {

	var makePromise = require(62);
	var Scheduler = require(48);
	var async = require(60).asap;

	return makePromise({
		scheduler: new Scheduler(async)
	});

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });

},{"48":48,"60":60,"62":62}],48:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {


	function Scheduler(async) {
		this._async = async;
		this._running = false;

		this._queue = this;
		this._queueLen = 0;
		this._afterQueue = {};
		this._afterQueueLen = 0;

		var self = this;
		this.drain = function() {
			self._drain();
		};
	}

	Scheduler.prototype.enqueue = function(task) {
		this._queue[this._queueLen++] = task;
		this.run();
	};

	Scheduler.prototype.afterQueue = function(task) {
		this._afterQueue[this._afterQueueLen++] = task;
		this.run();
	};

	Scheduler.prototype.run = function() {
		if (!this._running) {
			this._running = true;
			this._async(this.drain);
		}
	};

	Scheduler.prototype._drain = function() {
		var i = 0;
		for (; i < this._queueLen; ++i) {
			this._queue[i].run();
			this._queue[i] = void 0;
		}

		this._queueLen = 0;
		this._running = false;

		for (i = 0; i < this._afterQueueLen; ++i) {
			this._afterQueue[i].run();
			this._afterQueue[i] = void 0;
		}

		this._afterQueueLen = 0;
	};

	return Scheduler;

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],49:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	function TimeoutError (message) {
		Error.call(this);
		this.message = message;
		this.name = TimeoutError.name;
		if (typeof Error.captureStackTrace === 'function') {
			Error.captureStackTrace(this, TimeoutError);
		}
	}

	TimeoutError.prototype = Object.create(Error.prototype);
	TimeoutError.prototype.constructor = TimeoutError;

	return TimeoutError;
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));
},{}],50:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	makeApply.tryCatchResolve = tryCatchResolve;

	return makeApply;

	function makeApply(Promise, call) {
		if(arguments.length < 2) {
			call = tryCatchResolve;
		}

		return apply;

		function apply(f, thisArg, args) {
			var p = Promise._defer();
			var l = args.length;
			var params = new Array(l);
			callAndResolve({ f:f, thisArg:thisArg, args:args, params:params, i:l-1, call:call }, p._handler);

			return p;
		}

		function callAndResolve(c, h) {
			if(c.i < 0) {
				return call(c.f, c.thisArg, c.params, h);
			}

			var handler = Promise._handler(c.args[c.i]);
			handler.fold(callAndResolveNext, c, void 0, h);
		}

		function callAndResolveNext(c, x, h) {
			c.params[c.i] = x;
			c.i -= 1;
			callAndResolve(c, h);
		}
	}

	function tryCatchResolve(f, thisArg, args, resolver) {
		try {
			resolver.resolve(f.apply(thisArg, args));
		} catch(e) {
			resolver.reject(e);
		}
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));



},{}],51:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var state = require(63);
	var applier = require(50);

	return function array(Promise) {

		var applyFold = applier(Promise);
		var toPromise = Promise.resolve;
		var all = Promise.all;

		var ar = Array.prototype.reduce;
		var arr = Array.prototype.reduceRight;
		var slice = Array.prototype.slice;


		Promise.any = any;
		Promise.some = some;
		Promise.settle = settle;

		Promise.map = map;
		Promise.filter = filter;
		Promise.reduce = reduce;
		Promise.reduceRight = reduceRight;

		Promise.prototype.spread = function(onFulfilled) {
			return this.then(all).then(function(array) {
				return onFulfilled.apply(this, array);
			});
		};

		return Promise;

		function any(promises) {
			var p = Promise._defer();
			var resolver = p._handler;
			var l = promises.length>>>0;

			var pending = l;
			var errors = [];

			for (var h, x, i = 0; i < l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					--pending;
					continue;
				}

				h = Promise._handler(x);
				if(h.state() > 0) {
					resolver.become(h);
					Promise._visitRemaining(promises, i, h);
					break;
				} else {
					h.visit(resolver, handleFulfill, handleReject);
				}
			}

			if(pending === 0) {
				resolver.reject(new RangeError('any(): array must not be empty'));
			}

			return p;

			function handleFulfill(x) {
				errors = null;
				this.resolve(x); 
			}

			function handleReject(e) {
				if(this.resolved) { 
					return;
				}

				errors.push(e);
				if(--pending === 0) {
					this.reject(errors);
				}
			}
		}

		function some(promises, n) {
			var p = Promise._defer();
			var resolver = p._handler;

			var results = [];
			var errors = [];

			var l = promises.length>>>0;
			var nFulfill = 0;
			var nReject;
			var x, i; 

			for(i=0; i<l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					continue;
				}
				++nFulfill;
			}

			n = Math.max(n, 0);
			nReject = (nFulfill - n + 1);
			nFulfill = Math.min(n, nFulfill);

			if(n > nFulfill) {
				resolver.reject(new RangeError('some(): array must contain at least '
				+ n + ' item(s), but had ' + nFulfill));
			} else if(nFulfill === 0) {
				resolver.resolve(results);
			}

			for(i=0; i<l; ++i) {
				x = promises[i];
				if(x === void 0 && !(i in promises)) {
					continue;
				}

				Promise._handler(x).visit(resolver, fulfill, reject, resolver.notify);
			}

			return p;

			function fulfill(x) {
				if(this.resolved) { 
					return;
				}

				results.push(x);
				if(--nFulfill === 0) {
					errors = null;
					this.resolve(results);
				}
			}

			function reject(e) {
				if(this.resolved) { 
					return;
				}

				errors.push(e);
				if(--nReject === 0) {
					results = null;
					this.reject(errors);
				}
			}
		}

		function map(promises, f) {
			return Promise._traverse(f, promises);
		}

		function filter(promises, predicate) {
			var a = slice.call(promises);
			return Promise._traverse(predicate, a).then(function(keep) {
				return filterSync(a, keep);
			});
		}

		function filterSync(promises, keep) {
			var l = keep.length;
			var filtered = new Array(l);
			for(var i=0, j=0; i<l; ++i) {
				if(keep[i]) {
					filtered[j++] = Promise._handler(promises[i]).value;
				}
			}
			filtered.length = j;
			return filtered;

		}

		function settle(promises) {
			return all(promises.map(settleOne));
		}

		function settleOne(p) {
			var h = Promise._handler(p);
			if(h.state() === 0) {
				return toPromise(p).then(state.fulfilled, state.rejected);
			}

			h._unreport();
			return state.inspect(h);
		}

		function reduce(promises, f ) {
			return arguments.length > 2 ? ar.call(promises, liftCombine(f), arguments[2])
					: ar.call(promises, liftCombine(f));
		}

		function reduceRight(promises, f ) {
			return arguments.length > 2 ? arr.call(promises, liftCombine(f), arguments[2])
					: arr.call(promises, liftCombine(f));
		}

		function liftCombine(f) {
			return function(z, x, i) {
				return applyFold(f, void 0, [z,x,i]);
			};
		}
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"50":50,"63":63}],52:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function flow(Promise) {

		var resolve = Promise.resolve;
		var reject = Promise.reject;
		var origCatch = Promise.prototype['catch'];

		Promise.prototype.done = function(onResult, onError) {
			this._handler.visit(this._handler.receiver, onResult, onError);
		};

		Promise.prototype['catch'] = Promise.prototype.otherwise = function(onRejected) {
			if (arguments.length < 2) {
				return origCatch.call(this, onRejected);
			}

			if(typeof onRejected !== 'function') {
				return this.ensure(rejectInvalidPredicate);
			}

			return origCatch.call(this, createCatchFilter(arguments[1], onRejected));
		};

		function createCatchFilter(handler, predicate) {
			return function(e) {
				return evaluatePredicate(e, predicate)
					? handler.call(this, e)
					: reject(e);
			};
		}

		Promise.prototype['finally'] = Promise.prototype.ensure = function(handler) {
			if(typeof handler !== 'function') {
				return this;
			}

			return this.then(function(x) {
				return runSideEffect(handler, this, identity, x);
			}, function(e) {
				return runSideEffect(handler, this, reject, e);
			});
		};

		function runSideEffect (handler, thisArg, propagate, value) {
			var result = handler.call(thisArg);
			return maybeThenable(result)
				? propagateValue(result, propagate, value)
				: propagate(value);
		}

		function propagateValue (result, propagate, x) {
			return resolve(result).then(function () {
				return propagate(x);
			});
		}

		Promise.prototype['else'] = Promise.prototype.orElse = function(defaultValue) {
			return this.then(void 0, function() {
				return defaultValue;
			});
		};

		Promise.prototype['yield'] = function(value) {
			return this.then(function() {
				return value;
			});
		};

		Promise.prototype.tap = function(onFulfilledSideEffect) {
			return this.then(onFulfilledSideEffect)['yield'](this);
		};

		return Promise;
	};

	function rejectInvalidPredicate() {
		throw new TypeError('catch predicate must be a function');
	}

	function evaluatePredicate(e, predicate) {
		return isError(predicate) ? e instanceof predicate : predicate(e);
	}

	function isError(predicate) {
		return predicate === Error
			|| (predicate != null && predicate.prototype instanceof Error);
	}

	function maybeThenable(x) {
		return (typeof x === 'object' || typeof x === 'function') && x !== null;
	}

	function identity(x) {
		return x;
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],53:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function fold(Promise) {

		Promise.prototype.fold = function(f, z) {
			var promise = this._beget();

			this._handler.fold(function(z, x, to) {
				Promise._handler(z).fold(function(x, z, to) {
					to.resolve(f.call(this, z, x));
				}, x, this, to);
			}, z, promise._handler.receiver, promise._handler);

			return promise;
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],54:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var inspect = require(63).inspect;

	return function inspection(Promise) {

		Promise.prototype.inspect = function() {
			return inspect(Promise._handler(this));
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"63":63}],55:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function generate(Promise) {

		var resolve = Promise.resolve;

		Promise.iterate = iterate;
		Promise.unfold = unfold;

		return Promise;

		function iterate(f, condition, handler, x) {
			return unfold(function(x) {
				return [x, f(x)];
			}, condition, handler, x);
		}

		function unfold(unspool, condition, handler, x) {
			return resolve(x).then(function(seed) {
				return resolve(condition(seed)).then(function(done) {
					return done ? seed : resolve(unspool(seed)).spread(next);
				});
			});

			function next(item, newSeed) {
				return resolve(handler(item)).then(function() {
					return unfold(unspool, condition, handler, newSeed);
				});
			}
		}
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],56:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function progress(Promise) {

		Promise.prototype.progress = function(onProgress) {
			return this.then(void 0, void 0, onProgress);
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],57:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var env = require(60);
	var TimeoutError = require(49);

	function setTimeout(f, ms, x, y) {
		return env.setTimer(function() {
			f(x, y, ms);
		}, ms);
	}

	return function timed(Promise) {
		Promise.prototype.delay = function(ms) {
			var p = this._beget();
			this._handler.fold(handleDelay, ms, void 0, p._handler);
			return p;
		};

		function handleDelay(ms, x, h) {
			setTimeout(resolveDelay, ms, x, h);
		}

		function resolveDelay(x, h) {
			h.resolve(x);
		}

		Promise.prototype.timeout = function(ms, reason) {
			var p = this._beget();
			var h = p._handler;

			var t = setTimeout(onTimeout, ms, reason, p._handler);

			this._handler.visit(h,
				function onFulfill(x) {
					env.clearTimer(t);
					this.resolve(x); 
				},
				function onReject(x) {
					env.clearTimer(t);
					this.reject(x); 
				},
				h.notify);

			return p;
		};

		function onTimeout(reason, h, ms) {
			var e = typeof reason === 'undefined'
				? new TimeoutError('timed out after ' + ms + 'ms')
				: reason;
			h.reject(e);
		}

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"49":49,"60":60}],58:[function(require,module,exports){

(function(define) { 'use strict';
define(function(require) {

	var setTimer = require(60).setTimer;
	var format = require(61);

	return function unhandledRejection(Promise) {

		var logError = noop;
		var logInfo = noop;
		var localConsole;

		if(typeof console !== 'undefined') {
			localConsole = console;
			logError = typeof localConsole.error !== 'undefined'
				? function (e) { localConsole.error(e); }
				: function (e) { localConsole.log(e); };

			logInfo = typeof localConsole.info !== 'undefined'
				? function (e) { localConsole.info(e); }
				: function (e) { localConsole.log(e); };
		}

		Promise.onPotentiallyUnhandledRejection = function(rejection) {
			enqueue(report, rejection);
		};

		Promise.onPotentiallyUnhandledRejectionHandled = function(rejection) {
			enqueue(unreport, rejection);
		};

		Promise.onFatalRejection = function(rejection) {
			enqueue(throwit, rejection.value);
		};

		var tasks = [];
		var reported = [];
		var running = null;

		function report(r) {
			if(!r.handled) {
				reported.push(r);
				logError('Potentially unhandled rejection [' + r.id + '] ' + format.formatError(r.value));
			}
		}

		function unreport(r) {
			var i = reported.indexOf(r);
			if(i >= 0) {
				reported.splice(i, 1);
				logInfo('Handled previous rejection [' + r.id + '] ' + format.formatObject(r.value));
			}
		}

		function enqueue(f, x) {
			tasks.push(f, x);
			if(running === null) {
				running = setTimer(flush, 0);
			}
		}

		function flush() {
			running = null;
			while(tasks.length > 0) {
				tasks.shift()(tasks.shift());
			}
		}

		return Promise;
	};

	function throwit(e) {
		throw e;
	}

	function noop() {}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

},{"60":60,"61":61}],59:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return function addWith(Promise) {
		Promise.prototype['with'] = Promise.prototype.withThis = function(receiver) {
			var p = this._beget();
			var child = p._handler;
			child.receiver = receiver;
			this._handler.chain(child, receiver);
			return p;
		};

		return Promise;
	};

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));


},{}],60:[function(require,module,exports){
(function (process){

(function(define) { 'use strict';
define(function(require) {


	var MutationObs;
	var capturedSetTimeout = typeof setTimeout !== 'undefined' && setTimeout;

	var setTimer = function(f, ms) { return setTimeout(f, ms); };
	var clearTimer = function(t) { return clearTimeout(t); };
	var asap = function (f) { return capturedSetTimeout(f, 0); };

	if (isNode()) { 
		asap = function (f) { return process.nextTick(f); };

	} else if (MutationObs = hasMutationObserver()) { 
		asap = initMutationObserver(MutationObs);

	} else if (!capturedSetTimeout) { 
		var vertxRequire = require;
		var vertx = vertxRequire('vertx');
		setTimer = function (f, ms) { return vertx.setTimer(ms, f); };
		clearTimer = vertx.cancelTimer;
		asap = vertx.runOnLoop || vertx.runOnContext;
	}

	return {
		setTimer: setTimer,
		clearTimer: clearTimer,
		asap: asap
	};

	function isNode () {
		return typeof process !== 'undefined' &&
			Object.prototype.toString.call(process) === '[object process]';
	}

	function hasMutationObserver () {
		return (typeof MutationObserver === 'function' && MutationObserver) ||
			(typeof WebKitMutationObserver === 'function' && WebKitMutationObserver);
	}

	function initMutationObserver(MutationObserver) {
		var scheduled;
		var node = document.createTextNode('');
		var o = new MutationObserver(run);
		o.observe(node, { characterData: true });

		function run() {
			var f = scheduled;
			scheduled = void 0;
			f();
		}

		var i = 0;
		return function (f) {
			scheduled = f;
			node.data = (i ^= 1);
		};
	}
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(require); }));

}).call(this,require(23))
},{"23":23}],61:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return {
		formatError: formatError,
		formatObject: formatObject,
		tryStringify: tryStringify
	};

	function formatError(e) {
		var s = typeof e === 'object' && e !== null && e.stack ? e.stack : formatObject(e);
		return e instanceof Error ? s : s + ' (WARNING: non-Error used)';
	}

	function formatObject(o) {
		var s = String(o);
		if(s === '[object Object]' && typeof JSON !== 'undefined') {
			s = tryStringify(o, s);
		}
		return s;
	}

	function tryStringify(x, defaultValue) {
		try {
			return JSON.stringify(x);
		} catch(e) {
			return defaultValue;
		}
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],62:[function(require,module,exports){
(function (process){

(function(define) { 'use strict';
define(function() {

	return function makePromise(environment) {

		var tasks = environment.scheduler;
		var emitRejection = initEmitRejection();

		var objectCreate = Object.create ||
			function(proto) {
				function Child() {}
				Child.prototype = proto;
				return new Child();
			};

		function Promise(resolver, handler) {
			this._handler = resolver === Handler ? handler : init(resolver);
		}

		function init(resolver) {
			var handler = new Pending();

			try {
				resolver(promiseResolve, promiseReject, promiseNotify);
			} catch (e) {
				promiseReject(e);
			}

			return handler;

			function promiseResolve (x) {
				handler.resolve(x);
			}
			function promiseReject (reason) {
				handler.reject(reason);
			}

			function promiseNotify (x) {
				handler.notify(x);
			}
		}


		Promise.resolve = resolve;
		Promise.reject = reject;
		Promise.never = never;

		Promise._defer = defer;
		Promise._handler = getHandler;

		function resolve(x) {
			return isPromise(x) ? x
				: new Promise(Handler, new Async(getHandler(x)));
		}

		function reject(x) {
			return new Promise(Handler, new Async(new Rejected(x)));
		}

		function never() {
			return foreverPendingPromise; 
		}

		function defer() {
			return new Promise(Handler, new Pending());
		}


		Promise.prototype.then = function(onFulfilled, onRejected, onProgress) {
			var parent = this._handler;
			var state = parent.join().state();

			if ((typeof onFulfilled !== 'function' && state > 0) ||
				(typeof onRejected !== 'function' && state < 0)) {
				return new this.constructor(Handler, parent);
			}

			var p = this._beget();
			var child = p._handler;

			parent.chain(child, parent.receiver, onFulfilled, onRejected, onProgress);

			return p;
		};

		Promise.prototype['catch'] = function(onRejected) {
			return this.then(void 0, onRejected);
		};

		Promise.prototype._beget = function() {
			return begetFrom(this._handler, this.constructor);
		};

		function begetFrom(parent, Promise) {
			var child = new Pending(parent.receiver, parent.join().context);
			return new Promise(Handler, child);
		}


		Promise.all = all;
		Promise.race = race;
		Promise._traverse = traverse;

		function all(promises) {
			return traverseWith(snd, null, promises);
		}

		function traverse(f, promises) {
			return traverseWith(tryCatch2, f, promises);
		}

		function traverseWith(tryMap, f, promises) {
			var handler = typeof f === 'function' ? mapAt : settleAt;

			var resolver = new Pending();
			var pending = promises.length >>> 0;
			var results = new Array(pending);

			for (var i = 0, x; i < promises.length && !resolver.resolved; ++i) {
				x = promises[i];

				if (x === void 0 && !(i in promises)) {
					--pending;
					continue;
				}

				traverseAt(promises, handler, i, x, resolver);
			}

			if(pending === 0) {
				resolver.become(new Fulfilled(results));
			}

			return new Promise(Handler, resolver);

			function mapAt(i, x, resolver) {
				if(!resolver.resolved) {
					traverseAt(promises, settleAt, i, tryMap(f, x, i), resolver);
				}
			}

			function settleAt(i, x, resolver) {
				results[i] = x;
				if(--pending === 0) {
					resolver.become(new Fulfilled(results));
				}
			}
		}

		function traverseAt(promises, handler, i, x, resolver) {
			if (maybeThenable(x)) {
				var h = getHandlerMaybeThenable(x);
				var s = h.state();

				if (s === 0) {
					h.fold(handler, i, void 0, resolver);
				} else if (s > 0) {
					handler(i, h.value, resolver);
				} else {
					resolver.become(h);
					visitRemaining(promises, i+1, h);
				}
			} else {
				handler(i, x, resolver);
			}
		}

		Promise._visitRemaining = visitRemaining;
		function visitRemaining(promises, start, handler) {
			for(var i=start; i<promises.length; ++i) {
				markAsHandled(getHandler(promises[i]), handler);
			}
		}

		function markAsHandled(h, handler) {
			if(h === handler) {
				return;
			}

			var s = h.state();
			if(s === 0) {
				h.visit(h, void 0, h._unreport);
			} else if(s < 0) {
				h._unreport();
			}
		}

		function race(promises) {
			if(typeof promises !== 'object' || promises === null) {
				return reject(new TypeError('non-iterable passed to race()'));
			}

			return promises.length === 0 ? never()
				 : promises.length === 1 ? resolve(promises[0])
				 : runRace(promises);
		}

		function runRace(promises) {
			var resolver = new Pending();
			var i, x, h;
			for(i=0; i<promises.length; ++i) {
				x = promises[i];
				if (x === void 0 && !(i in promises)) {
					continue;
				}

				h = getHandler(x);
				if(h.state() !== 0) {
					resolver.become(h);
					visitRemaining(promises, i+1, h);
					break;
				} else {
					h.visit(resolver, resolver.resolve, resolver.reject);
				}
			}
			return new Promise(Handler, resolver);
		}


		function getHandler(x) {
			if(isPromise(x)) {
				return x._handler.join();
			}
			return maybeThenable(x) ? getHandlerUntrusted(x) : new Fulfilled(x);
		}

		function getHandlerMaybeThenable(x) {
			return isPromise(x) ? x._handler.join() : getHandlerUntrusted(x);
		}

		function getHandlerUntrusted(x) {
			try {
				var untrustedThen = x.then;
				return typeof untrustedThen === 'function'
					? new Thenable(untrustedThen, x)
					: new Fulfilled(x);
			} catch(e) {
				return new Rejected(e);
			}
		}

		function Handler() {}

		Handler.prototype.when
			= Handler.prototype.become
			= Handler.prototype.notify 
			= Handler.prototype.fail
			= Handler.prototype._unreport
			= Handler.prototype._report
			= noop;

		Handler.prototype._state = 0;

		Handler.prototype.state = function() {
			return this._state;
		};

		Handler.prototype.join = function() {
			var h = this;
			while(h.handler !== void 0) {
				h = h.handler;
			}
			return h;
		};

		Handler.prototype.chain = function(to, receiver, fulfilled, rejected, progress) {
			this.when({
				resolver: to,
				receiver: receiver,
				fulfilled: fulfilled,
				rejected: rejected,
				progress: progress
			});
		};

		Handler.prototype.visit = function(receiver, fulfilled, rejected, progress) {
			this.chain(failIfRejected, receiver, fulfilled, rejected, progress);
		};

		Handler.prototype.fold = function(f, z, c, to) {
			this.when(new Fold(f, z, c, to));
		};

		function FailIfRejected() {}

		inherit(Handler, FailIfRejected);

		FailIfRejected.prototype.become = function(h) {
			h.fail();
		};

		var failIfRejected = new FailIfRejected();

		function Pending(receiver, inheritedContext) {
			Promise.createContext(this, inheritedContext);

			this.consumers = void 0;
			this.receiver = receiver;
			this.handler = void 0;
			this.resolved = false;
		}

		inherit(Handler, Pending);

		Pending.prototype._state = 0;

		Pending.prototype.resolve = function(x) {
			this.become(getHandler(x));
		};

		Pending.prototype.reject = function(x) {
			if(this.resolved) {
				return;
			}

			this.become(new Rejected(x));
		};

		Pending.prototype.join = function() {
			if (!this.resolved) {
				return this;
			}

			var h = this;

			while (h.handler !== void 0) {
				h = h.handler;
				if (h === this) {
					return this.handler = cycle();
				}
			}

			return h;
		};

		Pending.prototype.run = function() {
			var q = this.consumers;
			var handler = this.handler;
			this.handler = this.handler.join();
			this.consumers = void 0;

			for (var i = 0; i < q.length; ++i) {
				handler.when(q[i]);
			}
		};

		Pending.prototype.become = function(handler) {
			if(this.resolved) {
				return;
			}

			this.resolved = true;
			this.handler = handler;
			if(this.consumers !== void 0) {
				tasks.enqueue(this);
			}

			if(this.context !== void 0) {
				handler._report(this.context);
			}
		};

		Pending.prototype.when = function(continuation) {
			if(this.resolved) {
				tasks.enqueue(new ContinuationTask(continuation, this.handler));
			} else {
				if(this.consumers === void 0) {
					this.consumers = [continuation];
				} else {
					this.consumers.push(continuation);
				}
			}
		};

		Pending.prototype.notify = function(x) {
			if(!this.resolved) {
				tasks.enqueue(new ProgressTask(x, this));
			}
		};

		Pending.prototype.fail = function(context) {
			var c = typeof context === 'undefined' ? this.context : context;
			this.resolved && this.handler.join().fail(c);
		};

		Pending.prototype._report = function(context) {
			this.resolved && this.handler.join()._report(context);
		};

		Pending.prototype._unreport = function() {
			this.resolved && this.handler.join()._unreport();
		};

		function Async(handler) {
			this.handler = handler;
		}

		inherit(Handler, Async);

		Async.prototype.when = function(continuation) {
			tasks.enqueue(new ContinuationTask(continuation, this));
		};

		Async.prototype._report = function(context) {
			this.join()._report(context);
		};

		Async.prototype._unreport = function() {
			this.join()._unreport();
		};

		function Thenable(then, thenable) {
			Pending.call(this);
			tasks.enqueue(new AssimilateTask(then, thenable, this));
		}

		inherit(Pending, Thenable);

		function Fulfilled(x) {
			Promise.createContext(this);
			this.value = x;
		}

		inherit(Handler, Fulfilled);

		Fulfilled.prototype._state = 1;

		Fulfilled.prototype.fold = function(f, z, c, to) {
			runContinuation3(f, z, this, c, to);
		};

		Fulfilled.prototype.when = function(cont) {
			runContinuation1(cont.fulfilled, this, cont.receiver, cont.resolver);
		};

		var errorId = 0;

		function Rejected(x) {
			Promise.createContext(this);

			this.id = ++errorId;
			this.value = x;
			this.handled = false;
			this.reported = false;

			this._report();
		}

		inherit(Handler, Rejected);

		Rejected.prototype._state = -1;

		Rejected.prototype.fold = function(f, z, c, to) {
			to.become(this);
		};

		Rejected.prototype.when = function(cont) {
			if(typeof cont.rejected === 'function') {
				this._unreport();
			}
			runContinuation1(cont.rejected, this, cont.receiver, cont.resolver);
		};

		Rejected.prototype._report = function(context) {
			tasks.afterQueue(new ReportTask(this, context));
		};

		Rejected.prototype._unreport = function() {
			if(this.handled) {
				return;
			}
			this.handled = true;
			tasks.afterQueue(new UnreportTask(this));
		};

		Rejected.prototype.fail = function(context) {
			this.reported = true;
			emitRejection('unhandledRejection', this);
			Promise.onFatalRejection(this, context === void 0 ? this.context : context);
		};

		function ReportTask(rejection, context) {
			this.rejection = rejection;
			this.context = context;
		}

		ReportTask.prototype.run = function() {
			if(!this.rejection.handled && !this.rejection.reported) {
				this.rejection.reported = true;
				emitRejection('unhandledRejection', this.rejection) ||
					Promise.onPotentiallyUnhandledRejection(this.rejection, this.context);
			}
		};

		function UnreportTask(rejection) {
			this.rejection = rejection;
		}

		UnreportTask.prototype.run = function() {
			if(this.rejection.reported) {
				emitRejection('rejectionHandled', this.rejection) ||
					Promise.onPotentiallyUnhandledRejectionHandled(this.rejection);
			}
		};


		Promise.createContext
			= Promise.enterContext
			= Promise.exitContext
			= Promise.onPotentiallyUnhandledRejection
			= Promise.onPotentiallyUnhandledRejectionHandled
			= Promise.onFatalRejection
			= noop;


		var foreverPendingHandler = new Handler();
		var foreverPendingPromise = new Promise(Handler, foreverPendingHandler);

		function cycle() {
			return new Rejected(new TypeError('Promise cycle'));
		}


		function ContinuationTask(continuation, handler) {
			this.continuation = continuation;
			this.handler = handler;
		}

		ContinuationTask.prototype.run = function() {
			this.handler.join().when(this.continuation);
		};

		function ProgressTask(value, handler) {
			this.handler = handler;
			this.value = value;
		}

		ProgressTask.prototype.run = function() {
			var q = this.handler.consumers;
			if(q === void 0) {
				return;
			}

			for (var c, i = 0; i < q.length; ++i) {
				c = q[i];
				runNotify(c.progress, this.value, this.handler, c.receiver, c.resolver);
			}
		};

		function AssimilateTask(then, thenable, resolver) {
			this._then = then;
			this.thenable = thenable;
			this.resolver = resolver;
		}

		AssimilateTask.prototype.run = function() {
			var h = this.resolver;
			tryAssimilate(this._then, this.thenable, _resolve, _reject, _notify);

			function _resolve(x) { h.resolve(x); }
			function _reject(x)  { h.reject(x); }
			function _notify(x)  { h.notify(x); }
		};

		function tryAssimilate(then, thenable, resolve, reject, notify) {
			try {
				then.call(thenable, resolve, reject, notify);
			} catch (e) {
				reject(e);
			}
		}

		function Fold(f, z, c, to) {
			this.f = f; this.z = z; this.c = c; this.to = to;
			this.resolver = failIfRejected;
			this.receiver = this;
		}

		Fold.prototype.fulfilled = function(x) {
			this.f.call(this.c, this.z, x, this.to);
		};

		Fold.prototype.rejected = function(x) {
			this.to.reject(x);
		};

		Fold.prototype.progress = function(x) {
			this.to.notify(x);
		};


		function isPromise(x) {
			return x instanceof Promise;
		}

		function maybeThenable(x) {
			return (typeof x === 'object' || typeof x === 'function') && x !== null;
		}

		function runContinuation1(f, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.become(h);
			}

			Promise.enterContext(h);
			tryCatchReject(f, h.value, receiver, next);
			Promise.exitContext();
		}

		function runContinuation3(f, x, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.become(h);
			}

			Promise.enterContext(h);
			tryCatchReject3(f, x, h.value, receiver, next);
			Promise.exitContext();
		}

		function runNotify(f, x, h, receiver, next) {
			if(typeof f !== 'function') {
				return next.notify(x);
			}

			Promise.enterContext(h);
			tryCatchReturn(f, x, receiver, next);
			Promise.exitContext();
		}

		function tryCatch2(f, a, b) {
			try {
				return f(a, b);
			} catch(e) {
				return reject(e);
			}
		}

		function tryCatchReject(f, x, thisArg, next) {
			try {
				next.become(getHandler(f.call(thisArg, x)));
			} catch(e) {
				next.become(new Rejected(e));
			}
		}

		function tryCatchReject3(f, x, y, thisArg, next) {
			try {
				f.call(thisArg, x, y, next);
			} catch(e) {
				next.become(new Rejected(e));
			}
		}

		function tryCatchReturn(f, x, thisArg, next) {
			try {
				next.notify(f.call(thisArg, x));
			} catch(e) {
				next.notify(e);
			}
		}

		function inherit(Parent, Child) {
			Child.prototype = objectCreate(Parent.prototype);
			Child.prototype.constructor = Child;
		}

		function snd(x, y) {
			return y;
		}

		function noop() {}

		function initEmitRejection() {
			if(typeof process !== 'undefined' && process !== null
				&& typeof process.emit === 'function') {
				return function(type, rejection) {
					return type === 'unhandledRejection'
						? process.emit(type, rejection.value, rejection)
						: process.emit(type, rejection);
				};
			} else if(typeof self !== 'undefined' && typeof CustomEvent === 'function') {
				return (function(noop, self, CustomEvent) {
					var hasCustomEvent = false;
					try {
						var ev = new CustomEvent('unhandledRejection');
						hasCustomEvent = ev instanceof CustomEvent;
					} catch (e) {}

					return !hasCustomEvent ? noop : function(type, rejection) {
						var ev = new CustomEvent(type, {
							detail: {
								reason: rejection.value,
								key: rejection
							},
							bubbles: false,
							cancelable: true
						});

						return !self.dispatchEvent(ev);
					};
				}(noop, self, CustomEvent));
			}

			return noop;
		}

		return Promise;
	};
});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

}).call(this,require(23))
},{"23":23}],63:[function(require,module,exports){

(function(define) { 'use strict';
define(function() {

	return {
		pending: toPendingState,
		fulfilled: toFulfilledState,
		rejected: toRejectedState,
		inspect: inspect
	};

	function toPendingState() {
		return { state: 'pending' };
	}

	function toRejectedState(e) {
		return { state: 'rejected', reason: e };
	}

	function toFulfilledState(x) {
		return { state: 'fulfilled', value: x };
	}

	function inspect(handler) {
		var state = handler.state();
		return state === 0 ? toPendingState()
			 : state > 0   ? toFulfilledState(handler.value)
			               : toRejectedState(handler.value);
	}

});
}(typeof define === 'function' && define.amd ? define : function(factory) { module.exports = factory(); }));

},{}],64:[function(require,module,exports){

(function(define) { 'use strict';
define(function (require) {

	var timed = require(57);
	var array = require(51);
	var flow = require(52);
	var fold = require(53);
	var inspect = require(54);
	var generate = require(55);
	var progress = require(56);
	var withThis = require(59);
	var unhandledRejection = require(58);
	var TimeoutError = require(49);

	var Promise = [array, flow, fold, generate, progress,
		inspect, withThis, timed, unhandledRejection]
		.reduce(function(Promise, feature) {
			return feature(Promise);
		}, require(47));

	var apply = require(50)(Promise);


	when.promise     = promise;              
	when.resolve     = Promise.resolve;      
	when.reject      = Promise.reject;       

	when.lift        = lift;                 
	when['try']      = attempt;              
	when.attempt     = attempt;              

	when.iterate     = Promise.iterate;      
	when.unfold      = Promise.unfold;       

	when.join        = join;                 

	when.all         = all;                  
	when.settle      = settle;               

	when.any         = lift(Promise.any);    
	when.some        = lift(Promise.some);   
	when.race        = lift(Promise.race);   

	when.map         = map;                  
	when.filter      = filter;               
	when.reduce      = lift(Promise.reduce);       
	when.reduceRight = lift(Promise.reduceRight);  

	when.isPromiseLike = isPromiseLike;      

	when.Promise     = Promise;              
	when.defer       = defer;                


	when.TimeoutError = TimeoutError;

	function when(x, onFulfilled, onRejected, onProgress) {
		var p = Promise.resolve(x);
		if (arguments.length < 2) {
			return p;
		}

		return p.then(onFulfilled, onRejected, onProgress);
	}

	function promise(resolver) {
		return new Promise(resolver);
	}

	function lift(f) {
		return function() {
			for(var i=0, l=arguments.length, a=new Array(l); i<l; ++i) {
				a[i] = arguments[i];
			}
			return apply(f, this, a);
		};
	}

	function attempt(f ) {
		for(var i=0, l=arguments.length-1, a=new Array(l); i<l; ++i) {
			a[i] = arguments[i+1];
		}
		return apply(f, this, a);
	}

	function defer() {
		return new Deferred();
	}

	function Deferred() {
		var p = Promise._defer();

		function resolve(x) { p._handler.resolve(x); }
		function reject(x) { p._handler.reject(x); }
		function notify(x) { p._handler.notify(x); }

		this.promise = p;
		this.resolve = resolve;
		this.reject = reject;
		this.notify = notify;
		this.resolver = { resolve: resolve, reject: reject, notify: notify };
	}

	function isPromiseLike(x) {
		return x && typeof x.then === 'function';
	}

	function join() {
		return Promise.all(arguments);
	}

	function all(promises) {
		return when(promises, Promise.all);
	}

	function settle(promises) {
		return when(promises, Promise.settle);
	}

	function map(promises, mapFunc) {
		return when(promises, function(promises) {
			return Promise.map(promises, mapFunc);
		});
	}

	function filter(promises, predicate) {
		return when(promises, function(promises) {
			return Promise.filter(promises, predicate);
		});
	}

	return when;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });

},{"47":47,"49":49,"50":50,"51":51,"52":52,"53":53,"54":54,"55":55,"56":56,"57":57,"58":58,"59":59}],65:[function(require,module,exports){

module.exports.AddressType = {
    GLOBAL : 1,
    LOCAL : 2,
    LOOPBACK : 3,
    UNKNOWN : 4
};
},{}],66:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('ClientSummary', [
    'principal',
    'clientType',
    'transportType'
]);


module.exports.ClientType = {
    JAVASCRIPT_BROWSER : 0,
    JAVASCRIPT_FLASH : 1,
    JAVASCRIPT_SILVERLIGHT : 2,
    ANDROID : 3,
    IOS : 4,
    J2ME : 5,
    FLASH : 6,
    SILVERLIGHT : 7,
    JAVA : 8,
    DOTNET : 9,
    C : 10,
    INTERNAL : 11,
    OTHER : 12
};

module.exports.TransportType = {
    WEBSOCKET : 0,
    HTTP_LONG_POLL : 1,
    IFRAME_LONG_POLL : 2,
    IFRAME_STREAMING : 3,
    DPT : 4,
    HTTPC : 5,
    HTTPC_DUPLEX : 6,
    OTHER : 7
};
},{"415":415}],67:[function(require,module,exports){
var CloseReason = require(112);


module.exports = {
    CLOSED_BY_CLIENT: new CloseReason(0, "The session was closed by the client", false),
    CLOSED_BY_SERVER : new CloseReason(1, "The session was closed by the server", false),
    RECONNECT_ABORTED : new CloseReason(2, "Client aborted a reconnect attempt", false),
    CONNECTION_TIMEOUT : new CloseReason(3, "The connection attempt timed out", false),
    HANDSHAKE_REJECTED : new CloseReason(4, "The connection handshake was rejected by the server", false),
    HANDSHAKE_ERROR : new CloseReason(5, "There was an error parsing the handshake response", false),
    TRANSPORT_ERROR : new CloseReason(6, "There was an unexpected error with the connection", true),
    CONNECTION_ERROR : new CloseReason(7, "A connection to the server was unable to be established", true),
    IDLE_CONNECTION : new CloseReason(8, "The activity monitor detected the connection was idle", true),
    LOST_MESSAGES : new CloseReason(16, "Loss of messages has been detected", false),
    ACCESS_DENIED : new CloseReason(99, "The connect attempt was rejected due to a security restraint", false)
};

},{"112":112}],68:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('SessionDetails', [
    'available',
    'connector',
    'server',
    'location',
    'summary'
]);

module.exports.DetailType = {
    SUMMARY : 0,
    LOCATION : 1,
    CONNECTOR_NAME : 2,
    SERVER_NAME : 3
};
},{"415":415}],69:[function(require,module,exports){
var _interface = require(415)._interface;

var RecordContent = _interface('RecordContent', [
    'get',

    'records',

    'forEach'
]);

RecordContent.Record = _interface('Record', [
    'get',

    'fields',

    'forEach'
]);

RecordContent.Builder = _interface('RecordContentBuilder', [
    'add',

    'set',

    'build',

    'addAndBuild',

    'setAndBuild'
]);

RecordContent.Builder.Record = _interface('RecordContentRecordBuilder', [
    'add',

    'set'
]);

module.exports = RecordContent;

},{"415":415}],70:[function(require,module,exports){
var _interface = require(415)._interface;
var DataType = require(74);

module.exports = _interface('BinaryDataType', DataType, [
    'from'
]);


},{"415":415,"74":74}],71:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('BinaryDelta', [
    'hasChanges'
]);

},{"415":415}],72:[function(require,module,exports){
var _interface = require(415)._interface;
var Bytes = require(73);

module.exports = _interface('Binary', Bytes, [
    'get',

    'diff',

    'apply'
]);

},{"415":415,"73":73}],73:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('Bytes', [
    'length',

    'asBuffer',

    'copyTo'
]);

},{"415":415}],74:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('DataType', [
    'name',

    'readValue',

    'writeValue',


    'canReadAs',

    'readAs',

    'deltaType'
]);

},{"415":415}],75:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('DataTypes', [
    'binary',

    'json',

    'int64',

    'string',

    'double',

    'recordv2',

    'get',

    'getByClass'
]);

},{"415":415}],76:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('DeltaType', [

    'name',

    'diff',

    'apply',

    'readDelta',

    'writeDelta',

    'noChange',

    'isValueCheaper'
]);

},{"415":415}],77:[function(require,module,exports){
var _interface = require(415)._interface;
var DataType = require(74);

module.exports = _interface('JSONDataType', DataType, [

    'from',

    'fromJsonString'
]);


},{"415":415,"74":74}],78:[function(require,module,exports){
var _interface = require(415)._interface;
var Bytes = require(73);

module.exports = _interface('JSON', Bytes, [

    'get',

    'diff',

    'jsonDiff',

    'apply'
]);

},{"415":415,"73":73}],79:[function(require,module,exports){
var _interface = require(415)._interface;
var DataType = require(74);

module.exports = _interface('DoubleDataType', DataType, [

]);
},{"415":415,"74":74}],80:[function(require,module,exports){
var _interface = require(415)._interface;
var DataType = require(74);

module.exports = _interface('Int64DataType', DataType, [

]);
},{"415":415,"74":74}],81:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('Int64', [
    'toString',

    'toNumber'
]);
},{"415":415}],82:[function(require,module,exports){
var _interface = require(415)._interface;
var DataType = require(74);

module.exports = _interface('StringDataType', DataType, [

]);
},{"415":415,"74":74}],83:[function(require,module,exports){
var _interface = require(415)._interface;
var DataType = require(74);


module.exports = _interface('RecordV2DataType', DataType, [
    'withSchema',

    'parseSchema',

    'valueBuilder',

    'schemaBuilder'
]);
},{"415":415,"74":74}],84:[function(require,module,exports){
var _interface = require(415)._interface;
var Bytes = require(73);

module.exports = _interface('RecordV2', Bytes, [
    'diff',

    'asModel',

    'asValidatedModel',

    'asRecords',

    'asFields'
]);
},{"415":415,"73":73}],85:[function(require,module,exports){
var InternalSessionFactory = require(115);
var SessionImpl = require(383);

var DataTypes = require(144);
var Metadata = require(207);
var Topics = require(435);
var TopicSelectors = require(432);
var ErrorReason = require(87);
var ErrorReport = require(294);
var ClientControlOptions = require(94);

var log = require(416);
log.setLevel('SILENT');

var diffusion = {
    version: '6.0.2',

    build: '2_01#52512',

    log: function (level) {
        log.setLevel(level);
    },

    connect: function (options) {
        return SessionImpl.create(InternalSessionFactory, options);
    },

    datatypes: DataTypes,

    selectors: TopicSelectors,

    metadata: Metadata,

    topics: Topics,

    errors: ErrorReason,

    errorReport: ErrorReport,

    clients: ClientControlOptions
};

module.exports = diffusion;

},{"115":115,"144":144,"207":207,"294":294,"383":383,"416":416,"432":432,"435":435,"87":87,"94":94}],86:[function(require,module,exports){
var _interface = require(415)._interface;

var ErrorReport = _interface('ErrorReport', [
    'message',

    'line',

    'column'
]);

module.exports = ErrorReport;

},{"415":415}],87:[function(require,module,exports){
var ErrorReason = require(114);

module.exports = {
    COMMUNICATION_FAILURE : new ErrorReason(100, "Communication with server failed"),

    SESSION_CLOSED : new ErrorReason(101, "Session is closed"),

    REQUEST_TIME_OUT : new ErrorReason(102, "Request time out"),

    ACCESS_DENIED : new ErrorReason(103, "Access denied"),

    UNSUPPORTED : new ErrorReason(104, "Unsupported service"),

    CALLBACK_EXCEPTION :
        new ErrorReason(105, "An application callback threw an exception. Check logs for more information"),

    INVALID_DATA : new ErrorReason(106, "Invalid data"),

    NO_SUCH_SESSION : new ErrorReason(107, "A requested session could not be found"),

    INCOMPATIBLE_DATATYPE : new ErrorReason(108, "Data type is incompatible"),

    UNHANDLED_MESSAGE : new ErrorReason(109, "A message was not handled"),

    TOPIC_TREE_REGISTRATION_CONFLICT :
        new ErrorReason(200, "A conflicting registration exists on the same branch of the topic tree"),

    HANDLER_CONFLICT : new ErrorReason(201, "Conflict with an existing handler"),

    INVALID_PATH : new ErrorReason(202, "Invalid path"),

    REJECTED_REQUEST : new ErrorReason(9003, "A request has been rejected by the recipient session")
};
},{"114":114}],88:[function(require,module,exports){
var _interface = require(415)._interface;
var Stream = require(90);

module.exports = _interface('FetchStream', Stream, [

]);
},{"415":415,"90":90}],89:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('Result', [
    'then'
]);



},{"415":415}],90:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('Stream', [
    'on',

    'off',

    'close'


]);

},{"415":415}],91:[function(require,module,exports){
var _interface = require(415)._interface;
var Stream = require(90);

module.exports = _interface('Subscription', Stream, [
    'selector',

    'asType',

    'view',

    'transform',

    'close'






]);

},{"415":415,"90":90}],92:[function(require,module,exports){
var _interface = require(415)._interface;
var Stream = require(90);

module.exports = _interface('ValueStream', Stream, [
    'selector',

    'close'





]);

},{"415":415,"90":90}],93:[function(require,module,exports){
var _interface = require(415)._interface;
var Stream = require(90);

module.exports = _interface('View', Stream, [
    'get'
]);

},{"415":415,"90":90}],94:[function(require,module,exports){
var TransportType = require(66).TransportType;
var ClientType = require(66).ClientType;

var AddressType = require(65).AddressType;
var DetailType = require(68).DetailType;

var CloseReason = require(67);


var GetSessionPropertiesKeys = {
    ALL_FIXED_PROPERTIES: ['*F'],
    ALL_USER_PROPERTIES: ['*U'],
    ALL_PROPERTIES: ['*F', '*U']
};

module.exports = {
    PropertyKeys: GetSessionPropertiesKeys,
    TransportType : TransportType,
    AddressType : AddressType,
    ClientType : ClientType,
    DetailType : DetailType,
    CloseReason : CloseReason,

    ANONYMOUS : ""
};

},{"65":65,"66":66,"67":67,"68":68}],95:[function(require,module,exports){
var _interface = require(415)._interface;

var ClientControl = _interface('ClientControl', [
    'close',

    'subscribe',

    'unsubscribe',

    'getSessionProperties',

    'setSessionPropertiesListener'
]);

ClientControl.SessionPropertiesListener = _interface('SessionPropertiesListener', [
    'onActive',

    'onClose',

    'onError',

    'onSessionOpen',

    'onSessionEvent',

    'onSessionClose'
]);



ClientControl.SessionEventType = {
    UPDATED : 0,
    RECONNECTED : 1,
    FAILED_OVER : 2,
    DISCONNECTED : 3
};

module.exports = ClientControl;

},{"415":415}],96:[function(require,module,exports){
var _interface = require(415)._interface;

var Messages = _interface('Messaging', [
    'send',

    'listen',

    'addHandler',

    'addRequestHandler',

    'sendRequest',

    'sendRequestToFilter',

    'setRequestStream',

    'removeRequestStream'
]);

Messages.MessageStream = _interface('MessageStream',[
]);

Messages.MessageHandler = _interface('MessageHandler', [
    'onMessage',

    'onActive',

    'onClose'
]);

Messages.RequestStream = _interface('RequestStream', [
    'onRequest',

    'orError',

    'onClose'
]);

Messages.RequestHandler = _interface('RequestHandler', [
    'onRequest',

    'onError',

    'onClose'
]);

Messages.FilteredResponseHandler = _interface('FilteredResponseHandler', [
    'onResponse',

    'onResponseError',

    'onError',

    'onClose'
]);

Messages.Responder = _interface('Responder', [
    'respond'
]);

Messages.Registration = _interface('Registration', [
    'close'
]);


Messages.Priority = {
    NORMAL : 0,
    HIGH : 1,
    LOW : 2
};

Messages.Message = {};

Messages.SessionMessage = {};



module.exports = Messages;

},{"415":415}],97:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('Pings', [
    'pingServer'

]);
},{"415":415}],98:[function(require,module,exports){
var _interface = require(415)._interface;


var SystemPrincipal = _interface('SystemPrincipal', ['name', 'roles']);



var Role = _interface('Role', ['name', 'global', 'default', 'topic', 'inherits']);

var SecurityConfiguration = _interface('SecurityConfiguration', ['named', 'anonymous', 'roles']);

var SystemAuthenticationConfiguration = _interface('SystemAuthenticationConfiguration', ['principals', 'anonymous']);

var SecurityScriptBuilder = _interface('SecurityScriptBuilder', [
    'build',

    'setRolesForAnonymousSessions',

    'setRolesForNamedSessions',

    'setGlobalPermissions',

    'setDefaultTopicPermissions',

    'removeTopicPermissions',

    'setTopicPermissions',

    'setRoleIncludes'
]);


var AuthenticationHandler = _interface('AuthenticationHandler', [
    'onAuthenticate',

    'onActive',

    'onClose',

    'onError'
]);

AuthenticationHandler.Callback = _interface('AuthenticationHandlerCallback', [
    'allow',
    'abstain',
    'deny'
]);

var SystemAuthenticationScriptBuilder = _interface('SystemAuthenticationScriptBuilder', [
    'build',

    'assignRoles',

    'addPrincipal',

    'setPassword',

    'verifyPassword',

    'removePrincipal',

    'allowAnonymousConnections',

    'denyAnonymousConnections',

    'abstainAnonymousConnections'
]);

var Security = _interface('Security', [
    'getPrincipal',

    'changePrincipal',

    'getSecurityConfiguration',

    'getSystemAuthenticationConfiguration',

    'updateSecurityStore',

    'updateAuthenticationStore',

    'securityScriptBuilder',

    'authenticationScriptBuilder',

    'setAuthenticationHandler'
]);

Security.AuthenticationHandler = AuthenticationHandler;

Security.SecurityScriptBuilder = SecurityScriptBuilder;
Security.SystemAuthenticationScriptBuilder = SystemAuthenticationScriptBuilder;

Security.SecurityConfiguration = SecurityConfiguration;
Security.SystemAuthenticationConfiguration = SystemAuthenticationConfiguration;

Security.Role = Role;
Security.SystemPrincipal = SystemPrincipal;

module.exports = Security;


},{"415":415}],99:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('TimeSeries', [
    'append',

    'edit',

    'rangeQuery'
]);

module.exports.RangeQuery = _interface('RangeQuery', [
    'forValues',

    'forEdits',

    'editRange',

    'allEdits',

    'latestEdits',

    'from',

    'fromStart',

    'fromLast',

    'fromLastMillis',

    'to',

    'toStart',

    'next',

    'nextMillis',

    'previous',

    'previousMillis',

    'untilLast',

    'untilLastMillis',

    'limit',

    'as',

    'selectFrom'
]);

module.exports.EventMetadata = _interface('EventMetadata', [
    'sequence',

    'timestamp',

    'author'
]);

module.exports.Event = _interface('Event', [
    'value',

    'originalEvent',

    'isEditEvent'
]);

module.exports.QueryResult = _interface('QueryResult', [
    'selectedCount',

    'stream',

    'isComplete',

    'streamStructure',

    'merge'
]);


},{"415":415}],100:[function(require,module,exports){
var _interface = require(415)._interface;


module.exports.TopicControl = _interface('TopicControl', [
    'add',

    'remove',

    'removeSelector',

    'removeWithSession',

    'update',

    'registerUpdateSource',

    'addMissingTopicHandler'
]);

module.exports.MissingTopicHandler = _interface('MissingTopicHandler', [
    'onMissingTopic',

    'onRegister',

    'onClose',

    'onError'
]);

module.exports.MissingTopicNotification = _interface('MissingTopicNotification', [
    'path',

    'selector',

    'sessionID',

    'proceed',

    'cancel'
]);

module.exports.TopicUpdateHandler = _interface('TopicUpdateHandler', [
    'onRegister',

    'onActive',

    'onStandBy',

    'onClose'
]);

module.exports.Updater = _interface('Updater', [
    'update'
]);



},{"415":415}],101:[function(require,module,exports){
var _interface = require(415)._interface;


module.exports.TopicNotifications = _interface('TopicNotifications', [
    'addListener'
]);

module.exports.TopicNotificationListener = _interface('TopicNotificationListener', [
    'onDescendantNotification',

    'onTopicNotification',

    'onClose',

    'onError'
]);

module.exports.TopicNotificationRegistration = _interface('TopicNotificationRegistration', [
    'select',

    'deselect',

    'close'
]);

function TopicNotificationType(id) {
    this.id = id;
}

module.exports.TopicNotificationType = {
    ADDED : new TopicNotificationType(0),

    SELECTED : new TopicNotificationType(1),

    REMOVED : new TopicNotificationType(2),

    DESELECTED : new TopicNotificationType(3)
};
},{"415":415}],102:[function(require,module,exports){
var _interface = require(415)._interface;

var Topics = _interface('Topics', [
    'subscribe',

    'unsubscribe',

    'stream',

    'view',

    'fetch'
]);

module.exports = Topics;

},{"415":415}],103:[function(require,module,exports){
var _interface = require(415)._interface;

var Metadata = _interface('Metadata', [
    'String',

    'Integer',

    'Decimal',

    'Stateless',

    'RecordContent'
]);


Metadata.Stateless = _interface('Stateless', []);

Metadata.String = _interface('String', [
    'value'
]);

Metadata.Integer = _interface('Integer', [
    'value'
]);

Metadata.Decimal = _interface('Decimal', [
    'value',
    'scale'
]);

Metadata.RecordContent = _interface('RecordContent', [
    'occurs',

    'addRecord',

    'getRecord',

    'getRecords',

    'string',

    'integer',

    'decimal',

    'builder',

    'parse'
]);

Metadata.RecordContent.Record = _interface('Record', [
    'name',

    'occurs',

    'addField',

    'getField',

    'getFields'
]);

Metadata.RecordContent.Field = _interface('MField', [
    'name',
    'type',
    'occurs'
]);

module.exports = Metadata;

},{"415":415}],104:[function(require,module,exports){
var ConnectionActivityMonitor = require(105);

var pingTimeoutFactor = 2;

var noop = {
    onSystemPing: function () {},
    shutdown: function () {}
};

function connectionActivityMonitorFactory(connection, response) {
    if (response.systemPingPeriod > 0) {
        var pingTimeout = response.systemPingPeriod * pingTimeoutFactor;
        return new ConnectionActivityMonitor(pingTimeout, connection);
    }
    else {
        return noop;
    }
}

module.exports = connectionActivityMonitorFactory;

},{"105":105}],105:[function(require,module,exports){
function ConnectionActivityMonitor(pingTimeout, connection) {
    var currentTimeout = null;

    this.onSystemPing = function() {
        if (currentTimeout !== null) {
            clearTimeout(currentTimeout);
            currentTimeout = setTimeout(connection.closeIdleConnection.bind(connection), pingTimeout);
        }
    };

    this.shutdown = function() {
        if (currentTimeout !== null) {
            clearTimeout(currentTimeout);
            currentTimeout = null;
        }
    };

    currentTimeout = setTimeout(connection.closeIdleConnection.bind(connection), pingTimeout);
}

module.exports = ConnectionActivityMonitor;

},{}],106:[function(require,module,exports){
function SessionActivityMonitor(connectionActivityMonitorFactory) {
    var currentConnectionMonitor = null;

    this.onNewConnection = function(connection, response) {
        currentConnectionMonitor = connectionActivityMonitorFactory(connection, response);
    };

    this.onConnectionClosed = function() {
        if (currentConnectionMonitor !== null) {
            currentConnectionMonitor.shutdown();
            currentConnectionMonitor = null;
        }
    };

    this.onSystemPing = function() {
        if (currentConnectionMonitor !== null) {
            currentConnectionMonitor.onSystemPing();
        }
    };
}

module.exports = {
    NOOP : {
        onNewConnection : function() {},
        onConnectionClosed : function() {},
        onSystemPing : function() {}
    },
    create : function(connectionActivityMonitorFactory) {
        return new SessionActivityMonitor(connectionActivityMonitorFactory);
    }
};

},{}],107:[function(require,module,exports){
module.exports.types = {
    UINT: 0,
    INT: 1,
    BYTES: 2,
    STRING: 3,
    ARRAY: 4,
    MAP: 5,
    SEMANTIC: 6,
    SIMPLE: 7,
    FLOAT: 7
};

module.exports.additional = {
    FALSE: 20,
    TRUE: 21,
    NULL: 22,
    UNDEFINED: 23,
    SIMPLE: 24,
    HALF_PRECISION: 25,
    SINGLE_PRECISION: 26,
    DOUBLE_PRECISION: 27,
    BREAK: 31
};

module.exports.tokens = {
    ARRAY_START: 0,
    ARRAY_END: 1,
    MAP_START: 2,
    MAP_END: 3,
    STRING_START: 4,
    STRING_END: 5,
    VALUE: 6
};

module.exports.isStructStart = function (token) {
    switch (token) {
        case module.exports.tokens.ARRAY_START :
        case module.exports.tokens.MAP_START :
        case module.exports.tokens.STRING_START :
            return true;
        default :
            return false;
    }
};

module.exports.isStructEnd = function (token) {
    switch (token) {
        case module.exports.tokens.ARRAY_END :
        case module.exports.tokens.MAP_END :
        case module.exports.tokens.STRING_END :
            return true;
        default :
            return false;
    }
};

},{}],108:[function(require,module,exports){

function Context() {
    var ROOT = { length : -1, type : 'root', read : 0 };
    var stack = [];
    var tail = ROOT;

    this.type = function() {
        return tail.type;
    };

    this.read = function() {
        return tail.read;
    };

    this.expected = function() {
        return tail.length;
    };

    this.push = function(type, length) {
        length = length === undefined ? -1 : length;

        tail = { length : length,
                 type : type,
                 read : 0 };

        stack.push(tail);
    };

    this.pop = function() {
        var prev = stack.pop();

        if (stack.length) {
            tail = stack[stack.length - 1];
        } else {
            tail = ROOT;
        }

        return prev;
    };

    this.next = function() {
        if (this.hasRemaining()) {
            tail.read++;
        } else {
            throw new Error('Exceeded expected collection limit');
        }
    };

    this.break = function() {
        if (this.acceptsBreakMarker()) {
            tail.length = 0;
            tail.read = 0;
        }
    };

    this.hasRemaining = function() {
        return tail.length === -1 ? true : tail.length > tail.read;
    };

    this.acceptsBreakMarker = function() {
        return tail !== ROOT && tail.length === -1;
    };
}

module.exports = Context;

},{}],109:[function(require,module,exports){
(function (Buffer){
var Tokeniser = require(111);
var consts = require(107);

var tokens = consts.tokens,
    types = consts.types;

function decode(token, tokeniser) {
    switch (token.type) {
        case tokens.VALUE :
            return token.value;
        case tokens.MAP_START :
            var obj = {};

            for (;;) {
                var field = tokeniser.nextToken();

                if (field === null) {
                    throw new Error('Unexpected EOF (reading: map key)');
                }

                if (field.type === tokens.MAP_END) {
                    break;
                }

                var value = tokeniser.nextToken();

                if (value === null || value.type === tokens.MAP_END) {
                    throw new Error('Unexpected EOF (reading: map value)');
                }

                obj[decode(field, tokeniser)] = decode(value, tokeniser);
            }

            return obj;
        case tokens.ARRAY_START :
            var arr = [];

            for (;;) {
                var element = tokeniser.nextToken();

                if (element === null) {
                    throw new Error('Unexpected EOF (reading: array value)');
                }

                if (element.type === tokens.ARRAY_END) {
                    break;
                }

                arr.push(decode(element, tokeniser));
            }

            return arr;
        case tokens.STRING_START :
            var chunks = [];

            for (;;) {
                var chunk = tokeniser.nextToken();

                if (chunk === null) {
                    throw new Error('Unexpected EOF (reading: indefinite-length string');
                }

                if (chunk.type === tokens.STRING_END) {
                    break;
                }

                if (chunk.header.type !== token.header.type) {
                    throw new Error('Unexpected chunk type (' + chunk.header.type + ') within string');
                }

                chunks.push(chunk.value);
            }

            var joined;

            if (token.header.type === types.BYTES) {
                joined = Buffer.concat(chunks);
            }

            if (token.header.type === types.STRING) {
                joined = chunks.join('');
            }

            return joined;
        default :
            throw new Error('Unexpected token: ' + JSON.stringify(token));
    }
}

module.exports = function Decoder(initial, offset, length) {
    var tokeniser = (Buffer.isBuffer(initial)) ? new Tokeniser(initial, offset, length) : initial;

    this.hasRemaining = tokeniser.hasRemaining;

    this.nextValue = function () {
        if (tokeniser.hasRemaining()) {
            return decode(tokeniser.nextToken(), tokeniser);
        } else {
            throw new Error('Token stream exhausted');
        }
    };
};


}).call(this,require(17).Buffer)
},{"107":107,"111":111,"17":17}],110:[function(require,module,exports){
(function (Buffer){
var BufferOutputStream = require(202);
var Int64Impl = require(154);

var consts = require(107);

var additional = consts.additional,
    types = consts.types;

function Encoder(initial) {
    var bos = initial || new BufferOutputStream();
    var self = this;

    function writeByte(b) {
        bos.write(b);
    }

    function writeBuffer(buf, offset, length) {
        offset = offset || 0;
        length = length === undefined ? buf.length : length;

        bos.writeMany(buf, offset, length);
    }

    function writeUint16(v) {
        writeByte((v >> 8) & 0xff);
        writeByte(v & 0xff);
    }

    function writeUint32(v) {
        writeUint16((v >> 16) & 0xffff);
        writeUint16(v & 0xffff);
    }

    function writeUint64(v) {
        writeUint32(Math.floor(v / 4294967296));
        writeUint32(v % 4294967296);
    }

    function writeBreakHeader(type) {
        writeByte(type << 5 | additional.BREAK);
    }

    function writeToken(type, val) {
        var first = type << 5;

        if (val < 24) {
            writeByte(first | val);
        } else if (val < 256) {
            writeByte(first | 24);
            writeByte(val);
        } else if (val < 65536) {
            writeByte(first | 25);
            writeUint16(val);
        } else if (val < 4294967296) {
            writeByte(first | 26);
            writeUint32(val);
        } else {
            writeByte(first | 27);
            writeUint64(val);
        }
    }

    function writeNumber(val) {
        if (val.toString().indexOf(".") > -1) {
            var buf = new Buffer(8);
            buf.writeDoubleBE(val, 0);

            writeByte((types.SIMPLE << 5) | additional.DOUBLE_PRECISION);
            writeBuffer(buf);
        } else {
            if (val < 0) {
                writeToken(types.INT, -1 - val);
            } else {
                writeToken(types.UINT, val);
            }
        }
    }

    function writeString(val) {
        var buf = new Buffer(val, 'utf-8');

        writeToken(types.STRING, buf.length);
        writeBuffer(buf);
    }

    function writeBinary(val, offset, length) {
        length = length === undefined ? val.length : length;

        writeToken(types.BYTES, length);
        writeBuffer(val, offset, length);
    }

    function writeBoolean(val) {
        if (val) {
            writeToken(types.SIMPLE, additional.TRUE);
        } else {
            writeToken(types.SIMPLE, additional.FALSE);
        }
    }

    function writeUndefined() {
        writeToken(types.SIMPLE, additional.UNDEFINED);
    }

    function writeNull() {
        writeToken(types.SIMPLE, additional.NULL);
    }

    function writeObject(val) {
        var keys = Object.keys(val);

        self.startObject(keys.length);

        for (var i = 0; i < keys.length; ++i) {
            self.encode(keys[i]);
            self.encode(val[keys[i]]);
        }
    }

    function writeArray(val) {
        self.startArray(val.length);

        for (var i = 0; i < val.length; ++i) {
            self.encode(val[i]);
        }
    }

    this.break = function () {
        writeBreakHeader(types.SIMPLE);

        return self;
    };

    this.startArray = function (length) {
        if (length === undefined) {
            writeBreakHeader(types.ARRAY);
        } else {
            writeToken(types.ARRAY, length);
        }

        return self;
    };

    this.startObject = function (length) {
        if (length === undefined) {
            writeBreakHeader(types.MAP);
        } else {
            writeToken(types.MAP, length);
        }

        return self;
    };

    this.writeInt64 = function(val) {
        if (val.isNegative()) {
            writeByte((types.INT << 5) | 27);
        } else {
            writeByte((types.UINT << 5) | 27);
        }

        writeBuffer(val.toBuffer());

        return self;
    };

    this.encode = function (value, offset, length) {
        if (value === null) {
            writeNull();
        } else if (value === undefined) {
            writeUndefined();
        } else if (value === true) {
            writeBoolean(true);
        } else if (value === false) {
            writeBoolean(false);
        } else if (Int64Impl.isPrototypeOf(value)) {
            self.writeInt64(value);
        } else {
            switch (typeof value) {
                case 'string' :
                    writeString(value);
                    break;
                case 'number' :
                    writeNumber(value);
                    break;
                case 'object' :
                    if (Buffer.isBuffer(value)) {
                        writeBinary(value, offset, length);
                    } else if (Array.isArray(value)) {
                        writeArray(value);
                    } else {
                        writeObject(value);
                    }
            }
        }

        return self;
    };

    this.flush = function () {
        var res = bos.getBuffer();
        bos = new BufferOutputStream();
        return res;
    };
}

module.exports = Encoder;

}).call(this,require(17).Buffer)
},{"107":107,"154":154,"17":17,"202":202}],111:[function(require,module,exports){
(function (Buffer){
var Int64Impl = require(154);

var Context = require(108);
var consts = require(107);

var additional = consts.additional,
    tokens = consts.tokens,
    types = consts.types;

var BREAK_FLAG = new Error();

var EMPTY_BUFFER = new Buffer([]);

function Tokeniser(data, offset, length) {
    length = length === undefined ? data.length : length;
    offset = offset || 0;

    var context = new Context();
    var token;
    var type;

    var self = this;
    var pos = offset;

    this.reset = function () {
        context = new Context();
        token = undefined;
        type = undefined;
        pos = offset;
    };

    this.hasRemaining = function () {
        var ctx = context.type();
        var len = ctx === 'root' ? length : length + 1;

        return pos < offset + len;
    };

    this.offset = function () {
        return pos;
    };

    this.getContext = function () {
        return context;
    };

    this.getToken = function () {
        return token;
    };

    this.nextToken = function () {
        if (!self.hasRemaining()) {
            return null;
        }

        var ctx = context.type();
        var previousPos = pos;

        if (ctx !== 'root' && !context.hasRemaining()) {
            switch (ctx) {
                case 'object' :
                    type = tokens.MAP_END;
                    break;
                case 'array' :
                    type = tokens.ARRAY_END;
                    break;
                case 'string' :
                    type = tokens.STRING_END;
                    break;
            }

            context.pop();

            return {
                pos: pos,
                type: type,
                getBuffer: function () {
                    return EMPTY_BUFFER;
                }
            };
        } else {
            context.next();
        }

        var header = readHeader();
        var value;

        switch (header.type) {
            case types.INT :
            case types.UINT :
            case types.FLOAT :
            case types.SIMPLE :
                type = tokens.VALUE;
                value = readValue(header);
                break;
            case types.BYTES :
            case types.STRING :
                if (header.raw === additional.BREAK) {
                    context.push('string', -1);
                    type = tokens.STRING_START;
                } else {
                    type = tokens.VALUE;
                    value = readValue(header);
                }

                break;
            case types.ARRAY :
                context.push('array', readCollectionLength(header));
                type = tokens.ARRAY_START;
                break;
            case types.MAP :
                var len = readCollectionLength(header);

                if (len >= 0) {
                    len = len * 2;
                }

                context.push('object', len);
                type = tokens.MAP_START;
                break;
            case types.SEMANTIC :
                return self.nextToken();
            default :
                throw new Error('Unknown CBOR header type: ' + header.type);
        }

        if (value === BREAK_FLAG) {
            if (context.acceptsBreakMarker()) {
                context.break();
                return self.nextToken();
            } else {
                throw new Error("Unexpected break flag outside of indefinite-length context");
            }
        }

        token = {
            pos: previousPos,
            type: type,
            value: value,
            header: header,
            length: pos,
            getBuffer: function () {
                return data.slice(this.pos, this.length);
            }
        };

        return token;
    };


    function readValue(header) {
        switch (header.type) {
            case types.UINT :
                return readHeaderValue(header);
            case types.INT :
                return readHeaderValue(header, true);
            case types.BYTES :
                return readBuffer(readHeaderValue(header));
            case types.STRING :
                return readBuffer(readHeaderValue(header)).toString('utf-8');
            case types.SIMPLE :
                return readSimpleValue(header.raw);
        }
    }

    function readSimpleValue(type) {
        switch (type) {
            case additional.TRUE :
                return true;
            case additional.FALSE :
                return false;
            case additional.NULL :
                return null;
            case additional.BREAK :
                return BREAK_FLAG;
            case additional.HALF_PRECISION :
                return readFloat16();
            case additional.SINGLE_PRECISION :
                return readFloat32();
            case additional.DOUBLE_PRECISION :
                return readFloat64();
        }
    }

    function readByte() {
        if (pos < data.length) {
            return data[pos++];
        } else {
            throw new Error('Exhausted token stream');
        }
    }

    function readBuffer(len) {
        return data.slice(pos, pos += len);
    }

    function readUint16() {
        var res = data.readUInt16BE(pos);
        pos += 2;

        return res;
    }

    function readUint32() {
        var res = data.readUInt32BE(pos);
        pos += 4;

        return res;
    }

    function readUint64(signed) {
        var i = new Int64Impl(readBuffer(8));
        var n = i.toNumber();

        if (n <= 9007199254740991 && n >= -9007199254740991) {
            return signed ? -1 - n : n;
        }

        return i;
    }

    function readFloat16() {
        var h = readUint16();
        var f = (h & 0x03ff);
        var s = (h & 0x8000) >> 15;
        var e = (h & 0x7c00) >> 10;

        var sign = s ? -1 : 1;

        if (e === 0) {
            return sign * Math.pow(2, -14) * (f / Math.pow(2, 10));
        } else if (e === 0x1f) {
            return f ? NaN : sign * Infinity;
        }

        return sign * Math.pow(2, e - 15) * (1 + (f / Math.pow(2, 10)));
    }

    function readFloat32() {
        var res = data.readFloatBE(pos);
        pos += 4;

        return res;
    }

    function readFloat64() {
        var res = data.readDoubleBE(pos);
        pos += 8;

        return res;
    }

    function readHeader() {
        var header = readByte();

        return {
            type : header >> 5,
            raw : header & 0x1F
        };
    }

    function readHeaderValue(header, signed) {
        var low = header.raw;
        var res;

        if (low < 24) {
            res = low;
        } else if (low === additional.SIMPLE) {
            res = readByte();
        } else if (low === additional.HALF_PRECISION) {
            res = readUint16();
        } else if (low === additional.SINGLE_PRECISION) {
            res = readUint32();
        } else if (low === additional.DOUBLE_PRECISION) {
            return readUint64(signed);
        } else if (low === additional.BREAK) {
            return BREAK_FLAG;
        }

        return signed ? -1 - res : res;
    }

    function readCollectionLength(header) {
        var l = readHeaderValue(header);

        if (l === BREAK_FLAG) {
            return -1;
        }

        return l;
    }
}

module.exports = Tokeniser;

}).call(this,require(17).Buffer)
},{"107":107,"108":108,"154":154,"17":17}],112:[function(require,module,exports){

function CloseReason(id, message, canReconnect) {
    this.id = id;
    this.message = message;
    this.canReconnect = canReconnect;

    this.toString = function() {
        return "id: " + message;
    };
}

module.exports = CloseReason;

},{}],113:[function(require,module,exports){
var ErrorReason = require(87);

module.exports.create = function(service) {
    return {
        onRequest : function(internal, message, callback) {
            try {
                service(internal, message, callback);
            } catch (e) {
                callback.fail(ErrorReason.CALLBACK_EXCEPTION, e.message);
            }
        }
    };
};

},{"87":87}],114:[function(require,module,exports){

function ErrorReason(id, reason) {
    this.id = id;
    this.reason = reason;

    this.toString = function() {
        return id + ": " + reason;
    };
}

ErrorReason.prototype = Error;

module.exports = ErrorReason;
},{}],115:[function(require,module,exports){
var InternalSession = require(116);
var ServiceRegistry = require(119);
var ConnectionFactory = require(425);

var ConversationSet = require(137);

var Services = require(325);
var MonitoredPingService = require(120);
var PingService = require(123);
var NotifySubscriptionService = require(121);
var NotifyUnsubscriptionService = require(122);

module.exports = function(options) {
    var serviceRegistry = new ServiceRegistry();

    serviceRegistry.add(Services.USER_PING, PingService);
    serviceRegistry.add(Services.SYSTEM_PING, MonitoredPingService);
    serviceRegistry.add(Services.NOTIFY_SUBSCRIPTION, NotifySubscriptionService);
    serviceRegistry.add(Services.UNSUBSCRIPTION_NOTIFICATION, NotifyUnsubscriptionService);

    return new InternalSession(ConversationSet, serviceRegistry, ConnectionFactory, options);
};
},{"116":116,"119":119,"120":120,"121":121,"122":122,"123":123,"137":137,"325":325,"425":425}],116:[function(require,module,exports){
var Emitter = require(173);

var sessionActivityMonitorModule = require(106);
var ServiceAdapter = require(117);
var ServiceLocator = require(118);

var StreamRegistry = require(217);
var TopicRouting = require(224);
var TopicCache = require(223);

var DataTypes = require(144);

var serialisers = require(324);

var ConnectionRequest = require(213);
var ResponseCode = require(216);

var CloseReason = require(67);
var Transports = require(407);
var Aliases = require(424);

var log = require(416).create('Internal Session');

var curry = require(414).curry;
var FSM = require(413);

var connectionActivityMonitorFactory = require(104);

function InternalSession(conversationSetFactory, serviceRegistry, connectionFactory, opts) {
    var emitter = Emitter.assign(this);

    var principal = "";
    var sessionID;
    var token;
    var sessionActivityMonitor;

    var conversationSet = conversationSetFactory.create();

    var fsm = FSM.create('initialising', {
        initialising: ['connecting'],
        connecting: ['connected', 'closing'],
        connected: ['disconnected', 'closing'],
        disconnected: ['reconnecting', 'closing'],
        reconnecting: ['connected', 'closing'],
        closing: ['closed'],
        closed: []
    });

    fsm.on('change', function (previous, current) {
        log.debug('State changed: ' + previous + ' -> ' + current);
    });

    var self = this;

    this.getState = function () {
        return fsm.state;
    };

    this.getConversationSet = function () {
        return conversationSet;
    };

    this.isConnected = function () {
        return fsm.state === 'connected';
    };

    this.getServiceRegistry = function () {
        return serviceRegistry;
    };

    this.getSessionId = function () {
        return sessionID;
    };

    this.setPrincipal = function (newPrincipal) {
        principal = newPrincipal;
    };

    this.checkConnected = function (emitter) {
        if (self.isConnected()) {
            return true;
        } else {
            emitter.error(new Error('The session is not connected. Operations are not possible at this time.'));
            return false;
        }
    };

    var transports = Transports.create();

    transports.on({
        'transport-selected': function (name) {
            emitter.emit('transport-selected', name);
        },
        cascade: function () {
            emitter.emit('cascade');
        }
    });

    var connection = connectionFactory.create(Aliases.create(), transports, opts.reconnect.timeout, 256);

    var serviceAdapter = new ServiceAdapter(this, serialisers, connection.send);
    var serviceLocator = new ServiceLocator(this, serialisers, serviceAdapter);

    var topicCache = new TopicCache(DataTypes);
    var topicStreamRegistry = new StreamRegistry(topicCache);
    var topicRouting = new TopicRouting(this, serviceAdapter, conversationSet, topicCache, topicStreamRegistry);

    serviceRegistry.addListener(serviceAdapter.addService);

    function close(reason) {
        if (fsm.change('closed')) {
            conversationSet.discardAll(reason);
            emitter.close(reason);
        } else {
            log.debug('Unable to handle session close, session state: ', fsm.state);
        }
    }

    var attempts = 0;
    var reconnect = function (opts) {
        if (fsm.change('reconnecting')) {
            log.info('Reconnect attempt (' + (++attempts) + ')');

            var request = ConnectionRequest.reconnect(
                token,
                connection.getAvailableSequence(),
                connection.lastReceivedSequence);

            connection.connect(request, opts, opts.reconnect.timeout);
        } else {
            log.debug('Unable to attempt reconnect, session state: ', fsm.state);
        }
    };

    var abort = function (reason) {
        if (fsm.change('closing')) {
            log.debug('Aborting reconnect');
            close(reason || CloseReason.RECONNECT_ABORTED);
        } else {
            log.debug('Unable to abort reconnect, session state: ', fsm.state);
        }
    };

    function replaceConversationSet(err) {
        var oldConversationSet = conversationSet;

        conversationSet = conversationSetFactory.create();

        oldConversationSet.discardAll(err);

        log.debug('Replaced conversation set', err);
    }

    this.connect = function () {
        if (fsm.change('connecting')) {
            var reconnectTimeout;

            var request = ConnectionRequest.connect();

            connection.on('data', topicRouting.route);

            connection.on('close', close);

            if (opts.activityMonitor) {
                sessionActivityMonitor = sessionActivityMonitorModule.create(connectionActivityMonitorFactory);
            } else {
                sessionActivityMonitor = sessionActivityMonitorModule.NOOP;
            }

            connection.on('disconnect', function (reason) {
                log.debug('Connection disconnected, reason: ', reason);

                if (fsm.change('disconnected') || fsm.state === 'reconnecting') {
                    sessionActivityMonitor.onConnectionClosed();
                    if (opts.reconnect.timeout > 0 && reason.canReconnect) {
                        opts.token = token;

                        if (fsm.state === 'disconnected') {
                            emitter.emit('disconnect', reason);

                            reconnectTimeout = setTimeout(function () {
                                if (fsm.state !== 'connected') {
                                    abort(CloseReason.CONNECTION_TIMEOUT);
                                }
                            }, opts.reconnect.timeout);
                        }

                        opts.reconnect.strategy(curry(reconnect, opts), abort, reason);
                    } else {
                        abort(reason);
                    }
                } else if (fsm.change('closing')) {
                    close(reason);
                } else {
                    sessionActivityMonitor.onConnectionClosed();
                    log.debug('Unable to handle session disconnect, session state: ', fsm.state);
                }
            });

            connection.on('connect', function (response) {
                if (fsm.change('connected')) {
                    token = response.token;

                    if (response.response === ResponseCode.OK) {
                        sessionID = response.identity;
                        sessionActivityMonitor.onNewConnection(connection, response);
                        emitter.emit('connect', response.identity);
                    } else if (
                        response.response === ResponseCode.RECONNECTED ||
                        response.response === ResponseCode.RECONNECTED_WITH_MESSAGE_LOSS) {

                        attempts = 0;
                        clearTimeout(reconnectTimeout);

                        if (response.response === ResponseCode.RECONNECTED_WITH_MESSAGE_LOSS) {
                            connection.resetSequences();
                            replaceConversationSet(new Error("Peer is disconnected"));

                            topicCache.notifyUnsubscriptionOfAllTopics(topicStreamRegistry);

                            log.info("Reconnected session, but messages may have been lost");
                        } else {
                            log.info('Reconnected session');
                        }

                        sessionActivityMonitor.onNewConnection(connection, response);
                        emitter.emit('reconnect');
                    }
                } else {
                    log.trace('Unknown connection response: ', response);
                }
            });

            log.debug('Connecting with options:', opts);
            log.trace('Connecting with request:', request);

            try {
                connection.connect(request, opts);
            } catch (e) {
                log.warn('Connection error', e);
                emitter.emit('error', e);

                if (fsm.change('closing')) {
                    close(CloseReason.CONNECTION_ERROR);
                }
            }

            if (opts.principal) {
                principal = opts.principal;
            }
        } else {
            log.warn('Unable to connect, session state: ', fsm.state);
            emitter.emit('error', new Error('Unable to connect, session state: ' + fsm.state));
        }
    };

    this.close = function () {
        if (fsm.change('closing')) {
            sessionActivityMonitor.onConnectionClosed();
            connection.close(CloseReason.CLOSED_BY_CLIENT);
        } else {
            log.debug('Unable to close, session state: ', fsm.state);
        }
    };

    this.getErrorHandler = function () {
        return function (error) {
            log.error("Session error:", error.message);
        };
    };

    this.getServiceLocator = function () {
        return serviceLocator;
    };

    this.getServiceAdapter = function () {
        return serviceAdapter;
    };

    this.getPrincipal = function () {
        return principal;
    };

    this.getStreamRegistry = function () {
        return topicStreamRegistry;
    };

    this.getRouting = function () {
        return topicRouting;
    };

    this.onSystemPing = function () {
        sessionActivityMonitor.onSystemPing();
    };
}

module.exports = InternalSession;

},{"104":104,"106":106,"117":117,"118":118,"144":144,"173":173,"213":213,"216":216,"217":217,"223":223,"224":224,"324":324,"407":407,"413":413,"414":414,"416":416,"424":424,"67":67}],117:[function(require,module,exports){
var COMMUNICATION_FAILURE = require(87).COMMUNICATION_FAILURE;
var ErrorReason = require(114);

var ConversationID = require(136);
var CommandHeader = require(247);

var Message = require(429);
var Codec = require(203);

var log = require(416).create('Service Adapter');

function ServiceAdapter(internalSession, serialisers, sender) {
    var NULL_CALLBACK = {
        respond : function() {
        },
        fail : function() {
        }
    };

    var headerSerialiser = serialisers.get(CommandHeader);
    var listeners = {};

    function sendRequest(header, command, serialiser) {
        var msg = Message.create({
            type : Message.types.SERVICE_REQUEST
        });

        headerSerialiser.write(msg, header);
        serialiser.write(msg, command);

        log.debug('Sending command request: ' + header, command);

        sender(msg);
    }

    function sendResponse(header, command, serialiser) {
        var msg = Message.create({
            type : Message.types.SERVICE_RESPONSE
        });

        headerSerialiser.write(msg, header);
        serialiser.write(msg, command);

        log.debug('Sending command response: ' + header, command);

        sender(msg);
    }

    function sendError(header, error, message) {
        var msg = Message.create({
            type : Message.types.SERVICE_ERROR
        });

        headerSerialiser.write(msg, header);

        Codec.writeString(msg, message);
        Codec.writeInt32(msg, error.id);

        log.debug('Sending command error: ' + error.id, message);

        sender(msg);
    }

    function handleRequest(header, input) {
        var listener = listeners[header.service];

        if (listener) {
            log.debug('Received command request for service: ' + header);

            listener(header, input);
        } else {
            log.error('Received command request for unknown service: ' + header);

            sendError(
                header.createErrorHeader(),
                COMMUNICATION_FAILURE,
                "Unknown client service: " + header.service);
        }
    }

    function handleResponse(header, input) {
        log.debug('Received command response: ' + header);
        internalSession.getConversationSet().respondIfPresent(header.cid, input);
    }

    function handleError(header, input) {
        var description = Codec.readString(input);
        var code = Codec.readInt32(input);

        var error = new ErrorReason(code, description);

        log.warn("Received command error", error);

        internalSession.getConversationSet().discard(header.cid, error);
    }

    this.sendRequest = sendRequest;
    this.sendResponse = sendResponse;
    this.sendError = sendError;

    this.addService = function addService(definition, service) {
        if (listeners[definition.id] === undefined) {
            var requestSerialiser = serialisers.get(definition.request);
            var responseSerialiser = serialisers.get(definition.response);

            listeners[definition.id] = function(header, input) {
                var request = requestSerialiser.read(input);

                var callback = header.cid.equals(ConversationID.ONEWAY_CID) ? NULL_CALLBACK : {
                    respond : function(response) {
                        var rHeader = header.createResponseHeader();
                        sendResponse(rHeader, response, responseSerialiser);
                    },
                    fail : function(error, message) {
                        var eHeader = header.createErrorHeader();
                        sendError(eHeader, error, message);
                    }
                };

                try {
                    service.onRequest(internalSession, request, callback);
                } catch (e) {
                    log.error(e);
                    throw new Error("Unable to handle request for " + definition.name);
                }
            };
        } else {
            throw new Error("Service already exists for " + definition);
        }
    };

    this.onMessage = function onMessage(modes, data) {
        var header = headerSerialiser.read(data);
        switch (modes) {
            case Message.types.SERVICE_REQUEST:
                handleRequest(header, data);
                break;
            case Message.types.SERVICE_RESPONSE:
                handleResponse(header, data);
                break;
            case Message.types.SERVICE_ERROR:
                handleError(header, data);
                break;
            default:
                throw new Error("Unknown Command Service message " + modes);
        }
    };
}

module.exports = ServiceAdapter;

},{"114":114,"136":136,"203":203,"247":247,"416":416,"429":429,"87":87}],118:[function(require,module,exports){
var CommandHeader = require(247);
var logger = require(416);

var log = logger.create("ServiceLocator");

function ServiceLocator(internalSession, serialisers, serviceAdapter) {
    var conversations = internalSession.getConversationSet();

    this.obtain = function(service) {
        var requestSerialiser = serialisers.get(service.request);
        var responseSerialiser = serialisers.get(service.response);

        var reference = {
            send : function(req, callback) {
                callback = callback || function() {};

                var handler = {
                    onOpen : function(cid) {
                        var header = new CommandHeader(service.id, cid);

                        try {
                            serviceAdapter.sendRequest(header, req, requestSerialiser);
                        } catch (e) {
                            callback(e);
                            throw e;
                        }
                    },
                    onResponse : function(cid, input) {
                        var response;

                        try {
                            response = responseSerialiser.read(input);
                        } catch (e) {
                            log.debug("Failed to deserialise response from " + service.name, e.stack);

                            callback(e);
                            return true;
                        }

                        try {
                            callback(null, response);
                        } catch (e) {
                            log.debug("Request callback for service '" + service.name + "' threw an error", e.stack);

                            try {
                                callback(e);
                            } catch (e) {
                                log.debug("Failed to notify callback error for service '" + service.name + "'", e);
                            }
                        }

                        return true;
                    },
                    onDiscard : function(cid, err) {
                        callback(err);
                    }
                };

                                var cid = conversations.newConversation(handler, callback);

                                return function() {
                    conversations.discard(cid, new Error("Cancelled"));
                };
            }
        };

        return reference;
    };
}

module.exports = ServiceLocator;

},{"247":247,"416":416}],119:[function(require,module,exports){
var HashMap = require(43);

function ServiceRegistry() {
    var services = new HashMap();
    var listeners = [];

    this.get = function(definition) {
        return services.get(definition);
    };

    this.add = function(definition, service) {
        if (services.has(definition)) {
            throw new Error("Service already exists for " + definition);
        }

        services.set(definition, service);

        listeners.forEach(function(listener) {
            listener(definition, service);
        });
    };

    this.addListener = function(listener) {
        listeners.push(listener);
        services.forEach(function(k, v) {
            listener(v, k);
        });
    };
}

module.exports = ServiceRegistry;

},{"43":43}],120:[function(require,module,exports){
var service = {
    onRequest : function(internal, ping, callback) {
        callback.respond();
        internal.onSystemPing();
    }
};

module.exports = service;

},{}],121:[function(require,module,exports){
var service = {
    onRequest : function(internal, request, callback) {
        callback.respond();

        internal.getRouting().subscribe(request);
    }
};

module.exports = service;

},{}],122:[function(require,module,exports){
var service = {
    onRequest : function(internal, notification, callback) {
        callback.respond();

        internal.getRouting().unsubscribe(notification.id, notification.reason);
    }
};

module.exports = service;

},{}],123:[function(require,module,exports){
var service = {
    onRequest : function(internal, ping, callback) {
        callback.respond();
    }
};

module.exports = service;
},{}],124:[function(require,module,exports){
module.exports = {
    RECORD_DELIMITER : 0x01,
    FIELD_DELIMITER : 0x02,
    EMPTY_FIELD : 0x03,
    RECORD_MU : 0x04,
    FIELD_MU : 0x05
};

},{}],125:[function(require,module,exports){
var BufferInputStream = require(201);
var RecordContent = require(126);
var consts = require(124);

module.exports = function RecordContentParser(buffer) {
    var records = [];

    if (buffer.length === 1 && buffer[0] === consts.RECORD_MU) {
        records.push(new RecordContent.Record([]));
    } else if (buffer.length > 0) {
        var bis = new BufferInputStream(buffer);

        while (bis.hasRemaining()) {
            var rbis = new BufferInputStream(bis.readUntil(consts.RECORD_DELIMITER));
            var fields = [];

            while (rbis.hasRemaining()) {
                var field = rbis.readUntil(consts.FIELD_DELIMITER);

                if (field.length === 0 || (field.length === 1 && ((fields.length === 0 && 
                                                                   field[0] === consts.FIELD_MU) || 
                                                                   field[0] === consts.EMPTY_FIELD))) {
                    fields.push(undefined);
                } else {
                    fields.push(field.toString());
                }
            }

            var rbuffer = rbis.buffer;
            if (rbuffer[rbuffer.length - 1] === consts.FIELD_DELIMITER) {
                fields.push(undefined);
            }

                        records.push(new RecordContent.Record(fields));
        }

        if (buffer[buffer.length - 1] === consts.RECORD_DELIMITER) {
            records.push(new RecordContent.Record([]));
        }
    }

    return new RecordContent(records);
};

},{"124":124,"126":126,"201":201}],126:[function(require,module,exports){
var consts = require(124);

var RecordContent = function RecordContentImpl(records) {
    this.length = records.length;

    this.get = function(i) {
        i = i || 0;
        return records[i];
    };

    this.records = function() {
        return records.concat([]);
    };

    this.forEach = function(iterator) {
        records.forEach(iterator);
    };

    this.toString = function() {
        return records.join(consts.RECORD_DELIMITER);
    };
};

RecordContent.Record = function RecordImpl(fields) {
    this.length = fields.length;

    this.get = function(i) {
        i = i || 0;
        return fields[i];
    };

    this.fields = function() {
        return fields.concat([]);
    };

    this.forEach = function(iterator) {
        fields.forEach(iterator);
    };

    this.toString = function() {
        return fields.join(consts.FIELD_DELIMITER);
    };
};

module.exports = RecordContent;
},{"124":124}],127:[function(require,module,exports){
var BEES = require(230);
var Codec = require(203);
var util = require(131);

var Encoding = {
    NONE: 0,
    ENCRYPTED: 1,
    COMPRESSED: 2
};

module.exports = {
    read: function (bis) {
        var encoding = BEES.read(bis, Encoding);
        var bytes = Codec.readBytes(bis);

        switch (encoding) {
            case Encoding.NONE :
                return bytes;
            default :
                throw new Error('Unable to handle encoding type ' + encoding);
        }
    },
    write: function (bos, content) {
        BEES.write(bos, Encoding.NONE);
        Codec.writeBytes(bos, util.getBytes(content));
    }
};

},{"131":131,"203":203,"230":230}],128:[function(require,module,exports){
var _implements = require(415)._implements;
var api = require(69);

var RecordContent = require(130);

function insertFields(record, fn, fields) {
    if (fields) {
        for (var field in fields) {
            var value = fields[field];

            if (value instanceof Array) {
                for (var i = 0; i < value.length; ++i) {
                    fn.call(record, field, value[i], i);
                }
            } else {
                fn.call(record, field, value);
            }
        }
    }
}

var RecordRecordBuilder = _implements(api.Builder.Record,
    function StructuredRecordContentRecordBuilderImpl(meta) {
        var fields = {};

        this.add = function (name, value) {
            var mfield = meta.getField(name);

            if (mfield) {
                if (fields[name] === undefined) {
                    fields[name] = [];
                }

                if (mfield.occurs.max === -1 || fields[name].length < mfield.occurs.max) {
                    fields[name].push(value);
                } else {
                    throw new Error("Field '" + name + "' can only occur up to " + mfield.occurs.max + " times");
                }
            } else {
                throw new Error("Invalid field name  '" + name + "'");
            }

            return this;
        };

        this.set = function (name, value, index) {
            var mfield = meta.getField(name);

            if (mfield) {
                if (fields[name] === undefined) {
                    fields[name] = [];
                }

                index = index || 0;

                if (fields[name][index] !== undefined) {
                    fields[name][index] = value;
                } else {
                    throw new Error("Cannot set nonexistent field '" + name + '" (' + index + ')');
                }
            } else {
                throw new Error("Invalid field name '" + name + "'");
            }

            return this;
        };

        this.build = function () {
            var $fields = {};

            meta.getFields().forEach(function (mfield) {
                var field = mfield.name;
                $fields[field] = [];

                if (fields[field] === undefined) {
                    fields[field] = [];
                }

                for (var i = fields[field].length; i < mfield.occurs.min; ++i) {
                    fields[field].push(mfield.type.value);
                }

                $fields[field] = fields[field];
            });

            return new RecordContent.Record($fields);
        };
    });

var RecordBuilder = _implements(api.Builder,
    function StructuredRecordContentBuilderImpl(meta) {
        var records = {};

        this.add = function (name, fields) {
            var mrecord = meta.getRecord(name);

            if (mrecord) {
                if (records[name] === undefined) {
                    records[name] = [];
                }

                if (mrecord.occurs.max === -1 || records[name].length < mrecord.occurs.max) {
                    var record = new RecordRecordBuilder(mrecord);
                    records[name].push(record);

                    insertFields(record, record.add, fields);

                    return record;
                } else {
                    throw new Error("Record '" + name + "' can only occur up to " + mrecord.occurs.max + " times");
                }
            } else {
                throw new Error("Invalid record name '" + name + "'");
            }
        };

        this.set = function (name, fields, index) {
            var mrecord = meta.getRecord(name);

            if (mrecord) {
                if (records[name] === undefined) {
                    records[name] = [];
                }

                index = index || 0;

                if (records[name][index] !== undefined) {
                    var record = records[name][index];

                    insertFields(record, record.set, fields);

                    return record;
                } else {
                    throw new Error('Cannot set nonexistent record ' + name + '" (' + index + ')');
                }
            } else {
                throw new Error("Invalid record name '" + name + "'");
            }
        };

        this.build = function () {
            var $records = {};

            meta.getRecords().forEach(function (mrecord) {
                var record = mrecord.name;
                $records[record] = [];

                if (records[record] === undefined) {
                    records[record] = [];
                }

                for (var i = records[record].length; i < mrecord.occurs.min; ++i) {
                    records[record].push(new RecordRecordBuilder(mrecord));
                }

                for (var j = 0; j < records[record].length; ++j) {
                    $records[record].push(records[record][j].build());
                }
            });

            return new RecordContent($records);
        };

        this.addAndBuild = function (name, value) {
            this.add(name, value);
            return this.build();
        };

        this.setAndBuild = function (name, fields, index) {
            this.set(name, fields, index);
            return this.build();
        };

    });

module.exports = RecordBuilder;

},{"130":130,"415":415,"69":69}],129:[function(require,module,exports){
var parseAsRecordContent = require(125);
var RecordContent = require(130);

function Reader(set) {
    var pos = 0;

    this.readUpTo = function(occurs, fn) {
        var max = Math.min(pos + (occurs.max === -1 ? set.length : occurs.max), set.length);

        if (set.length === 0) {
            return;
        }

        if (occurs.min > set.length - pos) {
            throw new Error('Data exhaused while parsing');
        }

        for (var i = pos; i < max; ++i) {
            fn(set[i]);
        }

        pos = max;
    };
}

function RecordContentMetadataParser(metadata) {
    this.parse = function(buffer) {
        var content = parseAsRecordContent(buffer);
        var reader = new Reader(content.records());
        var $records = {};

        metadata.getRecords().forEach(function(mrecord) {
            var records = [];

            reader.readUpTo(mrecord.occurs, function(record) {
                var reader = new Reader(record.fields());
                var $fields = {};

                mrecord.getFields().forEach(function(mfield) {
                    var fields = [];

                    reader.readUpTo(mfield.occurs, function(field) {
                        if (field) {
                            fields.push(mfield.type.parse(field));
                        } else {
                            fields.push(field);
                        }
                    });

                                        $fields[mfield.name] = fields;
                });

                records.push(new RecordContent.Record($fields));
            });

            $records[mrecord.name] = records;
        });

        return new RecordContent($records);
    };
}

module.exports = RecordContentMetadataParser;

},{"125":125,"130":130}],130:[function(require,module,exports){
var _implements = require(415)._implements;
var api = require(69);

var StructuredRecordContent = _implements(api, function StructuredRecordContentImpl(records) {
    var inlined = [];

    for (var k in records) {
        inlined = inlined.concat(records[k]);
    }

    this.get = function(name, index) {
        index = index || 0;

        if (records[name]) {
            return records[name][index];
        }

        return undefined;
    };

    this.records = function(name) {
        if (records[name]) {
            return records[name].concat([]);
        }

        return inlined.concat([]);
    };

    this.forEach = function(iterator) {
        inlined.forEach(iterator);
    };
});

StructuredRecordContent.Record = _implements(api.Record, function StructuredRecordImpl(fields) {
    var inlined = [];

    for (var k in fields) {
        inlined = inlined.concat(fields[k]);
    }

    this.get = function(name, index) {
        index = index || 0;

        if (fields[name]) {
            return fields[name][index];
        }

        return undefined;
    };

    this.fields = function(name) {
        if (fields[name]) {
            return fields[name].concat([]);
        }

        return inlined.concat([]);
    };

    this.forEach = function(iterator) {
        inlined.forEach(iterator);
    };
});

module.exports = StructuredRecordContent;
},{"415":415,"69":69}],131:[function(require,module,exports){
(function (Buffer){
var BufferOutputStream = require(202);

var RC = require(126);
var SRC = require(130);

var consts = require(124);

function getDefaultContentBytes(content) {
    return new Buffer(content.toString());
}

function getRecordContentBytes(content) {
    var bos = new BufferOutputStream();
    var recordDelimiter = false;

    content.forEach(function writeRecord(r) {
        var fieldDelimiter = false;

        if (recordDelimiter) {
            bos.writeInt8(consts.RECORD_DELIMITER);
        } else {
            recordDelimiter = true;
        }

        r.forEach(function writeField(f) {
            if (fieldDelimiter) {
                bos.writeInt8(consts.FIELD_DELIMITER);
            } else {
                fieldDelimiter = true;
            }

            bos.writeString(f.toString());
        });
    });

    return bos.getBuffer();
}

function isRecordContent(content) {
    return RC.isPrototypeOf(content) || SRC.isPrototypeOf(content);
}

function getBytes(content) {
    if (content.$buffer) {
        return content.$buffer.slice(content.$offset, content.$length);
    } else if (isRecordContent(content)) {
        return getRecordContentBytes(content);
    } else {
        return getDefaultContentBytes(content);
    }
}

module.exports = {
    getBytes : getBytes,
    isRecordContent : isRecordContent
};
}).call(this,require(17).Buffer)
},{"124":124,"126":126,"130":130,"17":17,"202":202}],132:[function(require,module,exports){
var ControlGroup = require(133);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        var name = Codec.readString(input);
        return new ControlGroup(name);
    },
    write : function(output, group) {
        Codec.writeString(output, group.name);
    }
};

module.exports = serialiser;

},{"133":133,"203":203}],133:[function(require,module,exports){
function ControlGroup(name) {
    this.name = name;
}

ControlGroup.DEFAULT = new ControlGroup("default");

module.exports = ControlGroup;

},{}],134:[function(require,module,exports){
var Services = require(325);

var Emitter = require(173);
var Result = require(174);

var CloseReason = require(112);

var ResponseHandlerState = {
    REGISTERING : 0,
    ACTIVE : 1,
    CLOSED : 2
};

function responseHandler(internal, adapter, deregistration) {
    var state = ResponseHandlerState.REGISTERING;
    var close;

    return {
        onDiscard : function(cid, err) {
            if (err instanceof CloseReason) {
                adapter.close();
            } else {
                adapter.close(err);
            }

            state = ResponseHandlerState.CLOSED;
        },
        onOpen : function(cid) {
            close = function close() {
                var emitter = new Emitter();
                var result = new Result(emitter);

                if (state !== ResponseHandlerState.CLOSED) {
                    deregistration(cid, function(err) {
                        if (err) {
                            internal.getConversationSet().discard(cid, err);
                            emitter.error(err);
                        } else {
                            internal.getConversationSet().respondIfPresent(cid, ResponseHandlerState.CLOSED);
                            emitter.emit('complete');
                        }
                    });
                } else {
                    emitter.error(new Error('Handler already closed'));
                }

                return result;
            };
        },
        onResponse : function(cid, response) {
            switch (response) {
                case ResponseHandlerState.ACTIVE:
                    adapter.active(close, cid);
                    state = response;
                    return false;
                case ResponseHandlerState.CLOSED:
                    adapter.close();
                    state = response;
                    return true;
                default:
                    return adapter.respond(response);
            }
        }
    };
}

function registrationCallback(conversationSet, cid, emitter) {
    return function(err) {
        if (err) {
            conversationSet.discard(cid, err);
            emitter.error(err);
        } else {
            conversationSet.respondIfPresent(cid, ResponseHandlerState.ACTIVE);
            emitter.emit('complete');
        }
    };
}

function registerHandler(internal, params, adapter, reg, dereg) {
    var conversationSet = internal.getConversationSet();
    var serviceLocator = internal.getServiceLocator();

    var registration = serviceLocator.obtain(reg);
    var deregistration = serviceLocator.obtain(dereg);

    var cid = conversationSet.newConversation(responseHandler(internal, adapter, function(cid, callback) {
        deregistration.send(params, callback);
    }));

    var emitter = new Emitter();
    var result = new Result(emitter);

    registration.send({ params : params, cid : cid }, registrationCallback(conversationSet, cid, emitter));

    return result;
}

module.exports.responseHandler = responseHandler;

module.exports.registrationCallback = registrationCallback;

module.exports.registerHandler = registerHandler;

module.exports.registerMessageHandler = function registerMessageHandler(internal, params, adapter) {
    return registerHandler(internal, params, adapter,
                                  Services.MESSAGE_RECEIVER_CONTROL_REGISTRATION,
                                  Services.MESSAGE_RECEIVER_CONTROL_DEREGISTRATION);
};

module.exports.registerRequestHandler = function registerMessageHandler(internal, params, adapter) {
    return registerHandler(internal, params, adapter,
                                  Services.MESSAGING_RECEIVER_CONTROL_REGISTRATION,
                                  Services.MESSAGING_RECEIVER_CONTROL_DEREGISTRATION);
};

module.exports.registerServerHandler = function registerServerHandler(internal, params, adapter) {
    return registerHandler(internal, params, adapter,
            Services.SERVER_CONTROL_REGISTRATION,
            Services.SERVER_CONTROL_DEREGISTRATION);
};

module.exports.registerTopicHandler = function registerTopicHandler(internal, params, adapter) {
    return registerHandler(internal, params, adapter,
        Services.TOPIC_CONTROL_REGISTRATION,
        Services.TOPIC_CONTROL_DEREGISTRATION);
};

module.exports.ResponseHandlerState = ResponseHandlerState;

},{"112":112,"173":173,"174":174,"325":325}],135:[function(require,module,exports){
var Codec = require(203);
var ConversationId = require(136);
var CIDSerialiser = {
    read : function(input) {
        return new ConversationId(Codec.readInt64(input));
    },
    write : function(out, cid) {
        Codec.writeInt64(out, cid.val);
    }
};

module.exports = CIDSerialiser;

},{"136":136,"203":203}],136:[function(require,module,exports){
var Long = require(46);

function ConversationId(val) {
    this.val = val;
}

ConversationId.prototype.toString = function() {
    return this.val.toString(10);
};

ConversationId.prototype.toValue = function() {
    return this.toString();
};

ConversationId.prototype.equals = function(other) {
    return other.val && this.val.equals(other.val);
};

ConversationId.fromString = function(val) {
    return new ConversationId(Long.fromString(val, false));
};

ConversationId.ONEWAY_CID = new ConversationId(Long.fromNumber(0));

module.exports = ConversationId;

},{"46":46}],137:[function(require,module,exports){
var ConversationId = require(136);
var logger = require(416);

var Long = require(46);

var NEXT_CID = (function() {
    var id = new Long(0);
    return function() {
        id = id.add(1);
        return new ConversationId(id);
    };
}());

var Result = {
    ALREADY_FINISHED : 0,
    HANDLED_AND_ACTIVE : 1,
    HANDLED_AND_FINISHED : 2
};

var LOG = logger.create('Conversation Set');

function ConversationSet(idGenerator) {
    var nextCID = idGenerator;

    var conversations = {};
    var discardReason;

    var self = this;

    function completedExceptionally(cid, conversation, e) {
        LOG.debug("Application handler threw exception [cid=" + cid + "]", e.stack);
        conversation.close();

        delete conversations[cid];
    }

    this.newConversation = function(responseHandler) {
        var conversation = new Conversation(responseHandler);
        var cid = nextCID();

        conversations[cid.toString()] = conversation;

        try {
            conversation.open(cid);
        } catch (e) {
            completedExceptionally(cid, conversation, e);
            throw e;
        }

        if (discardReason) {
            self.discard(cid, discardReason);
        }

        return cid;
    };

    this.respond = function(cid, response) {
        var conversation = conversations[cid];

        if (conversation === undefined) {
            LOG.debug("No conversation for cid: " + cid + ", response: ", response);
            throw new Error("No such conversation");
        }

        var result = conversation.respond(cid, response);

        switch (result) {
            case Result.ALREADY_FINISHED :
                throw new Error("No such conversation");
            case Result.HANDLED_AND_ACTIVE:
                break;
            default:
                delete conversations[cid];
        }
    };

    this.respondIfPresent = function(cid, response) {
        try {
            self.respond(cid, response);
        } catch (e) {
        }
    };

    this.discard = function(cid, reason) {
        var conversation = conversations[cid];
        delete conversations[cid];

        if (conversation) {
            conversation.discard(cid, reason);
        }
    };

    this.discardAll = function(reason) {
        if (discardReason !== undefined) {
            return;
        }

        discardReason = reason;

        for (var cid in conversations) {
            self.discard(ConversationId.fromString(cid), reason);
        }
    };

    this.size = function() {
        return Object.keys(conversations).length;
    };

    this.toString = function() {
        return "ConversationSet cids=[" + Object.keys(conversations) + "]";
    };

    function Conversation(handler) {
        var pendingDiscard;

        var reserved = false;
        var closed = false;

        this.open = function(cid) {
            handler.onOpen(cid);
        };

        this.respond = function(cid, response) {
            if (closed) {
                return Result.ALREADY_FINISHED;
            }

            var previous = reserved;
            reserved = true;

            var close;

            try {
                close = handler.onResponse(cid, response);
            } catch (e) {
                completedExceptionally(cid, this, e);
                throw e;
            }

            if (closed) {
                LOG.debug("Conversation already closed", cid);
                return Result.HANDLED_AND_FINISHED;
            } else if (close) {
                LOG.debug("Response handler closed conversation", cid);
                closed = true;
                return Result.HANDLED_AND_FINISHED;
            } else if (pendingDiscard) {
                LOG.debug("Conversation closed with pending reason", cid);
                notifyDiscard(cid, pendingDiscard);
                return Result.HANDLED_AND_FINISHED;
            } else {
                reserved = previous;
                return Result.HANDLED_AND_ACTIVE;
            }
        };

        this.discard = function(cid, reason) {
            if (reserved) {
                pendingDiscard = reason;
            } else {
                notifyDiscard(cid, reason);
            }
        };

        this.close = function() {
            closed = true;
        };

        function notifyDiscard(cid, reason) {
            if (!closed) {
                closed = true;

                try {
                    handler.onDiscard(cid, reason);
                } catch (e) {
                    LOG.error("Application handler threw exception [cid=" + cid + "]", e.stack);
                    throw e;
                }
            }
        }
    }
}

module.exports = ConversationSet;

module.exports.create = function() {
    return new ConversationSet(NEXT_CID);
};

module.exports.Result = Result;
module.exports.ConversationIdGenerator = NEXT_CID;

},{"136":136,"416":416,"46":46}],138:[function(require,module,exports){
var BinaryDeltaTypeImpl = require(141);
var BinaryDeltaImpl = require(140);

var BytesImpl = require(143);

module.exports = function(
    typeName,
    valueClass,
    implementation,
    vToC,
    cToV,
    converters,
    withBinaryDelta) {

    var binaryDeltaType = withBinaryDelta ?
        new BinaryDeltaTypeImpl(implementation, vToC, cToV) :
        null;

    var valueConverters = {};

    converters.forEach(function(impl) {
        valueConverters[impl.toString()] = function(buffer, offset, length) {
            return new impl(buffer, offset, length);
        };
    });

    this.valueClass = valueClass;

    this.name = function() {
        return typeName;
    };

    this.toString = function() {
        return typeName + " data type";
    };

    this.writeValue = function(value) {
        var internal = vToC(value);

        if (internal === null) {
            throw new Error(this + " cannot write " + value);
        }

        return internal.asBuffer();
    };

    this.toBytes = function(value) {
        var bytes = vToC(value);

        if (bytes) {
            return bytes;
        }

        throw new Error(this + " cannot convert value to Bytes: " + value);
    };

    this.readValue = function(buffer, offset, length) {
        if (buffer.$buffer) {
            return cToV(buffer);
        }

        return cToV(new implementation(buffer, offset, length));
    };

    valueConverters[BytesImpl.toString()] = function(buffer, offset, length) {
        return new BytesImpl(buffer, offset, length);
    };

    valueConverters[valueClass.toString()] = this.readValue;

    this.canReadAs = function(valueClass) {
        return valueConverters[valueClass] !== undefined;
    };

    this.readAs = function(valueClass, buffer, offset, length) {
        if (buffer.$buffer) {
            if (buffer instanceof implementation) {
                return buffer;
            }

            return this.readAs(valueClass, buffer.$buffer, buffer.$offset, buffer.$length);
        }

        if (valueConverters[valueClass]) {
            return valueConverters[valueClass](buffer, offset, length);
        }

        throw new Error(this + " is incompatible with values of " + valueClass);
    };

    this.deltaType = function(name) {
        if (!name || name === "binary" || BinaryDeltaImpl.isPrototypeOf(name)) {
            return this.binaryDeltaType();
        }

        throw new Error(this + " has no '" + name + "' delta type");
    };

    this.binaryDeltaType = function() {
        if (binaryDeltaType) {
            return binaryDeltaType;
        }

        throw new Error(this + " has no binary delta type");
    };
};
},{"140":140,"141":141,"143":143}],139:[function(require,module,exports){
var _implements = require(415)._implements;
var BinaryDataType = require(70);

var AbstractDataType = require(138);
var BinaryImpl = require(142);

var identity = require(414).identity;

module.exports = _implements(BinaryDataType, function() {
    AbstractDataType.call(
        this, "binary", BinaryImpl, BinaryImpl, BinaryImpl.from, identity, [], true);

    this.from = BinaryImpl.from;

    this.Binary = BinaryImpl;
});
},{"138":138,"142":142,"414":414,"415":415,"70":70}],140:[function(require,module,exports){
(function (Buffer){
var _implements = require(415)._implements;
var Tokeniser = require(111);

var BinaryDelta = require(71);

var util = require(172);

module.exports = _implements(BinaryDelta, function BinaryDeltaImpl(buffer, offset, length) {
    util.assignInternals(this, buffer, offset, length);

    this.visit = function(visitor) {
        if (this.hasChanges()) {
            var tokeniser = new Tokeniser(buffer, offset, length);

            while (tokeniser.hasRemaining()) {
                var token = tokeniser.nextToken();
                var isEnd = false;

                if (Buffer.isBuffer(token.value)) {
                    isEnd = !visitor.insert(token.value);
                } else {
                    var start = token.value;
                    var end = tokeniser.nextToken().value;

                    isEnd = !visitor.match(start, end);
                }

                if (isEnd) {
                    visitor.end();
                    return;
                }
            }

            visitor.end();
        } else {
            visitor.noChange();
        }
    };

    this.hasChanges = function() {
        return length !== 1;
    };

    this.equals = function(other) {
        if (other) {
            return this.$offset === other.$offset &&
                   this.$length === other.$length &&
                   this.$buffer.equals(other.$buffer);
        }

        return false;
    };
});
}).call(this,{"isBuffer":require(22)})
},{"111":111,"172":172,"22":22,"415":415,"71":71}],141:[function(require,module,exports){
(function (Buffer){
var _implements = require(415)._implements;
var DeltaType = require(76);

var BufferOutputStream = require(202);
var BinaryDeltaImpl = require(140);

var MyersBinaryDiff = require(145);

var Decoder = require(109);
var Encoder = require(110);

var encoder = new Encoder();

var NO_CHANGE = new BinaryDeltaImpl(new Buffer([-10]), 0, 1);

module.exports = _implements(DeltaType, function BinaryDeltaSupportImpl(implementation, vToC, cToV) {
    var binaryDiff = new MyersBinaryDiff();
    var self = this;

    this.name = function() {
        return "binary";
    };

    this.diff = function(oldValue, newValue) {
        oldValue = vToC(oldValue);
        newValue = vToC(newValue);

        var buffer = newValue.$buffer;
        var offset = newValue.$offset;
        var length = newValue.$length;

        var budget = length;

        var script = new Script(encoder, buffer, offset, function(cost) {
            return (budget -= cost) <= 0;
        });

        var result = binaryDiff.diff(
            oldValue.$buffer,
            oldValue.$offset,
            oldValue.$length,
            buffer,
            offset,
            length,
            script);

        var delta = encoder.flush();

        switch (result) {
            case MyersBinaryDiff.REPLACE :
                return replace(newValue);
            case MyersBinaryDiff.NO_CHANGE :
                return NO_CHANGE;
            default :
                return self.readDelta(delta);
        }
    };

    this.apply = function(oldValue, delta) {
        if (!delta || !delta.hasChanges()) {
            return oldValue;
        }

        oldValue = vToC(oldValue);

        var decoder = new Decoder(delta.$buffer);
        var bos = new BufferOutputStream();

        while (decoder.hasRemaining()) {
            var start = decoder.nextValue();

            if (Buffer.isBuffer(start)) {
                bos.writeMany(start);
            } else if (typeof start === "number") {
                bos.writeMany(oldValue.$buffer, oldValue.$offset + start, decoder.nextValue());
            }
        }

        return cToV(new implementation(bos.getBuffer()));
    };

    this.readDelta = function(buffer, offset, length) {
        var delta = new BinaryDeltaImpl(buffer, offset, length);

        if (delta.$length === 1 && buffer[delta.$offset] === NO_CHANGE.$buffer[0]) {
            return NO_CHANGE;
        }

        return delta;
    };

    this.writeDelta = function(delta) {
        return delta.$buffer.slice(delta.$offset, delta.$offset + delta.$length);
    };

    this.noChange = function() {
        return NO_CHANGE;
    };

    this.isValueCheaper = function(value, delta) {
        return value.$length <= delta.$length;
    };
});

module.exports.NO_CHANGE = NO_CHANGE;

function replace(value) {
    encoder.encode(value.$buffer, value.$offset, value.$length);
    return new BinaryDeltaImpl(encoder.flush());
}

function cborCost(i) {
    if (i < 24) {
        return 1;
    } else if (i < 0xFF) {
        return 2;
    } else if (i <= 0xFFFF) {
        return 3;
    }

    return 5;
}

function conflatableMatch(matchStart, matchLength, insertLength) {
    var matchCost = cborCost(matchStart) + 1;
    var insertCost = cborCost(matchLength + insertLength) - cborCost(insertLength) + matchLength;

    return insertCost <= matchCost;
}

function Script(encoder, buffer, offset, blowsBudget) {
    var pendingInsert;
    var pendingLength;
    var pendingStart = -1;

    this.insert = function(bStart, length) {
        if (!pendingInsert &&
            pendingStart !== -1 &&
            conflatableMatch(pendingStart, pendingLength, length)) {

            pendingStart = bStart - pendingLength;
            pendingLength += length;
            pendingInsert = true;

            return MyersBinaryDiff.SUCCESS;
        }

        return process(true, bStart, length);
    };

    this.match = function(aStart, length) {
        if (pendingInsert &&
            pendingStart !== -1 &&
            conflatableMatch(aStart, length, pendingLength)) {

            pendingLength += length;
            return MyersBinaryDiff.SUCCESS;
        }

        return process(false, aStart, length);
    };

    this.close = function() {
        return flush();
    };

    function flush() {
        if (pendingStart === -1) {
            return MyersBinaryDiff.SUCCESS;
        } else if (pendingInsert) {
            return writeInsert(pendingStart, pendingLength);
        } else {
            return writeMatch(pendingStart, pendingLength);
        }
    }

    function process(insert, start, length) {
        var r = flush();

        if (r !== MyersBinaryDiff.SUCCESS) {
            return r;
        }

        pendingInsert = insert;
        pendingStart = start;
        pendingLength = length;

        return MyersBinaryDiff.SUCCESS;
    }

    function writeInsert(start, length) {
        if (blowsBudget(length + cborCost(length))) {
            return MyersBinaryDiff.REPLACE;
        }

        encoder.encode(buffer, offset + start, length);

        pendingStart = -1;

        return MyersBinaryDiff.SUCCESS;
    }

    function writeMatch(start, length) {
        if (blowsBudget(cborCost(start) + cborCost(length))) {
            return MyersBinaryDiff.REPLACE;
        }

        encoder.encode(start);
        encoder.encode(length);

        pendingStart = -1;

        return MyersBinaryDiff.SUCCESS;
    }
}
}).call(this,require(17).Buffer)
},{"109":109,"110":110,"140":140,"145":145,"17":17,"202":202,"415":415,"76":76}],142:[function(require,module,exports){
(function (Buffer){
var _implements = require(415)._implements;
var BinaryType = require(72);

var BinaryDeltaTypeImpl = require(141);
var BytesImpl = require(143);

var identity = require(414).identity;

var BINARY_DELTA_TYPE = new BinaryDeltaTypeImpl(BinaryImpl, from, identity);

function from(value) {
    if (value instanceof BinaryImpl) {
        return value;
    } else if (Buffer.isBuffer(value)) {
        return new BinaryImpl(value);
    } else {
        throw new Error("Unable to read Binary value from: " + value);
    }
}

function BinaryImpl(buffer, offset, length) {
    BytesImpl.__constructor.call(this, buffer, offset, length);

    var self = this;

    this.get = function() {
        return self.asBuffer();
    };

    this.diff = function(original) {
        return BINARY_DELTA_TYPE.diff(original, self);
    };

    this.apply = function(delta) {
        return BINARY_DELTA_TYPE.apply(self, delta);
    };

    this.toString = function() {
        return "Binary <" + self.$length + " bytes> " + self.get().toString();
    };
}

module.exports = _implements(BinaryType, BinaryImpl);
module.exports.from = from;

module.exports.toString = function() {
    return "BinaryImpl";
};
}).call(this,{"isBuffer":require(22)})
},{"141":141,"143":143,"22":22,"414":414,"415":415,"72":72}],143:[function(require,module,exports){
var _implements = require(415)._implements;
var Bytes = require(73);
var util = require(172);

module.exports = _implements(Bytes, function BytesImpl(buffer, offset, length) {
    util.assignInternals(this, buffer, offset, length);

    var self = this;

    this.length = function() {
        return self.$length;
    };

    this.asBuffer = function() {
        return buffer.slice(self.$offset, self.$offset + self.$length);
    };

    this.copyTo = function(target, tOffset) {
        tOffset = tOffset || 0;

        buffer.copy(target, tOffset, self.$offset, self.$offset + self.$length);
    };

    this.equals = function(bytes) {
        return this.equalBytes(bytes.$buffer, bytes.$offset, bytes.$length);
    };

    this.equalBytes = function(buffer, offset, length) {
        if (this.$length !== length) {
            return false;
        }

        var $buffer = this.$buffer;
        var $offset = this.$offset;

        if ($buffer === buffer && $offset === offset) {
            return true;
        }

        for (var i = 0; i < length; ++i) {
            if ($buffer[$offset + i] !== buffer[offset + i]) {
                return false;
            }
        }

        return true;
    };
});

module.exports.toString = function() {
    return "BytesImpl";
};
},{"172":172,"415":415,"73":73}],144:[function(require,module,exports){
(function (Buffer){
var _implements = require(415)._implements;
var DataTypes = require(75);
var TopicType = require(435).TopicType;

var BinaryDataTypeImpl = require(139);
var BinaryImpl = require(142);

var JSONDataTypeImpl = require(146);
var JSONImpl = require(148);

var Int64DataTypeImpl = require(153);
var Int64Impl = require(154);

var StringDataTypeImpl = require(156);
var DoubleDataTypeImpl = require(152);

var RecordV2DataTypeImpl = require(161);
var RecordV2Impl = require(163);

var DataTypesImpl = _implements(DataTypes, function DataTypesImpl() {
    var recordv2 = new RecordV2DataTypeImpl();
    var binary = new BinaryDataTypeImpl();
    var string = new StringDataTypeImpl();
    var double = new DoubleDataTypeImpl();
    var int64 = new Int64DataTypeImpl();
    var json = new JSONDataTypeImpl();

    var self = this;

    this.binary = function() {
        return binary;
    };

    this.string = function() {
        return string;
    };

    this.double = function() {
        return double;
    };

    this.int64 = function() {
        return int64;
    };

    this.json = function() {
        return json;
    };

    this.recordv2 = function() {
        return recordv2;
    };

    this.getByName = function(name) {
        switch (name.toLowerCase()) {
            case 'json' :
                return self.json();
            case 'int64' :
                return self.int64();
            case 'binary' :
                return self.binary();
            case 'string' :
                return self.string();
            case 'double' :
                return self.double();
            case 'recordv2' :
                return self.recordv2();
        }
    };

    this.getByValue = function(value) {
        if (value === TopicType.BINARY || BinaryImpl.isPrototypeOf(value) || Buffer.isBuffer(value)) {
            return self.binary();
        }

        if (value === TopicType.JSON || JSONImpl.isPrototypeOf(value) || value.constructor === Object) {
            return self.json();
        }

        if (value === TopicType.STRING || typeof value === "string") {
            return self.string();
        }

        if (value === TopicType.DOUBLE || typeof value === "number") {
            return self.double();
        }

        if (value === TopicType.INT64 || Int64Impl.isPrototypeOf(value)) {
            return self.int64();
        }

        if (value === TopicType.RECORD_V2 || RecordV2Impl.isPrototypeOf(value)) {
            return self.recordv2();
        }
    };

    this.get = function(type) {
        var datatype;

        if (typeof type === "string") {
            datatype = this.getByName(type);
        }

        if (!datatype) {
            datatype = this.getByValue(type);
        }

        if (!datatype) {
            datatype = this.getByClassSafely(type);
        }

        return datatype ? datatype : null;
    };

    this.getChecked = function(type) {
        if (!type) {
            throw new Error("No data type for " + type);
        }

        if (RecordV2DataTypeImpl.isPrototypeOf(type) ||
            BinaryDataTypeImpl.isPrototypeOf(type) ||
            StringDataTypeImpl.isPrototypeOf(type) ||
            DoubleDataTypeImpl.isPrototypeOf(type) ||
            Int64DataTypeImpl.isPrototypeOf(type) ||
            JSONDataTypeImpl.isPrototypeOf(type)) {

            return type;
        }

        var datatype = this.get(type);
        if (datatype === null) {
            throw new Error("No data type for " + type);
        }
        return datatype;
    };

    this.getValueClassChecked = function(type) {

        var datatype = this.getChecked(type);
        if (datatype === json) {
            return JSONImpl;
        }
        else if (datatype === binary) {
            return BinaryImpl;
        }
        else if (datatype === string) {
            return String;
        }
        else if (datatype === double) {
            return Number;
        }
        else if (datatype === int64) {
            return Int64Impl;
        }
        else if (datatype === recordv2) {
            return RecordV2Impl;
        }
    };

    this.getByClassSafely = function(clazz) {
        switch (clazz) {
            case JSONImpl :
                return json;
            case BinaryImpl :
                return binary;
            case String :
                return string;
            case Number :
                return double;
            case Int64Impl :
                return int64;
            case RecordV2Impl :
                return recordv2;
        }
    };

    this.getByClass = function(clazz) {
        var datatype = this.getByClassSafely(clazz);

        if (!datatype) {
            throw new Error("No data type for " + clazz);
        }
        else {
            return datatype;
        }
    };
});

module.exports = new DataTypesImpl();

}).call(this,{"isBuffer":require(22)})
},{"139":139,"142":142,"146":146,"148":148,"152":152,"153":153,"154":154,"156":156,"161":161,"163":163,"22":22,"415":415,"435":435,"75":75}],145:[function(require,module,exports){
var approximateCubeRoot = require(417).approximateCubeRoot;

var BAIL_OUT_FACTOR = 10000;
var MAXIMUM_STORAGE = 0x7fffffff;

var SUCCESS = 0,
    REPLACE = 1,
    NO_CHANGE = 2;

function Storage(max) {
    var maximumD = (max - 3) / 4;
    var vectorLength = 15;
    var vector = [];


    function fill(start) {
        for (var i = start; i < vectorLength; i += 4) {
            vector[i + 0] = -1;
            vector[i + 1] = -1;
            vector[i + 2] = 0x7fffffff;
            vector[i + 3] = 0x7fffffff;
        }
    }

    function ensure(d) {
        var required = 4 * (d + 1) + 3;

        if (vectorLength < required) {
            vectorLength = required;
        }
    }

    this.initialise = function(d) {
        ensure(d);
        fill(3);

        return vector;
    };

    this.extend = function(d) {
        if (d > maximumD) {
            return null;
        }

        var originalLength = vectorLength;

        ensure(d);
        fill(originalLength);

        return vector;
    };
}

function keyF(k) {
    return k < 0 ? -4 * k - 1 : 4 * k;
}

function keyR(k) {
    return keyF(k) + 2;
}

function getF(v, k) {
    var i = v[keyF(k)];
    return i === undefined ? 0 : i;
}

function getR(v, k) {
    var i = v[keyR(k)];
    return i === undefined ? 0 : i;
}

function setF(v, k, i) {
    v[keyF(k)] = i;
}

function setR(v, k, i) {
    var key = keyR(k);
    v[key] = i;
}

function nextF(v, k) {
    var left = getF(v, k + 1);
    var right = getF(v, k - 1);

    return left < right ? right : left + 1;
}

function nextR(v, k) {
    var left = getR(v, k + 1);
    var right = getR(v, k - 1);

    return left < right ? left : right - 1;
}

function corner(d, length) {
    if (d <= length) {
        return d;
    } else {
        return 2 * length - d;
    }
}

function calculateBailOutLimit(l1, l2, bailOutFactor) {
    var total = l1 + l2;
    var cube = approximateCubeRoot(total);
    var mult = bailOutFactor * cube;

    return Math.max(256, mult / 100);
}

function checkBounds(buffer, offset, length) {
    if (offset < 0) {
        throw new Error("offset " + offset + " < 0");
    }

    if (length < 0) {
        throw new Error("length " + length + " < 0");
    }

    if (offset + length > buffer.length || offset + length < 0) {
        throw new Error("offset " + offset + " + " + length + " > " + buffer.length);
    }
}

function Execution(storage, a, b, script, bailOutLimit) {
    var self = this;

    this.diff = function(aOffset, aLength, bOffset, bLength) {
        checkBounds(a, aOffset, aLength);
        checkBounds(b, bOffset, bLength);

        var x = 0;
        var y = 0;

        while (x < aLength && y < bLength && a[aOffset + x] === b[bOffset + y]) {
            ++x;
            ++y;
        }

        var u = aLength;
        var v = bLength;

        while (u > x && v > y && a[aOffset + u - 1] === b[bOffset + v - 1]) {
            --u;
            --v;
        }

        var r1 = script.match(aOffset, x); 
        if (r1 !== SUCCESS) {
            return r1;
        }

        var r2;

        if (x === u) {
            r2 = script.insert(bOffset + y, v - y);
        } else if (y === v) {
            r2 = script.delete(aOffset + x, u - x);
        } else {
            r2 = self.middleSnake(aOffset + x, u - x, bOffset + y, v - y);
        }

        if (r2 !== SUCCESS) {
            return r2;
        }

        return script.match(aOffset + u, aLength - u); 
    };

    this.middleSnake = function(aOffset, aLength, bOffset, bLength) {
        var delta = aLength - bLength;
        var odd = delta & 1;

        var vec = storage.initialise(1);

        setF(vec, -1, 0);
        setR(vec, 1, aLength);

        var d = 0;

        for (;;) {
            for (var k1 = -corner(d, aLength); k1 <= corner(d, bLength); k1 +=2) {
                var x1 = nextF(vec, k1);
                var u1 = x1;

                while (u1 < aLength && u1 + k1 < bLength && a[aOffset + u1] === b[bOffset + u1 + k1]) {
                    ++u1;
                }

                setF(vec, k1, u1);

                if (odd && d > 1 && Math.abs(k1 + delta) <= d - 1 && u1 >= getR(vec, k1 + delta)) {
                    return self.recurse(aOffset, aLength, bOffset, bLength, x1, u1, k1);
                }
            }

            for (var k2 = -corner(d, bLength); k2 <= corner(d, aLength); k2 += 2) {
                var u2 = nextR(vec, k2);
                var x2 = u2;

                var kd = k2 - delta;

                while (x2 > 0 && x2 + kd > 0 && a[aOffset + x2 - 1] === b[bOffset + x2 + kd - 1]) {
                    --x2;
                }

                setR(vec, k2, x2);

                if (!odd && d > 0 && Math.abs(kd) <= d && x2 <= getF(vec, kd)) {
                    return self.recurse(aOffset, aLength, bOffset, bLength, x2, u2, kd);
                }
            }

            if (d > bailOutLimit) {
                return bail(vec, d, aOffset, aLength, bOffset, bLength);
            }

            ++d;

            vec = storage.extend(d);

            if (vec === null) {
                return REPLACE;
            }
        }
    };

    this.recurse = function(aOffset, aLength, bOffset, bLength, x, u, k) {
        var r1 = self.diff(aOffset, x, bOffset, x + k);

        if (r1 !== SUCCESS) {
            return r1;
        }

        var r2 = script.match(aOffset + x, u - x);

        if (r2 !== SUCCESS) {
            return r2;
        }

        return self.diff(aOffset + u, aLength - u, bOffset + u + k, bLength - u - k);
    };

    function bail(vec, d, aOffset, aLength, bOffset, bLength) {
        var xbest = 0;
        var ybest = 0;

        var x;
        var y;

        for (var k1 = -corner(d, aLength); k1 <= corner(d, bLength); k1 += 2) {
            var x1 = Math.min(getF(vec, k1), aLength);

            if (x1 + k1 > bLength) {
                x = bLength - k1;
            } else {
                x = x1;
            }

            y = x + k1;

            if (x + y > xbest + ybest) {
                xbest = x;
                ybest = y;
            }
        }

        for (var k2 = -corner(d, bLength); k2 <= corner(d, aLength); k2 += 2) {
            var x2 = Math.max(getR(vec, k2), 0);
            var kd = k2 - (aLength - bLength);

            if (x2 + kd < 0) {
                x = -kd;
            } else {
                x = x2;
            }

            y = x + kd;

            if (aLength + bLength - x - y > xbest + ybest) {
                xbest = x;
                ybest = y;
            }

            var r = boundedDiff(aOffset, xbest, bOffset, ybest, aLength, bLength);

            if (r !== SUCCESS) {
                return r;
            }

            return boundedDiff(
                aOffset + xbest,
                aLength - xbest,
                bOffset + ybest,
                bLength - ybest,
                aLength,
                bLength);
        }
    }

    function boundedDiff(aOffset, aLength, bOffset, bLength, totalN, totalM) {
        var totalSpace = totalN * totalM;
        var nm = aLength * bLength;

        var threshold = (1 << 24) + totalSpace / 2;

        if (nm >= threshold) {
            var x = aLength / 2;
            var y = bLength / 2;

            var r1 = self.diff(aOffset, x, bOffset, y);

            if (r1 !== SUCCESS) {
                return r1;
            }

            return self.diff(aOffset + x, aLength - x, bOffset + y, bLength - y);
        } else {
            return self.diff(aOffset, aLength, bOffset, bLength);
        }
    }
}

var INSERT = function(script, start, length) {
    return script.insert(start, length);
};

var MATCH = function(script, start, length) {
    return script.match(start, length);
};

var NOOP = function() {
    return SUCCESS;
};

function coalesce(delegate, aOffset, bOffset) {
    var neverFlushed = true;

    var pendingLength = 0;
    var pendingStart = 0;
    var pending = NOOP;

    function flushPending() {
        neverFlushed &= pending === NOOP;
        return pending(delegate, pendingStart, pendingLength);
    }

    function process(op, start, length) {
        if (length > 0) {
            if (pending !== op) {
                var r = flushPending();
                if (r !== SUCCESS) {
                    return r;
                }

                pending = op;
                pendingStart = start;
                pendingLength = length;
            } else {
                pendingLength += length;
            }
        }

        return SUCCESS;
    }

    return {
        insert : function(bStart, length) {
            return process(INSERT, bStart - bOffset, length);
        },
        match : function(aStart, length) {
            return process(MATCH, aStart - aOffset, length);
        },
        delete : function(aStart, length) { 

            if (pending === INSERT) {
                return SUCCESS;
            }

            var r = flushPending();
            pending = NOOP;
            return r;
        },
        close : function(aLength, bLength) { 
            if (neverFlushed) {
                if (pending === INSERT) {
                    return REPLACE;
                } else if (pendingStart === 0 && pendingLength === aLength) {
                    return NO_CHANGE;
                }
            }

            var r = flushPending();

            if (r !== SUCCESS) {
                return r;
            }

            return delegate.close();
        }
    };
}

module.exports = function MyersBinaryDiff(maximumStorage, bailOutFactor) {
    if (maximumStorage === undefined) {
        maximumStorage = MAXIMUM_STORAGE;
    }

    if (bailOutFactor === undefined) {
        bailOutFactor = BAIL_OUT_FACTOR;
    }

    var storage = new Storage(maximumStorage);

    this.diff = function(a, aOffset, aLength, b, bOffset, bLength, editScript) {
        var script = coalesce(editScript, aOffset, bOffset);
        var execution = new Execution(storage, a, b, script, calculateBailOutLimit(aLength, bLength, bailOutFactor));

        var result = execution.diff(aOffset, aLength, bOffset, bLength);

        if (result !== SUCCESS) {
            return result;
        }

        return script.close(aLength, bLength);
    };
};

module.exports.SUCCESS = SUCCESS;
module.exports.REPLACE = REPLACE;
module.exports.NO_CHANGE = NO_CHANGE;


},{"417":417}],146:[function(require,module,exports){
var _implements = require(415)._implements;
var JSONDataType = require(77);

var AbstractDataType = require(138);
var JSONImpl = require(148);

var identity = require(414).identity;

module.exports = _implements(JSONDataType, function() {
    AbstractDataType.call(this, "json", JSONImpl, JSONImpl, JSONImpl.from, identity, [], true);

    this.fromJsonString = function(value) {
        return this.readValue(JSONImpl.from(JSON.parse(value)));
    };

    this.from = JSONImpl.from;

    this.JSON = JSONImpl;
});
},{"138":138,"148":148,"414":414,"415":415,"77":77}],147:[function(require,module,exports){
(function (Buffer){
var JSONPointerMap = require(149);
var JSONPointer = require(150);
var SpanParser = require(151);

function isPartOf(original, other, start, length) {
    return original.equalBytes(other.$buffer, other.$offset + start, length);
}

module.exports = function JSONDeltaImpl(factory, original, newValue, binaryDelta) {
    var inserted = new JSONPointerMap();
    var removed = new JSONPointerMap();

    function partOf(value, start, length) {
        return new factory(value.$buffer, value.$offset + start, length);
    }

    function copyPartOf(value, start, length) {
        var offsetStart = value.$offset + start;
        var buffer = new Buffer(length);

        value.$buffer.copy(buffer, 0, offsetStart, offsetStart + length);

        return new factory(buffer, 0, length);
    }

    if (binaryDelta !== undefined) {
        binaryDelta.visit(new DeltaVisitor(original, newValue, inserted, removed, partOf, copyPartOf));
    } else {
        inserted.put(JSONPointer.ROOT, original);
        removed.put(JSONPointer.ROOT, newValue);
    }

    this.removed = function() {
        return new ChangeMapImpl(removed);
    };

    this.inserted = function() {
        return new ChangeMapImpl(inserted);
    };

    this.hasChanges = function() {
        return removed.size !== 0 || inserted.size !== 0;
    };

    this.toString = function() {
        return ['REMOVE ', removed, ' INSERT ', inserted].join('');
    };
};

function DeltaVisitor(oldValue, newValue, inserted, removed, partOf, copyPartOf) {
    var removedSplitStructures = new JSONPointerMap();
    var insertedSplitStructures = new JSONPointerMap();

    var oldParser = new SpanParser(oldValue);
    var newParser = new SpanParser(newValue);

    var oldOffset = 0;

    var newOffset = 0;

    var pendingRemove = null;
    var pendingRemoveValue;

    var pendingInsert = null;
    var pendingInsertValue;

    this.match = function(start, length) {
        handleDelete(oldOffset, start - oldOffset);
        handleMatch(start, length);

        return true;
    };

    function handleDelete(start, length) {
        checkInvariants();

        var end = start + length;

        if (oldParser.nextByte() < end &&
            (oldParser.spanTo(end, remover) !== 0 || oldParser.nextByte() > end)) {
            newParser.spanToNext(newOffset + 1, inserter);
        }
    }

    function handleMatch(start, length) {
        checkInvariants();

        var newStart = newOffset;
        var end = start + length;

        newOffset += length;
        oldOffset = end;

        var oldNextByte = oldParser.nextByte();
        var newNextByte = newParser.nextByte();

        if (newNextByte > newStart && oldNextByte === start) {
            oldParser.spanToNext(start + 1, remover);
        } else if (oldNextByte > start && newNextByte === newStart) {
            newParser.spanToNext(newStart + 1, inserter);
        }

        var lastOld = new LastResult(removedSplitStructures);
        var lastNew = new LastResult(insertedSplitStructures);

        oldParser.spanTo(end, lastOld);
        newParser.spanTo(newOffset, lastNew);

        var oldSplit = lastOld.foundLast() && oldParser.nextByte() > end;
        var newSplit = lastNew.foundLast() && newParser.nextByte() > newOffset;

        if (oldSplit && newSplit) {
            lastOld.consumeLast(remover);
            lastNew.consumeLast(inserter);
        }
    }

    this.insert = function(bytes) {
        checkInvariants();

        newOffset += bytes.length;

        if (newParser.nextByte() < newOffset &&
            (newParser.spanTo(newOffset, inserter) !== 0 || newParser.nextByte() > newOffset)) {
            oldParser.spanToNext(oldOffset + 1, remover);
        }

        return true;
    };

    this.end = function() {
        handleDelete(oldOffset, oldValue.$length - oldOffset);

        addInsert(null, null);
        addRemove(null, null);

        replaceFullRemovedStructures();
        replaceFullInsertedStructures();
    };

    function addInsert(nextPointer, nextValue) {
        if (pendingInsert !== null) {
            inserted.put(pendingInsert, pendingInsertValue);
        }

        pendingInsert = nextPointer;
        pendingInsertValue = nextValue;
    }

    function addRemove(nextPointer, nextValue) {
        if (pendingRemove !== null) {
            removed.put(pendingRemove, pendingRemoveValue);
        }

        pendingRemove = nextPointer;
        pendingRemoveValue = nextValue;
    }

    this.noChange = function() {

    };

    function checkInvariants() {
        if (oldParser.nextByte() < oldOffset ||
            newParser.nextByte() < newOffset) {
            throw new Error("Invalid binary delta");
        }
    }

    function replaceFullRemovedStructures() {
        var i = removedSplitStructures.postOrder();

        while (i.hasNext()) {
            var s = i.next();
            var split = s.value;
            var entry = removed.getEntry(s.pointer);

            if (entry !== null && entry.numberOfChildren() === split.elements) {
                entry.setValue(copyPartOf(oldValue, split.start, split.length));
                entry.removeDescendants();
            }
        }
    }

    function replaceFullInsertedStructures() {
        var i = insertedSplitStructures.postOrder();

        while (i.hasNext()) {
            var s = i.next();
            var split = s.value;
            var entry = inserted.getEntry(s.pointer);

            if (entry !== null && entry.numberOfChildren() === split.elements) {
                entry.setValue(partOf(newValue, split.start, split.length));
                entry.removeDescendants();
            }
        }
    }

    var inserter = {
        accept : function(pointer, start, length) {
            if (pendingRemove !== null &&
                pendingRemove.equalIgnoringIndexes(pointer) &&
                isPartOf(pendingRemoveValue, newValue, start, length)) {

                pendingRemove = null;
                pendingRemoveValue = null;
            } else {
                addInsert(pointer, partOf(newValue, start, length));

                addRemove(null, null);
            }
        },
        splitStructureEnd : function(pointer, count, start, length) {
            insertedSplitStructures.put(pointer, {
                start : start,
                length : length,
                elements : count
            });
        }
    };

    var remover = {
        accept : function(pointer, start, length) {
            if (pendingInsert !== null &&
                pendingInsert.equalIgnoringIndexes(pointer) &&
                isPartOf(pendingInsertValue, oldValue, start, length)) {

                pendingInsert = null;
                pendingInsertValue = null;
            } else {
                addRemove(pointer, copyPartOf(oldValue, start, length));

                addInsert(null, null);
            }
        },
        splitStructureEnd : function(pointer, count, start, length) {
            removedSplitStructures.put(pointer, {
                start : start,
                length : length,
                elements : count
            });
        }
    };
}

function LastResult(splitStructures) {
    var last;
    var lastStart;
    var lastLength;

    this.accept = function (pointer, start, length) {
        last = pointer;
        lastStart = start;
        lastLength = length;
    };

    this.foundLast = function () {
        return !!last;
    };

    this.consumeLast = function (delegate) {
        delegate.accept(last, lastStart, lastLength);
    };

    this.splitStructureEnd = function (pointer, count, start, length) {
        splitStructures.put(pointer, {
            start: start,
            length: length,
            elements: count
        });
    };
}

function ChangeMapImpl(parts) {
    var entrySet = [];

    var i = parts.iterator();

    while (i.hasNext()) {
        var n = i.next();

        entrySet.push({
            key : n.pointer.toString(),
            value : n.value.get()
        });
    }

    this.length = entrySet.length;

    this.get = function(key) {
        return parts.get(JSONPointer.parse(key)).get();
    };

    this.entrySet = function() {
        return entrySet;
    };

    this.containsKey = function(key) {
        return parts.contains(JSONPointer.parse(key));
    };

    this.descendants = function(pointer) {
        return new ChangeMapImpl(parts.descendants(JSONPointer.parse(pointer)));
    };

    this.intersection = function(pointer) {
        return new ChangeMapImpl(parts.intersection(JSONPointer.parse(pointer)));
    };
}
}).call(this,require(17).Buffer)
},{"149":149,"150":150,"151":151,"17":17}],148:[function(require,module,exports){
var _implements = require(415)._implements;
var BytesImpl = require(143);

var BinaryDeltaTypeImpl = require(141);
var JSONDeltaImpl = require(147);
var Int64Impl = require(154);

var JSONType = require(78);

var identity = require(414).identity;

var Encoder = require(110);
var Decoder = require(109);

var encoder = new Encoder();

var BINARY_DELTA_TYPE = new BinaryDeltaTypeImpl(JSONImpl, from, identity);

function from(value) {
    if (value instanceof JSONImpl) {
        return value;
    } else {
        return new JSONImpl(encoder.encode(value).flush());
    }
}

function int64Replacer(key, value) {
    if (Int64Impl.isPrototypeOf(value)) {
        return value.toString();
    }

    return value;
}

function JSONImpl(buffer, offset, length) {
    BytesImpl.__constructor.call(this, buffer, offset, length);

    var self = this;

    this.get = function() {
        var decoder = new Decoder(buffer, offset, length);
        return decoder.nextValue();
    };

    this.diff = function(original, type) {
        var binaryDiff = self.binaryDiff(original);

        if (type === "json") {
            return new JSONDeltaImpl(JSONImpl, original, self, binaryDiff);
        } else {
            return binaryDiff;
        }
    };

    this.binaryDiff = function(original) {
        return BINARY_DELTA_TYPE.diff(original, self);
    };

    this.jsonDiff = function(original) {
        return self.diff(original, "json");
    };

    this.apply = function(delta) {
        return BINARY_DELTA_TYPE.apply(self, delta);
    };

    this.toString = function() {
        return JSON.stringify(self.get());
    };

    this.toJsonString = function() {
        return JSON.stringify(self.get(), int64Replacer);
    };
}

module.exports = _implements(JSONType, JSONImpl);
module.exports.from = from;

module.exports.toString = function() {
    return "JSONImpl";
};
},{"109":109,"110":110,"141":141,"143":143,"147":147,"154":154,"414":414,"415":415,"78":78}],149:[function(require,module,exports){
var requireNonNull = require(422);
var JSONPointer = require(150);
var EMPTY = new JSONPointerMap();

function JSONPointerMap() {
    this.root = new Entry(null, JSONPointer.ROOT);
    this.size = 0;

    var self = this;

    this.put = function(pointer, value) {
        requireNonNull(value, "value");

        var node = self.root;

        var i = 0;

        for (; i < pointer.segments.length;) {
            var s = pointer.segments[i++];
            var c = node.findChild(s);

            if (c === null) {
                node = node.addChild(s);
                break;
            }

            node = c;
        }

        for (; i < pointer.segments.length; i++) {
            node = node.addChild(pointer.segments[i]);
        }

        return node.setValue(value);
    };

    this.contains = function(pointer) {
        return self.get(pointer) !== null;
    };

    this.get = function(pointer) {
        var entry = self.getEntry(pointer);

        if (entry) {
            return entry.value;
        }

        return null;
    };

    this.getEntry = function(pointer) {
        var result = self.root;

        for (var i = 0; i < pointer.segments.length; ++i) {
            result = result.findChild(pointer.segments[i]);

            if (result === null) {
                return null;
            }
        }

        return result;
    };

    this.descendants = function(pointer) {
        var result = new JSONPointerMap();

        var thisNode = self.root;
        var resultNode = result.root;

        var segments = pointer.segments;
        var length = segments.length;

        if (length === 0) {
            return this;
        }

        for (var i = 0; i < length - 1; ++i) {
            var s = segments[i];

            thisNode = thisNode.findChild(s);

            if (thisNode === null) {
                return EMPTY;
            }

            resultNode = resultNode.addChild(s);
        }

        var last = thisNode.findChild(segments[length - 1]);

        if (last === null) {
            return EMPTY;
        }

        resultNode.children.push(last);
        result.size = last.count();

        return result;
    };

    this.intersection = function(pointer) {
        var result = new JSONPointerMap();

        var thisNode = self.root;
        var resultNode = result.root;

        var segments = pointer.segments;
        var length = segments.length;

        if (length === 0) {
            return this;
        }

        if (thisNode.value !== null) {
            resultNode.setValue(thisNode.value);
        }

        for (var i = 0; i < length - 1; ++i) {
            var s = segments[i];

            thisNode = thisNode.findChild(s);

            if (thisNode === null) {
                return result;
            }

            resultNode = resultNode.addChild(s);

            if (thisNode.value !== null) {
                resultNode.setValue(thisNode.value);
            }
        }

        var last = thisNode.findChild(segments[length - 1]);

        if (last === null) {
            return result;
        }

        resultNode.children.push(last);
        result.size += last.count();

        return result;
    };

    this.iterator = function() {
        var stack = [self.root];

        return new EntryIterator(function() {
            for (;;) {
                var r = stack.shift();

                if (r === undefined) {
                    return null;
                }

                var n = r.children.length;

                for (; n > 0; --n) {
                    stack.unshift(r.children[n - 1]);
                }

                if (r.value !== null) {
                    return r;
                }
            }
        });
    };

    this.postOrder = function() {
        var stack = [new PostOrderState(self.root)];

        return new EntryIterator(function() {
            for (;;) {
                var r = stack[0];

                if (r === undefined) {
                    return null;
                }

                var c = r.nextChild();

                if (c === null) {
                    stack.shift();

                    if (r.node.value !== null) {
                        return r.node;
                    }
                } else if (c.children.length === 0) {
                    return c;
                } else {
                    stack.unshift(new PostOrderState(c));
                }
            }
        });
    };

    this.toString = function() {
        var parts = ['{'];
        var iter = self.iterator();

        while (iter.hasNext()) {
            if (parts.length > 1) {
                parts.push(', ');
            }

            parts.push(iter.next());
        }

        parts.push('}');

        return parts.join('');
    };

    function Entry(segment, pointer) {
        this.segment = segment;
        this.pointer = pointer;
        this.children = [];
        this.value = null;

        this.count = function() {
            var result = this.value ? 1 : 0;

            for (var i = 0; i < this.children.length; ++i) {
                result += this.children[i].count();
            }

            return result;
        };

        this.addChild = function(s) {
            var c = new Entry(s, pointer.withSegment(s));
            this.children.push(c);

            return c;
        };

        this.findChild = function(s) {
            for (var i = 0; i < this.children.length; ++i) {
                if (s.equals(this.children[i].segment)) {
                    return this.children[i];
                }
            }

            return null;
        };

        this.setValue = function(newValue) {
            var previous = this.value;

            if (previous === null) {
                self.size++;
            }

            this.value = newValue;

            return previous;
        };

        this.removeDescendants = function() {
            var removed = 0;

            for (var i = 0; i < this.children.length; ++i) {
                removed += this.children[i].count();
            }

            this.children = [];
            self.size -= removed;

            return removed;
        };

        this.numberOfChildren = function() {
            var result = 0;

            for (var i = 0; i < this.children.length; ++i) {
                result += (this.children[i].value ? 1 : 0);
            }

            return result;
        };

        this.toString = function() {
            return pointer + "=" + this.value;
        };
    }
}

function PostOrderState(node) {
    this.node = node;
    var nextChild = 0;

    this.nextChild = function() {
        var children = node.children;

        if (nextChild >= children.length) {
            return null;
        }

        return children[nextChild++];
    };
}

function EntryIterator(nextWithValue) {
    var next = nextWithValue();

    this.hasNext = function() {
        return next !== null;
    };

    this.next = function() {
        if (next === null) {
            throw new Error("No such element");
        }

        var result = next;
        next = nextWithValue();
        return result;
    };
}

module.exports = JSONPointerMap;
},{"150":150,"422":422}],150:[function(require,module,exports){
function JSONPointer(nodes) {
    this.segments = nodes;
}

JSONPointer.prototype.withKey = function (key) {
    return this.withSegment(new KeySegment(key));
};

JSONPointer.prototype.withIndex = function (index) {
    return this.withSegment(new IndexSegment(index));
};

JSONPointer.prototype.withSegment = function (newNode) {
    var newNodes = this.segments.concat(newNode);
    return new JSONPointer(newNodes);
};

JSONPointer.prototype.equalIgnoringIndexes = function (other) {
    if (other === this) {
        return true;
    }

    var length = this.segments.length;
    if (length !== other.segments.length) {
        return false;
    }

    for (var i = 0; i < length; ++i) {
        var s = this.segments[i];
        var o = other.segments[i];

        if (s instanceof IndexSegment) {
            if (o instanceof KeySegment) {
                return false;
            }
        } else if (!s.equals(o)) {
            return false;
        }
    }

    return true;
};

JSONPointer.prototype.toString = function () {
    var parts = [];

    for (var i = 0; i < this.segments.length; ++i) {
        parts.push(this.segments[i].toString());
    }

    return parts.join('');
};

JSONPointer.prototype.equals = function (other) {
    if (other === this) {
        return true;
    }

    if (!other || !(other instanceof JSONPointer)) {
        return false;
    }

    if (this.segments.length !== other.segments.length) {
        return false;
    }

    var length = this.segments.length;

    for (var i = 0; i < length; ++i) {
        var s1 = this.segments[i];
        var s2 = other.segments[i];

        if (!s1.equals(s2)) {
            return false;
        }
    }

    return true;
};

JSONPointer.ROOT = new JSONPointer([]);

JSONPointer.parse = function (expression) {
    if (expression instanceof JSONPointer) {
        return expression;
    }

    var result = JSONPointer.ROOT;

    var segmentParser = new Parser(expression);
    for (;;) {
        var s = segmentParser.next();

        if (s === null) {
            return result;
        }

        result = result.withSegment(s);
    }
};

module.exports = JSONPointer;

function escape(key) {
    for (var i = 0; i < key.length; ++i) {
        var c = key.charAt(i);

        if (c === '/' || c === '~') {
            var length = key.length;
            var parts = [];

            parts.push(key.substring(0, i));

            for (var j = i; j < length; ++j) {
                var c1 = key.charAt(j);

                if (c1 === '/') {
                    parts.push('~1');
                } else if (c1 === '~') {
                    parts.push('~0');
                } else {
                    parts.push(c1);
                }
            }

            return parts.join('');
        }
    }

    return key;
}

var INDEX0 = new IndexSegment(0);

function IndexSegment(index) {
    this.index = index;
}

IndexSegment.prototype.equals = function (other) {
    if (other === this) {
        return true;
    }

    if (!other || !(other instanceof IndexSegment)) {
        return false;
    }

    return other.index === this.index;
};

IndexSegment.prototype.toString = function () {
    return '/' + this.index;
};

function KeySegment(key) {
    this.expression = '/' + escape(key);
}

KeySegment.prototype.equals = function (other) {
    if (other === this) {
        return true;
    }

    if (!other || !(other instanceof KeySegment)) {
        return false;
    }

    return other.expression === this.expression;
};

KeySegment.prototype.toString = function () {
    return this.expression;
};

function Parser(expression) {
    var p = 0;

    if (expression !== "" && expression.charAt(0) !== '/') {
        throw new Error("JSON Pointer expression must be empty or start with '/' : " + expression);
    }

    function nextSegmentString() {
        var start = p;

        for (; p < expression.length; ++p) {
            var c = expression[p];

            if (c === '/') {
                break;
            } else if (c === '~') {
                var parts = [expression.substring(start, p)];
                var length = expression.length;

                for (; p < length; ++p) {
                    var c1 = expression.charAt(p);

                    if (c1 === '/') {
                        break;
                    } else if (c1 === '~' && p !== length - 1) {
                        ++p;

                        var c2 = expression.charAt(p);

                        if (c2 === '0') {
                            parts.push('~');
                        } else if (c2 === '1') {
                            parts.push('/');
                        } else {
                            parts.push('~');
                            parts.push(c2);
                        }
                    } else {
                        parts.push(c1);
                    }
                }

                return parts.join('');
            }
        }

        return expression.substring(start, p);
    }

    function maybeIndex(s) {
        var digits = s.length;

        if (digits === 0 || digits > 10) {
            return null;
        }

        for (var i = 0; i < digits; ++i) {
            var c = s.charAt(i);

            if (c < '0' || c > '9') {
                return null;
            }

            if (c === '0' && i === 0) {
                return digits === 1 ? INDEX0 : null;
            }
        }

        var index = parseInt(s, 10);

        if (index > 2147483647) {
            return null;
        }

        return new IndexSegment(index);
    }

    this.next = function () {
        if (p === expression.length) {
            return null;
        }

        ++p;

        var s = nextSegmentString();
        var is = maybeIndex(s);

        if (is !== null) {
            return is;
        }

        return new KeySegment(s);
    };
}
},{}],151:[function(require,module,exports){
var JSONPointer = require(150);
var Tokeniser = require(111);
var consts = require(107);


function ArrayAccumulator(tokeniser, base, start, next) {
    AbstractAccumulator.call(this, tokeniser, base, start, next);

    this.take = function(consumer, base, firstAccumulatedIndex, index, start, length) {
        consumer.accept(base.withIndex(index), start, length);
    };

    this.currentPointer = function(base, total) {
        return base.withIndex(total);
    };
}

function ObjectAccumulator(tokeniser, base, start, next) {
    AbstractAccumulator.call(this, tokeniser, base, start, next);

    var _add = this.add;

    var pendingKeyName = "";
    var pendingKey = true;

    var keys = [];

    this.add = function(tokenEnd) {
        if (pendingKey) {
            this.setNextStart(tokenEnd);

            pendingKeyName = tokeniser.getToken().value;
            pendingKey = false;
        } else {
            keys[this.accumulated()] = pendingKeyName;
            pendingKey = true;

            _add(tokenEnd);
        }

        return !pendingKey;
    };

    this.take = function(consumer, base, firstAccumulatedIndex, index, start, length) {
        consumer.accept(base.withKey(keys[index - firstAccumulatedIndex]), start, length);
    };

    this.currentPointer = function(base) {
        return base.withKey(pendingKeyName);
    };
}

function RootAccumulator(tokeniser) {
    AbstractAccumulator.call(this, tokeniser, JSONPointer.ROOT, -1, 0);

    this.take = function(consumer, base, firstAccumulatedIndex, index, start, length) {
        if (this.total() > 1) {
            throw new Error("Invalid JSON: multiple values found");
        }

        consumer.accept(base, start, length);
    };

    this.currentPointer = function(base) {
        return base;
    };
}

function AbstractAccumulator(tokeniser, base, start, next) {
    var tokenRange = [];

    var accumulated = 0;
    var total = 0;

    var startOffset = start;
    var nextOffset = next;

    var self = this;

    this.total = function() {
        return total;
    };

    this.accumulated = function() {
        return accumulated;
    };

    this.newArray = function(start, next) {
        return new ArrayAccumulator(tokeniser, self.currentPointer(base, total), start, next);
    };

    this.newObject = function(start, next) {
        return new ObjectAccumulator(tokeniser, self.currentPointer(base, total), start, next);
    };

    this.notEmptyAndSplitBy = function(offset) {
        return startOffset < offset && total !== 0;
    };

    this.add = function(next) {
        tokenRange[accumulated] = [nextOffset, next];
        nextOffset = next;

        ++accumulated;
        ++total;

        return false;
    };

    this.setNextStart = function(offset) {
        nextOffset = offset;
    };

    this.skip = function() {
        ++total;
        accumulated = 0;
    };

    this.takeAll = function(consumer) {
        var next = total;
        var begin = next - accumulated;

        for (var i = 0; i < accumulated; ++i) {
            var range = tokenRange[i];
            var start = range[0];
            var end = range[1];

            self.take(consumer, base, begin, begin + i, start, end - start);
        }

        accumulated = 0;
    };

    this.splitStructureEnd = function(consumer) {
        consumer.splitStructureEnd(base, total, startOffset, nextOffset + 1 - startOffset);
    };

    this.toString = function() {
        return base.toString();
    };
}

function SpanParser(json) {
    var tokeniser = new Tokeniser(json.$buffer, json.$offset, json.$length);

    var structure = new RootAccumulator(tokeniser);
    var parentStack = [];

    var startOffset = tokeniser.offset();
    var self = this;

    this.spanToNext = function(offset, result) {
        return self.spanTo(offset, result, true);
    };

    this.spanTo = function(offset, result, atLeastOne) {
        var start = self.nextByte();

        if (start >= offset) {
            return 0;
        }

        var lastHeight = parentStack.length;

        var consumeFirstValue = atLeastOne;
        var next = start;

        var t;

        do {
            t = tokeniser.nextToken();

            var tokenStart = next;
            next = self.nextByte();

            if (t === null) {
                if (parentStack.length !== 0) {
                    throw new Error("Invalid structure");
                }

                break;
            } else if (t.type === consts.tokens.VALUE) {
                consumeFirstValue = structure.add(next);
            } else if (t.type === consts.tokens.ARRAY_START) {
                parentStack.push(structure);
                structure = structure.newArray(tokenStart, next);
            } else if (t.type === consts.tokens.MAP_START) {
                parentStack.push(structure);
                structure = structure.newObject(tokenStart, next);
            } else if (consts.isStructEnd(t.type)) {
                var parent = parentStack.pop();

                if (structure.notEmptyAndSplitBy(start)) {
                    structure.takeAll(result);
                    structure.splitStructureEnd(result);

                    parent.skip();
                    parent.setNextStart(next);
                } else {
                    parent.add(next);
                }

                structure = parent;
                consumeFirstValue = false;
            }
        } while (consumeFirstValue || next < offset || self.nextTokenIsStructEnd(t, next));

        for (var i = 0; i < parentStack.length; ++i) {
            parentStack[i].takeAll(result);
        }

        structure.takeAll(result);

        return parentStack.length - lastHeight;
    };

    this.nextTokenIsStructEnd = function(t, next) {
        var context = tokeniser.getContext();

        if (consts.isStructStart(t.type)) {
            return context.expected() === 0;
        }

        return context.acceptsBreakMarker() &&
               next < json.$length &&
               (json.$buffer[json.$offset + next] & 0x1F) === consts.additional.BREAK ||
               !context.hasRemaining();
    };

    this.nextByte = function() {
        return tokeniser.offset() - startOffset;
    };

    this.toString = function() {
        var parts = ['SpanParser', ' next=', self.nextByte(), ' ['];

        parentStack.forEach(function(x) {
            parts.push(x);
            parts.push(', ');
        });

        parts.push(structure);
        parts.push(']');

        return parts.join('');
    };
}

module.exports = SpanParser;

},{"107":107,"111":111,"150":150}],152:[function(require,module,exports){
var _implements = require(415)._implements;
var DoubleDataType = require(79);

var PrimitiveDataType = require(155);

function serialise(v, encoder) {
    encoder.encode(v);
}

module.exports = _implements(DoubleDataType, function() {
    PrimitiveDataType.call(this, "double", Number, serialise, false);
});
},{"155":155,"415":415,"79":79}],153:[function(require,module,exports){
var _implements = require(415)._implements;
var Int64DataType = require(80);

var PrimitiveDataType = require(155);
var Int64Impl = require(154);

function serialise(v, encoder) {
    encoder.writeInt64(new Int64Impl(v));
}

module.exports = _implements(Int64DataType, function() {
    PrimitiveDataType.call(this, "int64", Int64Impl, serialise, false);

    this.Int64 = Int64Impl;
});
},{"154":154,"155":155,"415":415,"80":80}],154:[function(require,module,exports){
(function (Buffer){
var _implements = require(415)._implements;
var Int64 = require(81);

var P16 = 1 << 16;
var P24 = 1 << 24;
var P32 = P16 * P16;

function putInt32(value, buffer, high) {
    var offset = high ? 0 : 4;

    for (var i = 3; i >= 0; --i) {
        buffer[offset + i] = value & 255;
        value = value >> 8;
    }
}

function getInt32(buffer, high) {
    var offset = high ? 0 : 4;

    return (buffer[offset] * P24) +
        (buffer[offset + 1] << 16) +
        (buffer[offset + 2] << 8) +
        buffer[offset + 3];
}

function putPositiveNumber(value, buffer) {
    for (var i = 7; i >= 0; --i) {
        buffer[i] = value & 255;
        value /= 256;
    }
}

function putNegativeNumber(value, buffer) {
    value++;

    for (var i = 7; i >= 0; --i) {
        buffer[i] = ((-value) & 255) ^ 255;
        value /= 256;
    }
}

function putString(value, radix, buffer) {
    radix = radix || 10;

    var negative = value[0] === "-";

    if (negative) {
        value = value.substring(1);
    } else if (value.substring(0, 2) === "0x") {
        value = value.substring(2);
        radix = 16;
    }

    var high = 0;
    var low = 0;

    for (var i = 0; i < value.length; ++i) {
        low = low * radix + parseInt(value[i], radix);
        high = high * radix + Math.floor(low / P32);

        low %= P32;
    }

    if (negative) {
        high = ~high;

        if (low) {
            low = P32 - low;
        } else {
            high++;
        }
    }

    putInt32(high, buffer, true);
    putInt32(low, buffer, false);
}

module.exports = _implements(Int64, function Int64Impl(value, radix) {
    var buffer = new Buffer([0, 0, 0, 0, 0, 0, 0, 0]);

    if (Buffer.isBuffer(value)) {
        buffer = value;
    }

    if (typeof value === 'string') {
        putString(value, radix, buffer);
    }

    if (typeof value === 'number') {
        if (typeof radix === 'number') {
            putInt32(value, buffer, true);
            putInt32(radix, buffer, false);
        } else if (value >= 0) {
            putPositiveNumber(value, buffer);
        } else {
            putNegativeNumber(value, buffer);
        }
    }

    if (value instanceof Int64Impl) {
        putInt32(value.getHighBits(), buffer, true);
        putInt32(value.getLowBits(), buffer, false);
    }

    this.getHighBits = function() {
        return getInt32(buffer, true);
    };

    this.getLowBits = function() {
        return getInt32(buffer, false);
    };

    this.toBuffer = function() {
        return buffer;
    };

    this.toNumber = function() {
        var high = this.getHighBits() | 0;
        var low = this.getLowBits();

        return high ? high * P32 + low : low;
    };

    this.isNegative = function() {
        return this.getHighBits() & 0x80000000;
    };

    this.toString = function(radix) {
        radix = radix || 10;

        var high = this.getHighBits();
        var low = this.getLowBits();

        var sign = this.isNegative();

        if (sign) {
            high = ~high;
            low = P32 - low;
        }

        if (high === 0 && low === 0) {
            return "0";
        }

        var result = "";

        while (high || low) {
            var rem = (high % radix) * P32 + low;

            high = Math.floor(high / radix);
            low = Math.floor(rem / radix);

            result = (rem % radix).toString(radix) + result;
        }

        if (sign) {
            result = "-" + result;
        }

        return result;
    };

    this.equals = function(other) {
        if (other && other instanceof Int64Impl) {
            return this.getHighBits() === other.getHighBits() && this.getLowBits() === other.getLowBits();
        }

        return false;
    };
});

module.exports.toString = function() {
    return "Int64Impl";
};
}).call(this,require(17).Buffer)
},{"17":17,"415":415,"81":81}],155:[function(require,module,exports){
(function (Buffer){
var AbstractDataType = require(138);
var BytesImpl = require(143);
var JSONImpl = require(148);

var Encoder = require(110);
var Decoder = require(109);

var encoder = new Encoder();

var NULL_CBOR = new BytesImpl(new Buffer([0xf6]));

module.exports = function(name, valueClass, serialise, withBinaryDelta) {
    function vToC(value) {
        serialise(value, encoder);
        return new BytesImpl(encoder.flush());
    }

    function cToV(bytes) {
        if (NULL_CBOR.equals(bytes)) {
            return null;
        }

        var decoder = new Decoder(bytes.$buffer, bytes.$offset, bytes.$length);

        return decoder.nextValue();
    }

    AbstractDataType.call(this, name, valueClass, BytesImpl, vToC, cToV, [JSONImpl], withBinaryDelta);

    this.writeValue = function(value) {
        if (value === null || value === undefined) {
            return NULL_CBOR.asBuffer();
        } else {
            serialise(value, encoder);
            return encoder.flush();
        }
    };

    this.validate = function() {
    };

    this.from = function(value) {
        if (value.$buffer) {
            return value;
        } else {
            return vToC(value);
        }
    };
};
}).call(this,require(17).Buffer)
},{"109":109,"110":110,"138":138,"143":143,"148":148,"17":17}],156:[function(require,module,exports){
var _implements = require(415)._implements;
var StringDataType = require(82);

var PrimitiveDataType = require(155);

function serialise(v, encoder) {
    encoder.encode(v.toString());
}

module.exports = _implements(StringDataType, function() {
    PrimitiveDataType.call(this, "string", String, serialise, true);
});
},{"155":155,"415":415,"82":82}],157:[function(require,module,exports){
var RecordV2Writer = require(166);

function resolveRecordIndex(record, records, index) {
    var actualIndex = record.getAbsoluteIndex(index);
    if (actualIndex > records.length - 1) {
        throw new Error(
            "Index '" + index +
            "' does not have a matching entry for variable occurence record: '" + record.name + "'");
    }

    return actualIndex;
}

function resolveFieldIndex(field, fields, index) {
    var actualIndex = field.getAbsoluteIndex(index);
    if (actualIndex > fields.length - 1) {
        throw new Error(
            "Index '" + index +
            "' does not have a matching entry for variable occurence field: '" + field.name + "'");
    }

    return actualIndex;
}

module.exports = function AbstractRecordModel(constructor, schema) {
    var self = this;

    this.schema = function() {
        return schema;
    };

    this.asValue = function() {
        return new constructor(RecordV2Writer.toBuffer(self.model()));
    };

    this.get = function(recordName, recordIndex, fieldName, fieldIndex) {
        if (arguments.length < 4) {
            fieldIndex = 0;
        }

        if (arguments.length === 2) {
            fieldName = recordIndex;
            recordIndex = 0;
        }

        if (arguments.length === 1) {
            var key = parseKey(recordName);

            recordName = key.recordName;
            recordIndex = key.recordIndex;
            fieldName = key.fieldName;
            fieldIndex = key.fieldIndex;
        }

        var schemaRecord = schema.getRecord(recordName);
        var schemaField = schemaRecord.getField(fieldName);

        var record = getRecord(schemaRecord, recordIndex);
        return record[resolveFieldIndex(schemaField, record, fieldIndex)];
    };

    this.recordCount = function(recordName) {
        var schemaRecord = schema.getRecord(recordName);

        if (schemaRecord.isVariable) {
            return self.model().length - schemaRecord.getIndex();
        } else {
            return schemaRecord.min;
        }
    };

    this.fieldCount = function(recordName, recordIndex, fieldName) {
        var schemaRecord = schema.getRecord(recordName);
        var schemaField = schemaRecord.getField(fieldName);

        if (schemaField.isVariable) {
            var record = getRecord(schemaRecord, recordIndex);
            return record.length - schemaField.getIndex();
        } else {
            return schemaField.min;
        }
    };

    function getRecord(schemaRecord, index) {
        var model = self.model();
        return model[resolveRecordIndex(schemaRecord, model, index)];
    }

    function parseKey(key) {
        var parts = key.split(".", 2);
        var recordName;
        var recordIndex;
        var fieldKey;

        if (parts.length === 1) {
            recordName = schema.firstRecord().name;
            recordIndex = 0;

            fieldKey = parts[0];
        } else {
            var recordKeyParts = parseKeyPart(parts[0].trim());

            recordName = recordKeyParts[0];
            recordIndex = recordKeyParts[1];

            fieldKey = parts[1];
        }

        var keyParts = parseKeyPart(fieldKey);

        return {
            recordName: recordName,
            recordIndex: recordIndex,
            fieldName: keyParts[0],
            fieldIndex: keyParts[1]
        };
    }

    function parseKeyPart(keyPart) {
        if (keyPart.length === 0) {
            throw new Error("No name specified");
        }

        var openIndex = keyPart.indexOf("(");
        if (openIndex === -1) {
            return [keyPart, 0];
        }

        if (openIndex === 0) {
            throw new Error("No name specified");
        }

        var closeIndex = keyPart.indexOf(")");
        if (closeIndex === -1 || closeIndex < openIndex) {
            throw new Error("'(' found without closing ')");
        }

        if (closeIndex < keyPart.length - 1) {
            throw new Error("Characters found after closing ')");
        }

        return [keyPart.substring(0, openIndex), parseInt(keyPart.substring(openIndex + 1, closeIndex), 10)];
    }
};
},{"166":166}],158:[function(require,module,exports){
var AbstractRecordModel = require(157);
var FieldImpl = require(167);

var args2arr = require(412).argumentsToArray;

module.exports = function MutableRecordModelImpl(constructor, schema) {
    AbstractRecordModel.call(this, constructor, schema);

    var model = schema.createModel();

    this.model = function() {
        return model;
    };

    this.set = function(recordName, recordIndex, fieldName, fieldIndex, value) {
        if (arguments.length < 5) {
            fieldIndex = 0;
        }

        if (arguments.length === 3) {
            value = fieldName;
            fieldName = recordIndex;
            recordIndex = 0;
        }

        if (arguments.length === 2) {
            value = recordIndex;

            var key = parseKey(recordName);

            recordName = key.recordName;
            recordIndex = key.recordIndex;
            fieldName = key.fieldName;
            fieldIndex = key.fieldIndex;


        }

        var schemaRecord = schema.getRecord(recordName);
        var record = getRecord(schemaRecord, recordIndex);
        var schemaField = schemaRecord.getField(fieldName);

        record[resolveFieldIndex(schemaField, record, fieldIndex)] = FieldImpl.normalise(schemaField, value);

        return this;
    };

    this.add = function(recordName, recordIndex) {
        var values = args2arr(arguments);
        var schemaRecord;
        var record;

        if (arguments.length > 2 && typeof recordIndex === "number") {
            values = values.slice(2);
            schemaRecord = schema.getRecord(recordName);
            record = getRecord(schemaRecord, recordIndex);
        } else {
            schemaRecord = schema.lastRecord();

            if (model.length > schemaRecord.index) {
                record = model[model.length - 1];
            } else {
                throw new Error("Variable record '" + schemaRecord.name + "' has no occurrences to add a field to");
            }
        }

        addValuesToRecord(schemaRecord, record, values);
        return this;
    };

    this.addRecord = function() {
        var schemaRecord = schema.lastRecord();
        if (!schemaRecord.isVariable) {
            throw new Error("Record '" + schemaRecord.name + "' is not variable");
        }

        var max = schemaRecord.max;

        if (max !== -1 && model.length - schemaRecord.index >= max) {
            throw new Error("Record '" + schemaRecord.name + "' already has maximum number of occurrences");
        }

        model.push(schemaRecord.createModel());
        return this;
    };

    this.removeRecord = function(index) {
        var schemaRecord = schema.lastRecord();
        if (!schemaRecord.isVariable) {
            throw new Error("Record '" + schemaRecord.name + "' is not variable");
        }

        var actualIndex = resolveRecordIndex(schemaRecord, model, index);

        if (model.length - schemaRecord.index - 1 < schemaRecord.min) {
            throw new Error(
                "Removing an occurrence of record '" + schemaRecord.name +
                "' would violate the minimum number of occurrences");
        }

        model.splice(actualIndex, 1);
        return this;
    };

    this.removeField = function(recordName, recordIndex, fieldIndex) {
        var schemaRecord = schema.getRecord(recordName);
        var record = getRecord(schemaRecord, recordIndex);
        var schemaField = schemaRecord.lastField();

        if (!schemaField.isVariable) {
            throw new Error("Field '" + schemaField.name + "' is not a variable field");
        }

        var actualFieldIndex = resolveFieldIndex(schemaField, record, fieldIndex);

        if (record.length - schemaField.index - 1 < schemaField.min) {
            throw new Error(
                "Removing an occurrence of field '" + schemaField.name +
                "' would violate the minimum number of occurrences");
        }

        record.splice(actualFieldIndex, 1);
        return this;
    };

    this.clearVariableRecords = function() {
        var schemaRecord = schema.lastRecord();
        model.length = schemaRecord.index + schemaRecord.min;

        return this;
    };

    this.clearVariableFields = function(recordName, recordIndex) {
        var schemaRecord = schema.getRecord(recordName);
        var record = getRecord(schemaRecord, recordIndex);
        var schemaField = schemaRecord.lastField();

        record.length = schemaField.index + schemaField.min;
        return this;
    };

    function addValuesToRecord(schemaRecord, record, values) {
        var schemaField = schemaRecord.lastField();
        if (!schemaField.isVariable) {
            throw new Error("Final field of record '" + schemaRecord.name + "' is not variable");
        }

        var max = schemaField.max;

        if (max !== -1) {
            var current = record.length - schemaField.index;
            if (current + values.length > max) {
                throw new Error("Adding values would exceed maximum number of field occurrences");
            }
        }

        values.forEach(function(value) {
            record.push(FieldImpl.normalise(schemaField, value));
        });
    }

    function getRecord(schemaRecord, index) {
        return model[resolveRecordIndex(schemaRecord, model, index)];
    }

    function parseKey(key) {
        var parts = key.split(".", 2);
        var recordName;
        var recordIndex;
        var fieldKey;

        if (parts.length === 1) {
            recordName = schema.firstRecord().name;
            recordIndex = 0;

            fieldKey = parts[0];
        } else {
            var recordKeyParts = parseKeyPart(parts[0].trim());

            recordName = recordKeyParts[0];
            recordIndex = recordKeyParts[1];

            fieldKey = parts[1];
        }

        var keyParts = parseKeyPart(fieldKey);

        return {
            recordName: recordName,
            recordIndex: recordIndex,
            fieldName: keyParts[0],
            fieldIndex: keyParts[1]
        };
    }

    function parseKeyPart(keyPart) {
        if (keyPart.length === 0) {
            throw new Error("No name specified");
        }

        var openIndex = keyPart.indexOf("(");
        if (openIndex === -1) {
            return [keyPart, 0];
        }

        if (openIndex === 0) {
            throw new Error("No name specified");
        }

        var closeIndex = keyPart.indexOf(")");
        if (closeIndex === -1 || closeIndex < openIndex) {
            throw new Error("'(' found without closing ')");
        }

        if (closeIndex < keyPart.length - 1) {
            throw new Error("Characters found after closing ')");
        }

        return [keyPart.substring(0, openIndex), parseInt(keyPart.substring(openIndex + 1, closeIndex), 10)];
    }
};

function resolveRecordIndex(record, records, index) {
    var actualIndex = record.getAbsoluteIndex(index);
    if (actualIndex > records.length - 1) {
        throw new Error(
            "Index '" + index +
            "' does not have a matching entry for variable occurence record: '" + record.name + "'");
    }

    return actualIndex;
}

function resolveFieldIndex(field, fields, index) {
    var actualIndex = field.getAbsoluteIndex(index);
    if (actualIndex > fields.length - 1) {
        throw new Error(
            "Index '" + index +
            "' does not have a matching entry for variable occurence field: '" + field.name + "'");
    }

    return actualIndex;
}
},{"157":157,"167":167,"412":412}],159:[function(require,module,exports){
var AbstractRecordModel = require(157);
var FieldImpl = require(167);

function validateModel(schema, model) {
    var modelSize = model.length;

    var lastRecord = schema.lastRecord();
    var lastIndex = lastRecord.index;
    var lastMax = lastRecord.max;

    if (lastMax !== -1 && modelSize > lastIndex + lastMax) {
        throw new Error("Too many occurrences of record '" + lastRecord.name + "'");
    }

    if (modelSize < lastIndex + lastRecord.min) {
        throw new Error("Too few record occurrences");
    }

    var schemaRecords = schema.records();

    for (var r = 0; r < schemaRecords.length; ++r) {
        var schemaRecord = schemaRecords[r];

        var startIndex = schemaRecord.index;
        var min = schemaRecord.min;

        for (var i = startIndex; i < startIndex + min; ++i) {
            validateRecord(schemaRecord, i - startIndex, model[i]);
        }

        if (schemaRecord.isVariable) {
            for (var j = startIndex + min; j < modelSize; ++j) {
                var index = j - startIndex;
                validateRecord(schemaRecord, index, model[j]);
            }
        }
    }
}

function validateRecord(schemaRecord, recordIndex, record) {
    var recordSize = record.length;
    var lastField = schemaRecord.lastField();
    var lastMax = lastField.max;
    var lastIndex = lastField.index;

    if (lastMax !== -1 && recordSize > lastIndex + lastMax) {
        throw new Error(
            "Too many occurrences of field '" + schemaRecord.name +
            "(" + recordIndex + ")." + lastField.name + "'");
    }

    if (recordSize < lastIndex + lastField.min) {
        throw new Error("Too few field occurrences in record '" + schemaRecord.name + "(" + recordIndex + ")'");
    }

    var fields = schemaRecord.fields();
    for (var i = 0; i < fields.length; ++i) {
        var schemaField = fields[i];

        var startIndex = schemaField.index;
        var min = schemaField.min;

        for (var j = startIndex; j < startIndex + min; ++j) {
            validateField(schemaRecord, recordIndex, schemaField, j - startIndex, record[j]);
        }

        if (schemaField.isVariable) {
            for (var k = startIndex + min; k < recordSize; k++) {
                var index = k - startIndex;
                validateField(schemaRecord, recordIndex, schemaField, index, record[k]);
            }
        }
    }
}

function validateField(schemaRecord, recordIndex, schemaField, index, value) {
    try {
        FieldImpl.normalise(schemaField, value);
    } catch (e) {
        throw new Error(
            "Invalid value for field '" +
            schemaRecord.name + "(" + recordIndex + ")." +
            schemaField.name + "(" + index + ")");
    }
}

module.exports = function RecordModelImpl(constructor, schema, model, validate) {
    AbstractRecordModel.call(this, constructor, schema);

    if (validate) {
        validateModel(schema, model);
    }

    this.model = function() {
        return model;
    };
};
},{"157":157,"167":167}],160:[function(require,module,exports){
var RecordV2Writer = require(166);
var RecordV2Impl = require(163);

var args2arr = require(412).argumentsToArray;

module.exports = function RecordV2BuilderImpl() {
    var records = [];

    this.addFields = function(fields) {
        if (arguments.length > 1) {
            fields = args2arr(arguments);
        }

        if (fields) {
            var current = records.length ? records[records.length - 1] : [];
            records[Math.max(records.length - 1, 0)] = current.concat(fields);
        }

        return this;
    };

    this.addRecord = function(fields) {
        if (arguments.length > 1) {
            fields = args2arr(arguments);
        }

        if (fields) {
            records.push([].concat(fields));
        } else {
            records.push([]);
        }

        return this;
    };

    this.clear = function() {
        records = [];
        return this;
    };

    this.build = function() {
        return new RecordV2Impl(RecordV2Writer.toBuffer(records));
    };
};
},{"163":163,"166":166,"412":412}],161:[function(require,module,exports){
var _implements = require(415)._implements;
var RecordV2DataType = require(83);

var AbstractDataType = require(138);

var SchemaBuilder = require(169);
var SchemaParser = require(171);

var RecordV2Builder = require(160);
var RecordV2Impl = require(163);

var requireNonNull = require(422);
var identity = require(414).identity;

function defaultValidator() { }

module.exports = _implements(RecordV2DataType, function RecordV2DataTypeImpl(validator) {
    AbstractDataType.call(this, "record_v2", RecordV2Impl, RecordV2Impl, RecordV2Impl.from, identity, [], true);

    validator = validator || defaultValidator;

    this.from = RecordV2Impl.from;

    this.validate = function(value) {
        validator(requireNonNull(value, "RecordV2 instance"));
    };

    this.parseSchema = function(json) {
        return new SchemaParser(RecordV2Impl).parse(json);
    };

    this.schemaBuilder = function() {
        return new SchemaBuilder(RecordV2Impl);
    };

    this.valueBuilder = function() {
        return new RecordV2Builder();
    };

    this.withSchema = function(schema) {
        return new RecordV2DataTypeImpl(function(value) {
            return value.asValidatedModel(schema);
        });
    };

    this.RecordV2 = RecordV2Impl;
});
},{"138":138,"160":160,"163":163,"169":169,"171":171,"414":414,"415":415,"422":422,"83":83}],162:[function(require,module,exports){
(function (Buffer){
var RecordV2Parser = require(164);
var BytesImpl = require(143);

var utils = require(165);

var NO_CHANGE = new RecordV2DeltaImpl(new Buffer([]));
NO_CHANGE.changes = function() {
    return [];
};

var Type = {
    FIELD_CHANGED : "FIELD_CHANGED",
    FIELDS_ADDED : "FIELDS_ADDED",
    FIELDS_REMOVED : "FIELDS_REMOVED",
    RECORDS_ADDED : "RECORDS_ADDED",
    RECORDS_REMOVED : "RECORDS_REMOVED"
};

function ChangeImpl(type, recordName, recordIndex, fieldName, fieldIndex) {
    this.type = type;
    this.recordName = recordName;
    this.recordIndex = recordIndex;
    this.fieldName = fieldName;
    this.fieldIndex = fieldIndex;

    var key = recordName + "(" + recordIndex + ")";

    if (type !== Type.RECORDS_REMOVED && type !== Type.RECORDS_ADDED) {
        key += "." + fieldName + "(" + fieldIndex + ")";
    }

    this.key = key;

    this.toString = function() {
        return type + " " + key;
    };
}

function RecordV2DeltaImpl(buffer, offset, length, recordChanges, fieldChanges) {
    BytesImpl.__constructor.call(this, buffer, offset, length);

    var self = this;

    this.changes = function(schema) {
        var data = RecordV2Parser.parse(buffer, offset, length);

        var recordIndex = 0;
        var changes = [];

        schema.records().forEach(function(schemaRecord) {
            if (data.length < recordIndex + schemaRecord.min) {
                throw new Error("Insufficient records of type " + schemaRecord.name + " in delta for schema");
            }

            if (schemaRecord.isVariable) {
                recordIndex = processVariableRecordChanges(schemaRecord, recordIndex, data, changes);
            } else {
                recordIndex = processFixedRecordChanges(schemaRecord, recordIndex, data, changes);
            }
        });

        return changes;
    };

    function processFixedRecordChanges(schemaRecord, startIndex, data, changes) {
        var recordIndex = startIndex;

        for (var i = 0; i < schemaRecord.min; ++i) {
            processRecordChanges(recordIndex, schemaRecord, i, data[recordIndex], changes);
            recordIndex++;
        }

        return recordIndex;
    }

    function processVariableRecordChanges(schemaRecord, startIndex, data, changes) {
        var numberOfNewRecords = data.length;
        var maxAllowed = schemaRecord.max;

        if (maxAllowed !== -1 && numberOfNewRecords > startIndex + maxAllowed) {
            throw new Error("Too many records of type " + schemaRecord.name + " in delta for schema");
        }

        var recordIndex = startIndex;
        var matchRecordCount;

        if (recordChanges && recordChanges[0]) {
            matchRecordCount = recordChanges[1];
        } else {
            matchRecordCount = numberOfNewRecords;
        }

        var varIndex = 0;

        for (; recordIndex < matchRecordCount; recordIndex++) {
            processRecordChanges(recordIndex, schemaRecord, varIndex, data[recordIndex], changes);
            varIndex++;
        }

        if (recordChanges) {
            changes.push(new ChangeImpl(
                recordChanges[0] ? Type.RECORDS_ADDED : Type.RECORDS_REMOVED,
                schemaRecord.name,
                recordChanges[1] - startIndex,
                "",
                0));
        }

        return recordIndex;
    }

    function processRecordChanges(dataRecordIndex, record, recordIndex, data, changes) {
        var fieldIndex = 0;

        record.fields().forEach(function(field) {
            if (field.isVariable) {
                fieldIndex = processVariableFieldChanges(
                    record,
                    recordIndex,
                    dataRecordIndex,
                    field,
                    fieldIndex,
                    data,
                    changes);
            } else {
                fieldIndex = processFixedFieldChanges(
                    record,
                    recordIndex,
                    field,
                    fieldIndex,
                    data,
                    changes);
            }
        });
    }

    function processFixedFieldChanges(record, recordIndex, field, startIndex, data, changes) {
        var numberOfFields = data.length;
        var fieldIndex = startIndex;

        for (var i = 0; i < field.min; ++i) {
            if (fieldIndex >= numberOfFields) {
                throw new Error(
                    "Insufficient fields of type " + field.name +
                    " in delta for schema record " + record.name);
            }

            if (data[fieldIndex] !== "") {
                changes.push(new ChangeImpl(
                    Type.FIELD_CHANGED,
                    record.name,
                    recordIndex,
                    field.name,
                    i));
            }

            fieldIndex++;
        }

        return fieldIndex;
    }

    function processVariableFieldChanges(record, recordIndex, dataRecordIndex, field, startIndex, data, changes) {
        var numberOfFields = data.length;
        var fieldIndex = startIndex;

        if (numberOfFields < fieldIndex + field.min) {
            throw new Error(
                "Insufficient fields of type " + field.name +
                " in delta for schema record " + record.name);
        }

        var fChanges = fieldChanges[dataRecordIndex];
        var matchingFieldCount;

        if (fChanges) {
            matchingFieldCount = fChanges[1];
        } else {
            matchingFieldCount = numberOfFields;
        }

        var varIndex = 0;

        for (; fieldIndex < matchingFieldCount; fieldIndex++) {
            if (data[fieldIndex] !== "") {
                changes.push(new ChangeImpl(
                    Type.FIELD_CHANGED,
                    record.name,
                    recordIndex,
                    field.name,
                    varIndex));
            }

            varIndex++;
        }

        if (fChanges) {
            changes.push(new ChangeImpl(
                fChanges[0] ? Type.FIELDS_ADDED : Type.FIELDS_REMOVED,
                record.name,
                recordIndex,
                field.name,
                fChanges[1]));
        }

        return fieldIndex;
    }

    this.toString = function() {
        return utils.bytesToString(self.$buffer, self.$offset, self.$length);
    };
}

module.exports = RecordV2DeltaImpl;
module.exports.NO_CHANGE = NO_CHANGE;
}).call(this,require(17).Buffer)
},{"143":143,"164":164,"165":165,"17":17}],163:[function(require,module,exports){
(function (Buffer){
var _implements = require(415)._implements;
var identity = require(414).identity;

var BufferOutputStream = require(202);
var BinaryDeltaTypeImpl = require(141);
var BytesImpl = require(143);

var RecordV2Type = require(84);

var BINARY_DELTA_TYPE = new BinaryDeltaTypeImpl(RecordV2Impl, from, identity);
var RECORD_PARSER = require(164);

var RecordModelImpl = require(159);
var RecordV2Delta = require(162);

var utils = require(165);

function from(value) {
    if (value instanceof RecordV2Impl) {
        return value;
    } else {
        return new RecordV2Impl(value);
    }
}

function RecordV2Impl(buffer, offset, length) {
    BytesImpl.__constructor.call(this, buffer, offset, length);

    var self = this;

    this.apply = function(delta) {
        return BINARY_DELTA_TYPE.apply(self, delta);
    };

    this.toString = function() {
        return JSON.stringify(self.asRecords());
    };

    this.asRecords = function() {
        return RECORD_PARSER.parse(self.$buffer, self.$offset, self.$length);
    };

    this.asFields = function() {
        return self.asRecords().reduce(function(fields, record) {
            return fields.concat(record);
        }, []);
    };

    this.asModel = function(schema) {
        return new RecordModelImpl(RecordV2Impl, schema, self.asRecords(), false);
    };

    this.asValidatedModel = function(schema) {
        return new RecordModelImpl(RecordV2Impl, schema, self.asRecords(), true);
    };

    this.binaryDiff = function(original) {
        return BINARY_DELTA_TYPE.diff(original, self);
    };

    this.diff = function(original) {
        var oldBytes = original.$buffer;
        var oldOffset = original.$offset;
        var oldLength = original.$length;

        var newBytes = self.$buffer;
        var newOffset = self.$offset;
        var newLength = self.$length;

        if (newLength === 0) {
            if (oldLength === 0) {
                return RecordV2Delta.NO_CHANGE;
            }

            return deltaForEmptyNewValue();
        } else if (utils.isSingleEmptyRecord(newBytes, newOffset, newLength)) {
            if (utils.isSingleEmptyRecord(oldBytes, oldOffset, oldLength)) {
                return RecordV2Delta.NO_CHANGE;
            }

            return deltaForSingleEmptyRecordNewValue(utils.recordCount(oldBytes, oldOffset, oldLength));
        }

        if (oldLength === 0) {
            return deltaForAllNewRecords(newBytes, newOffset, newLength);
        } else if (utils.isSingleEmptyRecord(oldBytes, oldOffset, oldLength)) {
            return deltaForOldSingleEmptyRecord(newBytes, newOffset, newLength);
        }

        var result = diffRecords(
            oldBytes, oldOffset, oldLength,
            newBytes, newOffset, newLength);

        if (result.hasChanges()) {
            var delta = result.getDelta();

            return new RecordV2Delta(
                delta,
                0,
                delta.length,
                result.getRecordChanges(),
                result.getFieldChanges());
        } else {
            return RecordV2Delta.NO_CHANGE;
        }
    };
}

function diffRecords(
    oldBytes, oldOffset, oldLength,
    newBytes, newOffset, newLength) {

    var result = new DiffResult();

    var oldCount = utils.recordCount(oldBytes, oldOffset, oldLength);
    var newCount = utils.recordCount(newBytes, newOffset, newLength);

    var same = Math.min(oldCount, newCount);

    var oldStart = oldOffset;
    var oldEnd = oldOffset;
    var newStart = newOffset;
    var newEnd = newOffset;

    var delimiter = false;

    for (var i = 0; i < same; ++i) {
        oldEnd = utils.findDelimiter(oldBytes, oldEnd, oldOffset + oldLength, utils.RECORD_DELIMITER);
        newEnd = utils.findDelimiter(newBytes, newEnd, newOffset + newLength, utils.RECORD_DELIMITER);

        if (delimiter) {
            result.write(utils.RECORD_DELIMITER);
        } else {
            delimiter = true;
        }

        diffRecord(
            i,
            oldBytes, oldStart, oldEnd - oldStart,
            newBytes, newStart, newEnd - newStart, result);

        oldStart = ++oldEnd;
        newStart = ++newEnd;
    }

    if (newCount !== oldCount) {
        if (newCount > oldCount) {
            result.write(newBytes, newStart - 1, newOffset + newLength - newStart + 1);
        }

        result.setRecordsChanged(newCount > oldCount, same);
    }

    return result;
}

function diffRecord(
    recordIndex,
    oldBytes, oldOffset, oldLength,
    newBytes, newOffset, newLength,
    result) {

    if (newLength === 0) {
        if (oldLength !== 0) {
            result.setFieldsRemoved(recordIndex, 0);
        }

        return;
    }

    if (utils.recordIsSingleEmptyField(newBytes, newOffset, newLength)) {
        diffWhenNewIsSingleEmptyField(recordIndex, oldBytes, oldOffset, oldLength, result);
        return;
    }

    if (oldLength === 0) {
        result.write(newBytes, newOffset, newLength);
        result.setFieldsAdded(recordIndex, 0);
        return;
    }

    if (utils.recordIsSingleEmptyField(oldBytes, oldOffset, oldLength)) {
        diffWhenOldIsSingleEmptyField(recordIndex, newBytes, newOffset, newLength, result);
        return;
    }

    diffRecordFields(recordIndex, oldBytes, oldOffset, oldLength, newBytes, newOffset, newLength, result);
}

function diffWhenNewIsSingleEmptyField(recordIndex, oldBytes, oldOffset, oldLength, result) {
    if (utils.recordIsSingleEmptyField(oldBytes, oldOffset, oldLength)) {
        result.write(utils.FIELD_MU);
    } else {
        var oldCount = utils.fieldCount(oldBytes, oldOffset, oldLength);
        if (oldCount > 1) {
            result.setFieldsRemoved(recordIndex, 1);
        }

        result.write(utils.EMPTY_FIELD);
        result.setChanged();
    }
}

function diffWhenOldIsSingleEmptyField(recordIndex, newBytes, newOffset, newLength, result) {
    var b = newBytes[newOffset];

    if (b === utils.EMPTY_FIELD) {
        result.write(newBytes, newOffset + 1, newLength - 1);
    } else {
        result.write(newBytes, newOffset, newLength);

        if (b !== utils.FIELD_DELIMITER) {
            result.setChanged();
        }
    }

    if (utils.fieldCount(newBytes, newOffset, newLength) > 1) {
        result.setFieldsAdded(recordIndex, 1);
    }
}

function diffRecordFields(recordIndex, oldBytes, oldOffset, oldLength, newBytes, newOffset, newLength, result) {
    var oldCount = utils.fieldCount(oldBytes, oldOffset, oldLength);
    var newCount = utils.fieldCount(newBytes, newOffset, newLength);

    var same = Math.min(oldCount, newCount);

    var oldStart = oldOffset;
    var oldEnd = oldOffset;
    var newStart = newOffset;
    var newEnd = newOffset;

    var startSize = result.size();
    var delimiter = false;

    for (var i = 0; i < same; ++i) {
        oldEnd = utils.findDelimiter(oldBytes, oldEnd, oldOffset + oldLength, utils.FIELD_DELIMITER);
        newEnd = utils.findDelimiter(newBytes, newEnd, newOffset + newLength, utils.FIELD_DELIMITER);

        if (delimiter) {
            result.write(utils.FIELD_DELIMITER);
        } else {
            delimiter = true;
        }

        diffField(
            oldBytes, oldStart, oldEnd - oldStart,
            newBytes, newStart, newEnd - newStart,
            result);

        oldStart = ++oldEnd;
        newStart = ++newEnd;
    }

    if (newCount !== oldCount) {
        if (newCount > oldCount) {
            result.write(newBytes, newStart - 1, newOffset + newLength - newStart + 1);
            result.setFieldsAdded(recordIndex, same);
        } else {
            result.setFieldsRemoved(recordIndex, same);
        }
    }

    if (newCount === 1 && startSize === result.size()) {
        result.write(utils.FIELD_MU);
    }
}

function diffField(oldBytes, oldOffset, oldLength, newBytes, newOffset, newLength, result) {
    if (fieldsAreEqual(oldBytes, oldOffset, oldLength, newBytes, newOffset, newLength)) {
        return;
    }

    if (newLength === 0) {
        result.write(utils.EMPTY_FIELD);
    } else {
        result.write(newBytes, newOffset, newLength);
    }

    result.setChanged();
}

function fieldsAreEqual(oldBytes, oldOffset, oldLength, newBytes, newOffset, newLength) {
    if (oldLength === newLength) {
        if (oldLength === 0 || fieldEquals(oldBytes, oldOffset, newBytes, newOffset, oldLength)) {
            return true;
        }
    } else if (oldLength === 1 && oldBytes[oldOffset] === utils.EMPTY_FIELD && newLength === 0) {
        return true;
    } else if (newLength === 1 && newBytes[newOffset] === utils.EMPTY_FIELD && oldLength === 0) {
        return true;
    }

    return false;
}

function fieldEquals(oldBytes, oldOffset, newBytes, newOffset, length) {
    for (var i = 0; i < length; ++i) {
        if (oldBytes[oldOffset + i] !== newBytes[newOffset + i]) {
            return false;
        }
    }

    return true;
}

function deltaForEmptyNewValue() {
    return new RecordV2Delta(new Buffer([]), 0, 0, [false, 0], {});
}

function deltaForSingleEmptyRecordNewValue(oldRecordCount) {
    switch (oldRecordCount) {
        case 0 :
            return new RecordV2Delta(new Buffer([utils.RECORD_MU]), 0, 1, [true, 0], {});
        case 1 :
            return new RecordV2Delta(new Buffer([utils.RECORD_MU]), 0, 1, null, { 0 : [false, 0] });
        default :
            return new RecordV2Delta(new Buffer([utils.RECORD_MU]), 0, 1, [false, 1], { 0 : [false, 0] });
    }
}

function deltaForAllNewRecords(newBytes, newOffset, newLength) {
    return new RecordV2Delta(newBytes, newOffset, newLength, [true, 0], {});
}

function deltaForOldSingleEmptyRecord(newBytes, newOffset, newLength) {
    var newRecordCount = utils.recordCount(newBytes, newOffset, newLength);
    var recordChanges = newRecordCount === 1 ? null : [true, 0];

    return new RecordV2Delta(newBytes, newOffset, newLength, recordChanges, { 0 : [true, 0] });
}

function DiffResult() {
    var delta = new BufferOutputStream();
    var hasChanges = false;
    var recordChanges = null;
    var fieldChanges = {};

    this.write = function(b, o, l) {
        if (o !== undefined && l !== undefined) {
            delta.writeMany(b, o, l);
        } else {
            delta.write(b);
        }
    };

    this.getDelta = function() {
        return delta.getBuffer();
    };

    this.size = function() {
        return delta.count;
    };

    this.hasChanges = function() {
        return hasChanges;
    };

    this.setChanged = function() {
        hasChanges = true;
    };

    this.setRecordsChanged = function(added, index) {
        recordChanges = [added, index];
        hasChanges = true;
    };

    this.getRecordChanges = function() {
        return recordChanges;
    };

    this.setFieldsAdded = function(recordIndex, fieldIndex) {
        fieldChanges[recordIndex] = [true, fieldIndex];
        hasChanges = true;
    };

    this.setFieldsRemoved = function(recordIndex, fieldIndex) {
        fieldChanges[recordIndex] = [false, fieldIndex];
        hasChanges = true;
    };

    this.getFieldChanges = function() {
        return fieldChanges;
    };
}

module.exports = _implements(RecordV2Type, RecordV2Impl);
module.exports.from = from;

module.exports.toString = function() {
    return "RecordV2Impl";
};
}).call(this,require(17).Buffer)
},{"141":141,"143":143,"159":159,"162":162,"164":164,"165":165,"17":17,"202":202,"414":414,"415":415,"84":84}],164:[function(require,module,exports){
var RECORD_DELIMITER = 0x1;
var FIELD_DELIMITER = 0x2;
var RECORD_MU = 0x4;
var FIELD_MU = 0x5;

module.exports.parse = function(bytes, offset, length) {
    offset = offset || 0;
    length = length || bytes.length;

    if (length === 0) {
        return [];
    }

    if (length === 1 && bytes[offset] === RECORD_MU) {
        return [[]];
    }

    var records = [];
    var fields = [];

    var pos = offset;

    for (var i = offset; i < length; ++i) {
        if (bytes[i] === RECORD_DELIMITER) {
            processRecordDelimiter(bytes, records, pos, i, fields);
            pos = i + 1;
        } else if (bytes[i] === FIELD_DELIMITER) {
            addField(bytes, records, pos, i, fields);
            pos = i + 1;
        }
    }

    processEnd(bytes, records, pos, length, fields);
    return records;
};

function processRecordDelimiter(bytes, records, startPos, endPos, fields) {
    var written = false;

    if (startPos < endPos) {
        written = addField(bytes, records, startPos, endPos, fields);
    } else {
        if (startPos > 0 && bytes[startPos - 1] === FIELD_DELIMITER) {
            fields.push("");
        }
    }

    if (!written) {
        records.push([].concat(fields));
    }

    fields.length = 0;
}

function processEnd(bytes, records, startPos, endPos, fields) {
    if (endPos > startPos) {
        addField(bytes, records, startPos, endPos, fields);
    } else {
        var endByte = bytes[endPos - 1];

        if (endByte === RECORD_DELIMITER) {
            records.push([]);
        } else if (endByte === FIELD_DELIMITER) {
            fields.push("");
        }
    }

    if (fields.length) {
        records.push([].concat(fields));
    }
}

function addField(bytes, records, startPos, endPos, fields) {
    var len = endPos - startPos;

    if (fields.length === 0 && len === 1 && bytes[startPos] === FIELD_MU) {
        records.push([""]);
        return true;
    } else {
        fields.push(bytes.toString("utf8", startPos, endPos));
    }
}
},{}],165:[function(require,module,exports){
var RECORD_DELIMITER = 0x1;
var FIELD_DELIMITER = 0x2;
var EMPTY_FIELD = 0x3;
var RECORD_MU = 0x4;
var FIELD_MU = 0x5;

module.exports.bytesToString = function(bytes, offset, length) {
    var ret = "";

    for (var i = offset; i < offset + length; ++i) {
        switch (bytes[i]) {
            case FIELD_DELIMITER :
                ret += "<FD>";
                break;
            case RECORD_DELIMITER :
                ret += "<RD>";
                break;
            case EMPTY_FIELD :
                ret += "<EF>";
                break;
            case FIELD_MU :
                ret += "<FM>";
                break;
            case RECORD_MU :
                ret += "<RM>";
                break;
            default :
                ret += String.fromCharCode(bytes[i]);
        }
    }

    return ret;
};

module.exports.recordCount = function(bytes, offset, length) {
    if (length === 0) {
        return 0;
    }

    if (bytes[0] === RECORD_MU) {
        return 1;
    }

    var count = 1;

    for (var i = offset; i < offset + length; i++) {
        if (bytes[i] === RECORD_DELIMITER) {
            count++;
        }
    }

    return count;
};

module.exports.fieldCount = function(bytes, offset, length) {
    if (length === 0) {
        return 0;
    }

    if (bytes[0] === FIELD_MU) {
        return 1;
    }

    var count = 1;

    for (var i = offset; i < offset + length; i++) {
        if (bytes[i] === FIELD_DELIMITER) {
            count++;
        }
    }

    return count;
};

module.exports.findDelimiter = function(bytes, startIndex, endIndex, delimiter) {
    var pos = startIndex;

    for (; pos < endIndex; pos++) {
        if (bytes[pos] === delimiter) {
            break;
        }
    }

    return pos;
};

module.exports.isSingleEmptyRecord = function(bytes, offset, length) {
    return length === 1 && bytes[offset] === RECORD_MU;
};

module.exports.recordIsSingleEmptyField = function(bytes, offset, length) {
    return length === 1 && (bytes[offset] === FIELD_MU || bytes[offset] === EMPTY_FIELD);
};

module.exports.RECORD_DELIMITER = RECORD_DELIMITER;
module.exports.RECORD_DELIMITER_STRING = String.fromCharCode(RECORD_DELIMITER);

module.exports.FIELD_DELIMITER = FIELD_DELIMITER;
module.exports.FIELD_DELIMITER_STRING = String.fromCharCode(FIELD_DELIMITER);

module.exports.EMPTY_FIELD = EMPTY_FIELD;
module.exports.EMPTY_FIELD_STRING = String.fromCharCode(EMPTY_FIELD);

module.exports.RECORD_MU = RECORD_MU;
module.exports.RECORD_MU_STRING = String.fromCharCode(RECORD_MU);

module.exports.FIELD_MU = FIELD_MU;
module.exports.FIELD_MU_STRING = String.fromCharCode(FIELD_MU);
},{}],166:[function(require,module,exports){
(function (Buffer){
var BufferOutputStream = require(202);
var consts = require(165);

function write(records, out) {
    if (records.length === 0) {
        return;
    }

    if (records.length === 1 && records[0].length === 0) {
        out.write(consts.RECORD_MU);
        return;
    }

    var recordDelimiter = false;

    records.forEach(function(record) {
        if (recordDelimiter) {
            out.write(consts.RECORD_DELIMITER);
        } else {
            recordDelimiter = true;
        }

        if (record.length === 1 && record[0] === "") {
            out.write(consts.FIELD_MU);
        } else {
            var fieldDelimiter = false;

            record.forEach(function(field) {
                if (fieldDelimiter) {
                    out.write(consts.FIELD_DELIMITER);
                }  else {
                    fieldDelimiter = true;
                }

                if (field !== undefined && field !== null) {
                    out.writeMany(new Buffer(field));
                }
            });
        }
    });
}

module.exports.write = write;
module.exports.toBuffer = function(records) {
    var bos = new BufferOutputStream();

    write(records, bos);

    return bos.getBuffer();
};
}).call(this,require(17).Buffer)
},{"165":165,"17":17,"202":202}],167:[function(require,module,exports){
var Type = {
    DECIMAL : "DECIMAL",
    INTEGER : "INTEGER",
    STRING : "STRING"
};

module.exports = function FieldImpl(name, type, min, max, index, scale) {
    this.name = name;
    this.type = type;
    this.min = min;
    this.max = max;

    if (scale !== undefined) {
        this.scale = scale;
    }

    this.index = index;

    this.isVariable = min !== max;

    this.getAbsoluteIndex = function(_index) {
        if (_index < 0 || max !== -1 && _index > max - 1) {
            throw new Error("Invalid index '" + _index + "' for field '" + name + "'");
        }

        return index + _index;
    };

    this.toString = function() {
        var ret = "Field [Name=" + name + ", Type=" + type;

        if (min === max) {
            ret += ", Occurs=" + min;
        } else {
            ret += ", Min=" + min + ", Max=";

            if (max === -1) {
                ret += "Unlimited";
            } else {
                ret += max;
            }
        }

        if (type === Type.DECIMAL) {
            ret += ", Scale=" + scale;
        }

        return ret + "]";
    };
};

module.exports.normalise = function(field, value) {
    switch (field.type) {
        case Type.DECIMAL :
            var f = parseFloat(value);
            if (Number.isNaN(f)) {
                throw new Error("Invalid decimal value: '" + value + "'");
            }

            return f.toFixed(field.scale);
        case Type.INTEGER :
            var i = parseInt(value);
            var s = i.toString();

            if (Number.isNaN(i) || s.indexOf(".") > -1) {
                throw new Error("Invalid integer value: '" + value + '');
            }

            return s;
        case Type.STRING :
            return value;
        default :
            return "";
    }
};

module.exports.toJSON = function(field) {
    var o = {
        name : field.name,
        type : field.type,
        min : field.min,
        max : field.max
    };

    if (field.type === Type.DECIMAL) {
        o.scale = field.scale;
    }

    return o;
};

module.exports.modelValue = function(field) {
    switch (field.type) {
        case Type.DECIMAL :
            return decimalModelValue(field);
        case Type.INTEGER :
            return "0";
        case Type.STRING :
            return "";
        default:
            return "";
    }
};

function decimalModelValue(field) {
    switch (field.scale) {
        case 1 :
            return "0.0";
        case 2 :
            return "0.000";
        case 3 :
            return "0.000";
        default :
            var ret = "0.";

            for (var i = 0; i < field.scale; ++i) {
                ret += "0";
            }

            return ret;
    }
}

module.exports.Type = Type;
},{}],168:[function(require,module,exports){
var FieldImpl = require(167);

module.exports = function RecordImpl(name, min, max, index, _fields) {
    this.name = name;
    this.min = min;
    this.max = max;

    this.index = index;

    this.isVariable = min !== max;

    var fields = {};

    _fields.forEach(function(field) {
        fields[field.name] = field;
    });

    this.fields = function() {
        return _fields;
    };

    this.getFields = function() {
        return [].concat(_fields);
    };

    this.getField = function(fieldName) {
        var field = fields[fieldName];

        if (field) {
            return field;
        }

        throw new Error("Record '" + name + "' has no field named '" + fieldName + "'");
    };

    this.lastField = function() {
        return _fields[_fields.length - 1];
    };

    this.createModel = function() {
        var fields = [];

        _fields.forEach(function(field) {
            for (var i = 0; i < field.min; ++i) {
                fields.push(FieldImpl.modelValue(field));
            }
        });

        return fields;
    };

    this.getAbsoluteIndex = function(_index) {
        if (_index < 0 || max !== -1 && _index > max - 1) {
            throw new Error("Invalid index '" + _index + "' for record '" + name + "'");
        }

        return index + _index;
    };

    this.toString = function() {
        var ret = "Record [Name=" + name;

        if (min === max) {
            ret += ", Occurs=" + min;
        } else {
            ret += ", Min=" + min + ", Max=";

            if (max === -1) {
                ret += "Unlimited";
            } else {
                ret += max;
            }
        }

        ret +=", Fields=" + _fields + "]";
        return ret;
    };
};

module.exports.toJSON = function(record) {
    var sanitised = {};

    record.fields().forEach(function(field) {
        sanitised[field.name] = FieldImpl.toJSON(field);
    }, {});

    return {
        fields : record.fields().map(FieldImpl.toJSON),
        name : record.name,
        min : record.min,
        max : record.max
    };
};
},{"167":167}],169:[function(require,module,exports){
var requireNonNull = require(422);

var SchemaImpl = require(170);
var RecordImpl = require(168);
var FieldImpl = require(167);

function checkMultiplicity(name, min, max) {
    if (min < 0) {
        throw new Error("'" + name + "' has invalid 'min' value '" + min + "'");
    }

    if (max !== -1 && (max < 1 || max < min)) {
        throw new Error("'" + name + "' has invalid 'max' value '" + max + "'");
    }
}

function checkScale(name, scale) {
    if (scale < 1) {
        throw new Error("'" + name + "' has invalid 'scale' value '" + scale + "'");
    }
}

module.exports = function SchemaBuilderImpl(constructor) {
    var records = [];

    var currentName, currentFields, currentMin, currentMax;

    this.record = function(name, min, max) {
        min = typeof min === "number" ? min : 1;
        max = typeof max === "number" ? max : min;

        checkMultiplicity(name, min, max);
        checkRecord(name);

        if (currentFields !== undefined) {
            addRecord();
        }

        currentName = name;
        currentMin = min;
        currentMax = max;

        currentFields = [];

        return this;
    };

    this.string = function(name, min, max) {
        min = typeof min === "number" ? min : 1;
        max = typeof max === "number" ? max : min;

        checkMultiplicity(name, min, max);
        checkField(name);

        currentFields.push(new FieldImpl(name, FieldImpl.Type.STRING, min, max, fieldIndex()));
        return this;
    };

    this.integer = function(name, min, max) {
        min = typeof min === "number" ? min : 1;
        max = typeof max === "number" ? max : min;

        checkMultiplicity(name, min, max);
        checkField(name);

        currentFields.push(new FieldImpl(name, FieldImpl.Type.INTEGER, min, max, fieldIndex()));
        return this;
    };

    this.decimal = function(name, scale, min, max) {
        min = typeof min === "number" ? min : 1;
        max = typeof max === "number" ? max : min;

        checkMultiplicity(name, min, max);
        checkScale(name, scale);
        checkField(name);

        currentFields.push(new FieldImpl(name, FieldImpl.Type.DECIMAL, min, max, fieldIndex(), scale));
        return this;
    };

    this.build = function() {
        if (currentFields !== undefined) {
            addRecord();
        }

        if (records.length === 0) {
            throw new Error("Schema has no records");
        }

        return new SchemaImpl(constructor, records);
    };

    function fieldIndex() {
        if (currentFields.length === 0) {
            return 0;
        }

        var lastField = currentFields[currentFields.length - 1];
        return lastField.index + lastField.max;
    }

    function addRecord() {
        if (currentFields.length === 0) {
            throw new Error("No fields specified for record: '" + currentName + "'");
        }

        var index;

        if (records.length === 0) {
            index = 0;
        } else {
            var lastRecord = records[records.length - 1];
            index = lastRecord.index + lastRecord.max;
        }

        records.push(new RecordImpl(currentName, currentMin, currentMax, index, currentFields));
    }

    function checkRecord(name) {
        requireNonNull(name, "name");

        if (name === currentName) {
            throw new Error("Duplicate record name: '" + name + "'");
        }

        if (records.length) {
            for (var i = 0; i < records.length; ++i) {
                if (name === records[i].name) {
                    throw new Error("Duplicate record name: '" + name + "'");
                }
            }
        }

        if (currentFields !== undefined && currentMin !== currentMax) {
            throw new Error(
                "Record '" + name +
                "' not allowed after variable multiplicity record: '" + currentName + "'");
        }
    }

    function checkField(name) {
        requireNonNull(name, "name");

        if (currentFields === undefined) {
            throw new Error("Cannot add field '" + name + "' as there is no current record");
        }

        if (currentFields.length) {
            for (var i = 0; i < currentFields.length; ++i) {
                if (name === currentFields[i].name) {
                    throw new Error("Duplicate field name: '" + name + "'");
                }
            }

            var lastField = currentFields[currentFields.length - 1];

            if (lastField.isVariable) {
                throw new Error(
                    "Field '" + name +
                    "' is not allowed after variable multiplicity field '" + lastField.name + "'");
            }
        }
    }
};
},{"167":167,"168":168,"170":170,"422":422}],170:[function(require,module,exports){
var MutableRecordModelImpl = require(158);
var RecordImpl = require(168);

module.exports = function SchemaImpl(constructor, _records) {
    var records = {};
    var self = this;

    _records.forEach(function(record) {
        records[record.name] = record;
    });

    this.getRecord = function(name) {
        var record = records[name];

        if (record) {
            return record;
        }

        throw new Error("Record '" + name + "' is not known");
    };

    this.getRecords = function() {
        return [].concat(_records);
    };

    this.records = function() {
        return _records;
    };

    this.firstRecord = function() {
        return _records[0];
    };

    this.lastRecord = function() {
        return _records[_records.length - 1];
    };

    this.createMutableModel = function() {
        return new MutableRecordModelImpl(constructor, this);
    };

    this.createModel = function() {
        var model = [];

        _records.forEach(function(record) {
            var recordValue = record.createModel();

            if (record.min > 0) {
                for (var i = 0; i < record.min; ++i) {
                    model.push([].concat(recordValue));
                }
            }
        });

        return model;
    };

    this.asJSONString = function() {
        return JSON.stringify({ records : _records.map(RecordImpl.toJSON) });
    };

    this.equals = function(other) {
        if (other && other instanceof SchemaImpl) {
            var s1 = self.asJSONString();
            var s2 = other.asJSONString();

            return s1 === s2;
        }

        return false;
    };

    this.toString = function() {
        return "Schema [Records=" + _records + "]";
    };
};
},{"158":158,"168":168}],171:[function(require,module,exports){
var SchemaBuilder = require(169);

module.exports = function SchemaParserImpl(constructor) {
    this.parse = function(jsonString) {
        var builder = new SchemaBuilder(constructor);
        var json;

        try {
            json = JSON.parse(jsonString);
        } catch (e) {
            throw new Error("Invalid Schema JSON string");
        }

        if (json.records === undefined) {
            throw new Error("Schema has no records");
        }

        json.records.forEach(function(record) {
            if (record.occurs !== undefined) {
                builder.record(record.name, record.occurs);
            } else {
                builder.record(record.name, record.min, record.max);
            }

            if (record.fields) {
                record.fields.forEach(function(field) {
                    var min;
                    var max;

                    if (field.occurs !== undefined) {
                        min = field.occurs;
                        max = field.occurs;
                    } else {
                        min = field.min;
                        max = field.max;
                    }

                    var type = field.type || "STRING";

                    switch (type.toUpperCase()) {
                        case 'STRING' :
                            builder.string(field.name, min, max);
                            break;
                        case 'INTEGER' :
                            builder.integer(field.name, min, max);
                            break;
                        case 'DECIMAL' :
                            var scale = field.scale || 2;
                            builder.decimal(field.name, scale, min, max);
                            break;
                        default :
                            throw new Error("Invalid field type value '" + type + "'");
                    }
                });
            }
        });

        return builder.build();
    };
};
},{"169":169}],172:[function(require,module,exports){
module.exports.assignInternals = function(self, buffer, offset, length) {
    self.$buffer = buffer;
    self.$offset = offset || 0;
    self.$length = length === undefined ? buffer.length : length;
};
},{}],173:[function(require,module,exports){
(function (process){
var functions = require(414);
var array = require(412);

var Stream = require(175);

function Emitter(e, fn, events) {
    var listeners = {};

        var listener = function() { };
    var closed = false;

        var emit = function(event, args) {
        args = args || [];

        var dispatch = function(listener) {
            functions.callWithArguments(listener, args);
        };

        process.nextTick(function() {
            if (listeners[event]) {
                listeners[event].forEach(dispatch);
            }
        });

        listener(event, args);
    };

    var publicEmit = function(event) {
        if (closed || !event) {
            return;
        }

                var args = array.argumentsToArray(arguments);
        args.shift();

                emit(event, args);
    };

        var error = function(reason) {
        emit('error', [reason]);
        close();
    };

        var close = function(reason) {
        if (closed) {
            return;
        }

                closed = true;
        emit('close', [reason]);
    };

        var stream = new Stream(listeners, error, close, events);

        if (e !== undefined) {
        stream.on(e, fn);
    }

        this.listen = function(fn) {
        listener = fn;
    };

        this.get = function() {
        return stream;
    };

            this.close = close;
    this.error = error;
    this.emit = publicEmit;

    this.assign = function(instance) {
        for (var k in stream) {
            instance[k] = stream[k];
        }
    };
}

Emitter.assign = function(instance) {
    var emitter = new Emitter();
    var s = emitter.get();

        for (var k in s) {
        instance[k] = s[k];
    }

        return emitter;
};

module.exports = Emitter;

}).call(this,require(23))
},{"175":175,"23":23,"412":412,"414":414}],174:[function(require,module,exports){
var _implements = require(415)._implements;
var Result = require(89);

var when = require(64);

function ResultImpl(emitter) {
    var promise = when.promise(function(fulfilled, rejected) {
        emitter.listen(function(e, args) {
            if (e === 'error') {
                rejected(args[0]);
            } else {
                fulfilled(args[0]);
            }
        });
    });

        this.then = function(fulfillment, rejected, partial) {
        promise = promise.then(fulfillment, rejected, partial);
        return promise;
    };
}

module.exports = _implements(Result, ResultImpl);

},{"415":415,"64":64,"89":89}],175:[function(require,module,exports){
var _implements = require(415)._implements;

var Stream = require(90);

function attach(object, key, value, allowedEvents) {
    if (typeof key === 'object') {
        for (var k in key) {
            attach(object, k, key[k], allowedEvents);
        }
    } else if (value) {
        if (object[key] === undefined) {
            if (allowedEvents && allowedEvents.indexOf(key) === -1) {
                throw new Error('Event ' + key + ' not emitted on this stream, allowed events are ' + allowedEvents);
            }
            object[key] = [];
        } 

        object[key].push(value);
    }

        return object;
}

function remove(object, key, value) {
    if (typeof key === 'object') {
        for (var k in key) {
            remove(object, k, key[k]);
        }
    } else if (object[key]) {
        if (value) {
            object[key] = object[key].filter(function(e) {
                return e !== value;
            });
        } else {
            object[key] = [];
        }
    }

    return object;
}

function StreamImpl(listeners, error, close, events) {
    var allowedEvents;

    if (events) {
        allowedEvents = ['close', 'error', 'complete'].concat(events);
    }

    this.on = function(event, fn) {
        attach(listeners, event, fn, allowedEvents);
        return this;
    };

        this.off = function(event, fn) {
        remove(listeners, event, fn);
        return this;
    };

        this.close = function(reason) {
        close(reason);
        return this;
    };

        this.error = function(reason) {
        error(reason);
        return this;
    };
}

module.exports = _implements(Stream, StreamImpl);

},{"415":415,"90":90}],176:[function(require,module,exports){
var _implements = require(415)._implements;
var Services = require(325);

var Emitter = require(173);
var Result = require(174);

var SessionID = require(382);

var parseSelector = require(403);
var responseHandler = require(134).responseHandler;
var registrationCallback = require(134).registrationCallback;

var api = require(95);

var SessionPropertiesEventType = require(284);

var requireNotNull = require(422);
var logger = require(416).create('Session.Clients');

module.exports = _implements(api, function ClientControlImpl(internal) {
    var serviceLocator = internal.getServiceLocator();

    var getProperties = serviceLocator.obtain(Services.GET_SESSION_PROPERTIES);
    var registration = serviceLocator.obtain(Services.SESSION_PROPERTIES_REGISTRATION_2);

    internal.getServiceRegistry().add(Services.SESSION_PROPERTIES_EVENT_2, {
        onRequest : function(internal, message, callback) {
            try {
                internal.getConversationSet().respond(message.cid, message);
                callback.respond();
            } catch (e) {
                callback.fail(e);
            }
        }
    });

    this.SessionEventType = {
        UPDATED : 0,
        RECONNECTED : 1,
        FAILED_OVER : 2,
        DISCONNECTED : 3
    };

    this.close = function(sessionID) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        try {
            requireNotNull(sessionID, "SessionID");
        } catch (e) {
            emitter.error(e);
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var internalSessionID = SessionID.validate(sessionID);

            if (internalSessionID) {
                serviceLocator.obtain(Services.CLOSE_CLIENT).send({
                    sessionID : internalSessionID
                }, function(err, result) {
                    if (err) {
                        emitter.error(err);
                    } else {
                        emitter.emit('complete', result);
                    }
                });
            } else {
                emitter.error(new Error("Invalid SessionID: " + sessionID));
            }
        }

        return result;
    };

    this.subscribe = function(session, path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        try {
            requireNotNull(session, "SessionID or Session Filter");
            requireNotNull(path, "Topic Selector");
        } catch (e) {
            emitter.error(e);
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var selector;

            try {
                selector = parseSelector(path);
            } catch (e) {
                emitter.error(e);
                return result;
            }

            var sessionID = SessionID.validate(session);

            if (sessionID) {
                serviceLocator.obtain(Services.SUBSCRIBE_CLIENT).send({
                    sessionID : sessionID,
                    selector : selector
                }, function(err) {
                    if (err) {
                        emitter.error(err);
                    } else {
                        emitter.emit('complete');
                    }
                });
            } else {
                serviceLocator.obtain(Services.FILTER_SUBSCRIBE).send({
                    filter : session,
                    selector : selector
                }, function(err, result) {
                    if (err) {
                        emitter.error(err);
                    } else if (result.isSuccess()) {
                        emitter.emit('complete', result.selected);
                    } else {
                        emitter.error(result.errors);
                    }
                });
            }
        }

        return result;
    };

    this.unsubscribe = function(session, path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        try {
            requireNotNull(session, "SessionID or Session Filter");
            requireNotNull(path, "Topic Selector");
        } catch (e) {
            emitter.error(e);
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var selector;

            try {
                selector = parseSelector(path);
            } catch (e) {
                emitter.error(e);
                return result;
            }

            var sessionID = SessionID.validate(session);

            if (sessionID) {
                serviceLocator.obtain(Services.UNSUBSCRIBE_CLIENT).send({
                    sessionID : sessionID,
                    selector : selector
                }, function(err) {
                    if (err) {
                        emitter.error(err);
                    } else {
                        emitter.emit('complete');
                    }
                });
            } else {
                serviceLocator.obtain(Services.FILTER_UNSUBSCRIBE).send({
                    filter : session,
                    selector : selector
                }, function(err, result) {
                    if (err) {
                        emitter.error(err);
                    } else if (result.isSuccess()) {
                        emitter.emit('complete', result.selected);
                    } else {
                        emitter.error(result.errors);
                    }
                });
            }
        }

        return result;
    };

    this.getSessionProperties = function(sid, propertyKeys) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (typeof sid === 'string') {
            sid = SessionID.fromString(sid);
        }

        if (internal.checkConnected(emitter)) {
            getProperties.send({
                sessionID : sid,
                propertyKeys : propertyKeys
            }, function(err, result) {
                if (err) {
                    emitter.error(err);
                } else {
                    if (result === null) {
                        emitter.error(new Error('Invalid session ID'));
                    } else {
                        emitter.emit('complete', result.properties);
                    }
                }
            });
        }

        return result;
    };

    this.setSessionPropertiesListener = function(requiredProperties, handler) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!handler) {
            emitter.error(new Error('Session Properties listener is null or undefined'));
        }

        if (internal.checkConnected(emitter)) {
            logger.debug('Adding Session Properties Listener');

            var adapter = {
                active : function(close) {
                    logger.debug('Session Properties Listener active');
                    handler.onActive(close);
                },
                respond : function(message) {
                    for (var i = 0; i < message.events.length; ++i) {
                        var event = message.events[i];

                        switch (event.type) {
                            case SessionPropertiesEventType.OPEN:
                                handler.onSessionOpen(event.sessionId, event.oldProperties);
                                break;
                            case SessionPropertiesEventType.UPDATE:
                                handler.onSessionEvent(
                                    event.sessionId,
                                    event.updateType,
                                    event.newProperties,
                                    event.oldProperties);
                                break;
                            case SessionPropertiesEventType.CLOSE:
                                handler.onSessionClose(
                                    event.sessionId,
                                    event.oldProperties,
                                    event.closeReason);
                                break;
                            default :
                                logger.debug('Unknown event type received for session properties listener', event.type);
                        }
                    }
                },
                close : function(err) {
                    logger.debug('Session Properties Listener closed');

                    if (err) {
                        handler.onError(err);
                    } else {
                        handler.onClose();
                    }
                }
            };

            var conversationSet = internal.getConversationSet();

            var cid = conversationSet.newConversation(responseHandler(internal, adapter, function(cid, callback) {
                registration.send({ cid : cid }, callback);
            }));

            registration.send({ cid : cid, properties : requiredProperties },
                registrationCallback(conversationSet, cid, emitter));
        }

        return result;
    };
});

},{"134":134,"173":173,"174":174,"284":284,"325":325,"382":382,"403":403,"415":415,"416":416,"422":422,"95":95}],177:[function(require,module,exports){
var topicSelectorParser = require(403);

var _implements = require(415)._implements;

var Services = require(325);

var ConversationId = require(136);
var SessionId = require(382);

var CommandService = require(113);
var ControlGroup = require(133);
var registration = require(134);
var DataTypes = require(144);

var Emitter = require(173);
var Result = require(174);

var ErrorReason = require(87);
var api = require(96);

var arrays = require(412);

var logger = require(416).create('Session.Messages');
var requireNonNull = require(422);

var dummyCID = ConversationId.fromString("0");

function createSendOptions(options) {
    options = options || {};
    options.headers = options.headers || [];
    options.priority = options.priority || 0;

    return options;
}

function MessageStream(e, selector) {
    this.selector = selector;
    e.assign(this);
}

function RequestStreamProxy(reqType, resType, stream) {
    this.reqType = reqType;
    this.resType = resType;
    this.stream = stream;
}

function RequestResponder(callback, responseType) {
    this.respond = function() {
        if (arguments.length === 0) {
            throw new Error('No argument provided to the responder');
        }
        var response = arguments[0];

        if ((response === null || response === undefined) && !responseType) {
            throw new Error('No response type has been provided and the value provided is null. ' +
                'Unable to determine the response type.');
        }

        var dataType = responseType ? DataTypes.getChecked(responseType) : DataTypes.getByValue(response);
        var responseData = dataType.writeValue(response);

        callback.respond({
            responseDataType : dataType.name(),
            data : responseData
        });
    };

    this.reject = function(message) {
        callback.fail(ErrorReason.REJECTED_REQUEST, message);
    };
}

function parseSessionId(str) {
    try {
        var sid = SessionId.fromString(str);
        return sid;
    }
    catch(err) {
        return false;
    }
}

module.exports = _implements(api, function MessagesImpl(internal) {
    var serviceLocator = internal.getServiceLocator();

    var sender = serviceLocator.obtain(Services.SEND);
    var sessionSender = serviceLocator.obtain(Services.SEND_TO_SESSION);
    var filterSender = serviceLocator.obtain(Services.SEND_TO_FILTER);

    var MESSAGING_SEND = serviceLocator.obtain(Services.MESSAGING_SEND);
    var MESSAGING_FILTER_SEND = serviceLocator.obtain(Services.MESSAGING_FILTER_SEND);
    var MESSAGING_RECEIVER_SERVER = serviceLocator.obtain(Services.MESSAGING_RECEIVER_SERVER);

    var deregisterRequestHandler = serviceLocator.obtain(Services.MESSAGING_RECEIVER_CONTROL_DEREGISTRATION);

    var requestStreams = {};
    var streams = [];

    var serviceRegistry = internal.getServiceRegistry();

    serviceRegistry.add(Services.SEND, CommandService.create(function(internal, message, callback) {
        var received = false;

        streams.forEach(function(stream) {
            if (stream.l.selector.selects(message.path)) {
                stream.e.emit('message', message);
                received = true;
            }
        });

        if (received) {
            callback.respond();
        } else {
            logger.info("No messaging streams registered for message on path: " + message.path);

            callback.fail(new Error(
                    "Session " +
                    internal.getSessionId() +
                    " has no registered streams for message sent to: " + message.path));
        }
    }));

    serviceRegistry.add(Services.MESSAGING_SEND, CommandService.create(function(internal, request, callback) {
        var proxy = requestStreams[request.path];

        if (proxy === undefined) {
            callback.fail(
                ErrorReason.UNHANDLED_MESSAGE,
                "Session " + internal.getSessionId() + " has no registered streams for message sent to path '" +
                request.path + "'");
            return;
        }

        var responder = new RequestResponder(callback, proxy.resType);

        var requestDataType = request.dataType;
        var requestType = proxy.reqType ? proxy.reqType : requestDataType.valueClass;

        if (requestDataType.canReadAs(requestType)) {
            var requestData;

            try {
                requestData = requestDataType.readAs(requestType, request.request);
            } catch (e) {
                logger.error("Failed to convert " + requestDataType.name() + " datatype to " + requestType);

                proxy.stream.onError(ErrorReason.INVALID_DATA);
                delete requestStreams[request.path];
                return;
            }

            try {
                proxy.stream.onRequest(request.path, requestData, responder);
            } catch (e) {
                logger.error("Messaging request stream threw an error", e);
                callback.fail(ErrorReason.CALLBACK_EXCEPTION, e.stack);
            }
        } else {
            var message =
                "Messaging request for path " +
                request.path +
                " with " +
                requestDataType.name() +
                " datatype is incompatible for stream (" + requestType + ")";

            logger.debug(message);

            callback.fail(ErrorReason.INCOMPATIBLE_DATATYPE, message);
        }
    }));

    serviceRegistry.add(Services.FILTER_RESPONSE, CommandService.create(function(internal, request, callback) {
        callback.respond();
        internal.getConversationSet().respond(request.cid, request);
    }));

    serviceRegistry.add(Services.SEND_RECEIVER_CLIENT,
        CommandService.create(function(internal, request, callback) {
            callback.respond();
            internal.getConversationSet().respond(request.cid, request);
        }));

    serviceRegistry.add(Services.MESSAGING_RECEIVER_CLIENT,
        CommandService.create(function(internal, request, callback) {
            internal.getConversationSet().respond(request.cid, {
                request: request,
                callback: callback.respond
            });
        }));

    this.addHandler = function(path, handler, keys) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!path) {
            emitter.error(new Error('Message Handler path is null or undefined'));
            return result;
        }

        if (!handler) {
            emitter.error(new Error('Message Handler is null or undefined'));
            return result;
        }

        if (internal.checkConnected(emitter)) {
            logger.debug('Adding message handler', path);

            var params = {
                definition : Services.SEND_RECEIVER_CLIENT,
                group : ControlGroup.DEFAULT,
                path : path,
                keys : keys !== undefined ? keys : {}
            };

            var adapter = {
                active : function(close) {
                    logger.debug('Message handler active', path);

                    handler.onActive(close);
                },
                respond : function(response) {
                    logger.debug('Message handler response', path);

                    var message = {
                        session : response.sender.toString(),
                        content : response.message,
                        options : response.options,
                        path : response.path
                    };
                    var properties = response.sessionProperties;
                    if (properties && Object.keys(properties).length > 0) {
                        message.properties = properties;
                    }

                    handler.onMessage(message);
                    return false;
                },
                close : function() {
                    logger.debug('Message handler closed', path);

                    handler.onClose();
                }
            };

            return registration.registerMessageHandler(internal, params, adapter);
        } else {
            return result;
        }
    };

    this.listen = function(path, cb) {
        var emitter = new Emitter(undefined, undefined, ['message']);
        var selector = topicSelectorParser(path);

        var stream = new MessageStream(emitter, selector);

        var ref = {
            l : stream,
            e : emitter
        };

        stream.on('close', function() {
            arrays.remove(streams, ref);
        });

        if (cb && cb instanceof Function) {
            stream.on('message', cb);
        }

        streams.push(ref);
        return stream;
    };

    this.send = function(path, message, options, recipient) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        var sendCallback = function(err, result) {
            if (err) {
                logger.debug('Message send failed', path);
                emitter.error(err);
            } else {
                logger.debug('Message send complete', path);

                if (result) {
                    emitter.emit('complete', {
                        path : path,
                        recipient : recipient,
                        sent : result.sent,
                        errors : result.errors
                    });
                } else {
                    emitter.emit('complete', {
                        path : path,
                        recipient : recipient
                    });
                }
            }
        };

        if (options && (typeof options === 'string' || options instanceof SessionId)) {
            recipient = options;
            options = createSendOptions();
        } else {
            options = createSendOptions(options);
        }

        if (internal.checkConnected(emitter)) {
            if (!path) {
                emitter.error(new Error('Message path is null or undefined'));
                return result;
            }

            if (message === undefined || message === null) {
                emitter.error(new Error('Message content is null or undefined'));
                return result;
            }

            var request;

            if (recipient) {
                var session = typeof recipient === 'string' ? parseSessionId(recipient) : recipient;

                if (session) {
                    request = {
                        path : path,
                        content : message,
                        session : session,
                        options : options,
                        cid : dummyCID
                    };

                    logger.debug('Sending message to session', request);
                    sessionSender.send(request, sendCallback);
                } else { 
                    request = {
                        path : path,
                        content : message,
                        filter : recipient,
                        options : options,
                        cid : dummyCID
                    };

                    logger.debug('Sending message to filter', request);
                    filterSender.send(request, sendCallback);
                }
            } else {
                request = {
                    path : path,
                    content : message,
                    options : options
                };

                logger.debug('Sending message to server', request);
                sender.send(request, sendCallback);
            }
        }

        return result;
    };

    this.addRequestHandler = function(path, handler, keys, requestType) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            try {
                requireNonNull(path, "Message path");
                requireNonNull(handler, "Request handler");
            } catch (e) {
                emitter.error(e);
                return result;
            }

            logger.debug('Adding request handler', path);

            var params = {
                definition : Services.MESSAGING_RECEIVER_CLIENT,
                group : ControlGroup.DEFAULT,
                path : path,
                keys : keys !== undefined ? keys : {}
            };

            var adapter = {
                active : function(close, cid) {
                    logger.debug('Request handler active', path);

                    var registration = {
                        close: function() {
                            deregisterRequestHandler.send(params, function(err, response) {
                                if (err) {
                                    internal.getConversationSet().discard(cid, err);
                                    logger.debug('Error with request handler deregistration: ', err);
                                } else {
                                    handler.onClose();
                                    internal.getConversationSet().respondIfPresent(cid, response);
                                }
                            });
                        }
                    };

                    emitter.emit('complete', registration);
                },
                respond : function(pair) {
                    logger.debug('Request handler pair.request', path);

                    var request;

                    var context = {
                        sessionId: pair.request.sessionID,
                        path: pair.request.path,
                        properties: pair.request.properties
                    };

                    var requestDatatype;

                    try {
                        requestDatatype = requestType ? requestType : DataTypes.getChecked(pair.request.dataType);
                        request = requestDatatype.readValue(pair.request.content);
                    } catch (err) {
                        logger.info('An exception has occurred whilst reading the request:', err);
                        handler.onError(err);
                        pair.callback.fail(err.message);
                        return;
                    }

                    var responder = {
                        respond: function(response, respType) {
                            var responseType = DataTypes.getChecked(respType ? respType.name() : response);
                            pair.callback({
                                responseDataType: responseType.name(),
                                data: responseType.writeValue(response)
                            });
                        }
                    };

                    handler.onRequest(request, context, responder);
                },
                close : function() {
                    logger.debug('Request handler closed', path);
                    handler.onClose();
                }
            };

            registration.registerRequestHandler(internal, params, adapter).then(null, function(err) {
                emitter.error(err);
            });

        }
        return result;
    };

    function sendRequestToController(emitter, path, request, reqType, respType) {
        try {
            requireNonNull(path, "Message path");
            requireNonNull(request, "Request");
        } catch (e) {
            emitter.error(e);
            return;
        }

        var requestType;
        try {
            requestType = reqType ? DataTypes.getChecked(reqType) : DataTypes.getByValue(request);
        } catch (e) {
            emitter.error(e);
            return;
        }

        logger.debug('Sending request to server', request);

        MESSAGING_SEND.send({
            path : path,
            type: requestType.name(),
            request: requestType.writeValue(request)
        }, function(err, response) {
            var responseType;
            if (err) {
                logger.debug('Request failed', path);
                emitter.error(err);
            } else {
                logger.debug('Request complete', path);

                try {
                    responseType = DataTypes.getChecked(respType ? respType : response.responseDataType);
                } catch (e) {
                    emitter.error(e);
                    return;
                }
                emitter.emit('complete', responseType.readValue(response.data));
            }
        });
    }

    function sendRequestToSession(emitter, path, request, sessionId, reqType, respType) {
        try {
            requireNonNull(sessionId, "Session ID");
            requireNonNull(path, "Message path");
            requireNonNull(request, "Request");
        } catch (e) {
            emitter.error(e);
            return;
        }

        var requestType;
        try {
            requestType = reqType ? DataTypes.getChecked(reqType) : DataTypes.getByValue(request);
        } catch (e) {
            emitter.error(e);
            return;
        }

        logger.debug('Sending request to session', sessionId, request);

        MESSAGING_RECEIVER_SERVER.send({
            sessionId: sessionId,
            path : path,
            type: requestType.name(),
            request: requestType.writeValue(request)
        }, function(err, response) {
            var responseType;
            if (err) {
                logger.debug('Request failed', path);
                emitter.error(err);
            } else {
                logger.debug('Request complete', path);

                try {
                    responseType = DataTypes.getChecked(respType ? respType : response.responseDataType);
                } catch (e) {
                    emitter.error(e);
                    return;
                }
                emitter.emit('complete', responseType.readValue(response.data));
            }
        });
    }

    this.sendRequest = function(
            path,
            request,
            sessionIdOrRequestType,
            requestTypeOrResponseType,
            responseType) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            if (sessionIdOrRequestType instanceof SessionId) {
                sendRequestToSession(
                    emitter,
                    path,
                    request,
                    sessionIdOrRequestType,
                    requestTypeOrResponseType,
                    responseType);
            }
            else if (typeof sessionIdOrRequestType === 'string') {
                var sessionId = parseSessionId(sessionIdOrRequestType);
                if (sessionId) {
                    sendRequestToSession(
                        emitter,
                        path,
                        request,
                        sessionId,
                        requestTypeOrResponseType,
                        responseType);
                }
                else {
                    sendRequestToController(
                        emitter,
                        path,
                        request,
                        sessionIdOrRequestType,
                        requestTypeOrResponseType);
                }
            }
            else {
                sendRequestToController(
                    emitter,
                    path,
                    request,
                    sessionIdOrRequestType,
                    requestTypeOrResponseType);
            }
        }
        return result;
    };

    this.sendRequestToFilter = function(filter, path, request, callback, reqType, respType) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            try {
                requireNonNull(path, "Message path");
                requireNonNull(filter, "Session filter");
                requireNonNull(request, "Request");
                requireNonNull(callback, "Response callback");
            } catch (e) {
                emitter.error(e);
                return result;
            }

            var requestType;
            try {
                requestType = reqType ? DataTypes.getChecked(reqType) : DataTypes.getByValue(request);
            }
            catch (e) {
                emitter.error(e);
                return;
            }

            var cid = internal.getConversationSet().newConversation({
                onOpen : function() {
                },
                onResponse : function(cid, response) {
                    if (response === null) {
                        return true;
                    }

                    var sessionID = response.sessionID;

                    if (response.errorReason) {
                        callback.onResponseError(sessionID, response.errorReason);
                    } else {
                        var responseDataType = DataTypes.getByName(response.response.responseDataType);
                        var responseType;
                        try {
                            responseType = DataTypes.getChecked(
                                respType ? respType : response.response.responseDataType);
                        }
                        catch (e) {
                            emitter.error(e);
                            return;
                        }

                        if (responseDataType.canReadAs(responseType.valueClass)) {
                            var value;

                            try {
                                value = responseDataType.readAs(responseType.valueClass, response.response.data);
                            } catch (e) {
                                callback.onResponseError(sessionID, e);
                                return false;
                            }

                            try {
                                callback.onResponse(sessionID, value);
                            } catch (e) {
                                logger.error("Exception within messaging filter callback", e);
                            }
                        } else {
                            callback.onResponseError(sessionID,
                                new Error("The received response was a " + responseDataType +
                                          ", which cannot be read as: " + responseType));
                        }
                    }

                    return false;
                },
                onDiscard : function(cid, err) {
                    callback.onError(err);
                }
            });

            logger.debug('Sending filter request to server', request);

            MESSAGING_FILTER_SEND.send({
                cid : cid,
                path : path,
                filter : filter,
                dataType : requestType,
                request : requestType.writeValue(request)
            }, function(err, response) {
                if (err) {
                    logger.debug('Request failed', path);
                    emitter.error(err);
                } else if (response.isSuccess) {
                    logger.debug('Request complete', path);
                    internal.getConversationSet().respondIfPresent(cid, null);
                    emitter.emit('complete', response.numberSent);
                } else {
                    logger.debug('Request failed', path);
                    emitter.error(response.errors);
                }
            });
        }

        return result;
    };

    this.setRequestStream = function(path, stream, reqType, resType) {
        var proxy = new RequestStreamProxy(
            reqType ? DataTypes.getValueClassChecked(reqType) : null,
            resType ? DataTypes.getValueClassChecked(resType) : null,
            stream);

        var old = requestStreams[path];
        requestStreams[path] = proxy;

        if (old) {
            return old.stream;
        }
    };

    this.removeRequestStream = function(path) {
        var proxy = requestStreams[path];
        delete requestStreams[path];

        if (proxy) {
            return proxy.stream;
        }
    };
});


},{"113":113,"133":133,"134":134,"136":136,"144":144,"173":173,"174":174,"325":325,"382":382,"403":403,"412":412,"415":415,"416":416,"422":422,"87":87,"96":96}],178:[function(require,module,exports){
var _implements = require(415)._implements;
var Ping = require(97);

var Services = require(325);

var Emitter = require(173);
var Result = require(174);

module.exports = _implements(Ping, function PingImpl(internal, currentTime) {
    currentTime = currentTime || Date.now;

    var ping = internal.getServiceLocator().obtain(Services.USER_PING);

    this.pingServer = function() {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            var start = currentTime();

            ping.send(null, function(err) {
                if (err) {
                    emitter.error(err);
                } else {
                    var elapsed = currentTime() - start;

                    emitter.emit('complete', {
                        timestamp : start,
                        rtt : elapsed
                    });
                }
            });
        }

        return result;
    };
});
},{"173":173,"174":174,"325":325,"415":415,"97":97}],179:[function(require,module,exports){
var Services = require(325);
var CHANGE_PRINCIPAL = require(325).CHANGE_PRINCIPAL;
var GET_SECURITY_CONFIGURATION = require(325).GET_SECURITY_CONFIGURATION;
var UPDATE_SECURITY_CONFIGURATION = require(325).UPDATE_SECURITY_CONFIGURATION;

var GET_SYSTEM_AUTHENTICATION = require(325).GET_SYSTEM_AUTHENTICATION;
var UPDATE_SYSTEM_AUTHENTICATION = require(325).UPDATE_SYSTEM_AUTHENTICATION;

var Emitter = require(173);
var Result = require(174);

var registerHandler = require(134).registerHandler;
var ControlGroup = require(133);
var CommandService = require(113);

var AuthenticationResponse = require(310);

var SecurityCommandScript = require(238);

var SecurityScriptBuilder = require(180);
var AuthenticationScriptBuilder = require(181);

var _implements = require(415)._implements;
var api = require(98);

var requireNonNull = require(422);
var log = require(416).create('Session.Security');

var GlobalPermission = {
    AUTHENTICATE: 0,
    VIEW_SESSION: 1,
    MODIFY_SESSION: 2,
    REGISTER_HANDLER: 3,
    VIEW_SERVER: 4,
    CONTROL_SERVER: 5,
    VIEW_SECURITY: 6,
    MODIFY_SECURITY: 7
};

var TopicPermission = {
    READ_TOPIC: 0,
    UPDATE_TOPIC: 1,
    MODIFY_TOPIC: 2,
    SEND_TO_MESSAGE_HANDLER: 3,
    SEND_TO_SESSION: 4,
    SELECT_TOPIC: 5,
    QUERY_OBSOLETE_TIME_SERIES_EVENTS: 6,
    EDIT_TIME_SERIES_EVENTS: 7,
    EDIT_OWN_TIME_SERIES_EVENTS: 8
};

var SecurityConfiguration = _implements(api.SecurityConfiguration, {
    __constructor: function Configuration(anonymous, named, roles) {
        this.anonymous = anonymous;
        this.named = named;
        this.roles = roles;
    },
    toString: function () {
        return "SecurityConfiguration [anonymous=" + this.anonymous + ", named=" + this.named +
            ", roles=" + this.roles + "]";
    }
});

var SystemAuthentication = _implements(api.SystemAuthenticationConfiguration, {
    __constructor: function Configuration(principals, action, roles) {
        this.principals = principals || [];
        this.anonymous = {
            action: action,
            roles: roles || []
        };
    },
    toString: function () {
        return "SystemAuthenticationConfiguration [principals=" +
            this.principals + ", anonymous=" + this.anonymous + "]";
    }
});

SystemAuthentication.CONNECTION_ACTION = {
    ALLOW: {id: 0, value: 'allow'},
    DENY: {id: 1, value: 'deny'},
    ABSTAIN: {id: 2, value: 'abstain'},

    fromString: function (val) {
        return this[val.toUpperCase()];
    }
};

var SystemPrincipal = _implements(api.SystemPrincipal, {
    __constructor: function SystemPrincipal(name, roles) {
        this.name = requireNonNull(name);
        this.roles = roles || [];
    },
    toString: function () {
        return "SystemPrincipal [" + this.name + ", " + this.roles + "]";
    }
});

var Security = _implements(api, function SecurityImpl(internal) {
    var serviceLocator = internal.getServiceLocator();

    var changePrincipal = serviceLocator.obtain(CHANGE_PRINCIPAL);
    var getConfiguration = serviceLocator.obtain(GET_SECURITY_CONFIGURATION);
    var getSystemAuthentication = serviceLocator.obtain(GET_SYSTEM_AUTHENTICATION);
    var updateSystemAuthentication = serviceLocator.obtain(UPDATE_SYSTEM_AUTHENTICATION);
    var updateSecurityConfiguration = serviceLocator.obtain(UPDATE_SECURITY_CONFIGURATION);

    internal.getServiceRegistry().add(Services.AUTHENTICATION, CommandService.create(function(internal, req, callback) {
        internal.getConversationSet().respond(req.cid, { req : req, callback : callback });
    }));

    this.getPrincipal = function () {
        return internal.getPrincipal();
    };

    this.changePrincipal = function (principal, credentials) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            log.debug('Changing principal', principal);

            changePrincipal.send({
                principal: principal,
                credentials: credentials
            }, function (err, response) {
                if (err) {
                    emitter.error(err);
                } else {
                    if (response) {
                        internal.setPrincipal(principal);
                        emitter.emit('complete');
                    } else {
                        emitter.error(new Error('Unable to change principal due to authentication failure'));
                    }
                }
            });
        }

        return result;
    };

    this.getSecurityConfiguration = function () {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            log.debug('Getting security configuration');

            getConfiguration.send(null, function (err, response) {
                if (err) {
                    emitter.error(err);
                } else {
                    emitter.emit('complete', response);
                }
            });
        }

        return result;
    };

    this.getSystemAuthenticationConfiguration = function () {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            log.debug('Getting system authentication');

            getSystemAuthentication.send(null, function (err, response) {
                if (err) {
                    emitter.error(err);
                } else {
                    emitter.emit('complete', response);
                }
            });
        }

        return result;
    };

    this.setAuthenticationHandler = function(name, details, handler) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            log.debug('Setting Authentication Handler for "' + name + '"');

            var adapter = {
                active : function(close) {
                    log.debug('Authentication Handler active for "' + name + '"');
                    handler.onActive(close);
                },
                respond : function(msg) {
                    var req = msg.req;
                    var callback = msg.callback;

                    handler.onAuthenticate(req.principal, req.credentials, req.details, {
                        allow : function (res) {
                            if (res) {
                                callback.respond(AuthenticationResponse.allow(res));
                            } else {
                                callback.respond(AuthenticationResponse.ALLOW);
                            }
                        },
                        abstain : function () {
                            callback.respond(AuthenticationResponse.ABSTAIN);
                        },
                        deny : function () {
                            callback.respond(AuthenticationResponse.DENY);
                        }
                    });
                },
                close : function(err) {
                    if (err) {
                        log.debug('Authentication Handler closed with error for "' + name + '"');
                        handler.onError(err);
                    } else {
                        log.debug('Authentication Handler closed for "' + name + '"');
                        handler.onClose();
                    }
                }
            };

            var params = {
                definition : Services.AUTHENTICATION,
                group : ControlGroup.DEFAULT,
                details : details,
                name : name
            };

            return registerHandler(internal, params, adapter,
                Services.AUTHENTICATION_CONTROL_REGISTRATION,
                Services.AUTHENTICATION_CONTROL_DEREGISTRATION);
        }

        return result;
    };

    var updateStoreCallback = function (err, result, emitter) {
        if (err) {
            emitter.error(err);
        } else if (result.errors.length > 0) {
            emitter.error(result.errors);
        } else {
            emitter.emit('complete');
        }
    };

    var updateStore = function (updater, script) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (script === "") {
            emitter.emit('complete');
        } else if (!script || typeof script !== 'string') {
            emitter.error(new Error('Invalid argument for script:' + script));
        } else if (internal.checkConnected(emitter)) {
            updater.send(new SecurityCommandScript(script), function (err, result) {
                updateStoreCallback(err, result, emitter);
            });
        }

        return result;
    };

    this.updateSecurityStore = function (script) {
        log.debug('Updating security store');
        return updateStore(updateSecurityConfiguration, script);
    };

    this.updateAuthenticationStore = function (script) {
        log.debug('Updating authentication store');
        return updateStore(updateSystemAuthentication, script);
    };

    this.securityScriptBuilder = function (script) {
        return new SecurityScriptBuilder(script);
    };

    this.authenticationScriptBuilder = function (script) {
        return new AuthenticationScriptBuilder(script);
    };
});

module.exports.GlobalPermission = GlobalPermission;
module.exports.TopicPermission = TopicPermission;

module.exports.Security = Security;
module.exports.SystemPrincipal = SystemPrincipal;

module.exports.Configuration = SecurityConfiguration;
module.exports.SystemAuthentication = SystemAuthentication;

},{"113":113,"133":133,"134":134,"173":173,"174":174,"180":180,"181":181,"238":238,"310":310,"325":325,"415":415,"416":416,"422":422,"98":98}],180:[function(require,module,exports){
var _implements = require(415)._implements;
var util = require(182);
var api = require(98);

var requireNonNull = require(422);
var permissionsToString = util.permissionsToString;
var quoteAndEscape = util.quoteAndEscape;
var rolesToString = util.rolesToString;

var SecurityScriptBuilder = _implements(api.SecurityScriptBuilder,
    function SecurityScriptBuilderImpl(commands) {

        commands = commands || "";

        var withCommand = function (command) {
            if (commands) {
                return new SecurityScriptBuilder(commands + '\n' + command);
            }

            return new SecurityScriptBuilder(command);
        };

        this.build = function () {
            return commands;
        };

        this.toString = function () {
            return 'ConfigurationScriptBuilder [' + commands + ']';
        };

        this.setRolesForAnonymousSessions = function (roles) {
            return withCommand("set roles for anonymous sessions " +
                rolesToString(requireNonNull(roles, "roles")));
        };

        this.setRolesForNamedSessions = function (roles) {
            return withCommand("set roles for named sessions " +
                rolesToString(requireNonNull(roles, "roles")));
        };

        this.setGlobalPermissions = function (role, permissions) {
            return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " permissions " +
                permissionsToString(requireNonNull(permissions, "permissions")));
        };

        this.setDefaultTopicPermissions = function (role, permissions) {
            return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " default topic permissions " +
                permissionsToString(requireNonNull(permissions, "permissions")));
        };

        this.removeTopicPermissions = function (role, path) {
            return withCommand("remove " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " permissions for topic " +
                quoteAndEscape(requireNonNull(path, "path")));
        };

        this.setTopicPermissions = function (role, path, permissions) {
            return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " topic " +
                quoteAndEscape(requireNonNull(path, "path")) +
                " permissions " +
                permissionsToString(requireNonNull(permissions, "permissions")));
        };

        this.setRoleIncludes = function (role, included) {
            return withCommand("set " +
                quoteAndEscape(requireNonNull(role, "role")) +
                " includes " +
                rolesToString(requireNonNull(included, "included roles")));
        };
    });

module.exports = SecurityScriptBuilder;

},{"182":182,"415":415,"422":422,"98":98}],181:[function(require,module,exports){
var _implements = require(415)._implements;
var util = require(182);
var api = require(98);

var requireNonNull = require(422);
var quoteAndEscape = util.quoteAndEscape;
var rolesToString = util.rolesToString;

var SystemAuthenticationScriptBuilder = _implements(api.SystemAuthenticationScriptBuilder,
    function SystemAuthenticationScriptBuilderImpl(commands) {

        commands = commands || "";

        var withCommand = function (command) {
            if (commands) {
                return new SystemAuthenticationScriptBuilder(commands + '\n' + command);
            }

            return new SystemAuthenticationScriptBuilder(command);
        };

        this.build = function () {
            return commands;
        };

        this.toString = function () {
            return 'SystemAuthenticationScriptBuilder [' + commands + ']';
        };

        this.assignRoles = function (principal, roles) {
            return withCommand("assign roles " +
                quoteAndEscape(requireNonNull(principal, "principal")) +
                ' ' +
                rolesToString(requireNonNull(roles, "roles")));
        };

        this.addPrincipal = function (principal, password, roles) {
            roles = roles || [];

            return withCommand('add principal ' +
                quoteAndEscape(requireNonNull(principal, "principal")) +
                ' ' +
                quoteAndEscape(requireNonNull(password, "password")) +
                (roles.length ? ' ' + rolesToString(roles) : ""));
        };

        this.setPassword = function (principal, password) {
            return withCommand('set password ' +
                quoteAndEscape(requireNonNull(principal, "principal")) +
                ' ' +
                quoteAndEscape(requireNonNull(password, "password")));
        };

        this.verifyPassword = function (principal, password) {
            return withCommand("verify password " +
                quoteAndEscape(requireNonNull(principal, "principal")) +
                ' ' +
                quoteAndEscape(requireNonNull(password, "password")));
        };

        this.removePrincipal = function (principal) {
            return withCommand("remove principal" +
                ' ' +
                quoteAndEscape(requireNonNull(principal, "principal")));
        };

        this.allowAnonymousConnections = function (roles) {
            roles = roles || [];
            return withCommand("allow anonymous connections" +
                ' ' +
                rolesToString(roles));
        };

        this.denyAnonymousConnections = function () {
            return withCommand("deny anonymous connections");
        };

        this.abstainAnonymousConnections = function () {
            return withCommand("abstain anonymous connections");
        };
    });

module.exports = SystemAuthenticationScriptBuilder;

},{"182":182,"415":415,"422":422,"98":98}],182:[function(require,module,exports){
function quoteAndEscape(str) {
    var escaped = '\'', i = 0;

        if (str.indexOf('\'') < 0 && str.indexOf('\\') < 0) {
        escaped += str;
    } else {
        for (; i < str.length; ++i) {
            var c = str[i];

                        if (c === '\'' || c === '\\') {
                escaped += '\\';
            }

                        escaped += c;
        }
    }

        escaped += '\'';
    return escaped;
}

function rolesToString(roles) {
    return '[' + roles.map(quoteAndEscape).join(' ') + ']';    
}

function permissionsToString(permissions) {
    return '[' + permissions.join(' ') + ']';
}

module.exports.permissionsToString = permissionsToString;
module.exports.quoteAndEscape = quoteAndEscape;
module.exports.rolesToString = rolesToString;

},{}],183:[function(require,module,exports){
var requireNonNull = require(422);

function requireNonNegative(i, what) {
    if (i < 0 || i === undefined || i === null) {
        throw new Error(what + " is negative: " + i);
    }

    return i;
}

function Type(id, anchorOperator, spanOperator, unitsSuffix) {
    this.id = id;
    this.anchorOperator = anchorOperator;
    this.spanOperator = spanOperator;
    this.unitsSuffix = unitsSuffix;
}

var Types = {
    ABSOLUTE_START : new Type(0, "fromStart", "toStart", ""),
    ABSOLUTE_SEQUENCE : new Type(1, "from", "to", ""),
    ABSOLUTE_TIME : new Type(2, "from", "to", " ms"),
    OFFSET_SEQUENCE : new Type(3, "fromLast", "untilLast", ""),
    OFFSET_TIME : new Type(4, "fromLast", "untilLast", " ms"),
    NEXT_COUNT : new Type(5, null, "next", ""),
    NEXT_TIME : new Type(6, null, "next", " ms"),
    PREVIOUS_COUNT : new Type(7, null, "previous", ""),
    PREVIOUS_TIME : new Type(8, null, "previous", " ms")
};

function Point(value, type) {
    this.value = value;
    this.type = type;

    this.isAbsolute = function() {
        return this.type.anchorOperator;
    };

    this.equals = function(other) {
        if (other && other instanceof Point) {
            return this.value === other.value &&
                   this.type === other.type;
        }

        return false;
    };

    function toOperatorDescription(operator) {
        var s = "." + operator + "(";

        if (type !== Types.ABSOLUTE_START) {
            s += value + type.unitsSuffix;
        }

        s += ")";

        return s;
    }

    this.toAnchorDescription = function() {
        return toOperatorDescription(type.anchorOperator);
    };

    this.toSpanDescription = function() {
        return toOperatorDescription(type.spanOperator);
    };
}

Point.prototype.equals = function(other) {
    if (other && other instanceof Point) {
        return this.value === other.value && this.type === other.type;
    }

    return false;
};

var START_POINT = new Point(0, Types.ABSOLUTE_START);

Point.at = function(sequence) {
    if (sequence instanceof Date) {
        return new Point(requireNonNull(sequence, "Date").getTime(), Types.ABSOLUTE_TIME);
    }

    return new Point(requireNonNegative(sequence, "Sequence"), Types.ABSOLUTE_SEQUENCE);
};

Point.atStart = function() {
    return START_POINT;
};

Point.offset = function(count) {
    return new Point(requireNonNegative(count, "Count"), Types.OFFSET_SEQUENCE);
};

Point.offsetMillis = function(timespan) {
    return new Point(requireNonNegative(timespan, "Timespan"), Types.OFFSET_TIME);
};

Point.next = function(count) {
    return new Point(requireNonNegative(count, "Count"), Types.NEXT_COUNT);
};

Point.nextMillis = function(timespan) {
    return new Point(requireNonNegative(timespan, "Timespan"), Types.NEXT_TIME);
};

Point.previous = function(count) {
    return new Point(requireNonNegative(count, "Count"), Types.PREVIOUS_COUNT);
};

Point.previousMillis = function(timespan) {
    return new Point(requireNonNegative(timespan, "Timespan"), Types.PREVIOUS_TIME);
};

Point.Type = Types;

module.exports = Point;
},{"422":422}],184:[function(require,module,exports){
var RangeQueryParameters = require(186);
var Long = require(46);

function nonNull(e) {
    return !!e;
}

function comparingSequenceNumbers(e1, e2) {
    return e1.originalEvent.sequence.toNumber() > e2.sequence.toNumber() ? 1 : -1;
}

function replaceWithLatest(allEvents) {
    var latestByOriginalSequence = {};

    allEvents.forEach(function(e) {
        var k = e.originalEvent.sequence;

        if (!latestByOriginalSequence[k] || latestByOriginalSequence[k].sequence.toNumber() < e.sequence.toNumber()) {
            latestByOriginalSequence[k] = e;
        }
    });

    return function(e) {
        var originalSequence = e.originalEvent.sequence;
        var latest = latestByOriginalSequence[originalSequence];

        if (!latest) {
            return null;
        }

        if (!e.isEditEvent) {
            delete latestByOriginalSequence[originalSequence];
            return latest;
        } else if (e.sequence.equals(latest.sequence)) {
            delete latestByOriginalSequence[originalSequence];
            return e;
        } else {
            return null;
        }
    };
}

function QueryResult(selectedCount, eventCount, streamStructure, events) {
    this.events = events;

    this.selectedCount = selectedCount.toNumber();

    this.isComplete = (selectedCount.equals(eventCount));

    this.streamStructure = streamStructure;

    this.merge = function(other) {
        var merged = events.concat(other.events)
                           .sort(comparingSequenceNumbers)
                           .map(replaceWithLatest(events.concat(other.events)))
                           .filter(nonNull);

        var count = Long.fromNumber(merged.length);

        return new QueryResult(count, count, RangeQueryParameters.StreamStructure.VALUE_EVENT_STREAM, merged);
    };
}

module.exports = QueryResult;
},{"186":186,"46":46}],185:[function(require,module,exports){
var RangeQueryParameters = require(186);
var Range = require(187);

var QueryType = RangeQueryParameters.QueryType;

var BytesImpl = require(143);

var Emitter = require(173);
var Result = require(174);

function VIEW_RANGE(original, op) {
    return original.withViewRange(op(original.viewRange));
}

function EDIT_RANGE(original, op) {
    return original.withEditRange(op(original.editRange));
}

function RangeQueryImpl(context, valueType, parameters, mode) {
    this.valueType = valueType;
    this.parameters = parameters;
    this.mode = mode;

    function withCurrentRange(op) {
        return new RangeQueryImpl(context, valueType, mode(parameters, op), mode);
    }

    this.selectFrom = function(topicPath) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!topicPath) {
            emitter.error(new Error("Topic path is null"));
            return result;
        }

        var request = {
            topicPath : topicPath,
            parameters : parameters
        };

        context.send(request, function(err, response) {
            if (err) {
                emitter.error(err);
            } else {
                emitter.emit('complete', response.createQueryResult(
                    valueType,
                    parameters.queryType.streamStructure
                ));
            }
        });

        return result;
    };

    this.forValues = function() {
        return new RangeQueryImpl(
            context,
            valueType,
            parameters.withQueryType(QueryType.VALUES).withViewRange(Range.DEFAULT_RANGE),
            VIEW_RANGE
        );
    };

    this.forEdits = function() {
        return new RangeQueryImpl(
            context,
            valueType,
            parameters.withQueryType(QueryType.ALL_EDITS).withViewRange(Range.DEFAULT_RANGE),
            VIEW_RANGE);
    };

    this.editRange = function() {
        if (parameters.queryType !== QueryType.VALUES) {
            throw new Error(".editRange() cannot be applied to this edit range query");
        }

        return new RangeQueryImpl(context, valueType, parameters, EDIT_RANGE);
    };

    this.allEdits = function() {
        if (parameters.queryType === QueryType.VALUES) {
            throw new Error(".allEdits() cannot be applied to this value range query");
        }

        return new RangeQueryImpl(context, valueType, parameters.withQueryType(QueryType.ALL_EDITS), EDIT_RANGE);
    };

    this.latestEdits = function() {
        if (parameters.queryType === QueryType.VALUES) {
            throw new Error(".latestEdits() cannot be applied to this value range query");
        }

        return new RangeQueryImpl(context, valueType, parameters.withQueryType(QueryType.LATEST_EDITS), EDIT_RANGE);
    };

    this.limit = function(count) {
        return new RangeQueryImpl(context, valueType, parameters.withLimit(count), mode);
    };

    this.as = function(newValueType) {
        if (newValueType && newValueType.valueClass) {
            return new RangeQueryImpl(context, newValueType.valueClass, parameters, mode);
        } else if (newValueType instanceof Function) {
            return new RangeQueryImpl(context, newValueType, parameters, mode);
        } else {
            throw new Error("Invalid value type: " + newValueType);
        }
    };

    this.from = function(sequence) {
        return withCurrentRange(function(r) {
            return r.from(sequence);
        });
    };

    this.fromStart = function() {
        return withCurrentRange(function(r) {
            return r.fromStart();
        });
    };

    this.fromLast = function(count) {
        return withCurrentRange(function(r) {
            return r.fromLast(count);
        });
    };

    this.fromLastMillis = function(timespan) {
        return withCurrentRange(function(r) {
            return r.fromLastMillis(timespan);
        });
    };

    this.to = function(sequence) {
        return withCurrentRange(function(r) {
            return r.to(sequence);
        });
    };

    this.toStart = function() {
        return withCurrentRange(function(r) {
            return r.toStart();
        });
    };

    this.untilLast = function(count) {
        return withCurrentRange(function(r) {
            return r.untilLast(count);
        });
    };

    this.untilLastMillis = function(timespan) {
        return withCurrentRange(function(r) {
            return r.untilLastMillis(timespan);
        });
    };

    this.next = function(count) {
        return withCurrentRange(function(r) {
            return r.next(count);
        });
    };

    this.nextMillis = function(timespan) {
        return withCurrentRange(function(r) {
            return r.nextMillis(timespan);
        });
    };

    this.previous = function(count) {
        return withCurrentRange(function(r) {
            return r.previous(count);
        });
    };

    this.previousMillis = function(timespan) {
        return withCurrentRange(function(r) {
            return r.previousMillis(timespan);
        });
    };

    this.equals = function(other) {
        if (other && other instanceof RangeQueryImpl) {
            return parameters.equals(other.parameters);
        }

        return false;
    };
}

RangeQueryImpl.prototype.toString = function() {
    return this.parameters.toString();
};

RangeQueryImpl.createDefault = function(service) {
    return new RangeQueryImpl(service, BytesImpl, RangeQueryParameters.DEFAULT_RANGE_QUERY, VIEW_RANGE);
};

module.exports = RangeQueryImpl;

},{"143":143,"173":173,"174":174,"186":186,"187":187}],186:[function(require,module,exports){
var Range = require(187);

function StreamStructure(id, name) {
    this.id = id;
    this.name = name;
    this.toString = function() {
        return name;
    };
}

var StreamStructures = {
    VALUE_EVENT_STREAM : new StreamStructure(1, "VALUE_EVENT_STREAM"),
    EDIT_EVENT_STREAM : new StreamStructure(2, "EDIT_EVENT_STREAM")
};

function QueryType(id, primaryOperator, editRangeOperator, streamStructure) {
    this.id = id;
    this.primaryOperator = primaryOperator;
    this.editRangeOperator = editRangeOperator;
    this.streamStructure = streamStructure;
}

var QueryTypes = {
    VALUES : new QueryType(0, ".forValues()", ".editRange()", StreamStructures.VALUE_EVENT_STREAM),
    ALL_EDITS : new QueryType(1, ".forEdits()", ".allEdits()", StreamStructures.EDIT_EVENT_STREAM),
    LATEST_EDITS : new QueryType(2, ".forEdits()", ".latestEdits()", StreamStructures.EDIT_EVENT_STREAM)
};

function requireNonNegative(i, what) {
    if (i < 0) {
        throw new Error(what + " is negative: " + i);
    }

    return i;
}

function RangeQueryParameters(queryType, viewRange, editRange, limit) {
    this.queryType = queryType;
    this.viewRange = viewRange;
    this.editRange = editRange;
    this.limit = limit;

    this.withViewRange = function(range) {
        return new RangeQueryParameters(
            this.queryType,
            range,
            this.editRange,
            this.limit
        );
    };

    this.withEditRange = function(range) {
        return new RangeQueryParameters(
            this.queryType,
            this.viewRange,
            range,
            this.limit
        );
    };

    this.withLimit = function(limit) {
        return new RangeQueryParameters(
            this.queryType,
            this.viewRange,
            this.editRange,
            requireNonNegative(limit, "limit")
        );
    };

    this.withQueryType = function(type) {
        return new RangeQueryParameters(
            type,
            this.viewRange,
            this.editRange,
            this.limit
        );
    };
}

RangeQueryParameters.prototype.equals = function(other) {
    if (other && other instanceof RangeQueryParameters) {
        return this.queryType === other.queryType &&
               this.viewRange.equals(other.viewRange) &&
               this.editRange.equals(other.editRange) &&
               this.limit === other.limit;
    }

    return false;
};

RangeQueryParameters.QueryType = QueryTypes;
RangeQueryParameters.StreamStructure = StreamStructures;

RangeQueryParameters.DEFAULT_RANGE_QUERY = new RangeQueryParameters(
    QueryTypes.VALUES,
    Range.DEFAULT_RANGE,
    Range.DEFAULT_RANGE,
    9007199254740991);

RangeQueryParameters.prototype.toString = function() {
    var s = "rangeQuery()";

    if (this.queryType !== QueryTypes.VALUES) {
        s += this.queryType.primaryOperator;
    }

    if (!this.viewRange.equals(RangeQueryParameters.DEFAULT_RANGE_QUERY.viewRange)) {
        s += this.viewRange;
    }

    if (!this.editRange.equals(RangeQueryParameters.DEFAULT_RANGE_QUERY.editRange)) {
        s += this.queryType.editRangeOperator + this.editRange;
    } else if (this.queryType === QueryTypes.LATEST_EDITS) {
        s += QueryTypes.LATEST_EDITS.editRangeOperator;
    }

    if (this.limit !== RangeQueryParameters.DEFAULT_RANGE_QUERY.limit) {
        s += ".limit(" + this.limit + ")";
    }

    return s;
};

module.exports = RangeQueryParameters;
},{"187":187}],187:[function(require,module,exports){
var Point = require(183);

function Range(anchor, end) {
    this.anchor = anchor;
    this.span = end;

    this.equals = function(other) {
        if (other && other instanceof Range) {
            return this.anchor.equals(other.anchor) &&
                   this.span.equals(other.span);
        }

        return false;
    };

    this.from = function(start) {
        return new Range(Point.at(start), this.span);
    };

    this.fromStart = function() {
        return new Range(Point.atStart(), this.span);
    };

    this.fromLast = function(count) {
        return new Range(Point.offset(count), this.span);
    };

    this.fromLastMillis = function(timespan) {
        return new Range(Point.offsetMillis(timespan), this.span);
    };

    this.to = function(sequence) {
        return new Range(this.anchor, Point.at(sequence));
    };

    this.toStart = function() {
        return new Range(this.anchor, Point.atStart());
    };

    this.untilLast = function(count) {
        return new Range(this.anchor, Point.offset(count));
    };

    this.untilLastMillis = function(timespan) {
        return new Range(this.anchor, Point.offsetMillis(timespan));
    };

    this.next = function(count) {
        return new Range(anchor, Point.next(count));
    };

    this.nextMillis = function(timespan) {
        return new Range(this.anchor, Point.nextMillis(timespan));
    };

    this.previous = function(count) {
        return new Range(this.anchor, Point.previous(count));
    };

    this.previousMillis = function(timespan) {
        return new Range(this.anchor, Point.previousMillis(timespan));
    };
}

Range.DEFAULT_RANGE = new Range(Point.atStart(), Point.offset(0));

Range.prototype.toString = function() {
    var s = "";

    if (!this.anchor.equals(Range.DEFAULT_RANGE.anchor)) {
        s += this.anchor.toAnchorDescription();
    }

    if (!this.span.equals(Range.DEFAULT_RANGE.span)) {
        s += this.span.toSpanDescription();
    }

    return s;
};

module.exports = Range;
},{"183":183}],188:[function(require,module,exports){
var _implements = require(415)._implements;
var TimeSeries = require(99);

var RangeQueryImpl = require(185);
var DataTypes = require(144);
var Services = require(325);

var Emitter = require(173);
var Result = require(174);

var requireNonNull = require(422);

module.exports = _implements(TimeSeries, function TimeSeriesImpl(internal) {
    var locator = internal.getServiceLocator();
    var rangeQuery = RangeQueryImpl.createDefault(locator.obtain(Services.RANGE_QUERY));

    var appendService = locator.obtain(Services.TIME_SERIES_APPEND);
    var editService = locator.obtain(Services.TIME_SERIES_EDIT);

    this.rangeQuery = function() {
        return rangeQuery;
    };

    this.edit = function(path, sequence, value, valueType) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        var datatype;

        try {
            if (valueType) {
                datatype = DataTypes.getByClass(valueType);
            } else {
                datatype = DataTypes.getByValue(value);
            }

            requireNonNull(path, "Path");
            requireNonNull(value, "Value");
            requireNonNull(sequence, "Sequence");
            requireNonNull(datatype, "Data type");
        } catch (e) {
            emitter.error(e);
            return result;
        }

        editService.send({
            path : path,
            sequence : sequence,
            dataType : datatype,
            value : datatype.writeValue(value)
        }, function(err, metadata) {
            if (err) {
                emitter.error(err);
            } else {
                emitter.emit('complete', metadata);
            }
        });

        return result;
    };

    this.append = function(path, value, valueType) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        var datatype;

        try {
            if (valueType) {
                datatype = DataTypes.getByClass(valueType);
            } else {
                datatype = DataTypes.getByValue(value);
            }

            requireNonNull(path, "Path");
            requireNonNull(value, "Value");
            requireNonNull(datatype, "Data type");
        } catch (e) {
            emitter.error(e);
            return result;
        }

        appendService.send({
            path : path,
            value : datatype.writeValue(value),
            dataType : datatype
        }, function(err, metadata) {
            if (err) {
                emitter.error(err);
            } else {
                emitter.emit('complete', metadata);
            }
        });

        return result;
    };
});

},{"144":144,"173":173,"174":174,"185":185,"325":325,"415":415,"422":422,"99":99}],189:[function(require,module,exports){
var _implements = require(415)._implements;
var Services = require(325);

var Emitter = require(173);
var Result = require(174);

var CommandService = require(113);
var ControlGroup = require(133);
var registerTopicHandler = require(134).registerTopicHandler;

var updateResponseHandler = require(200);

var TopicAddFailReason = require(435).TopicAddFailReason;

var WillResult = require(380);
var WillParams = require(378);

var Update = require(411).Update;

var UniversalUpdater = require(190);
var ValueCache = require(191);

var parseSelector = require(403);
var TopicDetails = require(391);
var util = require(212);

var api = require(100);

var logger = require(416).create('Session.Topics');

module.exports = _implements(api.TopicControl, function TopicControlImpl(internal) {
    var serviceLocator = internal.getServiceLocator();

    var ADD_SERVICE = serviceLocator.obtain(Services.ADD_TOPIC);
    var REMOVE_SERVICE = serviceLocator.obtain(Services.REMOVE_TOPIC);
    var UPDATE_SERVICE = serviceLocator.obtain(Services.UPDATE_TOPIC);
    var TOPIC_REMOVAL = serviceLocator.obtain(Services.TOPIC_REMOVAL);
    var TOPIC_WILL_REGISTRATION = serviceLocator.obtain(Services.TOPIC_SCOPED_WILL_REGISTRATION);
    var TOPIC_WILL_DEREGISTRATION = serviceLocator.obtain(Services.TOPIC_SCOPED_WILL_DEREGISTRATION);
    var UPDATE_SOURCE_REGISTRATION = serviceLocator.obtain(Services.UPDATE_SOURCE_REGISTRATION);

    var valueCache = new ValueCache();
    var universalUpdater = new UniversalUpdater(internal);

    var serviceRegistry = internal.getServiceRegistry();

    serviceRegistry.add(Services.UPDATE_SOURCE_STATE, CommandService.create(function(internal, request, callback) {
        callback.respond();

        internal.getConversationSet().respondIfPresent(request.cid, {
            old : request.old,
            current : request.current
        });
    }));

    serviceRegistry.add(Services.MISSING_TOPIC, CommandService.create(function(internal, request, callback) {
        internal.getConversationSet().respond(request.cid, {
            request: request,
            callback: function (val) {
                callback.respond(val);
            }
        });
    }));

    this.add = function (path, specification, value) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!path) {
            emitter.error({
                id: 0,
                reason: 'Path cannot be empty or null'
            });

            return result;
        }

        if (value === undefined) {
            value = specification;
        }

        var details = TopicDetails.deriveDetails(specification);
        var content = TopicDetails.deriveValue(details.type, value);

        if (internal.checkConnected(emitter)) {
            logger.debug('Adding topic', path);

            ADD_SERVICE.send({
                path: path, details: details, content: content
            }, function (err, result) {
                if (err) {
                    emitter.error(err);
                } else {
                    switch (result.status) {
                        case 0:
                        case 1:
                            logger.debug("Topic add complete", path);

                            valueCache.put(path, content);

                            emitter.emit('complete', {
                                topic: path,
                                added: true
                            });
                            break;
                        case 2:
                            if (result.reason === TopicAddFailReason.EXISTS) {
                                logger.debug("Topic add complete (already exists)", path);

                                valueCache.put(path, content);

                                emitter.emit('complete', {
                                    topic: path,
                                    added: false
                                });
                            } else {
                                logger.debug("Topic add failed", path);
                                emitter.error(result.reason);
                            }

                            break;
                        case 3:
                            emitter.error({id: 0, reason: 'Cache failure'});
                            break;
                    }
                }
            });
        }

        return result;
    };

    this.remove = function (path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Removing topic', path);

            try {
                REMOVE_SERVICE.send({
                    selector: parseSelector(path)
                }, function (error) {
                    if (error) {
                        logger.debug('Topic removal failed', path);

                        emitter.error(error);
                    } else {
                        logger.debug('Topic removal complete', path);

                        emitter.emit('complete');
                    }
                });
            } catch (err) {
                logger.debug('Error parsing selector', path);
                emitter.error(err);
            }
        }

        return result;
    };

    this.removeSelector = function (expression) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Removing topics', expression);

            try {
                TOPIC_REMOVAL.send({
                    selector: parseSelector(expression)
                }, function (err) {
                    if (err) {
                        logger.debug('Topic removal failed', expression);
                        emitter.error(err);
                    } else {
                        logger.debug('Topic removal complete', expression);
                        emitter.emit('complete');
                    }
                });
            } catch (err) {
                logger.debug('Error parsing selector', expression);
                emitter.error(err);
            }
        }

        return result;
    };

    this.removeWithSession = function (path) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Registering removeWithSession', path);

            var params = new WillParams(path, WillParams.Will.REMOVE_TOPICS);

            TOPIC_WILL_REGISTRATION.send(params, function (err, result) {
                if (err) {
                    logger.debug('removeWithSession registration failed', path);
                    emitter.error(err);
                } else if (result === WillResult.SUCCESS) {
                    logger.debug('removeWithSession registration complete', path);

                    var registered = true;

                    var dEmitter = new Emitter();
                    var dResult = new Result(dEmitter);

                    var deregister = function () {
                        if (registered && internal.checkConnected(dEmitter)) {
                            logger.debug('Deregistering removeWithSession', path);

                            TOPIC_WILL_DEREGISTRATION.send(params, function (err) {
                                if (err) {
                                    dEmitter.error(err);
                                } else {
                                    registered = false;
                                    dEmitter.emit('complete', {
                                        topic: path
                                    });
                                }
                            });
                        }

                        return dResult;
                    };

                    emitter.emit('complete', {
                        deregister: deregister
                    });
                } else {
                    logger.debug('removeWithSession registration failed', path);
                    emitter.error(new Error('REGISTRATION_CONFLICT'));
                }
            });
        }

        return result;
    };

    this.update = function (path, content) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (internal.checkConnected(emitter)) {
            logger.debug('Updating topic', path);

            var callback = function (error, result) {
                if (error) {
                    logger.debug('Update failed', path);

                    emitter.error(error);
                } else if (result.isError) {
                    logger.debug('Update failed', path);

                    emitter.error(result.reason);
                } else {
                    logger.debug('Update complete', path);

                    emitter.emit('complete', path);
                }
            };

            if (util.isMetadataValue(content)) {
                UPDATE_SERVICE.send({
                    path: path,
                    update: new Update(content)
                }, callback);
            } else {
                universalUpdater.update(path, content, callback);
            }
        }

        return result;
    };

    this.registerUpdateSource = function (path, handler) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        var conversations = internal.getConversationSet();
        var cid = conversations.newConversation(updateResponseHandler(internal, valueCache, path, handler));

        if (internal.checkConnected(emitter)) {
            UPDATE_SOURCE_REGISTRATION.send({
                cid: cid,
                path: path
            }, function (err, state) {
                if (err || state === 'closed') {
                    conversations.discard(cid, err);
                    emitter.error(err);
                } else {
                    conversations.respond(cid, {old: 'init', current: state});
                    emitter.emit('complete');
                }
            });
        }

        return result;
    };

    this.addMissingTopicHandler = function (path, handler) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!handler) {
            emitter.error(new Error('Missing Topic handler is null or undefined'));
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var adapter = {
                active: function (close) {
                    logger.debug('Missing Topic Handler registered for ' + path);
                    handler.onRegister(path, close);
                },
                respond: function (response) {
                    logger.debug('Missing Topic Handler notification for ' +
                        response.request.sessionID +
                        ' using ' +
                        response.request.selector);

                    handler.onMissingTopic({
                        path: response.request.selector.prefix,
                        selector: response.request.selector,
                        sessionID: response.request.sessionID,
                        proceed: function () {
                            response.callback(true);
                        },
                        cancel: function () {
                            response.callback(false);
                        }
                    });
                },
                close: function (err) {
                    logger.debug('Missing Topic Handler closed for ' + path);
                    if (err) {
                        handler.onError(path, err);
                    } else {
                        handler.onClose(path);
                    }
                }
            };

            var params = {
                definition: Services.MISSING_TOPIC,
                group: ControlGroup.DEFAULT,
                path: path
            };

            return registerTopicHandler(internal, params, adapter);
        }

        return result;
    };
});

},{"100":100,"113":113,"133":133,"134":134,"173":173,"174":174,"190":190,"191":191,"200":200,"212":212,"325":325,"378":378,"380":380,"391":391,"403":403,"411":411,"415":415,"416":416,"435":435}],190:[function(require,module,exports){
var Services = require(325);
var DataTypes = require(144);

var UpdateFailReason = require(435).UpdateFailReason;

module.exports = function UniversalUpdater(internal) {
    var SET_SERVICE = internal.getServiceLocator().obtain(Services.UPDATE_TOPIC_SET);

    function dataToBytes(d) {
        return d.$buffer.slice(d.$offset, d.$length);
    }

    this.update = function(topic, content, callback) {
        var datatype = DataTypes.get(content);

        if (!datatype) {
            callback(UpdateFailReason.INCOMPATIBLE_UPDATE);
            return;
        }

        var value = datatype.from(content);

        SET_SERVICE.send({
            path : topic,
            bytes : dataToBytes(value)
        }, callback);
    };
};
},{"144":144,"325":325,"435":435}],191:[function(require,module,exports){
var canonicalise = require(402).canonicalise;

module.exports = function ValueCache() {
    var cache = {};

    this.get = function(path) {
        return cache[canonicalise(path)];
    };

    this.put = function(path, value) {
        cache[canonicalise(path)] = value;
    };

    this.remove = function(selector) {
        for (var k in cache) {
            if (selector.selects(k)) {
                delete cache[k];
            }
        }
    };
};
},{"402":402}],192:[function(require,module,exports){
var _implements = require(415)._implements;
var Services = require(325);

var Emitter = require(173);
var Result = require(174);

var CommandService = require(113);

var registration = require(134);

var api = require(101);

var logger = require(416).create('Session.Notifications');

var parseSelector = require(403);
var TopicSelector = require(431);

module.exports = _implements(api.TopicNotifications, function TopicNotificationsImpl(internal) {
    var serviceLocator = internal.getServiceLocator();
    var conversationSet = internal.getConversationSet();

    var TOPIC_NOTIFICATION_SELECTION = serviceLocator.obtain(Services.TOPIC_NOTIFICATION_SELECTION);
    var TOPIC_NOTIFICATION_DESELECTION = serviceLocator.obtain(Services.TOPIC_NOTIFICATION_DESELECTION);
    var TOPIC_NOTIFICATION_DEREGISTRATION = serviceLocator.obtain(Services.TOPIC_NOTIFICATION_DEREGISTRATION);

    var serviceRegistry = internal.getServiceRegistry();

    this.TopicNotificationType = api.TopicNotificationType;

    var eventService = CommandService.create(function(internal, request, callback) {
        callback.respond();
        internal.getConversationSet().respond(request.cid, request);
    });

    serviceRegistry.add(Services.TOPIC_NOTIFICATION_EVENTS, eventService);
    serviceRegistry.add(Services.TOPIC_DESCENDANT_EVENTS, eventService);

    this.addListener = function (listener) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (!listener) {
            emitter.error(new Error('Topic Notification Listener is null or undefined'));
            return result;
        }

        if (internal.checkConnected(emitter)) {
            logger.debug('Adding Topic Notification Listener');

            var adapter = {
                active : function(close, cid) {
                    logger.debug('Topic Notification Listener active');

                    var topicNotificationRegistration = {
                        select: function (topicSelector) {
                            var selectEmitter = new Emitter();
                            var selectResult = new Result(selectEmitter);
                            var selector;

                            if (!topicSelector) {
                                selectEmitter.error(new Error('No topic selector provided'));
                                return selectResult;
                            }

                            if (typeof topicSelector === 'string') {
                                try {
                                    selector = parseSelector(topicSelector);
                                } catch (err) {
                                    selectEmitter.error(err);
                                    return selectResult;
                                }
                            } else if (topicSelector instanceof TopicSelector) {
                                selector = topicSelector;
                            } else {
                                selectEmitter.error(new Error('Invalid topic selector'));
                                return selectResult;
                            }

                            TOPIC_NOTIFICATION_SELECTION.send({ cid : cid, selector: selector },
                                function(err) {
                                    if (err) {
                                        conversationSet.discard(cid, err);
                                        selectEmitter.error(err);
                                    } else {
                                        selectEmitter.emit('complete');
                                    }
                                });

                            return selectResult;
                        },
                        deselect: function (topicSelector) {
                            var deselectEmitter = new Emitter();
                            var deselectResult = new Result(deselectEmitter);
                            var selector;

                            if (!topicSelector) {
                                deselectEmitter.error(new Error('No topic selector provided'));
                                return deselectResult;
                            }

                            if (typeof topicSelector === 'string') {
                                try {
                                    selector = parseSelector(topicSelector);
                                } catch (err) {
                                    deselectEmitter.error(err);
                                    return deselectResult;
                                }
                            } else if (topicSelector instanceof TopicSelector) {
                                selector = topicSelector;
                            } else {
                                deselectEmitter.error(new Error('Invalid topic selector'));
                                return deselectResult;
                            }

                            TOPIC_NOTIFICATION_DESELECTION.send({ cid : cid, selector: selector },
                                function(err) {
                                    if (err) {
                                        conversationSet.discard(cid, err);
                                        deselectEmitter.error(err);
                                    } else {
                                        deselectEmitter.emit('complete');
                                    }
                                });

                            return deselectResult;
                        },
                        close: function() {
                            TOPIC_NOTIFICATION_DEREGISTRATION.send({ cid : cid }, function(err, response) {
                                if (err) {
                                    conversationSet.discard(cid, err);
                                    logger.debug('Error with topic notification deregistration: ', err);
                                } else {
                                    listener.onClose();
                                    conversationSet.respondIfPresent(cid, response);
                                }
                            });
                        }
                    };

                    emitter.emit('complete', topicNotificationRegistration);
                },
                respond : function(message) {
                    if (message.isDescendantEvent) {
                        listener.onDescendantNotification(message.path, message.type);
                    } else {
                        listener.onTopicNotification(
                            message.path,
                            message.specification,
                            message.type);
                    }
                },
                close : function(err) {
                    logger.debug('Topic Notification Listener closed');

                    if (err) {
                        listener.onError(err);
                    } else {
                        listener.onClose();
                    }
                }
            };

            var cid = conversationSet.newConversation(
                registration.responseHandler(internal, adapter, TOPIC_NOTIFICATION_DEREGISTRATION.send));

            var emitterProxy = {
                emit : function() {
                }
            };

            registration.registrationCallback(conversationSet, cid, emitterProxy)();
        }

        return result;
    };
});
},{"101":101,"113":113,"134":134,"173":173,"174":174,"325":325,"403":403,"415":415,"416":416,"431":431}],193:[function(require,module,exports){
var _implements = require(415)._implements;

var Emitter = require(173);
var Result = require(174);
var View = require(199);

var Services = require(325);
var parseSelector = require(403);

var SubscriptionProxy = require(195);
var FetchStream = require(194);

var Topics = require(102);

var CloseReason = require(112);

var requireNonNull = require(422);
var logger = require(416).create('Session');

module.exports = _implements(Topics, function TopicsImpl(internal) {
    var unsubscribe = internal.getServiceLocator().obtain(Services.UNSUBSCRIBE);
    var subscribe = internal.getServiceLocator().obtain(Services.SUBSCRIBE);
    var fetch = internal.getServiceLocator().obtain(Services.FETCH);

    var streamRegistry = internal.getStreamRegistry();

    this.subscribe = function(topic, callback) {
        logger.debug('Subscribing', topic);

        var selector = parseSelector(topic);
        var proxy = new SubscriptionProxy(streamRegistry, selector, false, callback);

                if (internal.checkConnected(proxy.emitter)) {
            subscribe.send(selector, function (err) {
                if (err) {
                    proxy.onSubscriptionError(err);
                }
            });
        }

        return proxy.subscription;
    };

    this.unsubscribe = function(topic) {
        var emitter = new Emitter();
        var result = new Result(emitter);
        var selector;

        try {
            selector = parseSelector(topic);
        } catch (err) {
            emitter.error(err);
            return result;
        }

        if (internal.checkConnected(emitter)) {
            var unsubscribeCallback = function(err) {
                if (err) {
                    logger.debug('Unsubscribe failed', topic);
                    emitter.error(err);
                } else {
                    logger.debug('Unsubscribe complete', topic);
                    emitter.emit('complete');
                }
            };

            logger.debug('Unsubscribing', topic);
            unsubscribe.send(selector, unsubscribeCallback);
        }
        return result;
    };

    this.stream = function(topic, callback) {
        logger.debug('Establishing topic stream', topic);

        var fallback = false;
        var selector = "";

        if (!topic || typeof topic === 'function') {
            callback = topic;
            fallback = true;
        } else {
            selector = parseSelector(topic);
        }

        var proxy = new SubscriptionProxy(streamRegistry, selector, fallback, callback);

        return proxy.subscription;
    };

    this.fetch = function(selector) {
        logger.debug('Fetch request for: ', selector);

        requireNonNull(selector, "Selector");

        selector = parseSelector(selector);

        var emitter = new Emitter();
        var stream = new FetchStream(emitter);

        if (internal.checkConnected(emitter)) {
            var cid = internal.getConversationSet().newConversation({
                onOpen : function() {
                    emitter.emit('open');
                },
                onResponse : function(cid, message) {
                    var content = message.data;
                    var path = message.topic;

                    if (path) {
                        emitter.emit('value', content, path);
                        return false;
                    } else if (content.length === 1 && content[0] === 1) {
                        emitter.close();
                        return true;
                    } else {
                        emitter.error(new Error("Unexpected end of fetch stream"));
                        return true;
                    }
                },
                onDiscard : function(cid, reason) {
                    if (reason instanceof CloseReason) {
                        emitter.close();
                    } else {
                        emitter.error(reason);
                    }
                }
            });

            fetch.send({
                cid : cid,
                selector : selector
            }, function(err) {
                if (err) {
                    emitter.error(err);
                }
            });
        }

        return stream;
    };

    this.view = function(selector, callback) {
        var s = this.subscribe(selector);
        return new View(s, callback);
    };
});

},{"102":102,"112":112,"173":173,"174":174,"194":194,"195":195,"199":199,"325":325,"403":403,"415":415,"416":416,"422":422}],194:[function(require,module,exports){
var _implements = require(415)._implements;
var FetchStream = require(88);

module.exports = _implements(FetchStream, function FetchStreamImpl(emitter) {
    emitter.assign(this);
});
},{"415":415,"88":88}],195:[function(require,module,exports){
var SubscriptionImpl = require(196);
var Emitter = require(173);

module.exports = function SubscriptionProxy(registry, selector, fallback, callback) {
    var emitter = new Emitter();
    var stream = emitter.get();
    var self = this;

    var subscription = new SubscriptionImpl(registry, stream, fallback, selector);
    var pending = true;

    stream.on('close', function() {
        registry.remove(self);
    });

    this.subscription = subscription;
    this.emitter = emitter;

    this.selects = function() {
        return true;
    };

    this.onOpen = function() {
        emitter.emit('open', subscription);
    };

    this.onDelta = function(topic, details, specification, value) {
        emitter.emit('update', value, topic);
    };

    this.onValue = function(topic, details, specification, value) {
        emitter.emit('update', value, topic);
    };

    this.onSubscription = function(topic, details) {
        emitter.emit('subscribe', details, topic);
    };

    this.onUnsubscription = function(topic, details, specification, reason) {
        emitter.emit('unsubscribe', reason, topic);
    };

    this.onSubscriptionError = function (err) {
        emitter.error(err);
        registry.remove(self);
    };

    if (callback) {
        subscription.on('update', callback);

        if (fallback) {
            registry.addFallback(self);
        } else {
            registry.add(selector, self);
        }
    } else {
        subscription.on = function(event, fn) {
            if (pending) {
                if (fallback) {
                    registry.addFallback(self);
                } else {
                    registry.add(selector, self);
                }

                pending = false;
            }

            return stream.on.call(subscription, event, fn);
        };
    }
};
},{"173":173,"196":196}],196:[function(require,module,exports){
var _implements = require(415)._implements;
var Subscription = require(91);

var ValueStreamProxy = require(197);
var View = require(199);

var Emitter = require(173);
var util = require(212);

module.exports = _implements(Subscription, function SubscriptionImpl(registry, stream, fallback, selector) {
    for (var fn in stream) {
        this[fn] = stream[fn];
    }

    this.selector = selector;
    var self = this;

    this.view = function() {
        return new View(self);
    };

    this.asType = function(datatype, callback) {
        var proxy = new ValueStreamProxy(registry, selector, datatype, fallback, callback);
        self.on('error', function(error) {
            proxy.onSubscriptionError(error);
        });
        return proxy.subscription;
    };

    this.transform = function(fn) {
        var e = new Emitter();

        fn = util.isMetadata(fn) ? fn.parse.bind(fn) : fn;

        self.on('open', function(selector) {
            e.emit('open', selector, self);
        });

        self.on('close', function() {
            e.emit('close');
        });

        self.on('update', function(update, topic) {
            e.emit('update', fn(update), topic);
        });

        self.on('subscribe', function(details) {
            e.emit('subscribe', details);
        });

        self.on('unsubscribe', function(reason, topic) {
            e.emit('unsubscribe', reason, topic);
        });

        self.on('error', function(error) {
            e.error(error);
        });

        return new SubscriptionImpl(registry, e.get(), selector);
    };
});

},{"173":173,"197":197,"199":199,"212":212,"415":415,"91":91}],197:[function(require,module,exports){
var ValueStreamImpl = require(198);
var DataTypes = require(144);
var TopicType = require(435).TopicType;
var Emitter = require(173);

module.exports = function ValueStreamProxy(registry, selector, datatype, fallback, callback) {
    var emitter = new Emitter();
    var stream = emitter.get();
    var self = this;

    var subscription = new ValueStreamImpl(registry, stream, selector);
    var pending = true;

    stream.on('close', function() {
        registry.remove(self);
    });

    this.subscription = subscription;
    this.emitter = emitter;

    this.selects = function(specification) {
        if (specification) {
            if (specification.type === TopicType.TIME_SERIES) {
                var eventType = DataTypes.get(specification.properties.TIME_SERIES_EVENT_VALUE_TYPE);
                return eventType && eventType.canReadAs(datatype.valueClass);
            } else {
                return specification.type === TopicType[datatype.name().toUpperCase()];
            }
        }

        return false;
    };

    this.onOpen = function() {
        emitter.emit('open', subscription);
    };

    this.onDelta = function(topic, details, specification, received, delta, oldValue, newValue) {
        emitter.emit('value', topic, specification, newValue, oldValue);
    };

    this.onValue = function(topic, details, specification, received, oldValue, newValue) {
        emitter.emit('value', topic, specification, newValue, oldValue);
    };

    this.onSubscription = function(topic, details, specification) {
        emitter.emit('subscribe', topic, specification);
    };

    this.onUnsubscription = function(topic, details, specification, reason) {
        emitter.emit('unsubscribe', topic, specification, reason);
    };

    this.onSubscriptionError = function(error) {
        emitter.error(error);
        registry.remove(self);

    };

    if (callback) {
        subscription.on('value', callback);

        if (fallback) {
            registry.addFallback(self);
        } else {
            registry.add(selector, self);
        }
    } else {
        subscription.on = function(event, fn) {
            if (pending) {
                if (fallback) {
                    registry.addFallback(self);
                } else {
                    registry.add(selector, self);
                }

                pending = false;
            }

            return stream.on.call(subscription, event, fn);
        };
    }
};
},{"144":144,"173":173,"198":198,"435":435}],198:[function(require,module,exports){
var _implements = require(415)._implements;

var ValueStream = require(92);

module.exports = _implements(ValueStream, function ValueStreamImpl(registry, stream, selector) {
    for (var fn in stream) {
        this[fn] = stream[fn];
    }

    this.selector = selector;
});
},{"415":415,"92":92}],199:[function(require,module,exports){
var _implements = require(415)._implements;

var Emitter = require(173);
var View = require(93);

function ViewImpl(subscription) {
    var emitter = Emitter.assign(this);
    var data = {};

    subscription.on('update', function(value, topic) {
        var parts = topic.split('/');
        var child = data;
        var i = 0;

                for (; i < parts.length - 1; i++) {
            if (child[parts[i]] === undefined) {
                child[parts[i]] = {};
            }

                        child = child[parts[i]];
        }

                child[parts[i]] = value;

                emitter.emit('update', data);
    });

    subscription.on('unsubscribe', function(reason, topic) {
        if (reason.id === 2) {
            var parts = topic.split('/');
            var child = data;
            var i = 0;

            for (; i < parts.length - 1; i++) {
                if (child[parts[i]] === undefined) {
                    break;
                }

                child = child[parts[i]];
            }

            delete child[parts[i]];

            emitter.emit('update', data);
        }
    });

        subscription.on('close', emitter.close);

        this.get = function() {
        return data;
    };
}

module.exports = _implements(View, ViewImpl);

},{"173":173,"415":415,"93":93}],200:[function(require,module,exports){
var parseSelector = require(403);
var canonicalise = require(402).canonicalise;

var Services = require(325);
var DataTypes = require(144);
var Emitter = require(173);
var Result = require(174);
var Update = require(411);

var UpdateFailReason = require(435).UpdateFailReason;
var CloseReason = require(112);

var util = require(212);

function dataToBytes(d) {
    return d.$buffer.slice(d.$offset, d.$length);
}

function clearCache(cache, path) {
    var selector = parseSelector('?' + canonicalise(path) + '//');
    cache.remove(selector);
}

function Updater(cid, dispatch) {
    var self = this;

    this.isClosed = false;

    this.update = function update(topic, value) {
        var emitter = new Emitter();
        var result = new Result(emitter);

        if (self.isClosed) {
            emitter.error(new Error('Updater is closed'));
        } else if (!topic) {
            emitter.error(new Error('Topic can not be null or empty'));
        } else if (value === undefined || value === null) {
            emitter.error(new Error('Update cannot be null'));
        } else {
            dispatch(emitter, cid, topic, value);
        }

        return result;
    };
}

module.exports = function UpdateResponseHandler(internal, valueCache, topic, handler) {
    var UPDATE_SOURCE_DEREGISTRATION = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_DEREGISTRATION);
    var UPDATE_SOURCE_UPDATE = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_UPDATE);
    var UPDATE_SOURCE_DELTA = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_DELTA);
    var UPDATE_SOURCE_SET = internal.getServiceLocator().obtain(Services.UPDATE_SOURCE_SET);

    var dispatch = function (emitter, cid, path, content) {
        var callback = function (err, result) {
            if (err) {
                emitter.error(err);
            } else if (result.error) {
                emitter.error(new Error("Topic update error for topic " + path + " : " + result.error));
            } else {
                emitter.emit('complete');
            }
        };

        if (internal.checkConnected(emitter)) {
            if (util.isMetadataValue(content)) {
                UPDATE_SOURCE_UPDATE.send({
                    cid: cid,
                    path: path,
                    update: new Update.Update(content)
                }, callback);
            } else {
                var datatype = DataTypes.get(content);

                if (!datatype) {
                    emitter.error(UpdateFailReason.INCOMPATIBLE_UPDATE);
                    return;
                }

                var value = datatype.from(content);
                var prev = valueCache.get(path);

                if (prev) {
                    var deltaType = datatype.deltaType("binary");
                    var delta = deltaType.diff(prev, value);

                    if (delta === deltaType.noChange()) {
                        callback(null, {});
                        return;
                    }

                    UPDATE_SOURCE_DELTA.send({
                        id: 0,
                        cid: cid,
                        path: path,
                        bytes: dataToBytes(delta)
                    }, callback);
                } else {
                    UPDATE_SOURCE_SET.send({
                        cid: cid,
                        path: path,
                        bytes: dataToBytes(value)
                    }, callback);
                }

                valueCache.put(path, value);
            }
        }
    };

    var state = 'init';
    var close, updater;

    return {
        onOpen: function (cid) {
            close = function close() {
                var emitter = new Emitter();
                var result = new Result(emitter);

                UPDATE_SOURCE_DEREGISTRATION.send({cid: cid}, function (err) {
                    if (err) {
                        internal.getConversationSet().discard(cid, err);
                        emitter.error(err);
                    } else {
                        internal.getConversationSet().respond(cid, {
                            old: state,
                            current: 'closed'
                        });

                        emitter.emit('complete');
                    }
                });

                return result;
            };
        },
        onResponse: function (cid, change) {
            if (change.old !== state) {
                internal.getConversationSet().discard(cid,
                    new Error("Inconsistent server/client update source state. Current: " +
                        state + ", expected: " + change.old));

                return false;
            }

            if (state === 'init' && change.current !== 'closed') {
                handler.onRegister(topic, close);
            }

            if (updater) {
                updater.isClosed = true;
            }

            state = change.current;

            switch (state) {
                case 'active' :
                    updater = new Updater(cid, dispatch);

                    clearCache(valueCache, topic);

                    handler.onActive(topic, updater);
                    return false;
                case 'standby' :
                    handler.onStandBy(topic);
                    return false;
                default :
                    clearCache(valueCache, topic);

                    handler.onClose(topic);
                    return true;
            }
        },
        onDiscard: function (cid, reason) {
            state = 'closed';

            if (updater) {
                updater.isClosed = true;
            }

            clearCache(valueCache, topic);

            if (reason instanceof CloseReason) {
                handler.onClose(topic);
            } else {
                handler.onClose(topic, reason);
            }
        }
    };
};

},{"112":112,"144":144,"173":173,"174":174,"212":212,"325":325,"402":402,"403":403,"411":411,"435":435}],201:[function(require,module,exports){
var Long = require(46);

function BufferInputStream(buffer) {
    if (buffer === null || buffer === undefined) {
        throw new Error("Undefined buffer for InputStream");
    }

        this.buffer = buffer;
    this.count = buffer.length;
    this.mark = 0;
    this.pos = 0;
}

BufferInputStream.prototype.read = function() {
    return (this.pos < this.count) ? (this.buffer[this.pos++] & 0xFF) : -1;
};

BufferInputStream.prototype.readMany = function(length) {
    length = Math.min(length, this.count - this.pos);

        if (length < 0) {
        throw new Error("Length out of bounds");
    }

    var buffer = this.buffer.slice(this.pos, this.pos + length);
    this.pos += length;
    return buffer;
};

BufferInputStream.prototype.readUntil = function(delim) {
    var found = this.count;

        for (var i = this.pos; i < this.count; i++) {
        if (this.buffer[i] === delim) {
            found = i;
            break;
        }
    }

        var buffer = this.buffer.slice(this.pos, found);
    if (found === this.count) {
        this.pos = found;
    } else {
        this.pos = found + 1;
    }

    return buffer;
};

BufferInputStream.prototype.readInt8 = function() {
    return this.buffer.readInt8(this.pos++);
};

BufferInputStream.prototype.readInt32 = function() {
    var i = this.buffer.readInt32BE(this.pos);
    this.pos += 4;
    return i;
};

BufferInputStream.prototype.readInt64 = function() {
    var hi = this.buffer.readInt32BE(this.pos);
    this.pos += 4;
    var lo = this.buffer.readInt32BE(this.pos);
    this.pos += 4;

       return Long.fromBits(lo, hi, false);
};

BufferInputStream.prototype.readUInt64 = function() {
    var hi = this.buffer.readInt32BE(this.pos);
    this.pos += 4;
    var lo = this.buffer.readInt32BE(this.pos);
    this.pos += 4;

       return Long.fromBits(lo, hi, true);
};

BufferInputStream.prototype.hasRemaining = function() {
    return this.pos < this.buffer.length;
};

module.exports = BufferInputStream;

},{"46":46}],202:[function(require,module,exports){
(function (Buffer){
function ensureCapacity(bos, min) {
    if (min - bos.buffer.length > 0) {
        grow(bos, min);
    }
}

function grow(bos, minCapacity) {
    var oldCapacity = bos.buffer.length,
        newCapacity = oldCapacity << 1;

    if (newCapacity - minCapacity < 0) {
        newCapacity = minCapacity;
    }

    try {
        var replacement = new Buffer(newCapacity);
        bos.buffer.copy(replacement);
        bos.buffer = replacement;
    } catch (e) {
        throw new Error("Unable to resize BufferOutputStream to " + newCapacity);
    }
}

function BufferOutputStream(initial) {
    if (Buffer.isBuffer(initial)) {
        this.buffer = initial;
        this.count = initial.length;
    } else {
        this.buffer = new Buffer((initial || 32));
        this.count = 0;
    }
}

BufferOutputStream.prototype.write = function (val) {
    ensureCapacity(this, this.count + 1);
    this.buffer[this.count++] = val;
};

BufferOutputStream.prototype.writeMany = function (buffer, offset, length) {
    if (length === 0) {
        return;
    }

    offset = offset || 0;
    length = length || buffer.length;

    ensureCapacity(this, this.count + length);

    buffer.copy(this.buffer, this.count, offset, offset + length);
    this.count += length;
};

BufferOutputStream.prototype.writeString = function (val) {
    var length = Buffer.byteLength(val);
    ensureCapacity(this, this.count + length);

    this.buffer.write(val, this.count, length);
    this.count += length;
};

BufferOutputStream.prototype.writeInt8 = function (val) {
    ensureCapacity(this, this.count + 1);

    this.buffer.writeInt8(val, this.count++);
};

BufferOutputStream.prototype.writeInt32 = function (val) {
    ensureCapacity(this, this.count + 4);

    this.buffer.writeInt32BE(val, this.count);
    this.count += 4;
};

BufferOutputStream.prototype.writeInt64 = function (val) {
    ensureCapacity(this, this.count + 8);

    this.buffer.writeInt32BE(val.getHighBits(), this.count);
    this.buffer.writeInt32BE(val.getLowBits(), this.count + 4);

    this.count += 8;
};

BufferOutputStream.prototype.getBuffer = function () {
    return this.buffer.slice(0, this.count);
};

BufferOutputStream.prototype.getBase64 = function () {
    return this.getBuffer().toString('base64');
};

module.exports = BufferOutputStream;

}).call(this,require(17).Buffer)
},{"17":17}],203:[function(require,module,exports){
(function (Buffer){
var Long = require(46);

var x80 = Long.fromNumber(0x80),
    x7F = Long.fromNumber(0x7F),
    x7FInv = x7F.not(); 

var readOneByte = function(input) {
    var i = input.read();

    if (i === -1) {
        throw new Error("End of stream");
    }

        return i;
};

var codec = {};

codec.readInt64 = function(input) {
    var result = Long.fromNumber(0), shift = 0;

    while (shift < 64) {
        var i = readOneByte(input),
            l = Long.fromNumber(i);

        result = result.or(l.and(x7F).shiftLeft(shift));

        if (l.and(x80).equals(0)) {
            return result;
        }

        shift += 7;
    }

    throw "Malformed int64";
};

codec.writeInt64 = function(bos, value) {
    var int64 = value instanceof Long ? value : Long.fromNumber(value, false);

    for (;;) {
        if (int64.and(x7FInv).equals(0)) {
            bos.write(int64.toInt());
            return;
        } else {
            bos.write(int64.and(x7F).or(x80).toInt());
            int64 = int64.shiftRightUnsigned(7);
        }
    }
};

codec.readInt32 = function(bis) {
    var shift = 0,
        result = 0;

    while (shift < 32) {
        var i = readOneByte(bis);

        result |= (i & 0x7F) << shift;

        if ((i & 0x80) === 0) {
            return result;
        }

        shift += 7;
    }

    throw "Malformed int32";
};

codec.writeInt32 = function(bos, value) {
    for (;;) {
        if ((value & ~0x7F) === 0) {
            bos.write(value);
            return;
        } else {
            bos.write((value & 0x7F) | 0x80);
            value = value >>> 7;
        }
    }
};

codec.writeByte = function(bos, value) {
    bos.writeInt8(value);

    if ((value & ~0x7F) !== 0) {
        bos.write(1);
    }
};

codec.readByte = function(bis) {
    var b1 = bis.readInt8();

    if ((b1 & ~0x7F) === 0) {
        return b1;
    }

    var b2 = readOneByte(bis);

    if (b2 !== 1) {
        throw "Malformed byte";
    }

    return (b1 | 0x80);
};

codec.readBytes = function(bis) {
    var length = codec.readInt32(bis);
    return bis.readMany(length);
};

codec.writeBytes = function(bos, value) {
    codec.writeInt32(bos, value.length);
    bos.writeMany(value);
};

codec.readString = function(bis) {
    var buffer = codec.readBytes(bis);
    return buffer ? buffer.toString('utf8') : "";
};

codec.writeString = function(bos, value) {
    codec.writeBytes(bos, new Buffer(value, 'utf8'));
};

codec.writeCollection = function(bos, arr, write) {
    codec.writeInt32(bos, arr.length);

        for (var i = 0; i < arr.length; ++i) {
        write(bos, arr[i]);
    }
};

codec.readCollection = function(bis, read) {
    var length = codec.readInt32(bis);
    var arr = [];

        for (var i = 0; i < length; ++i) {
        arr.push(read(bis));
    }

        return arr;
};

codec.readDictionary = function(bis, read) {
    return codec.readMap(bis, codec.readString, read);
};

codec.writeDictionary = function(bos, dict, write) {
    codec.writeMap(bos, dict, codec.writeString, write);
};

codec.writeMap = function(bos, dict, key, value) {
    codec.writeInt32(bos, Object.keys(dict).length);

    for (var k in dict) {
        key(bos, k);
        value(bos, dict[k]);
    }
};

codec.readMap = function(bis, key, value) {
    var length = codec.readInt32(bis);
    var dict = {};

    for (var i = 0; i < length; ++i) {
        var k = key(bis);
        dict[k] = value(bis);
    }

    return dict;
};

codec.readBoolean = function(bis) {
    var b = bis.readInt8();
    if (b === 0) {
        return false;
    }
    return true;
};

codec.writeBoolean = function(bos, value) {
    if (value) {
        bos.writeInt8(1);
    } else {
        bos.writeInt8(0);
    }
};

module.exports = codec;

}).call(this,require(17).Buffer)
},{"17":17,"46":46}],204:[function(require,module,exports){
var findNextPowerOfTwo = require(417).findNextPowerOfTwo;
var curryR = require(414).curryR;

var ofSize = require(412).ofSize;
var fill = require(412).fill;

var TIMESTAMP_ROUNDING = 10;

function roundTimeStamp(timestamp) {
    return timestamp >> TIMESTAMP_ROUNDING;
}

function mask(p, capacity) {
    return p & capacity - 1;
}

module.exports = function RecoveryBuffer(minimumMessages, minimumTimeIndex) {
    var messagesLength = findNextPowerOfTwo(minimumMessages);
    var indexCapacity = findNextPowerOfTwo(minimumTimeIndex);

    var messagesMask = curryR(mask, messagesLength);
    var indexMask = curryR(mask, indexCapacity);

    var messages = ofSize(messagesLength);
    var indices = ofSize(indexCapacity, -1);
    var times = ofSize(indexCapacity);

    var timesHead = 0;
    var timesTail = 0;

    var tail = messagesMask(0);
    var size = 0;

    this.size = function () {
        return size;
    };

    this.put = function (message) {
        messages[tail] = message;
        tail = messagesMask(tail + 1);

        if (size < messagesLength) {
            size = size + 1;
        } else {
            while (indices[timesHead] === tail) {
                indices[timesHead] = -1;
                timesHead = indexMask(timesHead + 1);
            }
        }
    };

    this.recover = function (n, consumer) {
        if (n < 0 || n > size) {
            return false;
        }

        for (var i = n; i >= 1; --i) {
            consumer(messages[messagesMask(tail - i)]);
        }

        return true;
    };

    this.clear = function () {
        fill(messages, null);
        fill(indices, -1);

        timesHead = 0;
        timesTail = 0;
        size = 0;
    };

    function removeElements(newElementsHead) {
        var messagesHead = messagesMask(tail - size);
        var newSize;

        if (messagesHead < newElementsHead) {
            fill(messages, null, messagesHead, newElementsHead);

            newSize = size - newElementsHead + messagesHead;
        } else if (messagesHead > newElementsHead) {
            fill(messages, null, messagesHead, messagesLength);
            fill(messages, null, 0, newElementsHead);

            newSize = size - messagesHead + newElementsHead;
        } else {
            fill(messages, null);
            newSize = 0;
        }

        size = newSize;
    }

    this.markTime = function (timestamp) {
        if (size > 0) {
            var h = timesHead;
            var t = timesTail;
            var firstIndex = indices[h];
            var roundedTimestamp = roundTimeStamp(timestamp);

            if (firstIndex >= 0) {
                var indexLast = indexMask(t - 1);

                if (indices[indexLast] === tail) {
                    return;
                }

                if (times[indexLast] === roundedTimestamp) {
                    indices[indexLast] = tail;
                    return;
                }
            }

            timesTail = indexMask(t + 1);
            times[t] = roundedTimestamp;
            indices[t] = tail;

            if (h === t && firstIndex >= 0) {
                removeElements(firstIndex);
                timesHead = timesTail;
            }
        }
    };

    this.flush = function (timestamp) {

        if (size === 0 || indices[timesHead] < 0) {
            return;
        }

        var i = timesTail;
        var roundedTimestamp = roundTimeStamp(timestamp);

        do {
            var previousI = i;
            i = indexMask(i - 1);

            if (times[i] <= roundedTimestamp) {
                removeElements(indices[i]);

                if (timesHead <= previousI) {
                    fill(indices, -1, timesHead, previousI);
                } else {
                    fill(indices, -1, timesHead, indexCapacity);
                    fill(indices, -1, 0, previousI);
                }

                timesHead = previousI;
                return;
            }
        } while (i !== timesHead);
    };
};
},{"412":412,"414":414,"417":417}],205:[function(require,module,exports){
module.exports = function MDecimal(val, scale) {
    var self = this;

        if (val !== undefined && typeof val !== "number") {
        throw new Error('Decimal metadata must be given a numeric default value');
    }

        if (scale !== undefined && typeof scale !== "number") {
        throw new Error('Decimal metadata must be given a numeric scale');
    }

        this.value = val || 0.00;

        if (scale === undefined) {
        var str = this.value + '', i = str.indexOf('.');
        this.scale = i > -1 ? str.length - (i + 1) : 2; 
    } else {
        this.scale = scale;
    }

        this.getDetails = function() {
        return {
            type : 'decimalString',
            name : 'decimalString',
            default : self.value,
            scale : self.scale
        };
    };

        this.parse = function(buffer) {
        var str = buffer.toString(), i = str.indexOf('.');
        return +str.substr(0, i + this.scale + 1);
    };
};
},{}],206:[function(require,module,exports){
module.exports = function MInteger(val) {
    var self = this;

        if (val !== undefined && typeof val !== "number") {
        throw new Error('Integer metadata must be given a numeric default value');
    }

        this.value = parseInt(val, 10) || 0;

        this.getDetails = function() {
        return {
            type : 'integerString',
            name : 'integerString',
            default : self.value
        };
    };

        this.parse = function(buffer) {
        return parseInt(buffer.toString(), 10);
    };
};
},{}],207:[function(require,module,exports){
var MRecordContent = require(208);
var MStateless = require(209);
var MDecimal = require(205);
var MInteger = require(206);
var MString = require(210);

   module.exports = {
    RecordContent : MRecordContent,
    Stateless : MStateless,
    Decimal : MDecimal,
    Integer : MInteger,
    String : MString
};


},{"205":205,"206":206,"208":208,"209":209,"210":210}],208:[function(require,module,exports){
var _implements = require(415)._implements;
var Metadata = require(103);

var RecordContentParser = require(129);
var RecordContentBuilder = require(128);

var MDecimal = require(205);
var MInteger = require(206);
var MString = require(210);

var util = require(211);

var count = 0;

function occurs(m, n) {
    var min = 1, max = 1;

    if (m !== undefined) {
        if (typeof(m) === 'string') {
            var i = m.indexOf('.');

            if (i !== -1) {
                var j = m.lastIndexOf('.');

                min = m.substring(0, i);
                max = m.substring(j + 1);

                if (max === '*') {
                    max = -1;
                }
            } else {
                min = m;
                max = m;
            }
        } else if (typeof(m) === 'number') {
            min = m;

            if (n !== undefined && typeof (n) === 'number') {
                max = n;
            } else {
                max = m;
            }
        } else if (m.min !== undefined) {
            min = m.min;
            max = m.max;
        }

        if (max === undefined) {
            max = min;
        }

        min = parseInt(min, 10);
        max = parseInt(max, 10);
    }

    return {
        min: min,
        max: max,
        toString: function () {
            var min = this.min;
            var max = this.max > -1 ? this.max : '*';

            return min === max ? '' + min : min + '..' + max;
        }
    };
}

var MFieldImpl = _implements(Metadata.RecordContent.Field, function MField(name, type, occurs) {
    this.name = name;
    this.occurs = occurs;

    if (type) {
        var t = util.deriveMetadata(type);

        if (util.getType(t) !== util.Type.SINGLE_VALUE) {
            throw new Error('Record fields can only have single values (string, integer or decimal)');
        }

        this.type = t;
    } else {
        this.type = new MString();
    }

    this.getDetails = function () {
        var details = this.type.getDetails();

        details.name = name;
        details.multiplicity = occurs.toString();

        return details;
    };
});

var MRecordImpl = _implements(Metadata.RecordContent.Record, function MRecord(name, m) {
    var fields = {};
    var order = [];

    this.name = name;
    this.occurs = m;

    this.getField = function (name) {
        switch (typeof name) {
            case 'string' :
                return fields[name];
            case 'number' :
                return fields[order[name]];
        }

        return undefined;
    };

    this.getFields = function () {
        return order.map(function (name) {
            return fields[name];
        });
    };

    this.addField = function (name, type, m) {
        if (fields[name]) {
            throw new Error('Field metadata already exists for: ' + name);
        }

        var previous = fields[order[order.length - 1]];
        if (previous && previous.occurs.min !== previous.occurs.max) {
            throw new Error('Fields cannot be added after a repeating field');
        }

        var field = new MFieldImpl(name, type, occurs(m));

        fields[name] = field;
        order.push(name);

        return field;
    };

    this.getDetails = function () {
        return {
            name: name,
            multiplicity: m.toString()
        };
    };
});

var MRecordContentImpl = _implements(Metadata.RecordContent, function MRecordContent(name) {
    var parser = new RecordContentParser(this);
    var self = this;

    var records = {};
    var order = [];

    name = name || 'MRecordData' + (count++);

    this.addRecord = function (name, m, fields) {
        if (records[name]) {
            throw new Error('Record metadata already exists for: ' + name);
        }

        var previous = records[order[order.length - 1]];
        if (previous && previous.occurs.min !== previous.occurs.max) {
            throw new Error('Records cannot be added after a repeating record');
        }

        var record = new MRecordImpl(name, occurs(m));
        records[name] = record;
        order.push(name);

        if (fields !== undefined) {
            for (var field in fields) {
                record.addField(field, fields[field]);
            }
        }

        return record;
    };

    this.getRecord = function (name) {
        switch (typeof name) {
            case 'string' :
                return records[name];
            case 'number' :
                return records[order[name]];
        }

        return undefined;
    };

    this.getRecords = function () {
        return order.map(function (name) {
            return records[name];
        });
    };

    this.occurs = occurs;
    this.name = name;

    this.string = function (val) {
        return new MString(val);
    };

    this.integer = function (val) {
        return new MInteger(val);
    };

    this.decimal = function (val, scale) {
        return new MDecimal(val, scale);
    };

    this.getDetails = function () {
        return {name: name};
    };

    this.builder = function () {
        return new RecordContentBuilder(self);
    };

    this.parse = function (buffer) {
        return parser.parse(buffer);
    };
});

module.exports = MRecordContentImpl;

},{"103":103,"128":128,"129":129,"205":205,"206":206,"210":210,"211":211,"415":415}],209:[function(require,module,exports){
var details = {
    type : 'stateless',
    name : 'stateless'
};

module.exports = function MStateless() {
    this.getDetails = function() {
        return details;
    };

        this.parse = function(buffer) {
        return buffer;
    };
};
},{}],210:[function(require,module,exports){
module.exports = function MString(val) {
    var self = this;

        this.value = val === undefined ? '' : '' + val;

        this.getDetails = function() {
        return {
            type : 'string',
            name : 'string',
            default : self.value
        };
    };

        this.parse = function(buffer) {
        return buffer.toString();
    };
};
},{}],211:[function(require,module,exports){

var MStateless = require(209);
var MDecimal = require(205);
var MInteger = require(206);
var MString = require(210);
var Type = require(435).TopicType;

function createFromSingleValue(value) {
    switch (typeof value) {
        case 'string' :
            return new MString(value);
        case 'number' :
            var str = '' + value, i = str.indexOf('.');

            if (i > -1) {
                return new MDecimal(value, str.length - (i + 1));
            } else {
                return new MInteger(value);
            }
    }

    throw new Error('Invalid Single Value type', value);
}

function getType(metadata) {
    if (metadata instanceof MDecimal ||
        metadata instanceof MInteger ||
        metadata instanceof MString) {
        return Type.SINGLE_VALUE;
    } else if (metadata instanceof MStateless) {
        return Type.STATELESS;
    }

    throw new Error('Unknown metadata type');
}

function isMetadata(value) {
    return value instanceof MStateless ||
        value instanceof MDecimal ||
        value instanceof MInteger ||
        value instanceof MString;
}

function isMetadataValue(value) {
    return isMetadata(value);
}

function deriveMetadata(value) {
    if (value === undefined) {
        return new MStateless();
    }

    if (isMetadata(value)) {
        return value;
    }

    return createFromSingleValue(value);
}

module.exports = {
    Type: Type,
    getType: getType,
    isMetadata: isMetadata,
    isMetadataValue: isMetadataValue,
    deriveMetadata: deriveMetadata
};
},{"205":205,"206":206,"209":209,"210":210,"435":435}],212:[function(require,module,exports){
var Type = require(435).TopicType;
var cutil = require(131);
var util = require(211);

var MRecordContent = require(208);

function getType(metadata) {
    if (MRecordContent.isPrototypeOf(metadata)) {
        return Type.RECORD;
    } else {
        return util.getType(metadata);
    }
}

function isMetadata(value) {
    return MRecordContent.isPrototypeOf(value) || util.isMetadata(value);
}

function isMetadataValue(value) {
    return MRecordContent.isPrototypeOf(value) || util.isMetadataValue(value) || cutil.isRecordContent(value);
}

function deriveMetadata(value) {
    if (isMetadata(value)) {
        return value;
    }

    return util.deriveMetadata(value);
}

module.exports = {
    Type : Type,
    getType : getType,
    isMetadata : isMetadata,
    isMetadataValue : isMetadataValue,
    deriveMetadata : deriveMetadata
};

},{"131":131,"208":208,"211":211,"435":435}],213:[function(require,module,exports){
var consts = require(215);

function createConnectionRequest() {
    return {
        type : consts.TYPE,
        version : consts.CURRENT_VERSION,
        capabilities : consts.CAPABILITIES
    };
}

function createReconnectionRequest(token, availableClientSequence, lastServerSequence) {
    var req = createConnectionRequest();

    req.token = token;
    req.availableClientSequence = availableClientSequence;
    req.lastServerSequence = lastServerSequence;

    return req;
}

module.exports = {
    connect : createConnectionRequest,
    reconnect : createReconnectionRequest
};

},{"215":215}],214:[function(require,module,exports){
var BufferInputStream = require(201);
var BEES = require(230);

var SessionId = require(382);
var SessionTokenDeserialiser = require(384);

var ResponseCode = require(216);
var consts = require(215);

function deserialise(buffer) {
    var input = new BufferInputStream(buffer);

    var protocol = input.read();
    if (protocol !== consts.PROTOCOL_BYTE) {
        throw new Error('Unrecognised protocol byte: ' + protocol);
    }

    var version = input.read();
    if (version < consts.CURRENT_VERSION) {
        throw new Error('Unrecognised protocol version: ' + version);
    } else if (version > consts.CURRENT_VERSION) {
        throw new Error('Unsupported protocol version: ' + version);
    }

    var responseCode = BEES.read(input, ResponseCode);

        if (ResponseCode.isSuccess(responseCode)) {
        var sessionID = new SessionId(input.readUInt64(), input.readUInt64());
        var sessionToken = SessionTokenDeserialiser(input);
        var systemPingPeriod = input.readInt64();

        var recoverySequence = 0;

        if (responseCode === ResponseCode.RECONNECTED) {
            recoverySequence = input.readInt32();
        }

        return {
            response : responseCode,
            identity : sessionID,
            token : sessionToken,
            systemPingPeriod : systemPingPeriod,
            version : version,
            success : true,
            sequence : recoverySequence
        };
    } else {
        return {
            response : responseCode,
            version : version,
            identity : null,
            token : null,
            systemPingPeriod : null,
            success : false,
            sequence : 0
        };
    }
}

module.exports = deserialise;

},{"201":201,"215":215,"216":216,"230":230,"382":382,"384":384}],215:[function(require,module,exports){
module.exports = {
    TYPE : "WB",
    CAPABILITIES : 10,
    PROTOCOL_BYTE : 35,
    CURRENT_VERSION : 12
};

},{}],216:[function(require,module,exports){
function code(id, message) {
    return {
        id : id, message : message
    };
}

var ResponseCode = {
    OK :                                code(100, "Connected successfully"),
    DOWNGRADE :                         code(102, "Server does not support the requested protocol level"),
    RECONNECTED :                       code(105, "Reconnected successfully"),
    RECONNECTED_WITH_MESSAGE_LOSS :     code(106, "Reconnected with message loss"),
    REJECTED :                          code(111, "Connection rejected"),
    CONNECTION_UNSUPPORTED :            code(112, "Connection type not supported by connector"),
    LICENSE_EXCEEDED :                  code(113, "Connection rejected due to license limit"),
    RECONNECTION_UNSUPPORTED :          code(114, "Reconnection not supported by connector"),
    CONNECTION_PROTOCOL_ERROR :         code(115, "Connection failed - protocol error"),
    AUTHENTICATION_FAILED :             code(116, "Connection failed - authentication failed"),
    UNKNOWN_SESSION :                   code(117, "Reconnection failed - the session is unknown"),
    RECONNECTION_FAILED_MESSAGE_LOSS :  code(118, "Reconnection failed due to message loss"),
    ERROR :                             code(127, "Connection failed due to server error")
};

ResponseCode.isSuccess = function isSuccess(code) {
    switch (code) {
        case ResponseCode.OK :
        case ResponseCode.RECONNECTED :
        case ResponseCode.RECONNECTED_WITH_MESSAGE_LOSS :
            return true;
        default :
            return false;
    }
};

module.exports = ResponseCode;

},{}],217:[function(require,module,exports){
var HashMap = require(43);
var Arrays = require(412);

module.exports = function StreamRegistry(topicCache) {
    var streams = new HashMap();
    var fallbacks = [];
    var self = this;

    this.add = function(selector, stream) {
        var existing = streams.get(selector);

        if (existing) {
            existing.push(stream);
        } else {
            streams.set(selector, [stream]);
        }

        topicCache.newStream(selector, stream, self);

        stream.onOpen();
    };

    this.addFallback = function(stream) {
        fallbacks.push(stream);
    };

    this.getFallbacks = function(specification) {
        return fallbacks.filter(function(fallback) {
            return fallback.selects(specification);
        });
    };

    this.remove = function(stream) {
        Arrays.remove(fallbacks, stream);

        streams.forEach(function(existing, selector) {
            if (Arrays.remove(existing, stream)) {
                topicCache.removeStream(stream, self);
            }

            if (existing.length === 0) {
                streams.remove(selector);
            }
        });
    };

    this.streamsFor = function(topic, specification) {
        var combined = [];

        if (streams.count() > 0) {
            streams.forEach(function(existing, selector) {
                if (selector.selects(topic)) {
                    combined = combined.concat(existing.filter(function(stream) {
                        return stream.selects(specification);
                    }));
                }
            });
        }

        return combined;
    };
};
},{"412":412,"43":43}],218:[function(require,module,exports){
var TopicCacheEntry = require(219);

module.exports = function DatatypeCacheEntry(streams, path, details, specification, datatype) {
    TopicCacheEntry.call(this, streams, path, details, specification);

    var value;
    var buffer;

    this.handleValue = function(received, registry, errorHandler) {
        var oldValue = value;

        try {
            value = datatype.readValue(received);
            buffer = received;
            this.notifyValue(received, oldValue, value, registry);
        } catch (e) {
            errorHandler(e);
        }
    };

    this.handleDelta = function(received, registry, errorHandler) {
        var oldValue = value;
        var deltaType;

        try {
            deltaType = datatype.deltaType("binary");

            var delta = deltaType.readDelta(received);
            value = deltaType.apply(oldValue, delta);
            buffer = datatype.writeValue(value);

            this.notifyDelta(received, delta, oldValue, value, registry);
        } catch (e) {
            errorHandler(e);
        }
    };

    this.notifyValueToNewStream = function(path, details, specification, stream) {
        if (buffer) {
            stream.onValue(path, details, specification, buffer, null, value);
        }
    };
};

},{"219":219}],219:[function(require,module,exports){
var UnsubscribeReason = require(435).UnsubscribeReason;
var Arrays = require(412);

module.exports = function TopicCacheEntryImpl(streams, path, details, specification) {
    function proxies(details, registry) {
        var p = streams;

        if (p.length === 0) {
            p = registry.getFallbacks(details);
        }

        return p;
    }

    var self = this;

    this.getTopicPath = function() {
        return path;
    };

    this.getTopicDetails = function() {
        return details;
    };

    this.getTopicSpecification = function() {
        return specification;
    };

    this.notifyInitialSubscription = function(registry) {
        proxies(specification, registry).forEach(function(proxy) {
            proxy.onSubscription(path, details, specification);
        });
    };

    this.notifySubscription = function(proxy) {
        proxy.onSubscription(path, details, specification);

        self.notifyValueToNewStream(path, details, specification, proxy);
    };

    this.notifyUnsubscription = function(reason, registry) {
        proxies(specification, registry).forEach(function(proxy) {
            proxy.onUnsubscription(path, details, specification, reason);
        });
    };

    this.notifyValue = function(content, oldValue, newValue, registry) {
        proxies(specification, registry).forEach(function(proxy) {
            proxy.onValue(path, details, specification, content, oldValue, newValue);
        });
    };

    this.notifyDelta = function(content, delta, oldValue, newValue, registry) {
        proxies(specification, registry).forEach(function(proxy) {
            proxy.onDelta(path, details, specification, content, delta, oldValue, newValue);
        });
    };

    this.addStream = function(stream, registry) {
        if (streams.length === 0) {
            registry.getFallbacks(specification).forEach(function(fallback) {
                fallback.onUnsubscription(path, details, specification, UnsubscribeReason.STREAM_CHANGE);
            });
        }

        if (streams.indexOf(stream) < 0) {
            streams.push(stream);

            self.notifySubscription(stream);
        }
    };

    this.removeStream = function(stream, registry) {
        Arrays.remove(streams, stream);

        if (streams.length === 0) {
            registry.getFallbacks(specification).forEach(function(fallback) {
                self.notifySubscription(fallback);
            });
        }
    };

    this.removeAllStreams = function() {
        streams.length = 0;
    };
};
},{"412":412,"435":435}],220:[function(require,module,exports){
var TopicCacheEntry = require(219);

module.exports = function NoValueEntry(streams, path, details, specification) {
    TopicCacheEntry.call(this, streams, path, details, specification);

    this.handleValue = function(received, registry) {
        this.notifyValue(received, null, received, registry);
    };

    this.handleDelta = function(received, registry) {
        this.notifyDelta(received, null, null, received, registry);
    };

    this.notifyValueToNewStream = function() {
    };
};
},{"219":219}],221:[function(require,module,exports){
var TopicCacheEntry = require(219);

module.exports = function SingleValueEntry(streams, path, details, specification) {
    TopicCacheEntry.call(this, streams, path, details, specification);

    var value;

    this.handleValue = function(received, registry) {
        this.notifyValue(received, value, received, registry);
        value = received;
    };

    this.handleDelta = function(received, registry) {
        this.notifyDelta(received, null, value, received, registry);
        value = received;
    };

    this.notifyValueToNewStream = function(path, details, specification, stream) {
        if (value) {
            stream.onValue(path, details, specification, value, null, value);
        }
    };
};
},{"219":219}],222:[function(require,module,exports){
var TimeSeriesEventDataType = require(386);
var DataTypeTopicCacheEntry = require(218);

module.exports = function TimeSeriesCacheEntry(streams, path, details, specification, datatype) {
    DataTypeTopicCacheEntry.call(this, streams, path, details, specification, TimeSeriesEventDataType.create(datatype));
};
},{"218":218,"386":386}],223:[function(require,module,exports){
var TimeSeriesEntry = require(222);
var NoValueEntry = require(220);
var DatatypeEntry = require(218);
var SingleEntry = require(221);

var UnsubscribeReason = require(435).UnsubscribeReason;
var TopicType = require(435).TopicType;

var TopicDetails = require(391);

module.exports = function TopicCache(datatypes) {
    var byPath = {};
    var byId = {};

    this.handleSubscription = function(info, registry) {
        var path = info.path;

        var streams = registry.streamsFor(path, info.specification);
        var entry;

        var details = TopicDetails.detailsFromSpecification(info.specification);

        switch (info.specification.type) {
            case TopicType.JSON :
            case TopicType.BINARY :
            case TopicType.STRING :
            case TopicType.DOUBLE :
            case TopicType.INT64 :
            case TopicType.RECORD_V2 :
                var datatype = datatypes.get(info.specification.type);
                entry = new DatatypeEntry(streams, path, details, info.specification, datatype);
                break;
            case TopicType.TIME_SERIES :
                var eventType = datatypes.get(info.specification.properties.TIME_SERIES_EVENT_VALUE_TYPE);
                entry = new TimeSeriesEntry(streams, path, details, info.specification, eventType);
                break;
            case TopicType.SINGLE_VALUE :
                entry = new SingleEntry(streams, path, details, info.specification);
                break;
            default :
                entry = new NoValueEntry(streams, path, details, info.specification);
        }

        var oldByPath = byPath[path];
        byPath[path] = entry;

        if (oldByPath) {
            throw new Error("Existing cache entry for " + path);
        }

        var oldById = byId[info.id];
        byId[info.id] = entry;

        if (oldById) {
            throw new Error("Existing cache entry for " + info.id);
        }

        entry.notifyInitialSubscription(registry);
    };

    this.notifyUnsubscriptionOfAllTopics = function(registry) {
        for (var id in byId) {
            byId[id].notifyUnsubscription(UnsubscribeReason.SUBSCRIPTION_REFRESH, registry);
        }

        byPath = {};
        byId = {};
    };

    this.handleValue = function(session, id, content, registry) {
        var errorHandler = session.getErrorHandler();
        var entry = byId[id];

        if (entry) {
            entry.handleValue(content, registry, errorHandler);
        } else {
            errorHandler(new Error("Data loss on topic with ID: " + id + " - possibly due to reconnection"));
        }
    };

    this.handleDelta = function(session, id, delta, registry) {
        var errorHandler = session.getErrorHandler();
        var entry = byId[id];

        if (entry) {
            entry.handleDelta(delta, registry, errorHandler);
        } else {
            errorHandler(new Error("Data loss on topic with ID: " + id + " - possibly due to reconnection"));
        }
    };

    this.handleUnsubscription = function(id, reason, registry) {
        var entry = byId[id];
        delete byId[id];

        if (entry) {
            var path = entry.getTopicPath();
            delete byPath[path];

            entry.notifyUnsubscription(reason, registry);
        }
    };

    this.newStream = function(selector, stream, registry) {
        for (var path in byPath) {
            var entry = byPath[path];

            if (selector.selects(path) && stream.selects(entry.getTopicSpecification())) {
                entry.addStream(stream, registry);
            }
        }
    };

    this.removeStream = function(stream, registry) {
        for (var path in byPath) {
            byPath[path].removeStream(stream, registry);
        }
    };

    this.removeAllStreams = function() {
        for (var path in byPath) {
            byPath[path].removeAllStreams();
        }
    };

    this.clear = function() {
        byPath = {};
        byId = {};
    };
};


},{"218":218,"220":220,"221":221,"222":222,"391":391,"435":435}],224:[function(require,module,exports){
var HeadersTunnel = require(428);
var Message = require(429);

var log = require(416).create('V4 Client Routing');

module.exports = function TopicRouting(session, serviceAdapter, conversationSet, cache, registry) {
    this.route = function(message) {

        switch(message.type) {
            case Message.types.FETCH_REPLY:
                var cid = HeadersTunnel.cidFromHeaders(message.headers);
                conversationSet.respondIfPresent(cid, message);
                break;
            case Message.types.SERVICE_REQUEST:
            case Message.types.SERVICE_RESPONSE:
            case Message.types.SERVICE_ERROR:
                serviceAdapter.onMessage(message.type, message.getInputStream());
                break;
            case Message.types.TOPIC_VALUE:
                cache.handleValue(session, message.id, message.data, registry);
                break;
            case Message.types.TOPIC_DELTA:
                cache.handleDelta(session, message.id, message.data, registry);
                break;
            default:
                log.debug('Unhandled V4 message: ' + message.type, message);

        }
    };

    this.subscribe = function(info) {
        cache.handleSubscription(info, registry);
    };

    this.unsubscribe = function(id, reason) {
        cache.handleUnsubscription(id, reason, registry);
    };
};
},{"416":416,"428":428,"429":429}],225:[function(require,module,exports){
module.exports = {
    ATTR : '$',
    CHAR : '_'
};
},{}],226:[function(require,module,exports){
var util = require(212);
var ATTR = require(225).ATTR;

function createRecordSchema(metadata) {
    if (metadata.getRecords().length === 0) {
        return null;
    }

    var schema = {message: {record: []}};
    schema.message[ATTR] = {
        topicDataType: 'record',
        name: metadata.name
    };

    metadata.getRecords().forEach(function (record) {
        var rschema = {field: []};
        rschema[ATTR] = record.getDetails();

        record.getFields().forEach(function (field) {
            rschema.field.push(createSingleValueSchema(field).field);
        });

        schema.message.record.push(rschema);
    });

    return schema;
}

function createSingleValueSchema(metadata) {
    var schema = {field: {}};
    schema.field[ATTR] = metadata.getDetails();

    return schema;
}

function create(meta) {
    if (!meta) {
        return null;
    }

    switch (util.getType(meta)) {
        case util.Type.RECORD :
            return createRecordSchema(meta);
        case util.Type.SINGLE_VALUE :
            return createSingleValueSchema(meta);
    }

    return null;
}

module.exports = create;
},{"212":212,"225":225}],227:[function(require,module,exports){
var xmljson = require(228);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        return xmljson.xml2json(Codec.readString(input));
    },
    write : function(output, schema) {
        if (schema && (schema.field || schema.message)) {
            Codec.writeString(output, xmljson.json2xml(schema));
        }
    }
};

module.exports = serialiser;

},{"203":203,"228":228}],228:[function(require,module,exports){
var ATTR = require(225).ATTR;

function attrs_str2js(str) {
    var js = {};

        var pairs = str.split(' ');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        js[pair[0]] = pair[1].match(/"(.*)"/)[1];
    }

    return js;
}

function escapeEntities(str) {
    return String(str).replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#x27;');
}

    function attrs_obj2arr(obj) {
    var attrs = [];
    if (typeof obj === 'object') {
        for (var key in obj) {
            attrs.push('' + key + '="' + escapeEntities(obj[key]) + '"');
        }
    }
    return attrs;
}

function xml2json(xml, pos) {
    var js = {};

    if (pos === undefined) {
        pos = {
            idx : 0,
            isRoot : true
        };

        var preamble = xml.match(/<\?.*?\?>/);
        if (preamble) {
            xml = xml.substr(preamble[0].length);
        }
    }

        var elem_match = null;

    for (;;) {
        elem_match = xml.substr(pos.idx).match(/<.*?>/);
        if (!elem_match) {
            break;
        }

        pos.idx += elem_match[0].length;

                var elem = elem_match[0].trim();

        if (elem.substr(0,2) === '</') {
            return js;
        }

        var name = elem.match(/\b(.+?)\b/)[0];
        var attrs_match = elem.match(/<.+?\b(.*)\/?>/);
        var attrs_str = '';

                if (attrs_match) {
            attrs_str = attrs_match[1].trim();
        }

                var attrs = attrs_str2js(attrs_str);
        var child_count;
        if (elem.substr(-2) === '/>') {

            if (pos.isRoot) {
                pos.isRoot = false;
                js[name] = xml2json(xml, pos);
                js[name][ATTR] = attrs;
            } else {
                if (!js[name]) {
                    js[name] = [];
                }

                child_count = js[name].push({});
                js[name][child_count - 1][ATTR] = attrs;
            }
        } else {

            if (pos.isRoot) {
                pos.isRoot = false;
                js[name] = xml2json(xml, pos);
                js[name][ATTR] = attrs;
            } else {
                if (!js[name]) {
                    js[name] = [];
                }

                child_count = js[name].push(xml2json(xml, pos));
                js[name][child_count - 1][ATTR] = attrs;
            } 
        }
    }

    return js;
}

function json2xml(obj, recur) {
    var str = '';

    if (!recur) {
        str = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    }

        for (var key in obj) {
        if (key === ATTR) {
            continue;
        }

                                  var val = obj[key];

        if (!(val instanceof Array)) {
            val = Array(val);
        }

        for (var i = 0; i < val.length; i++) {
            var attrs = attrs_obj2arr(val[i][ATTR]);

            str += '<' + key;
            if (attrs && attrs.length > 0) {
                str += ' ';
                str += attrs.join(' ');
            }

            var child_str = json2xml(val[i], true);
            if (child_str && child_str.length > 0) {
                str += '>';
                str += child_str;
                str += '</' + key + '>';
            } else {
                str += '/>';
            }
        }
    }

        return str;
}

module.exports = {
    xml2json : xml2json,
    json2xml : json2xml
};

},{"225":225}],229:[function(require,module,exports){
var Codec = require(203);

var serialiser = {
    read : function(input) {
        return Codec.readByte(input) === 1;
    },
    write : function(output, val) {
        Codec.writeByte(output, val ? 1 : 0);
    }
};

module.exports = serialiser;

},{"203":203}],230:[function(require,module,exports){
var serialiser = {
    read : function(bis, enums) {
        var i = bis.read(), k;

        for (k in enums) {
            var e = enums[k];

                        if (e === i || e.id !== undefined && e.id === i) {
                return e;  
            }
        }

                throw new Error("Unable to decode enum value " + i);
    },
    write : function(bos, val) {
        if (val.id) {
            bos.write(val.id);
        } else {
            bos.write(val);
        }
    }
};

module.exports = serialiser;
},{}],231:[function(require,module,exports){
var Codec = require(203);

var serialiser = {
    read : function(input, Enum) {
        var i = Codec.readByte(input);

                for (var k in Enum) {
            if (Enum[k] === i || Enum[k].id !== undefined && Enum[k].id === i) {
                return k;
            }
        }

        throw new Error('Unknown id (' + i + ') for enum');           
    },
    write : function(output, id) {
        Codec.writeByte(output, id);
    }
};

module.exports = serialiser;

},{"203":203}],232:[function(require,module,exports){
var Codec = require(203);

var ContentSerialiser = require(127);

var BEES = require(230);
var TopicAddFailReason = require(435).TopicAddFailReason;
var TopicDetailsSerialiser = require(390);

var Status = {
    OK : 0,
    OK_CACHED : 1,
    FAILURE : 2,
    CACHE_FAILURE : 3
};

var serialiser = {
    read : function(input) {
        var status = Codec.readByte(input);
        var reason = "";

        if (status === Status.FAILURE) {
            reason = BEES.read(input, TopicAddFailReason);
        }

        return {
            status : status,
            reason : reason
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);

        Codec.writeInt32(output, 0);

        TopicDetailsSerialiser.write(output, req.details);

        if (req.content) {
            Codec.writeBoolean(output, true);
            ContentSerialiser.write(output, req.content);
        } else {
            Codec.writeBoolean(output, false);
        }
    }
};

module.exports = serialiser;

},{"127":127,"203":203,"230":230,"390":390,"435":435}],233:[function(require,module,exports){
function AddTopic() {
}

module.exports = AddTopic;

},{}],234:[function(require,module,exports){
var BEES = require(230);
var Codec = require(203);

var Type = {
    NONE : 0,
    PLAIN : 1,
    CUSTOM : 2
};

var serialiser = {
    read : function(input) {
        var type = BEES.read(input, Type);
        switch (type) {
            case Type.NONE:
                Codec.readBytes(input);
                return null;
            case Type.PLAIN:
                return Codec.readString(input);
            case Type.CUSTOM:
                return Codec.readBytes(input);
        }
    },
    write : function(input, credentials) {
        if (credentials) {
            if (typeof credentials === 'string') {
                BEES.write(input, Type.PLAIN);
                Codec.writeString(input, credentials);
            } else {
                BEES.write(input, Type.CUSTOM);
                Codec.writeBytes(input, credentials);
            }
        } else {
            BEES.write(input, Type.NONE);
            Codec.writeInt32(input, 0);
        }
    }
};

module.exports = serialiser;

},{"203":203,"230":230}],235:[function(require,module,exports){
var Codec = require(203);
var ErrorReportSerialiser = require(293);

var SecurityCommandScriptResult = require(236);

var serialiser = {
    read : function(input) {
        var errors = Codec.readCollection(input, ErrorReportSerialiser.read);
        return new SecurityCommandScriptResult(errors);
    },
    write : function(output, value) {
        Codec.writeCollection(output, value.errors, ErrorReportSerialiser.write);
    }
};

module.exports = serialiser;

},{"203":203,"236":236,"293":293}],236:[function(require,module,exports){
var requireNonNull = require(422);

function SecurityCommandScriptResult(errors) {
    this.errors = requireNonNull(errors);
}

SecurityCommandScriptResult.prototype.toString = function() {
    return "SecurityCommandScriptResult [" + this.errors + "]";
};

module.exports = SecurityCommandScriptResult;
},{"422":422}],237:[function(require,module,exports){
var SecurityCommandScript = require(238);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        var script = Codec.readString(input);
        return new SecurityCommandScript(script);
    }, 
    write : function(output, value) {
        Codec.writeString(output, value.script);
    }
};

module.exports = serialiser;
},{"203":203,"238":238}],238:[function(require,module,exports){
var requireNonNull = require(422);

function SecurityCommandScript(script) {
    this.script = requireNonNull(script, "script");
}

SecurityCommandScript.prototype.toString = function() {
    return "SecurityCommandScript [" + this.script + "]";
};

module.exports = SecurityCommandScript;
},{"422":422}],239:[function(require,module,exports){
var Codec = require(203);
var Configuration = require(179).SystemAuthentication;

var ByteEncodedEnumSerialiser = require(230);
var SystemPrincipalSerialiser = require(240);

    var serialiser = {
    read : function(input) {
        var principals = Codec.readCollection(input, SystemPrincipalSerialiser.read);
        var action = ByteEncodedEnumSerialiser.read(input, Configuration.CONNECTION_ACTION);
        var roles = Codec.readCollection(input, Codec.readString);

                return new Configuration(principals, action.value, roles);
    },
    write : function(output, value) {
        Codec.writeCollection(output, value.principals, SystemPrincipalSerialiser.write);

                var action = Configuration.CONNECTION_ACTION.fromString(value.anonymous.action);
        Codec.writeByte(output, action.id);

                Codec.writeCollection(output, value.anonymous.roles, Codec.writeString);
    }
};

module.exports = serialiser;

},{"179":179,"203":203,"230":230,"240":240}],240:[function(require,module,exports){
var Codec = require(203);
var SystemPrincipal = require(179).SystemPrincipal;

var serialiser = {
    read : function(input) {
        var name = Codec.readString(input);
        var roles = Codec.readCollection(input, Codec.readString);

        return new SystemPrincipal(name, roles);
    },
    write : function(output, principal) {
        Codec.writeString(output, principal.name);
        Codec.writeCollection(output, principal.roles, Codec.writeString);
    }
};

module.exports = serialiser;
},{"179":179,"203":203}],241:[function(require,module,exports){
var CredentialsSerialiser = require(234);
var Codec = require(203);

var serialiser = {
    read : function(input) {
 Codec.readString(input);
 CredentialsSerialiser.read(input);
    },
    write : function(output, req) {
        Codec.writeString(output, req.principal);
        CredentialsSerialiser.write(output, req.credentials);
    }
};

module.exports = serialiser;

},{"203":203,"234":234}],242:[function(require,module,exports){
function ChangePrincipalRequest(principal, credentials) {
    this.principal = principal;
    this.credentials = credentials;
}

module.exports = ChangePrincipalRequest;

},{}],243:[function(require,module,exports){
var AddressType = require(65).AddressType;
var BEES = require(230);
var Codec = require(203);

var intBitsToFloat = require(417).intBitsToFloat;

module.exports = {
    read : function(bis) {
        var address = Codec.readString(bis);
        var hostname = Codec.readString(bis);

        if (hostname.length === 0) {
            hostname = address;
        }

        var resolved = Codec.readString(bis);

        if (resolved.length === 0) {
            resolved = hostname;
        }

        var addressType = BEES.read(bis, AddressType);

        var details = {
            country : Codec.readString(bis),
            language : Codec.readString(bis)
        };

        var coordinates = {
            latitude : intBitsToFloat(Codec.readInt32(bis)),
            longitude : intBitsToFloat(Codec.readInt32(bis))
        };

        return {
            type : addressType,
            address : address,
            hostname : hostname,
            resolved : resolved,
            details : details,
            coordinates : coordinates
        };
    }
};
},{"203":203,"230":230,"417":417,"65":65}],244:[function(require,module,exports){
var TransportType = require(66).TransportType;
var ClientType = require(66).ClientType;
var BEES = require(230);
var Codec = require(203);

module.exports = {
    read : function(bis) {
        var principal = Codec.readString(bis);
        var clientType = BEES.read(bis, ClientType);
        var transportType = BEES.read(bis, TransportType);

        return {
            principal : principal,
            clientType : clientType,
            transportType : transportType
        };
    }
};
},{"203":203,"230":230,"66":66}],245:[function(require,module,exports){
var DetailType = require(68).DetailType;

var ClientLocationSerialiser = require(243);
var ClientSummarySerialiser = require(244);

var Codec = require(203);

function nextOption(bis) {
    var r = Codec.readByte(bis);
    if (r === 1) {
        return true;
    } else if (r === 0) {
        return false;
    } else {
        throw new Error('Invalid option byte: ' + r);
    }
}

module.exports = {
    read : function(bis) {
        var details = {
            available : []
        };

        if (!nextOption(bis)) {
            return details;
        }

        if (nextOption(bis)) {
            details.available.push(DetailType.SUMMARY);
            details.summary = ClientSummarySerialiser.read(bis);
        }

        if (nextOption(bis)) {
            details.available.push(DetailType.LOCATION);
            details.location = ClientLocationSerialiser.read(bis);
        }

        if (nextOption(bis)) {
            details.available.push(DetailType.CONNECTOR_NAME);
            details.connector = Codec.readString(bis);
        }

        if (nextOption(bis)) {
            details.available.push(DetailType.SERVER_NAME);
            details.server = Codec.readString(bis);
        }

        return details;
    }
};
},{"203":203,"243":243,"244":244,"68":68}],246:[function(require,module,exports){
var ConversationIDSerialiser = require(135);
var CommandHeader = require(247);
var Codec = require(203);

var CommandHeaderSerialiser = {
    read : function(input) {
        var service = Codec.readInt32(input),
            cid = ConversationIDSerialiser.read(input);

        return new CommandHeader(service, cid);
    },
    write : function(out, header) {

        Codec.writeInt32(out, header.service);
        ConversationIDSerialiser.write(out, header.cid);
    }
};

module.exports = CommandHeaderSerialiser;
},{"135":135,"203":203,"247":247}],247:[function(require,module,exports){
function CommandHeader(service, cid) {
    this.service = service;
    this.cid = cid;
}

CommandHeader.prototype.toString = function() {
    return '<' + this.service + ', ' + this.cid.toString() + '>';
};

CommandHeader.createRequestHeader = function(service, cid) {
    return new CommandHeader(service, cid);
};

CommandHeader.prototype.createResponseHeader = function() {
    return new CommandHeader(this.service, this.cid);
};

CommandHeader.prototype.createErrorHeader = function() {
    return new CommandHeader(this.service, this.cid);
};


module.exports = CommandHeader;

},{}],248:[function(require,module,exports){
var ClientFilterSubscribeRequest = require(249);
var TopicSelectorSerialiser = require(404);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var filter = Codec.readString(input);
        var selector = TopicSelectorSerialiser.read(input);

        return new ClientFilterSubscribeRequest(filter, selector);
    },
    write : function(output, request) {
        Codec.writeString(output, request.filter);
        TopicSelectorSerialiser.write(output, request.selector);
    }
};

},{"203":203,"249":249,"404":404}],249:[function(require,module,exports){
module.exports = function ClientFilterSubscribeRequest(filter, selector) {
    this.filter = filter;
    this.selector = selector;
};
},{}],250:[function(require,module,exports){
var ClientFilterSubscribeResponse = require(251);
var ErrorReportSerialiser = require(293);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var error = Codec.readByte(input);

        if (error) {
            var errors = Codec.readCollection(input, ErrorReportSerialiser.read);
            return new ClientFilterSubscribeResponse(0, errors);
        } else {
            var numSelected = Codec.readInt32(input);
            return new ClientFilterSubscribeResponse(numSelected, []);
        }
    },
    write : function(output, response) {
        if (response.isSuccess()) {
            Codec.writeByte(output, 0);
            Codec.writeInt32(output, response.selected);
        } else {
            Codec.writeByte(output, 1);
            Codec.writeCollection(output, response.errors, ErrorReportSerialiser.write);
        }
    }
};
},{"203":203,"251":251,"293":293}],251:[function(require,module,exports){
module.exports = function ClientFilterSubscribeResponse(selected, errors) {
    this.selected = selected;
    this.errors = errors;

    this.isSuccess = function() {
        return errors.length === 0;
    };
};
},{}],252:[function(require,module,exports){
var ClientSubscribeRequest = require(253);
var TopicSelectorSerialiser = require(404);
var SessionIdSerialiser = require(381);

module.exports = {
    read : function(input) {
        var sessionID = SessionIdSerialiser.read(input);
        var selector = TopicSelectorSerialiser.read(input);

        return new ClientSubscribeRequest(sessionID, selector);
    },
    write : function(output, request) {
        SessionIdSerialiser.write(output, request.sessionID);
        TopicSelectorSerialiser.write(output, request.selector);
    }
};
},{"253":253,"381":381,"404":404}],253:[function(require,module,exports){
module.exports = function ClientSubscribeRequest(sessionID, selector) {
    this.sessionID = sessionID;
    this.selector = selector;
};
},{}],254:[function(require,module,exports){
var SessionIdSerialiser = require(381);
var CloseClientRequest = require(255);

module.exports = {
    read : function(input) {
        var sessionId = SessionIdSerialiser.read(input);
        return new CloseClientRequest(sessionId);
    },
    write : function(output, request) {
        SessionIdSerialiser.write(output, request.sessionID);
    }
};
},{"255":255,"381":381}],255:[function(require,module,exports){
module.exports = function CloseClientRequest(internalSessionID) {
    this.sessionID = internalSessionID;
};
},{}],256:[function(require,module,exports){
var ControlGroupSerialiser = require(132);
var BEES = require(230);

var Services = require(325);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        var definition = BEES.read(input, Services);
        var group = ControlGroupSerialiser.read(input);

        return {
            definition : definition,
            group : group
        };
    },
    write : function(output, params) {
        Codec.writeInt32(output, params.definition.id);
        ControlGroupSerialiser.write(output, params.group);        
    }
};

module.exports = serialiser;

},{"132":132,"203":203,"230":230,"325":325}],257:[function(require,module,exports){
function ControlRegistrationParams(definition, group) {
    this.definition = definition;
    this.group = group;
}

module.exports = ControlRegistrationParams;

},{}],258:[function(require,module,exports){
var ControlRegistrationParamsSerialiser = require(256);
var ConversationIDSerialiser = require(135);

var serialiser = {
    read : function(input) {
        var params = ControlRegistrationParamsSerialiser.read(input);
        var cid = ConversationIDSerialiser.read(input);

        return {
            params : params,
            cid : cid
        };
    },
    write : function(output, request) {
        ControlRegistrationParamsSerialiser.write(output, request.params);
        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"135":135,"256":256}],259:[function(require,module,exports){
function ControlRegistrationRequest(params, cid) {
    this.params = params;
    this.cid = cid;
}

module.exports = ControlRegistrationRequest;

},{}],260:[function(require,module,exports){
var Codec = require(203);
var SessionSerialiser = require(381);

var serialiser = {
    read : function(input) {
        if (Codec.readBoolean(input)) {
            return {
                properties : Codec.readDictionary(input, Codec.readString)
            };
        } else {
            return null;
        }
    },
    write : function(output, request) {
        if (request.sessionID === undefined || request.sessionID === null) {
            throw new Error('session ID is null or undefined');
        }
        SessionSerialiser.write(output, request.sessionID);

        if (request.propertyKeys !== undefined && request.propertyKeys !== null) {
            Codec.writeCollection(output, request.propertyKeys, Codec.writeString);
        } else {
            Codec.writeCollection(output, [], Codec.writeString);
        }

    }
};

module.exports = serialiser;

},{"203":203,"381":381}],261:[function(require,module,exports){
function GetSessionProperties() {
}

module.exports = GetSessionProperties;

},{}],262:[function(require,module,exports){
var ControlGroupSerialiser = require(132);
var BEES = require(230);

var Services = require(325);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        var definition = BEES.read(input, Services);
        var group = ControlGroupSerialiser.read(input);
        var path = Codec.readString(input);
        var keys = Codec.readCollection(input, Codec.readString);

        return {
            definition : definition,
            group : group,
            path : path,
            keys : keys
        };
    },
    write : function(output, params) {
        Codec.writeInt32(output, params.definition.id);
        ControlGroupSerialiser.write(output, params.group);
        Codec.writeString(output, params.path);
        Codec.writeCollection(output, params.keys, Codec.writeString);
    }
};

module.exports = serialiser;

},{"132":132,"203":203,"230":230,"325":325}],263:[function(require,module,exports){
function MessageReceiverControlRegistrationParams(definition, group, path, keys) {
    this.definition = definition;
    this.group = group;
    this.path = path;
    this.keys = keys;
}

module.exports = MessageReceiverControlRegistrationParams;

},{}],264:[function(require,module,exports){
var MessageReceiverControlRegistrationParamsSerialiser =
        require(262);
var ConversationIDSerialiser = require(135);

var serialiser = {
    read : function(input) {
        var params = MessageReceiverControlRegistrationParamsSerialiser.read(input);
        var cid = ConversationIDSerialiser.read(input);

        return {
            params : params,
            cid : cid
        };
    },
    write : function(output, request) {
        MessageReceiverControlRegistrationParamsSerialiser.write(output, request.params);
        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"135":135,"262":262}],265:[function(require,module,exports){
function MessageReceiverControlRegistrationRequest() {
}

module.exports = MessageReceiverControlRegistrationRequest;

},{}],266:[function(require,module,exports){
var ConversationIDSerialiser = require(135);
var SessionSerialiser = require(381);

var Codec = require(203);

var serialiser = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var sessionID = SessionSerialiser.read(input);
        var path = Codec.readString(input);
        var sessionProperties = Codec.readDictionary(input, Codec.readString);
        var dataType = Codec.readString(input);
        var content = Codec.readBytes(input);

                return {
            cid: cid,
            sessionID: sessionID,
            path: path,
            properties: sessionProperties,
            dataType: dataType,
            content: content
        };
    },
    write : function() {
    }
};

module.exports = serialiser;

},{"135":135,"203":203,"381":381}],267:[function(require,module,exports){
function MessagingClientForwardSendRequest() {
}

module.exports = MessagingClientForwardSendRequest;
},{}],268:[function(require,module,exports){
var MessagingClientSendRequest = require(269);

var SessionIdSerialiser = require(381);
var DataTypes = require(144);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var sessionId = SessionIdSerialiser.read(input);
        var path = Codec.readString(input);
        var dataType = DataTypes.get(Codec.readString(input));
        var request = Codec.readBytes(input);

        return new MessagingClientSendRequest(sessionId, path, dataType, request);
    },
    write : function(output, params) {
        SessionIdSerialiser.write(output, params.sessionId);
        Codec.writeString(output, params.path);
        Codec.writeString(output, params.type);
        Codec.writeBytes(output, params.request);
    }
};

},{"144":144,"203":203,"269":269,"381":381}],269:[function(require,module,exports){
module.exports = function MessagingClientSendRequest(sessionId, path, dataType, request) {
    this.sessionId = sessionId;
    this.path = path;
    this.dataType = dataType;
    this.request = request;
};

},{}],270:[function(require,module,exports){
var MessagingResponseSerialiser = require(276);
var ConversationIDSerialiser = require(135);
var ErrorReasonSerialiser = require(292);
var SessionIDSerialiser = require(381);

var Codec = require(203);

var MessagingFilterResponse = require(271);

module.exports = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var sessionID = SessionIDSerialiser.read(input);

        switch (Codec.readByte(input)) {
            case 0 :
                var response = MessagingResponseSerialiser.read(input);
                return new MessagingFilterResponse(cid, sessionID, response);
            default :
                var errorReason = ErrorReasonSerialiser.read(input);
                return new MessagingFilterResponse(cid, sessionID, null, errorReason);
        }
    },
    write : function() {
    }
};
},{"135":135,"203":203,"271":271,"276":276,"292":292,"381":381}],271:[function(require,module,exports){
module.exports = function MessagingFilterResponse(cid, sessionID, response, errorReason) {
    this.cid = cid;
    this.sessionID = sessionID;
    this.response = response;
    this.errorReason = errorReason;
};
},{}],272:[function(require,module,exports){
var ConversationIDSerialiser = require(135);
var Codec = require(203);

module.exports = {
    read : function() {

    },
    write : function(out, request) {
        ConversationIDSerialiser.write(out, request.cid);
        Codec.writeString(out, request.filter);
        Codec.writeString(out, request.path);
        Codec.writeString(out, request.dataType.name());
        Codec.writeBytes(out, request.request);
    }
};
},{"135":135,"203":203}],273:[function(require,module,exports){
module.exports = function MessagingFilterSendRequest(cid, filter, path, dataType, request) {
    this.cid = cid;
    this.filter = filter;
    this.path = path;
    this.dataType = dataType;
    this.request = request;
};
},{}],274:[function(require,module,exports){
var MessagingFilterSendResponse = require(275);

var ErrorReportSerialiser = require(293);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var code = Codec.readByte(input);

        if (code === 0) {
            var numberSent = Codec.readInt32(input);
            return new MessagingFilterSendResponse(numberSent, []);
        } else {
            var errors = Codec.readCollection(input, ErrorReportSerialiser.read);
            return new MessagingFilterSendResponse(0, errors);
        }
    },
    write : function() {
    }
};
},{"203":203,"275":275,"293":293}],275:[function(require,module,exports){
module.exports = function MessagingFilterSendResponse(numberSent, errors) {
    this.numberSent = numberSent;
    this.errors = errors;

    this.isSuccess = (errors.length === 0);
};
},{}],276:[function(require,module,exports){
var Codec = require(203);

        var serialiser = {
    read : function(input) {
        var dataType = Codec.readString(input);
        var content = Codec.readBytes(input);
        return {
            responseDataType : dataType,
            data : content
        };
    },
    write : function(output, response) {
        Codec.writeString(output, response.responseDataType);
        Codec.writeBytes(output, response.data);
    }
};

module.exports = serialiser;

},{"203":203}],277:[function(require,module,exports){
function MessagingResponse(responseDataType, data) {
    this.responseDataType = responseDataType;
    this.data = data;
}

module.exports = MessagingResponse;
},{}],278:[function(require,module,exports){
var MessagingSendRequest = require(279);

var DataTypes = require(144);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var path = Codec.readString(input);
        var dataType = DataTypes.get(Codec.readString(input));
        var request = Codec.readBytes(input);

        return new MessagingSendRequest(path, dataType, request);
    },
    write : function(output, params) {
        Codec.writeString(output, params.path);
        Codec.writeString(output, params.type);
        Codec.writeBytes(output, params.request);
    }
};

},{"144":144,"203":203,"279":279}],279:[function(require,module,exports){
module.exports = function MessagingSendRequest(path, dataType, request) {
    this.path = path;
    this.dataType = dataType;
    this.request = request;
};
},{}],280:[function(require,module,exports){
var SessionCloseReason = {
    CONNECTION_LOST : 0,
    IO_EXCEPTION : 1,
    CLIENT_UNRESPONSIVE : 2,
    MESSAGE_QUEUE_LIMIT_REACHED : 3,
    CLOSED_BY_CLIENT : 4,
    MESSAGE_TOO_LARGE : 5,
    INTERNAL_ERROR : 6,
    INVALID_INBOUND_MESSAGE : 7,
    ABORT : 8,
    LOST_MESSAGES : 9,
    SERVER_CLOSING : 10,
    CLOSED_BY_CONTROLLER : 11
};

module.exports = SessionCloseReason;
},{}],281:[function(require,module,exports){
var SessionPropertiesEventSerialiser = require(283);
var ConversationIDSerialiser = require(135);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var events = Codec.readCollection(input, SessionPropertiesEventSerialiser.read);

        return {
            cid : cid,
            events : events
        };
    },
    write : function(output, batch) {
        ConversationIDSerialiser.write(output, batch.cid);
        Codec.writeCollection(output, batch.events);
    }
};
},{"135":135,"203":203,"283":283}],282:[function(require,module,exports){
module.exports = function SessionPropertiesEventBatch(cid, events) {
    this.cid = cid;
    this.events = events;
};
},{}],283:[function(require,module,exports){
var SessionIdSerialiser = require(381);
var SessionPropertiesEventType = require(284);
var SessionCloseReason = require(280);

var Codec = require(203);
var BEES = require(230);

var serialiser = {
    read: function (input) {
        var result = {
            sessionId: SessionIdSerialiser.read(input),
            type: BEES.read(input, SessionPropertiesEventType)
        };

        switch (result.type) {
            case SessionPropertiesEventType.OPEN:
                result.oldProperties = Codec.readDictionary(input, Codec.readString);
                break;
            case SessionPropertiesEventType.UPDATE:
                result.updateType = Codec.readByte(input);
                result.oldProperties = Codec.readDictionary(input, Codec.readString);
                result.newProperties = Codec.readDictionary(input, Codec.readString);
                break;
            case SessionPropertiesEventType.CLOSE:
                result.closeReason = BEES.read(input, SessionCloseReason);
                result.oldProperties = Codec.readDictionary(input, Codec.readString);
                break;
            default:
                throw new Error('Unknown session properties event type: ' + result.type);
        }

        return result;
    },
    write: function () {
    }
};

module.exports = serialiser;

},{"203":203,"230":230,"280":280,"284":284,"381":381}],284:[function(require,module,exports){
var SessionPropertiesEventType = {
    OPEN : 0,
    UPDATE : 1,
    CLOSE : 2
};

module.exports = SessionPropertiesEventType;

},{}],285:[function(require,module,exports){
function SessionPropertiesEvent() {
}

module.exports = SessionPropertiesEvent;

},{}],286:[function(require,module,exports){
var ConversationIDSerialiser = require(135);
var Codec = require(203);

var serialiser = {
    read : function() {
    },
    write : function(output, request) {
        if (request.properties) {
            Codec.writeInt32(output, 0);
            Codec.writeCollection(output, request.properties, Codec.writeString);
        } else {
            Codec.writeInt32(output, 1);
        }

        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"135":135,"203":203}],287:[function(require,module,exports){
function SetSessionPropertiesListener() {
}

module.exports = SetSessionPropertiesListener;

},{}],288:[function(require,module,exports){
var ControlGroupSerialiser = require(132);
var BEES = require(230);

var Services = require(325);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        var definition = BEES.read(input, Services);
        var group = ControlGroupSerialiser.read(input);
        var path = Codec.readString(input);

        return {
            definition : definition,
            group : group,
            path : path
        };
    },
    write : function(output, params) {
        Codec.writeInt32(output, params.definition.id);
        ControlGroupSerialiser.write(output, params.group);
        Codec.writeString(output, params.path);
    }
};

module.exports = serialiser;

},{"132":132,"203":203,"230":230,"325":325}],289:[function(require,module,exports){
function TopicControlRegistrationParams(definition, group, path) {
    this.definition = definition;
    this.group = group;
    this.path = path;
}

module.exports = TopicControlRegistrationParams;

},{}],290:[function(require,module,exports){
var TopicControlRegistrationParamsSerialiser = require(288);
var ConversationIDSerialiser = require(135);

var serialiser = {
    read : function(input) {
        var params = TopicControlRegistrationParamsSerialiser.read(input);
        var cid = ConversationIDSerialiser.read(input);

        return {
            params : params,
            cid : cid
        };
    },
    write : function(output, request) {
        TopicControlRegistrationParamsSerialiser.write(output, request.params);
        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;

},{"135":135,"288":288}],291:[function(require,module,exports){
function TopicControlRegistrationRequest() {

}

module.exports = TopicControlRegistrationRequest;

},{}],292:[function(require,module,exports){
var ErrorReason = require(114);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var code = Codec.readInt32(input);
        var description = Codec.readString(input);

        return new ErrorReason(code, description);
    },
    write : function(output, reason) {
        Codec.writeInt32(output, reason.id);
        Codec.writeString(output, reason.reason);
    }
};
},{"114":114,"203":203}],293:[function(require,module,exports){
var Codec = require(203);
var ErrorReport = require(294);

var serialiser = {
    read : function(input) {
        var message = Codec.readString(input);
        var line = Codec.readInt32(input);
        var column = Codec.readInt32(input);

        return new ErrorReport(message, line, column);
    },
    write : function(output, value) {
        Codec.writeString(output, value.message);
        Codec.writeInt32(output, value.line);
        Codec.writeInt32(output, value.column);
    }
};

module.exports = serialiser;

},{"203":203,"294":294}],294:[function(require,module,exports){
var _implements = require(415)._implements;
var requireNonNull = require(422);
var ErrorReport = require(86);

module.exports = _implements(ErrorReport, function ErrorReportImpl(message, line, column) {
    this.message = requireNonNull(message);
    this.line = line || 0;
    this.column = column || 0;

    this.toString = function() {
        return "ErrorReport [" + this.message + " at [" + this.line + ":" + this.column + "]]";
    };
});

},{"415":415,"422":422,"86":86}],295:[function(require,module,exports){
var TopicSelectorSerialiser = require(404);
var CIDSerialiser = require(135);

var FetchRequest = require(296);

module.exports = {
    read : function(input) {
        var selector = TopicSelectorSerialiser.read(input);
        var cid = CIDSerialiser.read(input);

        return new FetchRequest(cid, selector);
    },
    write : function(output, request) {
        TopicSelectorSerialiser.write(output, request.selector);
        CIDSerialiser.write(output, request.cid);
    }
};
},{"135":135,"296":296,"404":404}],296:[function(require,module,exports){
module.exports = function FetchRequest(cid, selector) {
    this.cid = cid;
    this.selector = selector;
};
},{}],297:[function(require,module,exports){
var Codec = require(203);

var TopicDetailsSerialiser = require(390);

var serialiser = {
    read : function(input) {
        return TopicDetailsSerialiser.read(input);
    },
    write : function(output, request) {
        Codec.writeString(output, request.topic_name);
        Codec.writeByte(output, request.level);
    }
};

module.exports = serialiser;

},{"203":203,"390":390}],298:[function(require,module,exports){
function GetTopicDetails() {
}

module.exports = GetTopicDetails;

},{}],299:[function(require,module,exports){
var TopicSelectorSerialiser = require(404);
var CIDSerialiser = require(135);
var SessionSerialiser = require(381);

module.exports = {
    read : function(input) {
        var sessionID = SessionSerialiser.read(input);
        var selector = TopicSelectorSerialiser.read(input);
        var cid = CIDSerialiser.read(input);

        return {
            sessionID : sessionID,
            selector : selector,
            cid : cid
        };
    },
    write : function(output, req) {
        SessionSerialiser.write(output, req.sessionID);
        TopicSelectorSerialiser.write(output, req.selector);
        CIDSerialiser.write(output, req.cid);
    }
};
},{"135":135,"381":381,"404":404}],300:[function(require,module,exports){
module.exports = function MissingTopicRequest(sessionID, selector) {
    this.sessionID = sessionID;
    this.selector = selector;
};
},{}],301:[function(require,module,exports){
var Codec = require(203);

var serialiser = {
    read : function() {
    },
    write : function(output, req) {
        Codec.writeString(output, req.selector.expression);
    }
};

module.exports = serialiser;

},{"203":203}],302:[function(require,module,exports){
function RemoveTopic() {
}

module.exports = RemoveTopic;

},{}],303:[function(require,module,exports){
var ControlRegistrationParamsSerialiser = require(256);

var DetailType = require(68).DetailType;
var BEES = require(230);
var Codec = require(203);

function readDetailType(bis) {
    return BEES.read(bis, DetailType);
}

module.exports = {
    write : function(bos, params) {
        ControlRegistrationParamsSerialiser.write(bos, params);
        Codec.writeString(bos, params.name);

        Codec.writeByte(bos, params.details.length);

        for (var i = 0; i < params.details.length; ++i) {
            Codec.writeByte(bos, params.details[i]);
        }
    },
    read : function(bis) {
        var params = ControlRegistrationParamsSerialiser.read(bis);

        params.name = Codec.readString(bis);
        params.details = Codec.readCollection(bis, readDetailType);

        return params;
    }
};
},{"203":203,"230":230,"256":256,"68":68}],304:[function(require,module,exports){
module.exports = function AuthenticationControlRegistrationParameters() {

};
},{}],305:[function(require,module,exports){
var AuthenticationControlRegistrationParametersSerialiser =
    require(303);

var ConversationIdSerialiser = require(135);

module.exports = {
    write : function(bos, request) {
        AuthenticationControlRegistrationParametersSerialiser.write(bos, request.params);
        ConversationIdSerialiser.write(bos, request.cid);
    },
    read : function(bis) {
        var params = AuthenticationControlRegistrationParametersSerialiser.read(bis);
        var cid = ConversationIdSerialiser.read(bis);

        return {
            params : params,
            cid : cid
        };
    }
};
},{"135":135,"303":303}],306:[function(require,module,exports){
module.exports = function AuthenticationControlRegistrationRequest() {

};
},{}],307:[function(require,module,exports){
var SessionDetailsSerialiser = require(245);
var ConversationIdSerialiser = require(135);
var CredentialsSerialiser = require(234);
var Codec = require(203);

module.exports = {
    read : function(bis) {
        var principal = Codec.readString(bis);
        var credentials = CredentialsSerialiser.read(bis);
        var details = SessionDetailsSerialiser.read(bis);
        var cid = ConversationIdSerialiser.read(bis);

        return {
            principal : principal,
            credentials : credentials,
            details : details,
            cid : cid
        };
    },
    write : function(bos, request) {
        Codec.writeString(bos, request.principal);
        CredentialsSerialiser.write(bos, request.credentials);
        SessionDetailsSerialiser.write(bos, request.details);
        ConversationIdSerialiser.write(bos, request.cid);
    }
};
},{"135":135,"203":203,"234":234,"245":245}],308:[function(require,module,exports){
module.exports = function AuthenticationRequest() {

};
},{}],309:[function(require,module,exports){
var AuthenticationResponse = require(310);
var TypeCode = require(310).TypeCode;
var BEES = require(230);
var Codec = require(203);

module.exports = {
    read : function(bis) {
        var type = BEES.read(bis, TypeCode);

        switch (type) {
            case TypeCode.ABSTAIN :
                return AuthenticationResponse.ABSTAIN;
            case TypeCode.ALLOW :
                return AuthenticationResponse.ALLOW;
            case TypeCode.ALLOW_WITH_RESULT :
                return AuthenticationResponse.allow({
                    roles : Codec.readCollection(bis, Codec.readString),
                    properties : Codec.readDictionary(bis, Codec.readString)
                });
            default:
                return AuthenticationResponse.DENY;

        }
    },
    write : function(bos, response) {
        BEES.write(bos, response.type);

        if (response.type === TypeCode.ALLOW_WITH_RESULT) {
            Codec.writeCollection(bos, response.result.roles, Codec.writeString);
            Codec.writeDictionary(bos, response.result.properties, Codec.writeString);
        }
    }
};
},{"203":203,"230":230,"310":310}],310:[function(require,module,exports){
var TypeCode = {
    DENY : 0,
    ABSTAIN : 1,
    ALLOW : 2,
    ALLOW_WITH_RESULT : 3
};

function AuthenticationResult(result) {
    var roles = result.roles || [];
    var properties = result.properties || {};

    return {
        roles : roles,
        properties : properties
    };
}

function AuthenticationResponse(typecode, result) {
    this.type = typecode;
    this.result = result;
}

module.exports = AuthenticationResponse;

module.exports.allow = function(result) {
    return new AuthenticationResponse(TypeCode.ALLOW_WITH_RESULT, AuthenticationResult(result));
};

module.exports.TypeCode = TypeCode;

module.exports.DENY = new AuthenticationResponse(TypeCode.DENY);
module.exports.ALLOW = new AuthenticationResponse(TypeCode.ALLOW);
module.exports.ABSTAIN = new AuthenticationResponse(TypeCode.ABSTAIN);

},{}],311:[function(require,module,exports){
var TopicPermission = require(179).TopicPermission;
var GlobalPermission = require(179).GlobalPermission;

var Converter = require(231);
var curryR = require(414).curryR;
var Codec = require(203);

var writeGlobalPerm = curryR(Converter.write, GlobalPermission);
var writeTopicPerm = curryR(Converter.write, TopicPermission);

var readGlobalPerm = curryR(Converter.read, GlobalPermission);
var readTopicPerm = curryR(Converter.read, TopicPermission);

var writeTopicPerms = curryR(Codec.writeCollection, writeTopicPerm);
var readTopicPerms = curryR(Codec.readCollection, readTopicPerm);

var serialiser = {
    read : function(input) {
        return {
            name : Codec.readString(input),
            global : Codec.readCollection(input, readGlobalPerm),
            default : Codec.readCollection(input, readTopicPerm),
            topic : Codec.readDictionary(input, readTopicPerms),
            roles : Codec.readCollection(input, Codec.readString)
        };
    },
    write : function(output, role) {
        Codec.writeString(output, role.name);
        Codec.writeCollection(output, role.global, writeGlobalPerm);
        Codec.writeCollection(output, role.default, writeTopicPerm);
        Codec.writeDictionary(output, role.topic, writeTopicPerms);
        Codec.writeCollection(output, role.roles, Codec.writeString);
    }
};

module.exports = serialiser;

},{"179":179,"203":203,"231":231,"414":414}],312:[function(require,module,exports){
var Configuration = require(179).Configuration;
var RoleSerialiser = require(311);

var Codec = require(203);

var serialiser = {
    read : function(input) {
        var anonymous = Codec.readCollection(input, Codec.readString);
        var named = Codec.readCollection(input, Codec.readString);
        var roles = Codec.readCollection(input, RoleSerialiser.read);

        return new Configuration(anonymous, named, roles);
    },
    write : function(output, config) {
        Codec.writeCollection(output, config.anonymous, Codec.writeString);
        Codec.writeCollection(output, config.named, Codec.writeString);
        Codec.writeCollection(output, config.roles, RoleSerialiser.write);
    }
};

module.exports = serialiser;


},{"179":179,"203":203,"311":311}],313:[function(require,module,exports){
var OptionsSerialiser = require(317);
var ContentSerialiser = require(127);
var CIDSerialiser = require(135);

var Codec = require(203);

var serialiser = {
    read : function(input) {
        var filter = Codec.readString(input);
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var options = OptionsSerialiser.read(input);
        var cid = CIDSerialiser.read(input);
        return {
            cid : cid,
            path : path,
            filter : filter,
            message : content,
            options : options
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.filter);
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        OptionsSerialiser.write(output, req.options);
        CIDSerialiser.write(output, req.cid);
    }
};

module.exports = serialiser;

},{"127":127,"135":135,"203":203,"317":317}],314:[function(require,module,exports){
function FilterSendRequest(path, message, filter) {
    this.path = path;
    this.message = message;
    this.filter = filter;
}

module.exports = FilterSendRequest;

},{}],315:[function(require,module,exports){
var Codec = require(203);
var ErrorReportSerialiser = require(293);

var FilterSendResult = require(316);

var serialiser = {
    read : function(input) {
        var sent = Codec.readInt32(input);
        var errors = Codec.readCollection(input, ErrorReportSerialiser.read);
        return new FilterSendResult(sent, errors);
    },
    write : function(output, req) {
        Codec.writeInt32(output, req.sent);
        Codec.writeCollection(output, req.errors, ErrorReportSerialiser.write);
    }
};

module.exports = serialiser;

},{"203":203,"293":293,"316":316}],316:[function(require,module,exports){
var requireNonNull = require(422);

function FilterSendResult(sent, errors) {
    this.sent = requireNonNull(sent);
    this.errors = requireNonNull(errors);
}

FilterSendResult.prototype.toString = function() {
    return 'FilterSendResult [' + this.sent + ' sent, errors=' + this.errors + ']';
};

module.exports = FilterSendResult;

},{"422":422}],317:[function(require,module,exports){
var BEES = require(230);
var Codec = require(203);

var Priority = require(96).Priority;

var serialiser = {
    read : function(input) {
        var priority = BEES.read(input, Priority);
        var headers = Codec.readCollection(input, Codec.readString);

        return {
            priority : priority,
            headers : headers
        };
    },
    write : function(output, req) {
        BEES.write(output, req.priority);
        Codec.writeCollection(output, req.headers, Codec.writeString);
    }
};

module.exports = serialiser;

},{"203":203,"230":230,"96":96}],318:[function(require,module,exports){
var OptionsSerialiser = require(317);
var ContentSerialiser = require(127);

var Codec = require(203);

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var options = OptionsSerialiser.read(input);

        return {
            path : path,
            content : content,
            options : options
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        OptionsSerialiser.write(output, req.options);
    }
};

module.exports = serialiser;

},{"127":127,"203":203,"317":317}],319:[function(require,module,exports){
function SendRequest(path, message) {
    this.path = path;
    this.message = message;
}

module.exports = SendRequest;

},{}],320:[function(require,module,exports){
var OptionsSerialiser = require(317);
var ContentSerialiser = require(127);
var SessionSerialiser = require(381);
var CIDSerialiser = require(135);

var Codec = require(203);

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var sender = SessionSerialiser.read(input);
        var options = OptionsSerialiser.read(input);
        var sessionProperties = Codec.readDictionary(input, Codec.readString);
        var cid = CIDSerialiser.read(input);
        return {
            cid : cid,
            path : path,
            sender : sender,
            message : content,
            options : options,
            sessionProperties : sessionProperties
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        SessionSerialiser.write(output, req.session);
        OptionsSerialiser.write(output, req.options);
        Codec.writeDictionary(output, req.sessionProperties, Codec.writeString);
        CIDSerialiser.write(output, req.cid);
    }
};

module.exports = serialiser;

},{"127":127,"135":135,"203":203,"317":317,"381":381}],321:[function(require,module,exports){
function SessionForwardSendRequest(path, message, recipient, sessionProperties) {
    this.path = path;
    this.message = message;
    this.recipient = recipient;
    this.sessionProperties = sessionProperties;
}

module.exports = SessionForwardSendRequest;

},{}],322:[function(require,module,exports){
var OptionsSerialiser = require(317);
var ContentSerialiser = require(127);
var SessionSerialiser = require(381);
var CIDSerialiser = require(135);

var Codec = require(203);

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var content = ContentSerialiser.read(input);
        var sender = SessionSerialiser.read(input); 
        var options = OptionsSerialiser.read(input);
        var cid = CIDSerialiser.read(input);
        return {
            cid : cid,
            path : path,
            sender : sender,
            message : content,
            options : options
        };
    },
    write : function(output, req) {
        Codec.writeString(output, req.path);
        ContentSerialiser.write(output, req.content);
        SessionSerialiser.write(output, req.session);
        OptionsSerialiser.write(output, req.options);
        CIDSerialiser.write(output, req.cid);
    }
};

module.exports = serialiser;

},{"127":127,"135":135,"203":203,"317":317,"381":381}],323:[function(require,module,exports){
function SessionSendRequest(path, message, recipient) {
    this.path = path;
    this.message = message;
    this.recipient = recipient;
}

module.exports = SessionSendRequest;

},{}],324:[function(require,module,exports){
var HashMap = require(43);

var CommandHeader = require(247);
var CommandHeaderSerialiser = require(246);

var TopicSelector = require(431);
var TopicSelectorSerialiser = require(404);

var ErrorReport = require(294);
var ErrorReportSerialiser = require(293);

var serialisers = new HashMap();

serialisers.set(CommandHeader, CommandHeaderSerialiser);
serialisers.set(TopicSelector, TopicSelectorSerialiser);
serialisers.set(ErrorReport, ErrorReportSerialiser);

serialisers.set(null, {
    read : function() { return null; },
    write : function() { }
});

serialisers.set(Boolean, require(229));

serialisers.set(
    require(296),
    require(295)
);
serialisers.set(
    require(327),
    require(326)
);
serialisers.set(
    require(368),
    require(367)
);
serialisers.set(
    require(298),
    require(297)
);
serialisers.set(
    require(233),
    require(232)
);
serialisers.set(
    require(302),
    require(301)
);
serialisers.set(
    require(372),
    require(371)
);
serialisers.set(
    require(374),
    require(373)
);
serialisers.set(
    require(378),
    require(377)
);
serialisers.set(
    require(380),
    require(379)
);

serialisers.set(
    require(319),
    require(318)
);

serialisers.set(
    require(323),
    require(322)
);

var security = require(98);

serialisers.set(
    security.SystemAuthenticationConfiguration,
    require(239)
);
serialisers.set(
    security.SecurityConfiguration,
    require(312)
);
serialisers.set(
    security.SystemPrincipal,
    require(240)
);
serialisers.set(
    require(238),
    require(237)
);
serialisers.set(
    require(236),
    require(235)
);
serialisers.set(
    require(382),
    require(381)
);
serialisers.set(
    require(291),
    require(290)
);
serialisers.set(
    require(289),
    require(288)
);
serialisers.set(
    require(259),
    require(258)
);
serialisers.set(
    require(259),
    require(258)
);
serialisers.set(
    require(242),
    require(241)
);
serialisers.set(
    require(321),
    require(320)
);
serialisers.set(
    require(265),
    require(264)
);
serialisers.set(
    require(263),
    require(262)
);
serialisers.set(
    require(314),
    require(313)
);
serialisers.set(
    require(316),
    require(315)
);
serialisers.set(
    require(261),
    require(260)
);
serialisers.set(
    require(287),
    require(286)
);
serialisers.set(
    require(285),
    require(283)
);
serialisers.set(
    require(282),
    require(281)
);
serialisers.set(
    require(341),
    require(340)
);
serialisers.set(
    require(347),
    require(346)
);
serialisers.set(
    require(343),
    require(342)
);
serialisers.set(
    require(349),
    require(348)
);
serialisers.set(
    require(345),
    require(344)
);
serialisers.set(
    require(279),
    require(278)
);
serialisers.set(
    require(277),
    require(276)
);
serialisers.set(
    require(267),
    require(266)
);
serialisers.set(
    require(273),
    require(272)
);
serialisers.set(
    require(275),
    require(274)
);
serialisers.set(
    require(356),
    require(355)
);
serialisers.set(
    require(354),
    require(353)
);
serialisers.set(
    require(360),
    require(359)
);
serialisers.set(
    require(362),
    require(361)
);
serialisers.set(
    require(366),
    require(365)
);
serialisers.set(
    require(364),
    require(363)
);
serialisers.set(
    require(376),
    require(375)
);
serialisers.set(
    require(370),
    require(369)
);
serialisers.set(
    require(358),
    require(357)
);
serialisers.set(
    require(352),
    require(351)
);
serialisers.set(
    require(300),
    require(299)
);
serialisers.set(
    require(253),
    require(252)
);
serialisers.set(
    require(249),
    require(248)
);
serialisers.set(
    require(251),
    require(250)
);
serialisers.set(
    require(308),
    require(307)
);
serialisers.set(
    require(310),
    require(309)
);
serialisers.set(
    require(306),
    require(305)
);
serialisers.set(
    require(304),
    require(303)
);
serialisers.set(
    require(330),
    require(329)
);
serialisers.set(
    require(332),
    require(331)
);
serialisers.set(
    require(334),
    require(333)
);
serialisers.set(
    require(336),
    require(335)
);
serialisers.set(
    require(338),
    require(337)
);
serialisers.set(
    require(396),
    require(395)
);
serialisers.set(
    require(255),
    require(254)
);
serialisers.set(
    require(271),
    require(270)
);
serialisers.set(
    require(269),
    require(268)
);

module.exports = serialisers;

},{"229":229,"232":232,"233":233,"235":235,"236":236,"237":237,"238":238,"239":239,"240":240,"241":241,"242":242,"246":246,"247":247,"248":248,"249":249,"250":250,"251":251,"252":252,"253":253,"254":254,"255":255,"258":258,"259":259,"260":260,"261":261,"262":262,"263":263,"264":264,"265":265,"266":266,"267":267,"268":268,"269":269,"270":270,"271":271,"272":272,"273":273,"274":274,"275":275,"276":276,"277":277,"278":278,"279":279,"281":281,"282":282,"283":283,"285":285,"286":286,"287":287,"288":288,"289":289,"290":290,"291":291,"293":293,"294":294,"295":295,"296":296,"297":297,"298":298,"299":299,"300":300,"301":301,"302":302,"303":303,"304":304,"305":305,"306":306,"307":307,"308":308,"309":309,"310":310,"312":312,"313":313,"314":314,"315":315,"316":316,"318":318,"319":319,"320":320,"321":321,"322":322,"323":323,"326":326,"327":327,"329":329,"330":330,"331":331,"332":332,"333":333,"334":334,"335":335,"336":336,"337":337,"338":338,"340":340,"341":341,"342":342,"343":343,"344":344,"345":345,"346":346,"347":347,"348":348,"349":349,"351":351,"352":352,"353":353,"354":354,"355":355,"356":356,"357":357,"358":358,"359":359,"360":360,"361":361,"362":362,"363":363,"364":364,"365":365,"366":366,"367":367,"368":368,"369":369,"370":370,"371":371,"372":372,"373":373,"374":374,"375":375,"376":376,"377":377,"378":378,"379":379,"380":380,"381":381,"382":382,"395":395,"396":396,"404":404,"43":43,"431":431,"98":98}],325:[function(require,module,exports){
var TopicSelector = require(431);

var FetchRequest = require(296);
var SubscriptionNotification = require(327);
var UnsubscriptionNotification = require(368);

var GetTopicDetails = require(298);

var AddTopic = require(233);
var RemoveTopic = require(302);
var UpdateTopicRequest = require(372);
var UpdateTopicSetRequest = require(376);
var UpdateTopicDeltaRequest = require(370);
var UpdateTopicResponse = require(374);

var CloseClientRequest = require(255);

var ClientSubscribeRequest = require(253);
var ClientFilterSubscribeRequest = require(249);
var ClientFilterSubscribeResponse = require(251);

var AuthenticationControlRegistrationRequest = require(306);
var AuthenticationControlRegistrationParameters =
    require(304);

var AuthenticationRequest = require(308);
var AuthenticationResponse = require(310);

var SecurityCommandScript = require(238);
var SecurityCommandScriptResult = require(236);

var SecurityConfiguration = require(98).SecurityConfiguration;
var SystemAuthenticationConfiguration = require(98).SystemAuthenticationConfiguration;

var TopicWillParameters = require(378);
var WillRegistrationResult = require(380);

var SendRequest = require(319);
var SessionSendRequest = require(323);

var TopicControlRegistrationRequest = require(291);
var TopicControlRegistrationParams = require(289);

var UpdateSourceRegistrationRequest = require(356);
var UpdateSourceDeregistrationRequest = require(354);
var UpdateSourceUpdate = require(366);
var UpdateSourceSetRequest = require(358);
var UpdateSourceDeltaRequest = require(352);
var UpdateSourceUpdateResponse = require(364);
var UpdateSourceStateRequest = require(360);
var UpdateSourceState = require(362);
var ControlRegistrationRequest = require(259);
var ControlRegistrationParams = require(257);

var ChangePrincipalRequest = require(242);

var SessionForwardSendRequest = require(321);
var MessageReceiverControlRegistrationRequest =
        require(265);
var MessageReceiverControlRegistrationParams =
        require(263);

var FilterSendRequest = require(314);
var FilterSendResult = require(316);

var GetSessionProperties = require(261);
var SetSessionPropertiesListener = require(287);
var SessionPropertiesEvent = require(285);
var SessionPropertiesEventBatch = require(282);

var MessagingClientForwardSendRequest = require(267);
var MessagingSendRequest = require(279);
var MessagingClientSendRequest = require(269);
var MessagingResponse = require(277);

var MessagingFilterSendRequest = require(273);
var MessagingFilterSendResponse = require(275);
var MessagingFilterResponse = require(271);

var TopicSpecificationInfo = require(396);

var TopicNotificationDeregistrationRequest =
        require(341);
var TopicNotificationEvent = require(347);
var TopicNotificationDescendantEvent = require(343);
var TopicNotificationSelection = require(349);
var TopicNotificationDeselection = require(345);

var MissingTopicRequest = require(300);

var TimeSeriesAppendRequest = require(334);
var TimeSeriesEditRequest = require(336);
var TimeSeriesEventMetadata = require(338);

var RangeQueryRequest = require(330);
var RangeQueryResult = require(332);

module.exports = {
    FETCH : {
        id : 2,
        name : "Fetch",
        request : FetchRequest,
        response : null
    },
    SUBSCRIBE : {
        id :  3,
        name : "Subscribe",
        request : TopicSelector,
        response : null
    },
    UNSUBSCRIBE : {
        id : 4,
        name : "Unsubscribe",
        request : TopicSelector,
        response : null
    },
    SUBSCRIBE_CLIENT : {
        id : 10,
        name : "Subscribe session",
        request : ClientSubscribeRequest,
        response : null
    },
    UNSUBSCRIBE_CLIENT : {
        id : 11,
        name : "Unsubscribe session",
        request : ClientSubscribeRequest,
        response : null
    },
    CLOSE_CLIENT : {
        id : 14,
        name : "Close client",
        request : CloseClientRequest,
        response : null
    },
    SEND : {
        id : 6,
        name : "Send",
        request : SendRequest,
        response : null
    },
    SEND_TO_SESSION : {
        id : 28,
        name : "Send to session",
        request : SessionSendRequest,
        response : null
    },
    SUBSCRIPTION_NOTIFICATION : {
        id : 40,
        name : "Subscription Notification",
        request : SubscriptionNotification,
        response : null
    },
    GET_TOPIC_DETAILS : {
        id : 41,
        name : "Get Topic Details",
        request : GetTopicDetails,
        response : GetTopicDetails
    },
    UNSUBSCRIPTION_NOTIFICATION : {
        id : 42,
        name : "Unsubscription Notification",
        request : UnsubscriptionNotification,
        response : null
    },
    ADD_TOPIC : {
        id : 46,
        name : "Add Topic",
        request : AddTopic,
        response : AddTopic
    },
    REMOVE_TOPIC : {
        id : 47,
        name : "Remove Topic",
        request : RemoveTopic,
        response : null
    },
    MISSING_TOPIC : {
        id : 50,
        name : "Missing topic",
        request : MissingTopicRequest,
        response : Boolean
    },
    TOPIC_SCOPED_WILL_REGISTRATION : {
        id : 53,
        name : "Topic Will Registration",
        request : TopicWillParameters,
        response : WillRegistrationResult
    },
    TOPIC_SCOPED_WILL_DEREGISTRATION : {
        id : 54,
        name : "Topic Will Deregistration",
        request : TopicWillParameters,
        response : null
    },
    SYSTEM_PING : {
        id : 55,
        name : "System Ping",
        request : null,
        response : null
    },
    USER_PING : {
        id : 56,
        name : "User Ping",
        request : null,
        response : null
    },
    CHANGE_PRINCIPAL : {
        id : 5,
        name : "Change Principal",
        request : ChangePrincipalRequest,
        response : Boolean
    },
    GET_SYSTEM_AUTHENTICATION : {
        id : 57,
        name : "Get System Authentication",
        request : null,
        response : SystemAuthenticationConfiguration
    },
    UPDATE_SYSTEM_AUTHENTICATION : {
        id : 58,
        name : "Update System Authentication",
        request : SecurityCommandScript,
        response : SecurityCommandScriptResult
    },
    GET_SECURITY_CONFIGURATION : {
        id : 59,
        name : "Get Security Configuration",
        request : null,
        response : SecurityConfiguration
    },
    UPDATE_SECURITY_CONFIGURATION : {
        id : 60,
        name : "Update Security Configuration",
        request : SecurityCommandScript,
        response : SecurityCommandScriptResult
    },
    UPDATE_TOPIC : {
        id : 61,
        name : "Update Topic",
        request : UpdateTopicRequest,
        response : UpdateTopicResponse
    },
    SERVER_CONTROL_REGISTRATION : {
        id : 18,
        name : "Server Control Registration",
        request : ControlRegistrationRequest,
        response : null
    },
    SERVER_CONTROL_DEREGISTRATION : {
        id : 19,
        name : "Server Control Deregistration",
        request : ControlRegistrationParams,
        response : null
    },
    TOPIC_CONTROL_REGISTRATION : {
        id : 20,
        name : "Topic Control Registration",
        request : TopicControlRegistrationRequest,
        response : null
    },
    TOPIC_CONTROL_DEREGISTRATION : {
        id : 21,
        name : "Topic Control Deregistration",
        request : TopicControlRegistrationParams,
        response : null
    },
    AUTHENTICATION_CONTROL_REGISTRATION : {
        id : 22,
        name : "Authentication Control Registration",
        request : AuthenticationControlRegistrationRequest,
        response : null
    },
    AUTHENTICATION_CONTROL_DEREGISTRATION : {
        id : 23,
        name : "Authentication Control Deregistration",
        request : AuthenticationControlRegistrationParameters,
        response : null
    },
    AUTHENTICATION : {
        id : 24,
        name : "Authentication Control",
        request : AuthenticationRequest,
        response : AuthenticationResponse
    },
    UPDATE_SOURCE_REGISTRATION : {
        id : 30,
        name : "Update Source Registration",
        request : UpdateSourceRegistrationRequest,
        response : UpdateSourceState
    },
    UPDATE_SOURCE_DEREGISTRATION : {
        id : 31,
        name : "Update Source Deregistration",
        request : UpdateSourceDeregistrationRequest,
        response : null
    },
    UPDATE_SOURCE_STATE : {
        id : 32,
        name : "Update Source State",
        request : UpdateSourceStateRequest,
        response : null
    },
    UPDATE_SOURCE_UPDATE : {
        id : 35,
        name : "Update Source Update",
        request : UpdateSourceUpdate,
        response : UpdateSourceUpdateResponse
    },
    SEND_RECEIVER_CLIENT : {
        id : 62,
        name : "Control Client Send Service",
        request : SessionForwardSendRequest,
        response : null
    },
    MESSAGE_RECEIVER_CONTROL_REGISTRATION : {
        id : 63,
        name : "Message Receiver Control Registration",
        request : MessageReceiverControlRegistrationRequest,
        response : null
    },
    MESSAGE_RECEIVER_CONTROL_DEREGISTRATION : {
        id : 64,
        name : "Message Receiver Control Deregistration",
        request : MessageReceiverControlRegistrationParams,
        response : null
    },
    FILTER_SUBSCRIBE : {
        id : 65,
        name : "Subscribe to filter",
        request : ClientFilterSubscribeRequest,
        response : ClientFilterSubscribeResponse
    },
    FILTER_UNSUBSCRIBE : {
        id : 66,
        name : "Unsubscribe from filter",
        request : ClientFilterSubscribeRequest,
        response : ClientFilterSubscribeResponse
    },
    SEND_TO_FILTER : {
        id : 29,
        name : "Send to filter",
        request : FilterSendRequest,
        response : FilterSendResult
    },
    GET_SESSION_PROPERTIES : {
        id : 67,
        name : "Get session properties",
        request : GetSessionProperties,
        response : GetSessionProperties
    },
    SESSION_PROPERTIES_REGISTRATION : {
        id : 69,
        name : "Set session properties listener - legacy, see SESSION_PROPERTIES_REGISTRATION_2",
        request : SetSessionPropertiesListener,
        response : null
    },
    SESSION_PROPERTIES_EVENT : {
        id : 70,
        name : "Session properties event - legacy, see SESSION_PROPERTIES_EVENT_2",
        request : SessionPropertiesEvent,
        response : null
    },
    MESSAGING_SEND : {
        id : 85,
        name : "Send request",
        request : MessagingSendRequest,
        response : MessagingResponse
    },
    MESSAGING_RECEIVER_SERVER : {
        id : 86,
        name : "Send request to session",
        request : MessagingClientSendRequest,
        response : MessagingResponse
    },
    NOTIFY_SUBSCRIPTION : {
        id : 87,
        name : "TopicSpecification Subscription Notification",
        request : TopicSpecificationInfo,
        response : null
    },
    MESSAGING_RECEIVER_CLIENT : {
        id : 88,
        name : "Receive requests from other clients",
        request : MessagingClientForwardSendRequest,
        response : MessagingResponse
    },
    MESSAGING_FILTER_SEND : {
        id : 102,
        name : "Message Filter send",
        request : MessagingFilterSendRequest,
        response : MessagingFilterSendResponse
    },
    FILTER_RESPONSE : {
        id : 103,
        name : "Messaging Filter response",
        request : MessagingFilterResponse,
        response : null
    },
    TOPIC_NOTIFICATION_DEREGISTRATION : {
        id : 91,
        name : "Topic notification deregistration",
        request : TopicNotificationDeregistrationRequest,
        response : null
    },
    TOPIC_NOTIFICATION_EVENTS : {
        id : 92,
        name : "Topic notification events",
        request : TopicNotificationEvent,
        response : null
    },
    TOPIC_DESCENDANT_EVENTS : {
        id : 93,
        name : "Topic descendant events",
        request : TopicNotificationDescendantEvent,
        response : null
    },
    TOPIC_NOTIFICATION_SELECTION : {
        id : 94,
        name : "Topic notification selection",
        request : TopicNotificationSelection,
        response : null
    },
    TOPIC_NOTIFICATION_DESELECTION : {
        id : 95,
        name : "Topic notification deselection",
        request : TopicNotificationDeselection,
        response : null
    },
    MESSAGING_RECEIVER_CONTROL_REGISTRATION : {
        id : 97,
        name : "Request handler registration",
        request : MessageReceiverControlRegistrationRequest,
        response : null
    },
    MESSAGING_RECEIVER_CONTROL_DEREGISTRATION : {
        id : 98,
        name : "Request handler deregistration",
        request : MessageReceiverControlRegistrationParams,
        response : null
    },
    UPDATE_SOURCE_SET : {
        id : 77,
        name : "Update source set",
        request : UpdateSourceSetRequest,
        response : UpdateSourceUpdateResponse
    },
    UPDATE_SOURCE_DELTA : {
        id : 78,
        name : "Update source delta",
        request : UpdateSourceDeltaRequest,
        response : UpdateSourceUpdateResponse
    },
    UPDATE_TOPIC_SET : {
        id : 79,
        name : "Update topic set",
        request : UpdateTopicSetRequest,
        response : UpdateTopicResponse
    },
    UPDATE_TOPIC_DELTA : {
        id : 80,
        name : "Update topic delta",
        request : UpdateTopicDeltaRequest,
        response : UpdateTopicResponse
    },
    SESSION_PROPERTIES_REGISTRATION_2 : {
        id : 81,
        name : "Session Properties registration 2",
        request : SetSessionPropertiesListener,
        response : null
    },
    SESSION_PROPERTIES_EVENT_2 : {
        id : 82,
        name : "Session Properties event 2",
        request : SessionPropertiesEventBatch,
        response : null
    },
    TOPIC_REMOVAL : {
        id : 83,
        name : "Topic removal",
        request : RemoveTopic,
        response : null
    },
    RANGE_QUERY : {
        id : 84,
        name : "Time series range query",
        request : RangeQueryRequest,
        response : RangeQueryResult
    },
    TIME_SERIES_APPEND  : {
        id : 99,
        name : "Append an event to a time series",
        request : TimeSeriesAppendRequest,
        response : TimeSeriesEventMetadata
    },
    TIME_SERIES_EDIT : {
        id : 100,
        name : "Edit an event in a time series",
        request : TimeSeriesEditRequest,
        response : TimeSeriesEventMetadata
    }
};

},{"233":233,"236":236,"238":238,"242":242,"249":249,"251":251,"253":253,"255":255,"257":257,"259":259,"261":261,"263":263,"265":265,"267":267,"269":269,"271":271,"273":273,"275":275,"277":277,"279":279,"282":282,"285":285,"287":287,"289":289,"291":291,"296":296,"298":298,"300":300,"302":302,"304":304,"306":306,"308":308,"310":310,"314":314,"316":316,"319":319,"321":321,"323":323,"327":327,"330":330,"332":332,"334":334,"336":336,"338":338,"341":341,"343":343,"345":345,"347":347,"349":349,"352":352,"354":354,"356":356,"358":358,"360":360,"362":362,"364":364,"366":366,"368":368,"370":370,"372":372,"374":374,"376":376,"378":378,"380":380,"396":396,"431":431,"98":98}],326:[function(require,module,exports){
var TopicInfoSerialiser = require(393);
var SubscriptionNotification = require(327);

module.exports = {
    read : function(input) {
        var info = TopicInfoSerialiser.read(input);
        return new SubscriptionNotification(info);
    },
    write : function(out, notification) {
        TopicInfoSerialiser.write(out, notification.info);
    }
};

},{"327":327,"393":393}],327:[function(require,module,exports){
function SubscriptionNotification(info) {
    this.info = info;
}

module.exports = SubscriptionNotification;

},{}],328:[function(require,module,exports){
var RangeQueryParameters = require(186);
var Point = require(183);
var Range = require(187);

var BEES = require(230);
var Codec = require(203);

function writePoint(output, point) {
    Codec.writeInt64(output, point.value);
    BEES.write(output, point.type);
}

function writeRange(output, range) {
    writePoint(output, range.anchor);
    writePoint(output, range.span);
}

function readPoint(input) {
    var value = Codec.readInt64(input);
    var type = BEES.read(input, Point.Type);

    return new Point(value, type);
}

function readRange(input) {
    var anchor = readPoint(input);
    var span = readPoint(input);

    return new Range(anchor, span);
}

module.exports = {
    read : function(input) {
        var queryType = BEES.read(input, RangeQueryParameters.QueryType);
        var viewRange = readRange(input);
        var editRange = readRange(input);
        var limit = Codec.readInt64(input);

        return new RangeQueryParameters(queryType, viewRange, editRange, limit);
    },
    write : function(output, parameters) {
        BEES.write(output, parameters.queryType);
        writeRange(output, parameters.viewRange);
        writeRange(output, parameters.editRange);
        Codec.writeInt64(output, parameters.limit);
    }
};
},{"183":183,"186":186,"187":187,"203":203,"230":230}],329:[function(require,module,exports){
var RangeQueryParametersSerialiser = require(328);
var RangeQueryRequest = require(330);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var topicPath = Codec.readString(input);
        var parameters = RangeQueryParametersSerialiser.read(input);

        return new RangeQueryRequest(topicPath, parameters);
    },
    write : function(output, request) {
        Codec.writeString(output, request.topicPath);
        RangeQueryParametersSerialiser.write(output, request.parameters);
    }
};
},{"203":203,"328":328,"330":330}],330:[function(require,module,exports){
module.exports = function RangeQueryRequest(topicPath, parameters) {
    this.topicPath = topicPath;
    this.parameters = parameters;
};
},{}],331:[function(require,module,exports){
var RangeQueryResult = require(332);
var EventMetadata = require(338);
var Event = require(339);

var DataTypes = require(144);
var Codec = require(203);
var Long = require(46);

var ORIGINAL_EVENT      = 0;
var EDIT_EVENT          = 1;
var METADATA_OFFSETS    = 2;
var AUTHOR_ENCODING     = 3;

function Offsets(sequence, timestamp) {
    this.sequence = sequence;
    this.timestamp = timestamp;

    this.with = function(s, t) {
        if (sequence.equals(s) && timestamp.equals(t)) {
            return this;
        }

        return new Offsets(sequence, timestamp);
    };

    this.equals = function(other) {
        if (other && other instanceof Offsets) {
            return sequence.equals(other.sequence) &&
                   timestamp.equals(other.timestamp);
        }

        return false;
    };
}

Offsets.IDENTITY = new Offsets(Long.MAX_VALUE, Long.MAX_VALUE);

function readMetadata(input, offsets, codeToAuthor) {
    var sequence = Codec.readInt64(input).add(offsets.sequence);
    var timestamp = Codec.readInt64(input).add(offsets.timestamp);

    var code = Codec.readBytes(input);
    var decoded = codeToAuthor[code];
    var author = decoded ? decoded : code.toString('utf-8');

    return new EventMetadata(sequence.toNumber(), timestamp.toNumber(), author);
}

function readOriginalEvent(input, offsets, codeToAuthor) {
    var metadata = readMetadata(input, offsets, codeToAuthor);
    return Event.create(metadata, metadata, Codec.readBytes(input));
}

function readEditEvent(input, offsets, codeToAuthor) {
    var originalEvent = readMetadata(input, offsets, codeToAuthor);
    var metadata = readMetadata(input, offsets, codeToAuthor);

    return Event.create(metadata, originalEvent, Codec.readBytes(input));
}

function readMetadataOffsets(input) {
    return new Offsets(Codec.readInt64(input), Codec.readInt64(input));
}

function readAuthorEncoding(input, codeToAuthor) {
    var code = Codec.readBytes(input);
    codeToAuthor[code] = Codec.readString(input);
}

module.exports = {
    read : function(input) {
        var dataType = DataTypes.get(Codec.readString(input));
        var selectedCount = Codec.readInt64(input);

        var offsets = new Offsets(Long.ZERO, Long.ZERO);
        var codeToAuthor = {};
        var events = [];

        var limit = Codec.readInt32(input);

        for (var i = 0; i < limit; ++i) {
            switch (Codec.readByte(input)) {
                case ORIGINAL_EVENT :
                    events.push(readOriginalEvent(input, offsets, codeToAuthor));
                    break;
                case EDIT_EVENT :
                    events.push(readEditEvent(input, offsets, codeToAuthor));
                    break;
                case METADATA_OFFSETS :
                    offsets = readMetadataOffsets(input);
                    break;
                case AUTHOR_ENCODING :
                    readAuthorEncoding(input, codeToAuthor);
                    break;
                default:
                    readAuthorEncoding(input, codeToAuthor);
            }
        }

        return new RangeQueryResult(dataType, selectedCount, events);
    },
    write : function() {
    }
};
},{"144":144,"203":203,"332":332,"338":338,"339":339,"46":46}],332:[function(require,module,exports){
var QueryResult = require(184);
var Long = require(46);

module.exports = function RangeQueryResult(valueDataType, selectedCount, events) {
    this.createQueryResult = function(queryValueType, streamStructure) {
        if (!valueDataType.canReadAs(queryValueType)) {
            throw new Error("Time series topic has an incompatible event data type: " + valueDataType);
        }

        return new QueryResult(
            selectedCount,
            Long.fromNumber(events.length),
            streamStructure,
            events.map(function(raw) {
                return raw.withValue(valueDataType.readAs(queryValueType, raw.value));
            }));
    };
};
},{"184":184,"46":46}],333:[function(require,module,exports){
var TimeSeriesAppendRequest = require(334);
var DataTypes = require(144);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var path = Codec.readString(input);
        var dataType = DataTypes.get(Codec.readString(input));
        var value = Codec.readBytes(input);

        return new TimeSeriesAppendRequest(path, dataType, value);
    },
    write : function(out, value) {
        Codec.writeString(out, value.path);
        Codec.writeString(out, value.dataType.name());
        Codec.writeBytes(out, value.value);
    }
};
},{"144":144,"203":203,"334":334}],334:[function(require,module,exports){
module.exports = function TimeSeriesAppendRequest(path, dataType, value) {
    this.path = path;
    this.dataType = dataType;
    this.value = value;
};
},{}],335:[function(require,module,exports){
var TimeSeriesEditRequest = require(336);
var DataTypes = require(144);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var path = Codec.readString(input);
        var datatype = DataTypes.get(Codec.readString(input));
        var sequence = Codec.readInt64(input);
        var value = Codec.readBytes(input);

        return new TimeSeriesEditRequest(path, datatype, sequence, value);
    },
    write : function(output, request) {
        Codec.writeString(output, request.path);
        Codec.writeString(output, request.dataType.name());
        Codec.writeInt64(output, request.sequence);
        Codec.writeBytes(output, request.value);
    }
};
},{"144":144,"203":203,"336":336}],336:[function(require,module,exports){
module.exports = function TimeSeriesEditRequest(path, dataType, sequence, value) {
    this.path = path;
    this.dataType = dataType;
    this.sequence = sequence;
    this.value = value;
};
},{}],337:[function(require,module,exports){
var EventMetadata = require(338);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var sequence = Codec.readInt64(input);
        var timestamp = Codec.readInt64(input);
        var author = Codec.readString(input);

        return new EventMetadata(sequence.toNumber(), timestamp.toNumber(), author);
    },
    write : function(output, metadata) {
        Codec.writeInt64(output, metadata.sequence);
        Codec.writeInt64(output, metadata.timestamp);
        Codec.writeString(output, metadata.author);
    }
};

},{"203":203,"338":338}],338:[function(require,module,exports){

function EventMetadataImpl(sequence, timestamp, author) {
    this.sequence = sequence;
    this.timestamp = timestamp;
    this.author = author;
}

EventMetadataImpl.prototype.equals = function(other) {
    if (other && other instanceof EventMetadataImpl) {
        return this.sequence === other.sequence &&
               this.timestamp === other.timestamp &&
               this.author === other.author;
    }

    return false;
};

EventMetadataImpl.prototype.toString = function() {
    return "sequence=" + this.sequence +
           " timestamp=" + this.timestamp +
           " author=" + this.author;
};

module.exports = EventMetadataImpl;
},{}],339:[function(require,module,exports){
function Event(metadata, originalEvent, value) {
    this.metadata = metadata;
    this.originalEvent = originalEvent;
    this.value = value;

    this.sequence = metadata.sequence;
    this.timestamp = metadata.timestamp;
    this.author = metadata.author;

    this.isEditEvent = !metadata.equals(originalEvent);
    this.isOriginalEvent = !this.isEditEvent;
}

Event.prototype.withValue = function(newValue) {
    return new Event(this.metadata, this.originalEvent, newValue);
};

Event.prototype.equals = function(other) {
    if (other && other instanceof Event) {
        return this.metadata.equals(other.metadata) &&
            this.originalEvent.equals(other.originalEvent) &&
            this.value === other.value;
    }

    return false;
};

Event.prototype.toString = function() {
    var s = this.isEditEvent ? "Edit of [" + this.originalEvent + "] " : "";
    return s + "[" + this.metadata + "] " + this.value;
};

Event.create = function(metadata, originalEvent, value) {
    return new Event(metadata, originalEvent, value);
};

module.exports = Event;
},{}],340:[function(require,module,exports){
var ConversationIDSerialiser = require(135);

var serialiser = {
    write : function(output, request) {
        ConversationIDSerialiser.write(output, request.cid);
    }
};

module.exports = serialiser;
},{"135":135}],341:[function(require,module,exports){
function TopicNotificationDeregistrationRequest(cid) {
    this.cid = cid;
}

module.exports = TopicNotificationDeregistrationRequest;

},{}],342:[function(require,module,exports){
var BEES = require(230);
var Codec = require(203);
var ConversationIDSerialiser = require(135);
var TopicNotificationType = require(101).TopicNotificationType;
var TopicDescendantEvent = require(343);

var serialiser = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var topicPath = Codec.readString(input);
        var type = BEES.read(input, TopicNotificationType);

        return new TopicDescendantEvent(cid, topicPath, type);
    }
};

module.exports = serialiser;

},{"101":101,"135":135,"203":203,"230":230,"343":343}],343:[function(require,module,exports){
function TopicNotificationDescendantEvent(cid, topicPath, type) {
    this.cid = cid;
    this.path = topicPath;
    this.type = type;

    this.isDescendantEvent = true;
}

module.exports = TopicNotificationDescendantEvent;
},{}],344:[function(require,module,exports){
var ConversationIDSerialiser = require(135);
var Codec = require(203);
var TopicDeselection = require(345);
var TopicSelectorSerialiser = require(404);

var serialiser = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var selector = Codec.readString(input);

        return new TopicDeselection(cid, selector);
    },
    write : function(output, request) {
        ConversationIDSerialiser.write(output, request.cid);
        TopicSelectorSerialiser.write(output, request.selector);
    }
};

module.exports = serialiser;
},{"135":135,"203":203,"345":345,"404":404}],345:[function(require,module,exports){
function TopicNotificationDeselection(cid, selector) {
    this.cid = cid;
    this.selector = selector;
}

module.exports = TopicNotificationDeselection;

},{}],346:[function(require,module,exports){
var Codec = require(203);
var ConversationIDSerialiser = require(135);
var TopicSpecificationSerialiser = require(350);
var BEES = require(230);
var TopicNotificationType = require(101).TopicNotificationType;
var TopicNotificationEvent = require(347);

var serialiser = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var topicPath = Codec.readString(input);
        var type = BEES.read(input, TopicNotificationType);
        var specification = TopicSpecificationSerialiser.read(input);

        return new TopicNotificationEvent(cid, topicPath, type, specification);
    }
};

module.exports = serialiser;
},{"101":101,"135":135,"203":203,"230":230,"347":347,"350":350}],347:[function(require,module,exports){
function TopicNotificationEvent(cid, topicPath, type, specification) {
    this.cid = cid;
    this.path = topicPath;
    this.type = type;
    this.specification = specification;

    this.isDescendantEvent = false;
}

module.exports = TopicNotificationEvent;
},{}],348:[function(require,module,exports){
var ConversationIDSerialiser = require(135);
var Codec = require(203);
var TopicNotificationSelection = require(349);
var TopicSelectorSerialiser = require(404);

var serialiser = {
    read : function(input) {
        var cid = ConversationIDSerialiser.read(input);
        var selector = Codec.readString(input);

        return new TopicNotificationSelection(cid, selector);
    },
    write : function(output, request) {
        ConversationIDSerialiser.write(output, request.cid);
        TopicSelectorSerialiser.write(output, request.selector);
    }
};

module.exports = serialiser;
},{"135":135,"203":203,"349":349,"404":404}],349:[function(require,module,exports){
function TopicNotificationSelection(cid, selector) {
    this.cid = cid;
    this.selector = selector;
}

module.exports = TopicNotificationSelection;

},{}],350:[function(require,module,exports){
var Codec = require(203);
var ConversationIDSerialiser = require(135);

var serialiser = {
    read : function(input) {
        var type = ConversationIDSerialiser.read(input);
        var properties = Codec.readDictionary(input, Codec.readString);

        return {
            type: type,
            properties: properties
        };
    }
};

module.exports = serialiser;

},{"135":135,"203":203}],351:[function(require,module,exports){
var CIDSerialiser = require(135);
var Codec = require(203);

module.exports = {
    read : function(bis) {
        return {
            cid : CIDSerialiser.read(bis),
            path : bis.readString(),
            id : bis.readInt32(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        CIDSerialiser.write(bos, req.cid);
        Codec.writeString(bos, req.path);
        Codec.writeInt32(bos, req.id);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"135":135,"203":203}],352:[function(require,module,exports){
module.exports = function UpdateSourceDeltaRequest(cid, path, id, bytes) {
    this.cid = cid;
    this.path = path;
    this.id = id;
    this.bytes = bytes;
};
},{}],353:[function(require,module,exports){
var CIDSerialiser = require(135);

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        return { cid : cid };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
    }
};

},{"135":135}],354:[function(require,module,exports){
module.exports = function UpdateSourceDeregistrationRequest(cid) {
    this.cid = cid;
};

},{}],355:[function(require,module,exports){
var CIDSerialiser = require(135);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        var path = Codec.readString(input);

        return { cid : cid, path : path };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
        Codec.writeString(output, req.path);
    }
};

},{"135":135,"203":203}],356:[function(require,module,exports){
module.exports = function UpdateSourceRegistrationRequest(cid, path) {
    this.cid = cid;
    this.path = path;
};

},{}],357:[function(require,module,exports){
var CIDSerialiser = require(135);
var Codec = require(203);

module.exports = {
    read : function(bis) {
        return {
            cid : CIDSerialiser.read(bis),
            path : bis.readString(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        CIDSerialiser.write(bos, req.cid);
        Codec.writeString(bos, req.path);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"135":135,"203":203}],358:[function(require,module,exports){
module.exports = function UpdateSourceSetRequest(cid, path, bytes) {
    this.cid = cid;
    this.path = path;
    this.bytes = bytes;
};

},{}],359:[function(require,module,exports){
var UpdateStateSerialiser = require(361);
var CIDSerialiser = require(135);

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        var old = UpdateStateSerialiser.read(input);
        var current = UpdateStateSerialiser.read(input);

        return {
            cid : cid,
            old : old,
            current : current
        };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
        UpdateStateSerialiser.write(output, req.old);
        UpdateStateSerialiser.write(output, req.current);
    }
};

},{"135":135,"361":361}],360:[function(require,module,exports){
module.exports = function UpdateSourceStateRequest(cid, old, current) {
    this.cid = cid;
    this.old = old;
    this.current = current;
};

},{}],361:[function(require,module,exports){
var EnumSerialiser = require(231);

var State = {
    init : 0,
    active : 1,
    closed : 2,
    standby : 3
};

module.exports = {
    read : function(input) {
        return EnumSerialiser.read(input, State);
    },
    write : function(output, req) {
        EnumSerialiser.write(output, req, State);
    }
};

},{"231":231}],362:[function(require,module,exports){
module.exports = function UpdateSourceState(state) {
    this.state = state;
};

},{}],363:[function(require,module,exports){
var UpdateSourceUpdateResponse = require(364);
var EnumSerialiser = require(231);
var Codec = require(203);

var ResponseCodes = {
    SUCCESS : 0,
    INCOMPATIBLE_UPDATE : 1,
    UPDATE_FAILED : 2,
    INVALID_UPDATER : 3,
    MISSING_TOPIC : 4,
    INVALID_ADDRESS : 5,
    DUPLICATES : 6,
    EXCLUSIVE_UPDATER_CONFLICT : 7,
    DELTA_WITHOUT_VALUE : 8,
    CLUSTER_REPARTITION : 9,
    INCOMPATIBLE_STATE : 10
};

module.exports = {
    read : function(input) {
        var error = EnumSerialiser.read(input, ResponseCodes);

        return new UpdateSourceUpdateResponse(error === 'SUCCESS' ? null : error);
    },
    write : function(output, req) {
        if (req.error) {
            EnumSerialiser.write(output, req.error, ResponseCodes);
        } else {
            Codec.writeByte(output, 0);
        }
    }
};

},{"203":203,"231":231,"364":364}],364:[function(require,module,exports){
module.exports = function UpdateSourceUpdateResponse(error) {
    this.error = error;
};

},{}],365:[function(require,module,exports){
var CIDSerialiser = require(135);
var UpdateSerialiser = require(410);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var cid = CIDSerialiser.read(input);
        var path = Codec.readString(input);
        var val = UpdateSerialiser.read(input);

        return { cid : cid, path : path, update : val };
    },
    write : function(output, req) {
        CIDSerialiser.write(output, req.cid);
        Codec.writeString(output, req.path);
        UpdateSerialiser.write(output, req.update);
    }
};

},{"135":135,"203":203,"410":410}],366:[function(require,module,exports){
module.exports = function UpdateSourceUpdate(cid, path, update) {
    this.cid = cid;
    this.path = path;
    this.update = update;
};

},{}],367:[function(require,module,exports){
var UnsubscribeReason = require(435).UnsubscribeReason;

var TopicIdSerialiser = require(392);
var UnsubscriptionNotification = require(368);
var ByteEncodedEnumSerialiser = require(230);

module.exports = {
    read : function(input) {
        var id = TopicIdSerialiser.read(input);
        var reason = ByteEncodedEnumSerialiser.read(input, UnsubscribeReason);

                return new UnsubscriptionNotification(id, reason);
    },
    write : function(out, notification) {
        TopicIdSerialiser.write(out, notification.id);
        ByteEncodedEnumSerialiser.write(out, notification.reason);
    }
};
},{"230":230,"368":368,"392":392,"435":435}],368:[function(require,module,exports){
function UnsubscriptionNotification(id, reason) {
    this.id = id;
    this.reason = reason;
}

module.exports = UnsubscriptionNotification;

},{}],369:[function(require,module,exports){
var Codec = require(203);

module.exports = {
    read : function(bis) {
        return {
            path : bis.readString(),
            id : bis.readInt32(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        Codec.writeString(bos, req.path);
        Codec.writeInt32(bos, req.id);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"203":203}],370:[function(require,module,exports){
module.exports = function UpdateTopicDeltaRequest(path, id, bytes) {
    this.path = path;
    this.id = id;
    this.bytes = bytes;
};
},{}],371:[function(require,module,exports){
var UpdateSerialiser = require(410);
var Codec = require(203);

module.exports = {
    read : function(input) {
        return {
            path : Codec.readString(input),
            update : UpdateSerialiser.read(input)
        };
    },
    write : function(output, request) {
        Codec.writeString(output, request.path);
        UpdateSerialiser.write(output, request.update);
    }
};

},{"203":203,"410":410}],372:[function(require,module,exports){
module.exports = function UpdateTopicRequest(path, update) {
    this.path = path;
    this.update = update;
};
},{}],373:[function(require,module,exports){
var BEES = require(230);
var UpdateTopicResponse = require(374);

module.exports = {
    read : function(input) {
        var response = BEES.read(input, UpdateTopicResponse);

        return {
            reason : response.reason,
            isError : response !== UpdateTopicResponse.SUCCESS
        };
    },
    write : function(output, request) {
        BEES.write(output, request.response);
    }
};

},{"230":230,"374":374}],374:[function(require,module,exports){
var UpdateFailReason = require(435).UpdateFailReason;

module.exports = {
    SUCCESS : {
        id : 0,
        reason : null
    },
    INCOMPATIBLE_UPDATE : {
        id : 1,
        reason : UpdateFailReason.INCOMPATIBLE_UPDATE
    },
    UPDATE_FAILED : {
        id : 2,
        reason : UpdateFailReason.UPDATE_FAILED
    },
    INVALID_UPDATER : {
        id : 3,
        reason : UpdateFailReason.INVALID_UPDATER
    },
    MISSING_TOPIC : {
        id : 4,
        reason : UpdateFailReason.MISSING_TOPIC
    },
    INVALID_ADDRESS : {
        id : 5,
        reason : UpdateFailReason.INVALID_ADDRESS
    },
    DUPLICATES : {
        id : 6,
        reason : UpdateFailReason.DUPLICATES
    },
    EXCLUSIVE_UPDATER_CONFLICT : {
        id : 7,
        reason : UpdateFailReason.EXCLUSIVE_UPDATER_CONFLICT
    },
    DELTA_WITHOUT_VALUE : {
        id : 8,
        reason : UpdateFailReason.DELTA_WITHOUT_VALUE
    },
    CLUSTER_REPARTITION : {
        id : 9,
        reason : UpdateFailReason.CLUSTER_REPARTITION
    }
};

},{"435":435}],375:[function(require,module,exports){
var Codec = require(203);

module.exports = {
    read : function(bis) {
        return {
            path : bis.readString(),
            bytes : bis.readBytes()
        };
    },
    write : function(bos, req) {
        Codec.writeString(bos, req.path);
        Codec.writeBytes(bos, req.bytes);
    }
};
},{"203":203}],376:[function(require,module,exports){
module.exports = function UpdateTopicSetRequest(path, bytes) {
    this.path = path;
    this.bytes = bytes;
};


},{}],377:[function(require,module,exports){
var BEES = require(230);
var TopicWillParameters = require(378);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        var path = Codec.readString(input);
        var will = BEES.read(input, TopicWillParameters.Will);

                return new TopicWillParameters(path, will);
    },
    write : function(out, params) {
        Codec.writeString(out, params.path);
        BEES.write(out, params.will);
    }
};

module.exports = serialiser;

},{"203":203,"230":230,"378":378}],378:[function(require,module,exports){
var Will = {
    REMOVE_TOPICS : 0
};

function TopicWillParameters(path, will) {
    this.path = path;
    this.will = will;
}

TopicWillParameters.prototype.toString = function() {
    return "TopicWillParams [" + this.will + ", " + this.path + "]";  
};

TopicWillParameters.Will = Will;

module.exports = TopicWillParameters;
},{}],379:[function(require,module,exports){
var BEES = require(230);
var WillRegistrationResult = require(380);

var serialiser = {
    read : function(input) {
        return BEES.read(input, WillRegistrationResult);
    },
    write : function(output, result) {
        BEES.write(output, result);
    }
};

module.exports = serialiser;
},{"230":230,"380":380}],380:[function(require,module,exports){
var WillRegistrationResult = {
    SUCCESS : 0,
    GROUP_CONFLICT : 1,
    TOPIC_TREE_CONFLICT : 2
};

WillRegistrationResult.toString = function() {
    return "WillRegistrationResult";
};

module.exports = WillRegistrationResult;
},{}],381:[function(require,module,exports){
var SessionID = require(382);
var Codec = require(203);

var serialiser = {
    read : function(input) {
        var server = Codec.readInt64(input);
        var value = Codec.readInt64(input);

        return new SessionID(server, value);
    },
    write : function(output, sessionID) {
        Codec.writeInt64(output, sessionID.server);
        Codec.writeInt64(output, sessionID.value);
    }
};

module.exports = serialiser;

},{"203":203,"382":382}],382:[function(require,module,exports){
var Long = require(46);

var padding = "0000000000000000";

function SessionId(server, value) {
    this.server = server;
    this.value = value;
}

    SessionId.prototype.toString = function() {
    var server = padding + this.server.toUnsigned().toString(16);
    var client = padding + this.value.toUnsigned().toString(16);

    return (server.substr(server.length - 16) + '-' + client.substr(client.length - 16)).toLowerCase();
};

SessionId.prototype.toValue = function() {
    return this.toString();
};

SessionId.fromString = function(str) {
    var parts = str.split('-');
    var sessionId;

    try {
        var server = Long.fromString(parts[0], false, 16);
        var value = Long.fromString(parts[1], false, 16);

        sessionId = new SessionId(server, value);
    } catch (e) {
        throw new Error('SessionId must consist of two hexadecimal parts, joined with a \'-\'');
    }

    return sessionId;
};

SessionId.prototype.equals = function(sessionid) {
    if (!(sessionid instanceof SessionId) ||
        !(sessionid.server instanceof Long) ||
        !(sessionid.value instanceof Long)) {
        return false;
    }
    return this.server.compare(sessionid.server) === 0 &&
        this.value.compare(sessionid.value) === 0;
};

SessionId.validate = function(str) {
    if (str instanceof SessionId) {
        return str;
    }

    if (str.length === 0 || str.indexOf('-') < 0 || str.indexOf(' ') >= 0) {
        return false;
    }

    try {
        return SessionId.fromString(str);
    } catch (e) {
        return false;
    }
};

module.exports = SessionId;

},{"46":46}],383:[function(require,module,exports){
var _implements = require(415)._implements;

var Emitter = require(173);
var Result = require(174);

var Messages = require(177);
var TopicNotifications = require(192);
var Security = require(179).Security;
var TopicControl = require(189);
var ClientControl = require(176);
var TimeSeries = require(188);

var features = [
    require(193),
    require(178)
];

var Options = require(430);
var Session = require(433);

var SessionImpl = _implements(Session, function SessionImpl(sessionID, options, internalSession) {
    var emitter = Emitter.assign(this);
    var self = this;

    internalSession.on({
        reconnect: function () {
            emitter.emit('reconnect');
        },
        disconnect: function (reason) {
            emitter.emit('disconnect', reason);
        },
        error: function (err) {
            emitter.emit('error', err);
        },
        close: function (reason) {
            emitter.emit('close', reason);
        }
    });

    features.forEach(function(feature) {
        var instance = new feature(internalSession);
        for (var k in instance) {
            self[k] = instance[k];
        }
    });

    this.topics   = new TopicControl(internalSession, this);
    this.clients  = new ClientControl(internalSession, this);
    this.messages = new Messages(internalSession, this);
    this.notifications = new TopicNotifications(internalSession, this);
    this.security = new Security(internalSession, this);
    this.timeseries = new TimeSeries(internalSession, this);

    this.sessionID = sessionID.toString();

    this.options = options.with({
        credentials : undefined
    });

    this.close = function() {
        internalSession.close();
        return self;
    };

    this.isClosed = function() {
        var state = internalSession.getState();
        return state === "closing" || state === "closed";
    };

    this.isConnected = function() {
        return internalSession.isConnected();
    };

    this.toString = function() {
        return "Session<" + self.sessionID + " " + internalSession.getState() + ">";
    };

    this.getPrincipal = function() {
        return options.principal || "";
    };
});

module.exports = SessionImpl;

module.exports.create = function(internalSessionFactory, options) {
    if (typeof options === 'string') {
        options = new Options({ host : options });
    } else {
        options = new Options(options);
    }

    var emitter = new Emitter();
    var result = new Result(emitter);

    function onConnect(sessionID) {
        internalSession.off('connect', onConnect);
        internalSession.off('close', onError);
        internalSession.off('error', onError);

        emitter.emit('complete', new SessionImpl(sessionID, options, internalSession));
    }

    function onError(err) {
        internalSession.off('connect', onConnect);
        internalSession.off('close', onError);
        internalSession.off('error', onError);

        emitter.error(err);
    }

    var internalSession = internalSessionFactory(options);

    internalSession.on('connect', onConnect);
    internalSession.on('close', onError);
    internalSession.on('error', onError);

    internalSession.connect();

    return result;
};
},{"173":173,"174":174,"176":176,"177":177,"178":178,"179":179,"188":188,"189":189,"192":192,"193":193,"415":415,"430":430,"433":433}],384:[function(require,module,exports){
var SessionToken = require(385);

function readToken(input) {
    return new SessionToken(input.readMany(SessionToken.TOKEN_LENGTH));
}

module.exports = readToken;

},{"385":385}],385:[function(require,module,exports){
(function (Buffer){
var TOKEN_LENGTH = 24;

function SessionToken(external) {
    this.put = function(destination) {
        destination.writeBytes(external);
    };

    this.toString = function() {
        return external.toString('ascii');
    };
}

SessionToken.fromExternalString = function(external) {
    return new SessionToken(new Buffer(external, 'ascii'));
};

SessionToken.TOKEN_LENGTH = TOKEN_LENGTH;

module.exports = SessionToken;

}).call(this,require(17).Buffer)
},{"17":17}],386:[function(require,module,exports){
var AbstractDataType = require(138);
var BytesImpl = require(143);

var BufferOutputStream = require(202);
var BufferInputStream = require(201);

var Codec = require(203);

var EventMetadataSerialiser = require(337);
var EventImpl = require(339);

var ORIGINAL_EVENT = 0;
var EDIT_EVENT = 1;

function eventToBytes(valueToBytes, event) {
    if (!event) {
        throw new Error("Cannot convert null event to Bytes");
    }

    return writeValue(valueToBytes, event);
}

function writeValue(valueToBytes, event) {
    var output = new BufferOutputStream();

    if (event.isEditEvent) {
        Codec.writeByte(output, EDIT_EVENT);
        EventMetadataSerialiser.write(output, event.originalEvent);
    } else {
        Codec.writeByte(output, ORIGINAL_EVENT);
    }

    EventMetadataSerialiser.write(output, event);

    var serialised = valueToBytes(event.value);
    Codec.writeBytes(output, serialised.$buffer);

    return new BytesImpl(output.getBuffer());
}

function bytesToEvent(bytesToValue, bytes) {
    var input = new BufferInputStream(bytes.$buffer);

    var eventType = Codec.readByte(input);

    switch (eventType) {
        case ORIGINAL_EVENT :
            return readOriginalEvent(bytesToValue, input);
        case EDIT_EVENT :
            return readEditEvent(bytesToValue, input);
        default :
            throw new Error('Unrecognised event type: ' + eventType);
    }
}

function readOriginalEvent(bytesToValue, input) {
    var metadata = EventMetadataSerialiser.read(input);
    var value = bytesToValue(Codec.readBytes(input));

    return EventImpl.create(metadata, metadata, value);
}

function readEditEvent(bytesToValue, input) {
    var originalEvent = EventMetadataSerialiser.read(input);
    var metadata = EventMetadataSerialiser.read(input);
    var value = bytesToValue(Codec.readBytes(input));

    return EventImpl.create(metadata, originalEvent, value);
}

function implementation(b, o, l) {
    return new BytesImpl(b, o, l);
}

function TimeSeriesEventDataType(name, bytesToValue, valueToBytes) {
    AbstractDataType.call(
        this,
        name,
        EventImpl,
        implementation,
        function (e) {
            return eventToBytes(valueToBytes, e);
        },
        function(b) {
            return bytesToEvent(bytesToValue, b);
        },
        [],
        true);

    this.writeValue = function(event) {
        return writeValue(valueToBytes, event);
    };

    this.validate = function() {
    };
}

module.exports = TimeSeriesEventDataType;

module.exports.create = function(delegate) {
    return new TimeSeriesEventDataType("timeseriesevent-" + delegate.name(), delegate.readValue, delegate.toBytes);
};
},{"138":138,"143":143,"201":201,"202":202,"203":203,"337":337,"339":339}],387:[function(require,module,exports){
var Codec = require(203);

module.exports = {
    write : function(output, attributes) {
        Codec.writeString(output, attributes.emptyFieldValue);
    },
    read : function(input) {
        var emptyFieldValue = Codec.readString(input);

        return {
            emptyFieldValue : emptyFieldValue
        };
    }
};
},{"203":203}],388:[function(require,module,exports){
var Codec = require(203);

module.exports = {
    write : function(output, attributes) {
        Codec.writeString(output, attributes.handler);
    },
    read : function(input) {
        var handler = Codec.readString(input);

        return {
            handler : handler
        };
    }
};
},{"203":203}],389:[function(require,module,exports){
var Codec = require(203);

module.exports = {
    write : function(output, attributes) {
        Codec.writeString(output, attributes.slaveMasterTopic);
    },
    read : function(input) {
        var master = Codec.readString(input);

        return {
            slaveMasterTopic : master
        };
    }
};
},{"203":203}],390:[function(require,module,exports){
var Codec = require(203);
var BEES = require(230);
var TopicType = require(435).TopicType;

var SchemaSerialiser = require(227);
var TopicPropertiesSerialiser = require(394);
var RecordTopicDetailsSerialiser = require(387);
var RoutingTopicDetailsSerialiser = require(388);
var SlaveTopicDetailsSerialiser = require(389);

function getSubSerialiser(type) {
    switch (type) {
        case TopicType.RECORD :
            return RecordTopicDetailsSerialiser;
        case TopicType.ROUTING :
            return RoutingTopicDetailsSerialiser;
        case TopicType.SLAVE :
            return SlaveTopicDetailsSerialiser;
    }

    return null;
}

module.exports = {
    read : function(input) {
        var details = {};

        if (Codec.readBoolean(input)) {
            details.type = BEES.read(input, TopicType);

            if (Codec.readBoolean(input) === false) {
                return details;
            }

            details.schema = SchemaSerialiser.read(input);

            if (Codec.readBoolean(input) === false) {
                return details;
            }

            Codec.readBoolean(input);

            var tidyOnUnsubscribe = Codec.readBoolean(input);
            var reference = Codec.readString(input);

            var properties = TopicPropertiesSerialiser.read(input);

            var serialiser = getSubSerialiser(details.type);
            var attributes = {};

            if (serialiser) {
                attributes = serialiser.read(input);
            }

            attributes.tidyOnUnsubscribe = tidyOnUnsubscribe;
            attributes.properties = properties;
            attributes.reference = reference;

            details.attributes = attributes;
        }

        return details;
    },
    write : function(output, details) {
        if (details) {
            Codec.writeBoolean(output, true);
            BEES.write(output, details.type);

            Codec.writeBoolean(output, true);

            if (details.schema) {
                SchemaSerialiser.write(output, details.schema);
            }

            Codec.writeBoolean(output, true);

            Codec.writeBoolean(output, true);

            Codec.writeBoolean(output, details.attributes.tidyOnUnsubscribe);
            Codec.writeString(output, details.attributes.reference);

            TopicPropertiesSerialiser.write(output, details.attributes.properties);

            var serialiser = getSubSerialiser(details.type);

            if (serialiser) {
                serialiser.write(output, details.attributes);
            }
        } else {
            Codec.writeBoolean(output, false);
        }
    }
};

},{"203":203,"227":227,"230":230,"387":387,"388":388,"389":389,"394":394,"435":435}],391:[function(require,module,exports){
var TopicType = require(435).TopicType;
var DataTypes = require(144);
var schema = require(226);

var MString = require(210);

var MetadataUtil = require(212);
var ContentUtil = require(131);

var DEFAULT_ATTRIBUTES = {
    slaveMasterTopic : "",
    emptyFieldValue : "",
    handler : "",
    tidyOnUnsubscribe : false,
    properties : {},
    reference : "",
};

function isTopicType(value) {
    for (var t in TopicType) {
        if (TopicType[t] === value) {
            return true;
        }
    }

    return false;
}

function createDetails(type, schema, properties) {
    var attributes = {};

    for (var k in DEFAULT_ATTRIBUTES) {
        attributes[k] = DEFAULT_ATTRIBUTES[k];
    }

    if (properties) {
        var props = {};

        for (var p in properties) {
            switch (p) {
                case "TIDY_ON_UNSUBSCRIBE" :
                    attributes.tidyOnUnsubscribe = properties.TIDY_ON_UNSUBSCRIBE === "true";
                    break;
                case "SLAVE_MASTER_TOPIC" :
                    attributes.slaveMasterTopic = properties.SLAVE_MASTER_TOPIC;
                    break;
                default :
                    props[p] = properties[p];
                    break;
            }
        }

        attributes.properties = props;
    }

    return {
        type : type,
        schema : schema,
        attributes : attributes
    };
}

module.exports.deriveDetails = function(value) {
    var details = {};
    var meta;

    if (value === undefined) {
        return createDetails(TopicType.STATELESS);
    }

    if (isTopicType(value)) {
        if (value === TopicType.SINGLE_VALUE) {
            meta = new MString();
        }

        return createDetails(value, schema(meta));
    }

    if (MetadataUtil.isMetadata(value)) {
        meta = value;
        details = createDetails(MetadataUtil.getType(meta), schema(meta));
    } else if (ContentUtil.isRecordContent(value)) {
        meta = MetadataUtil.deriveMetadata(value);
        details = createDetails(MetadataUtil.getType(meta), schema(meta));
    } else if (isTopicType(value.type)) {
        details = createDetails(value.type, null, value.properties);
    } else {
        var datatype = DataTypes.get(value);

        if (datatype) {
            details = createDetails(TopicType[datatype.name().toUpperCase()], null);
        } else {
            throw new Error("Invalid topic specification / details: " + value.toString());
        }
    }

    return details;
};

module.exports.deriveValue = function(type, value) {
    if (value === undefined) {
        return;
    }

    if (MetadataUtil.isMetadata(value) || isTopicType(value) || isTopicType(value.type)) {
        return undefined;
    } else if (ContentUtil.isRecordContent(value)) {
        return value;
    } else {
        var datatype = DataTypes.get(type);
        if (datatype) {
            return datatype.from(value);
        }
    }
};

module.exports.detailsFromSpecification = function(specification) {
    return createDetails(specification.type, {}, specification.properties);
};
},{"131":131,"144":144,"210":210,"212":212,"226":226,"435":435}],392:[function(require,module,exports){
var Codec = require(203);

var serialiser = {
    read : function(bis) {
        return Codec.readInt32(bis);
    },
    write : function(bos, id) {
        Codec.writeInt32(bos, id);
    }
};

module.exports = serialiser;
},{"203":203}],393:[function(require,module,exports){
var Codec = require(203);
var TopicIdSerialiser = require(392);
var TopicDetailsSerialiser = require(390);

var serialiser = {
    read : function(input) {
        return {
            id : TopicIdSerialiser.read(input),
            path : Codec.readString(input),
            details : TopicDetailsSerialiser.read(input)
        };
    },
    write : function(out, info) {
        TopicIdSerialiser.write(out, info.id);
        Codec.writeString(out, info.path);
        TopicDetailsSerialiser.write(out, info.details);
    }
};

module.exports = serialiser;
},{"203":203,"390":390,"392":392}],394:[function(require,module,exports){
var Codec = require(203);

var KEY_MAP = {
    PUBLISH_VALUES_ONLY : 30,
    VALIDATE_VALUES : 31,
    TIME_SERIES_EVENT_VALUE_TYPE : 2,
    TIME_SERIES_RETAINED_RANGE : 3,
    TIME_SERIES_SUBSCRIPTION_RANGE : 4,
    SCHEMA : 5,
    DONT_RETAIN_VALUE : 6
};

function findKey(id) {
    for (var k in KEY_MAP) {
        if (KEY_MAP[k] === id) {
            return k;
        }
    }
}

module.exports = {
    read : function(input) {
        return Codec.readMap(input, function(input) {
            return findKey(Codec.readByte(input));
        }, Codec.readString);
    },
    write : function(output, properties) {
        Codec.writeMap(output, properties, function(output, k) {
            Codec.writeByte(output, KEY_MAP[k]);
        }, Codec.writeString);
    }
};

},{"203":203}],395:[function(require,module,exports){
var TopicSpecificationSerialiser = require(397);
var TopicSpecificationInfo = require(396);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var topicId = Codec.readInt32(input);
        var path = Codec.readString(input);
        var spec = TopicSpecificationSerialiser.read(input);

        return new TopicSpecificationInfo(topicId, path, spec);
    },
    write : function(output, value) {
        Codec.writeInt32(output, value.id);
        Codec.writeString(output, value.path);
        TopicSpecificationSerialiser.write(output, value.specification);
    }
};

},{"203":203,"396":396,"397":397}],396:[function(require,module,exports){
module.exports = function TopicSpecificationInfo(id, path, specification) {
    this.id = id;
    this.path = path;
    this.specification = specification;
};
},{}],397:[function(require,module,exports){
var TopicSpecification = require(434);
var TopicType = require(435).TopicType;

var BEES = require(230);
var Codec = require(203);

module.exports = {
    read : function(input) {
        var type = BEES.read(input, TopicType);
        var properties = Codec.readDictionary(input, Codec.readString);

        return new TopicSpecification(type, properties);
    },
    write : function(output, specification) {
        BEES.write(output, specification.type);
        Codec.writeDictionary(output, specification.properties, Codec.writeString);
    }
};
},{"203":203,"230":230,"434":434,"435":435}],398:[function(require,module,exports){
var inherits = require(44);

var regex = require(421);
var utils = require(402);
var TopicSelector = require(431);

var DQ = utils.DescendantQualifier;

function FullPathSelector(components) {
    switch (components.qualifier) {
        case DQ.MATCH :
            this.regex = regex(components.base);
            break;
        case DQ.DESCENDANTS_OF_MATCH :
            this.regex = regex(components.base + "/.+");
            break;
        case DQ.MATCH_AND_DESCENDANTS :
            this.regex = regex(components.base + "(?:$|/.+)");
            break;
    }

    TopicSelector.call(
        this,
        components.type,
        components.prefix,
        components.expression);
}

inherits(FullPathSelector, TopicSelector);

FullPathSelector.prototype.doSelects = function (topicPath) {
    return this.regex(topicPath);
};

module.exports = FullPathSelector;

},{"402":402,"421":421,"431":431,"44":44}],399:[function(require,module,exports){
var inherits = require(44);
var utils = require(402);
var TopicSelector = require(431);

var DQ = utils.DescendantQualifier;

function TopicPathSelector(components) {
    var remainder = components.remainder;

    if (components.type === TopicSelector.Type.PATH) {
        if (components.prefix === "") {
            throw new Error("Topic path must have at least one part: " + remainder);
        }

        if (components.prefix.indexOf("//") > -1) {
            throw new Error("Topic path contains empty part: " + remainder);
        }
    }

    this.qualifier = components.qualifier;
    this.path = components.prefix;

    TopicSelector.call(
        this,
        components.type,
        components.prefix,
        components.expression);
}

inherits(TopicPathSelector, TopicSelector);

TopicPathSelector.prototype.doSelects = function (topicPath) {
    var match = this.path === topicPath;
    var descendants = topicPath.indexOf(this.path + '/') === 0;

    switch (this.qualifier) {
        case DQ.MATCH :
            return match;
        case DQ.DESCENDANTS_OF_MATCH :
            return descendants;
        case DQ.MATCH_AND_DESCENDANTS :
            return match || descendants;
        default :
            return false;
    }
};

module.exports = TopicPathSelector;

},{"402":402,"431":431,"44":44}],400:[function(require,module,exports){
var inherits = require(44);
var utils = require(402);
var TopicSelector = require(431);

var DELIMITER = "////";

function extractCommonPrefix(selectors) {
    if (selectors.length === 0) {
        return "";
    }

    var minimum = 2147483647;

        var prefixes = [], i = 0;

        selectors.forEach(function(selector) {      
        var part = utils.split(selector.prefix);

                minimum = Math.min(minimum, part.length);
        prefixes[i++] = part;
    });

        var composite = [];

        assembly:
    for (var j = 0; j < minimum; ++j) {
        var part = prefixes[0][j];

                for (var k = 0; k < prefixes.length; ++k) {
            if (part !== prefixes[k][j]) {
                break assembly;
            }
        }

                composite.push(part);
        composite.push(utils.PATH_SEPARATOR);
    }

        return utils.canonicalise(composite.join(''));
}

function SelectorSet(selectors) {
    var expanded = [];

        selectors.forEach(function(selector) {
        if (selector instanceof SelectorSet) {
            selector.selectors.forEach(function(nested) {
                expanded.push(nested);
            });
        } else {
            expanded.push(selector);
        }
    });

        this.selectors = expanded;

        var remainder = expanded.join(DELIMITER);
    var prefix = extractCommonPrefix(expanded);

        TopicSelector.call(
        this, 
        TopicSelector.Type.SELECTOR_SET, 
        prefix, 
        TopicSelector.Prefix.SELECTOR_SET + remainder);
}

SelectorSet.DELIMITER = DELIMITER;

inherits(SelectorSet, TopicSelector);

SelectorSet.prototype.selects = function(topicPath) {
    for (var i = 0; i < this.selectors.length; ++i) {
        if (this.selectors[i].selects(topicPath)) {
            return true;
        }
    }

        return false;
};

module.exports = SelectorSet;

},{"402":402,"431":431,"44":44}],401:[function(require,module,exports){
var inherits = require(44);

var regex = require(421);
var utils = require(402);
var TopicSelector = require(431);

var DQ = utils.DescendantQualifier;

function SplitPathSelector(components) {
    this.qualifier = components.qualifier;
    this.patterns = [];

    var parts = utils.split(components.base);

    parts.forEach(function (p) {
        this.patterns.push(regex(p));
    }.bind(this));

    TopicSelector.call(
        this,
        components.type,
        components.prefix,
        components.expression);
}

inherits(SplitPathSelector, TopicSelector);

SplitPathSelector.prototype.doSelects = function (topicPath) {
    var length = this.patterns.length,
        parts = topicPath.split(utils.PATH_SEPARATOR);

    switch (this.qualifier) {
        case DQ.MATCH :
            if (parts.length !== length) {
                return false;
            }
            break;
        case DQ.DESCENDANTS_OF_MATCH :
            if (parts.length <= length) {
                return false;
            }
            break;
        case DQ.MATCH_AND_DESCENDANTS :
            if (parts.length < length) {
                return false;
            }
            break;
    }

    var i = 0;
    for (; i < length; ++i) {
        if (!this.patterns[i](parts[i])) {
            return false;
        }
    }

    return true;
};

module.exports = SplitPathSelector;

},{"402":402,"421":421,"431":431,"44":44}],402:[function(require,module,exports){
var PATH_SEPARATOR = '/';

function split(path) {
    return path.split(PATH_SEPARATOR);
}

function canonicalise(path) {
    if (!path) {
        return "";
    }

    var l = path.length,
        s = l > 0 && path.charAt(0) === PATH_SEPARATOR ? 1 : 0,
        e = l > 1 && path.charAt(l - 1) === PATH_SEPARATOR ? 1 : 0;

    return path.substring(s, l - e);
}

function DQ(descendants) {
    this.includesDescendants = descendants;
}

var qualifiers = {
    MATCH: new DQ(false),
    DESCENDANTS_OF_MATCH: new DQ(true),
    MATCH_AND_DESCENDANTS: new DQ(true)
};

module.exports = {
    split: split,
    canonicalise: canonicalise,
    PATH_SEPARATOR: PATH_SEPARATOR,
    DescendantQualifier: qualifiers
};
},{}],403:[function(require,module,exports){
var split = require(423).split;
var cache = require(418);
var arr = require(412);

var TopicSelector = require(431);

var utils = require(402);
var PathSelector = require(399);
var SplitPathSelector = require(401);
var FullPathSelector = require(398);
var SelectorSet = require(400);

var T = TopicSelector.Type,
    P = TopicSelector.Prefix;

var DQ = utils.DescendantQualifier;

function parse(expression) {
    if (!expression) {
        throw new Error("Empty topic selector expression");
    }

    if (arguments.length > 1) {
        expression = arr.argumentsToArray(arguments);
    }

    if (expression instanceof Array) {
        var actual = [];

        expression.forEach(function (s) {
            if (s instanceof TopicSelector) {
                actual.push(s);
            } else {
                actual.push(parse(s));
            }
        });

        return new SelectorSet(actual);
    }

    if (typeof expression === "string") {
        var components = getComponents(expression);

        if (isTopicPath(components)) {
            return new PathSelector(components);
        }

        switch (components.type) {
            case T.SPLIT_PATH_PATTERN :
                return new SplitPathSelector(components);
            case T.FULL_PATH_PATTERN :
                return new FullPathSelector(components);
            case T.SELECTOR_SET :
                var parts = split(components.remainder, SelectorSet.DELIMITER);
                var selectors = [];

                parts.forEach(function (e) {
                    selectors.push(parse(e));
                });

                return new SelectorSet(selectors);
        }
    }

    throw new Error("Topic selector expression must be a string or array");
}

function getComponents(expression) {
    var type = getType(expression);

    if (type === null) {
        expression = P.PATH + expression;
        type = T.PATH;
    }

    var remainder = expression.substring(1);
    var qualifier, prefix, base;

    if (type === T.PATH) {
        base = remainder;
        qualifier = DQ.MATCH;
    } else if (remainder[remainder.length - 1] === '/') {
        if (remainder[remainder.length - 2] === '/') {
            qualifier = DQ.MATCH_AND_DESCENDANTS;
            base = utils.canonicalise(remainder.substring(0, remainder.length - 2));
        } else {
            qualifier = DQ.DESCENDANTS_OF_MATCH;
            base = utils.canonicalise(remainder.substring(0, remainder.length - 1));
        }
    } else {
        base = utils.canonicalise(remainder);
        qualifier = DQ.MATCH;
    }

    if (type === T.PATH) {
        prefix = utils.canonicalise(remainder);
    } else {
        prefix = extractPrefixFromRegex(base);
    }

    return {
        type: type,
        base: base,
        prefix: prefix,
        remainder: remainder,
        qualifier: qualifier,
        expression: type + remainder
    };
}

function isTopicPath(c) {
    if (c.type === T.PATH) {
        return true;
    }

    if (c.type === T.SELECTOR_SET || c.prefix === "") {
        return false;
    }

    return c.prefix === c.base;
}


function getType(expression) {
    switch (expression[0]) {
        case P.PATH:
            return T.PATH;
        case P.SPLIT_PATH_PATTERN :
            return T.SPLIT_PATH_PATTERN;
        case P.FULL_PATH_PATTERN :
            return T.FULL_PATH_PATTERN;
        case P.SELECTOR_SET :
            return T.SELECTOR_SET;
        case '$':
        case '%':
        case '&':
        case '<':
            throw new Error("Invalid expression type: " + expression);
        default:
            return null;
    }
}

var metaChars = ['*', '.', '+', '?', '^', '$', '{', '}', '(', ')', '[', ']', '\\'];

var normal = function (c) {
    return metaChars.indexOf(c) === -1;
};

var quoted = function () {
    return true;
};

function extractPrefixFromRegex(remainder) {
    var buffer = [], composite = [];
    var validator = normal;

    for (var i = 0, l = remainder.length; i < l; ++i) {
        var char = remainder[i];

        if (char === '\\' && i < l - 1) {
            var next = remainder[i + 1];

            if (validator === normal && next === 'Q') {
                validator = quoted;
            } else if (validator === quoted && next === 'E') {
                validator = normal;
            } else if ((next < 'a' || next > 'z') && (next < 'A' || next > 'Z')) {
                buffer.push(next);
            } else {
                buffer.length = 0;
                break;
            }

            i++;
        } else if (validator(char)) {
            if (char === utils.PATH_SEPARATOR) {
                composite.push(buffer.join(''));
                buffer.length = 0;
            }

            buffer.push(char);
        } else {
            buffer.length = 0;
            break;
        }
    }

    composite.push(buffer.join(''));
    return utils.canonicalise(composite.join(''));
}

module.exports = cache(parse);

},{"398":398,"399":399,"400":400,"401":401,"402":402,"412":412,"418":418,"423":423,"431":431}],404:[function(require,module,exports){
var Codec = require(203);
var parser = require(403);

var topicSelectorSerialiser = {
    read : function(input) {
        return parser(Codec.readString(input));
    },
    write : function(out, selector) {
        Codec.writeString(out, selector.expression);
    }
};

module.exports = topicSelectorSerialiser;
},{"203":203,"403":403}],405:[function(require,module,exports){

var connectionResponseDeserialiser = require(214);
var logger = require(416).create('Parsing');

function parseResponse(onHandshakeSuccess, onHandshakeError, message) {
    logger.trace('Received connection handshake response');
    var response;

    try {
        response = connectionResponseDeserialiser(message);
    } catch (e) {
        onHandshakeError(e);
        return null;
    }

    return onHandshakeSuccess(response);
}

module.exports = {
    connectionResponse : parseResponse
};

},{"214":214,"416":416}],406:[function(require,module,exports){
var NodeWebSocket = require(1);

var log = require(416).create('Transports');


function websocketSubTransportFactoryProvider() {
    if (typeof WebSocket === 'function') {
        log.debug('Using native websocket');
        return {
            constructor : WebSocket,
            enabled : true
        };
    } else if (typeof NodeWebSocket === 'function') {
        log.debug('Using Node WS library');
        return {
            constructor : NodeWebSocket,
            enabled : true
        };
    } else {
        log.debug('Websocket transport not available');
        return {
            enabled : false
        };
    }
}

function xhrSubTransportFactoryProvider() {
    if (typeof XMLHttpRequest === 'function') {
        log.debug('Using native XHR');
        return {
            constructor : XMLHttpRequest,
            enabled : true
        };
    } else {
        log.debug('XHR transport not available');
        return {
            enabled : false
        };
    }
}

module.exports = {
    WS : websocketSubTransportFactoryProvider(),
    XHR : xhrSubTransportFactoryProvider(),
    WEBSOCKET : websocketSubTransportFactoryProvider(),
    HTTP_POLLING : xhrSubTransportFactoryProvider()
};

},{"1":1,"416":416}],407:[function(require,module,exports){
var WSTransport = require(408);
var XHRTransport = require(409);
var Emitter = require(173);
var ResponseCode = require(216);
var standardSubtransports = require(406);
var parsing = require(405);
var logger = require(416).create('Cascading');

var knownTransports = {
    WS : WSTransport,
    XHR : XHRTransport,
    WEBSOCKET : WSTransport,
    HTTP_POLLING : XHRTransport
};

function filterTransports(requestedTransports, subtransports) {
    return requestedTransports.filter(function(name) {
        return name && knownTransports.hasOwnProperty(name);
    }).filter(function(name) {
        return subtransports[name].enabled;
    });
}

function isCascadableResponse(response) {
    return response === ResponseCode.ERROR;
}

function createTransport(subtransports, options, name) {
    var transport = knownTransports[name];
    var subtransport = subtransports[name];
    if (transport && subtransport && subtransport.enabled) {
        return new transport(options, subtransport.constructor);
    } else {
        throw new Error('Unknown transport name: ' + name);
    }
}

function CascadeDriver(subtransports, opts, transports, cascadeNotifications) {
    var emitter = Emitter.assign(this);
    var self = this;

    this.name = null;
    var req = null;
    var onHandshakeSuccess = null;
    var onHandshakeError = null;

    var responseReceived = false;
    var transportFactory = createTransport.bind(null, subtransports, opts);
    var transport = selectTransport();

    function onData(data) {
        emitter.emit('data', data);
    }

    function onClose(reason) {
        if (!responseReceived && cascade(true)) {
            return;
        }

        emitter.emit('close', reason);
    }

    function onError(error) {
        emitter.emit('error', error);
    }

    function onConnectionResponse(response) {
        responseReceived = true;
        if (isCascadableResponse(response.response)) {
            if (cascade()) {
                return;
            }
        }
        return onHandshakeSuccess(response);
    }

    function onParsingError(error) {
        responseReceived = true;
        if (!cascade()) {
            return onHandshakeError(error);
        }
    }

    function closeQuietly() {
        if (transport) {
            transport.off('data', onData);
            transport.off('close', onClose);
            transport.off('error', onError);
            transport.close();
            transport = null;
        }
    }

    function selectTransport(suppressClose) {
        if (transports.length === 0) {
            self.name = null;
            transport = null;
            logger.debug('Transports exhausted');
            cascadeNotifications.emit('transports-exhausted');
            if (!suppressClose) {
                emitter.emit('close');
            }
            return null;
        }

        self.name = transports.shift();
        transport = transportFactory(self.name);

        logger.debug('Selecting transport', self.name);
        cascadeNotifications.emit('transport-selected', self.name);
        return transport;
    }

    function internalConnect() {
        responseReceived = false;
        logger.debug('Attempting to connect');
        cascadeNotifications.emit('cascading-connect');

        transport.on('data', onData);
        transport.on('close', onClose);
        transport.on('error', onError);

        try {
            transport.connect(
                req,
                parsing.connectionResponse.bind(null, onConnectionResponse, onParsingError)
            );
        }
        catch(e) {
            if (!cascade()) {
                throw e;
            }
        }

        return true;
    }

    function cascade(suppressClose) {
        closeQuietly();

        if (!selectTransport(suppressClose)) {
            return false;
        }

        cascadeNotifications.emit('cascade');

        return internalConnect();
    }

    this.connect = function connect(request, handshake, handshakeError) {
        req = request;

        onHandshakeSuccess = handshake;
        onHandshakeError = handshakeError;

        internalConnect();
    };

    this.dispatch = function dispatch(message) {
        if (transport === null) {
            throw new Error('Unable to send message when no transport is set');
        }

        transport.dispatch(message);
    };

    this.close = function close() {
        if (transport !== null) {
            responseReceived = true; 
            transport.close();
            transport = null;
        }
    };
}

function Transports(subtransports) {
    var emitter = Emitter.assign(this);

    this.get = function get(options) {
        var validTransports = filterTransports(options.transports, subtransports);

        if (validTransports.length === 0) {
            logger.warn('No valid transports found');
            return null;
        }

        return new CascadeDriver(subtransports, options, validTransports, emitter);
    };
}

Transports.create = function(subtransports) {
    if (subtransports) {
        return new Transports(subtransports);
    } else {
        return new Transports(standardSubtransports);
    }
};

module.exports = Transports;

},{"173":173,"216":216,"405":405,"406":406,"408":408,"409":409,"416":416}],408:[function(require,module,exports){
(function (Buffer){
var encodeAsString = require(427).encodeAsString;
var Emitter = require(173);

var log = require(416).create('Websocket transport');

function constructURI(req, opts) {
    var scheme = opts.secure ? 'wss' : 'ws';
    var uri =  scheme + "://" + opts.host + ":" + opts.port + opts.path;

    uri += '?ty=' + req.type;
    uri += '&v=' + req.version;
    uri += '&ca=' + req.capabilities;
    uri += '&r=' + opts.reconnect.timeout;

    if (req.token) {
        uri += '&c=' + encodeURIComponent(req.token);
        uri += "&cs=" + req.availableClientSequence;
        uri += "&ss=" + req.lastServerSequence;
    }

    if (opts.principal) {
        uri += "&username=" + encodeURIComponent(opts.principal);
        uri += "&password=" + encodeURIComponent(encodeAsString(opts.credentials));
    }

    return uri;
}

function WSTransport(opts, constructor) {
    var emitter = Emitter.assign(this);

    var handler = function(message) {
        if (message.data.length > opts.maxMessageSize) {
            emitter.error(new Error(
                "Received message of size: " +
                message.data.length +
                ", exceeding max message size: " +
                opts.maxMessageSize));
        } else {
            emitter.emit('data', new Buffer(new Uint8Array(message.data)));
        }
    };

    var socket;

    this.connect = function connect(req, handshake) {
        try {
            var uri = constructURI(req, opts);
            socket = new constructor(uri);

            log.debug("Created websocket", uri);
        } catch (error) {
            throw new Error('Unable to construct WebSocket', error);
        }

                socket.binaryType = "arraybuffer";

        socket.onmessage = function(msg) {
            socket.onmessage = handler;
            socket.onerror = emitter.error;

            handshake(new Buffer(new Uint8Array(msg.data)));
        };

        socket.onclose = emitter.close;
        socket.onerror = emitter.close;
    };

    this.dispatch = function dispatch(message) {
        log.debug("Sending websocket message", message);

        try {
            socket.send(message);
            return true;
        } catch (err) {
            log.error("Websocket send error", err);
            return false;
        }
    };

    this.close = function() {
        log.debug("Closing websocket"); 
        socket.close();
    };
}

module.exports = WSTransport;

}).call(this,require(17).Buffer)
},{"17":17,"173":173,"416":416,"427":427}],409:[function(require,module,exports){
(function (Buffer){
var encodeAsString = require(427).encodeAsString;
var Emitter = require(173);
var Queue = require(420);
var ResponseCode = require(216);
var BufferInputStream = require(201);
var BufferOutputStream = require(202);

var log = require(416).create('XHR transport');
function constructURI(req, opts) {
    var scheme = opts.secure ? 'https' : 'http';
    var uri =  scheme + "://" + opts.host + ":" + opts.port + opts.path;

    var headers = {
        m : "0",
        ty : "B",
        ca : req.capabilities,
        r : opts.reconnect.timeout,
        v : req.version
    };

    if (req.token) {
        headers.c = req.token;
        headers.cs = req.availableClientSequence;
        headers.ss =req.lastServerSequence;
    }

    if (opts.principal) {
        headers.username = encodeURIComponent(opts.principal);
        headers.password = encodeURIComponent(encodeAsString(opts.credentials));
    }

    return { 'uri' : uri , 'headers' : headers };
}

function XHRTransport(opts, xhr) {
    var emitter = Emitter.assign(this);

    var token;
    var pollSequence = 0;
    var pingSequence = 0;
    var URI;
    var pollRequest = null;
    var aborted = false;
    var isSending = false;
    var protocolVersion = null;

    var self = this;

    var queue = Queue.create();
    var constructor = xhr;

    this.connect = function connect(req, handshake) {
        try {
            var url = constructURI(req, opts);
            protocolVersion = url.headers.v;
            URI = url.uri;
            var request = this.createRequest(url.headers, url.uri);
            log.debug('Created XHR', url.uri);

            request.onreadystatechange = function() {
                if (request.readyState === 4 ) {
                    if (request.status === 200) {
                        var handShakeData = request.responseText;
                        var serverResponse = handshake(new Buffer(handShakeData, 'binary'));

                        if (!serverResponse) {
                            return;
                        }

                        token = serverResponse.token;

                        var responseCode = serverResponse.response;
                        var isSuccesss = ResponseCode.isSuccess(responseCode);

                        if (isSuccesss) {
                            self.poll();
                        } else {
                            log.debug(responseCode.id + ' - '  + responseCode.message);
                            emitter.close();
                        }
                    } else {
                        emitter.close();
                    }
                }
            };

            request.send(null);

        } catch (error) {
            throw new Error('Unable to create polling transport', error);
        }
    };

    this.close = function() {
        log.trace("Closing XHR transport");

        if (pollRequest) {
            aborted = true;
            pollRequest.abort();
        }

        queue = Queue.create();

        emitter.close();
    };

    this.createRequest = function createRequest(headers, uri) {
        var request = new constructor();

        request.open("POST", uri, true);
        request.overrideMimeType('text\/plain; charset=x-user-defined');

        for (var header in headers) {
            try {
                request.setRequestHeader(header,headers[header]);
            } catch(e) {
                log.warn("Can't set header " + header + ":" + headers.join(":"));
            }
        }

        return request;
    };

    this.poll = function poll() {
        var request = this.createRequest(
            { "m" : "1", "c" : token, "s" : pollSequence++, "v" : protocolVersion },
            URI);

        request.onreadystatechange = function() {
            if (request.readyState === 4) {
                if (request.getResponseHeader('diffusion-connection') === 'reconnect') {
                    emitter.close();
                }
                else if (request.status === 200) {
                    var data = request.responseText;

                    var bis = new BufferInputStream(new Buffer(data, 'base64'));

                    while (bis.pos !== bis.count) {
                        var length = bis.readInt32();

                        if (length > opts.maxMessageSize) {
                            emitter.error(new Error(
                                "Received message of size: " +
                                length +
                                ", exceeding max message size: " +
                                opts.maxMessageSize));

                            return;
                        }

                        var typeAndEncoding = bis.read();

                        var body = bis.readMany(length);
                        var payload = new Buffer(body.length + 1); 

                        payload.writeUInt8(typeAndEncoding);
                        body.copy(payload, 1);

                        emitter.emit('data', payload);
                    }

                    self.poll();
                }
                else {
                    emitter.close();
                }
            }
        };

        pollRequest = request;
        request.send(null);
    };

    this.dispatch = function dispatch(message) {
        if (aborted) {
            return false;
        }

        queue.add(message);

        if (isSending) {
            return true;
        }

        self.flush();
    };

    this.flush = function flush() {
        if (queue.isEmpty()) {
            return;
        }

        var buffer = drainToBuffer(queue.drain());

        var request = this.createRequest(
                            { "m" : "2", "c" : token, "s" : pingSequence++, "v" : protocolVersion },
                            URI);

        request.onreadystatechange = function() {
            if (request.readyState === 4) {
                if (request.getResponseHeader('diffusion-connection') === 'reconnect') {
                    emitter.close();
                }
                else if (request.status === 200) {
                    isSending = false;
                    self.flush();
                } else {
                    emitter.close();
                }
            }
        };

         isSending = true;
        request.send(buffer.toString('base64'));
    };
}

function drainToBuffer (messages) {
    var messagesBOS = new BufferOutputStream();

    for (var i = 0; i < messages.length; i++) {
        var msg = messages[i];

        messagesBOS.writeInt32(msg.length - 1); 
        messagesBOS.writeMany(msg, 0, msg.length);
    }

    return messagesBOS.getBuffer();
}

module.exports = XHRTransport;

}).call(this,require(17).Buffer)
},{"17":17,"173":173,"201":201,"202":202,"216":216,"416":416,"420":420,"427":427}],410:[function(require,module,exports){
var Update = require(411);
var BEES = require(230);
var ContentSerialiser = require(127);

module.exports = {
    read: function (input) {
        var type = BEES.read(Update.Type, input);
        switch (type) {
            case Update.Type.CONTENT :
                var data = ContentSerialiser.read(input);
                return new Update(data);
            default:
                throw new Error('Received unimplemented update type ' + type);
        }
    },

    write: function (output, update) {
        output.write(update.type);
        switch (update.type) {
            case Update.Type.CONTENT:
                output.write(output, update.action);
                ContentSerialiser.write(output, update.data);
                break;
            default:
                throw new Error('Trying to write unimplemented update type ' + update.type);
        }
    }
};

},{"127":127,"230":230,"411":411}],411:[function(require,module,exports){
var UpdateType = {
    CONTENT : 0,
    PAGED_RECORD_ORDERED : 1,
    PAGED_STRING_ORDERED : 2,
    PAGED_RECORD_UNORDERED : 3,
    PAGED_STRING_UNORDERED : 4
};

var UpdateAction = {
    UPDATE : 0,
    REPLACE : 1
};

var Update = function(data, type, action) {
    this.data = data;
    this.type =  type !== undefined ? type : UpdateType.CONTENT;
    this.action = action !== undefined ? action : UpdateAction.UPDATE;
};

module.exports = {
    Type : UpdateType,
    Update : Update
};

},{}],412:[function(require,module,exports){
function remove(arr, e) {
    var i = arr.indexOf(e);
    if (i > -1) {
        arr.splice(i, 1);
        return true;
    }

        return false;
}

function fill(array, value, start, end) {
    start = start || 0;
    end = end || array.length;

    for (var i = start; i < end; ++i) {
        array[i] = value;
    }
}

function ofSize(size, initialValue) {
    var a = [];

    a.length = size;

    if (initialValue !== undefined) {
        fill(a, initialValue);
    }

    return a;
}

var s = Array.prototype.slice;

function argumentsToArray(args) {
    return s.call(args, 0);
}

module.exports = {
    fill : fill,
    ofSize : ofSize,
    remove : remove,
    argumentsToArray : argumentsToArray
};
},{}],413:[function(require,module,exports){
var Emitter = require(173);

function FSM(initial, states) {
    var emitter = Emitter.assign(this);
    var current = states[initial];

    this.state = initial;

    this.change = function change(state) {
        if (this.state === state) {
            return true;
        }

        if (states[state] && (current === '*' || current.indexOf(state) > -1)) {
            emitter.emit('change', this.state, state);
            current = states[state];

                        this.state = state;
            return true;
        }

        return false;
    };
}

module.exports = {
    create : function create(initial, states) {
        return new FSM(initial, states);
    }
};

},{"173":173}],414:[function(require,module,exports){
var a2a = require(412).argumentsToArray;

function curry() {
    var args = a2a(arguments);
    var fn = args.shift();

    return function () {
        return callWithArguments(fn, args.concat(a2a(arguments)));
    };
}

function curryR() {
    var args = a2a(arguments);
    var fn = args.shift();

    return function () {
        return callWithArguments(fn, a2a(arguments).concat(args));
    };
}

function callWithArguments(f, args, count) {
    count = count === undefined ? args.length : count;

    switch (count) {
        case 0 :
            return f();
        case 1 :
            return f(args[0]);
        case 2 :
            return f(args[0], args[1]);
        case 3 :
            return f(args[0], args[1], args[2]);
        case 4 :
            return f(args[0], args[1], args[2], args[3]);
        case 5 :
            return f(args[0], args[1], args[2], args[3], args[4]);
        case 6 :
            return f(args[0], args[1], args[2], args[3], args[4], args[5]);
        case 7 :
            return f(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
        case 8 :
            return f(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }

    return f.apply(f, args);
}

function newWithArguments(f, args, count) {
    count = count === undefined ? args.length : count;

    switch (count) {
        case 0 :
            return new f();
        case 1 :
            return new f(args[0]);
        case 2 :
            return new f(args[0], args[1]);
        case 3 :
            return new f(args[0], args[1], args[2]);
        case 4 :
            return new f(args[0], args[1], args[2], args[3]);
        case 5 :
            return new f(args[0], args[1], args[2], args[3], args[4]);
        case 6 :
            return new f(args[0], args[1], args[2], args[3], args[4], args[5]);
        case 7 :
            return new f(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
        case 8 :
            return new f(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }

    function Proxy() {
        f.apply(this, args);
    }

    Proxy.prototype = f.prototype;

    return new Proxy();
}

module.exports = {
    curry: curry,
    curryR: curryR,
    callWithArguments: callWithArguments,
    newWithArguments: newWithArguments
};

module.exports.identity = function(o) {
    return o;
};

},{"412":412}],415:[function(require,module,exports){
var functions = require(414);
var array = require(412);


function _interface() {
    var args = array.argumentsToArray(arguments);

    var name = args.shift();
    var members = args.pop() || [];

        args.forEach(function(ex) {
        members = members.concat(ex.members);
    });

        var boundMembers = members.map(function(member) {
        return {
            name : member,
            check : function(impl) {
                return impl[member] === undefined;
            },
            throw : function() {
                throw new Error(name + '#' + member + '" must be implemented');
            }
        };
    });

        var filter = function(implementation) {
        return boundMembers.filter(function(member) {
            return member.check(implementation);
        });
    };

        filter.toString = function() {
        return name;
    };

        filter.members = members;
    return filter;
}

function _implements() {
    var args = Array.prototype.slice.call(arguments, 0);
    var impl = args.pop();

        var unsatisfied = [];
    var constructor;

        if (impl instanceof Function) {
        constructor = impl;
        impl = constructor.prototype;
    } else {
        constructor = impl.__constructor || function() { };

        for (var m in constructor.prototype) {
            impl[m] = constructor.prototype[m]; 
        }
    }

    args.forEach(function(_interface) {
        unsatisfied = unsatisfied.concat(_interface(impl));
    });

    constructor.prototype = impl;

    if (unsatisfied.length === 0) {
        return constructor;
    } else {
        var proxy = function() {
            var instance = functions.newWithArguments(constructor, arguments);

                        unsatisfied.forEach(function(m) {
                if (m.check(instance)) {
                    m.throw();
                }
            });

                                    return instance;
        };

        proxy.toString = function() {
            return constructor.toString();
        };

        proxy.isPrototypeOf = function(c) {
            return c instanceof constructor;
        };

        proxy.__constructor = constructor;

                return proxy;
    }
}

module.exports = {
    _interface : _interface,
    _implements : _implements
};

},{"412":412,"414":414}],416:[function(require,module,exports){
var logger = require(45);

var methods = ['trace', 'debug', 'info', 'warn', 'error'];

function d() {
    return new Date();
}

function log(level, prefix) {
    var c = "|" + level.toUpperCase() + "|" + prefix + "|";

        return function(msg, arg) {
        if (arg) {
            logger[level](d() + c + msg, arg); 
        } else {
            logger[level](d() + c + msg);
        }
    };
}

function create(classname) {    
    var l = {};

        methods.forEach(function(m) {
        l[m] = log(m, classname);
    });

            return l;
}

module.exports = {
    create : create,
    setLevel : logger.setLevel
};

},{"45":45}],417:[function(require,module,exports){
function leadingZeros(i) {
    if (i === 0) {
        return 32;
    }

    var n = 1;

    if (i >>> 16 === 0) { n += 16; i <<= 16; }
    if (i >>> 24 === 0) { n +=  8; i <<=  8; }
    if (i >>> 28 === 0) { n +=  4; i <<=  4; }
    if (i >>> 30 === 0) { n +=  2; i <<=  2; }

    n -= i >>> 31;

    return n;
}

module.exports.approximateSquareRoot = function(value) {
    if (value < 0) {
        throw new Error("Value must be greater or equal to 0");
    }

    if (value === 0) {
        return 0;
    }

    var result = 1;

    for (var i = value; i > 0; i >>= 2) {
        result *= 2;
    }

    return result;
};

module.exports.approximateCubeRoot = function(value) {
    if (value < 0) {
        throw new Error("Value must be great or equal to 0");
    }

    if (value === 0) {
        return 0;
    }

    var h = 32 - leadingZeros(value);

    return 1 << h / 3;
};

module.exports.findNextPowerOfTwo = function(value) {
    if (value < 0 || value > 1 << 30) {
        throw new Error("Illegal argument: " + value);
    }


    value--;
    value |= value >> 1;
    value |= value >> 2;
    value |= value >> 4;
    value |= value >> 8;
    value |= value >> 16;
    value++;

    return value;
};

module.exports.intBitsToFloat = function(bytes) {
    var sign = (bytes & 0x80000000) ? -1 : 1;
    var exponent = ((bytes >> 23) & 0xFF) - 127;
    var significand = (bytes & ~(-1 << 23));

    if (exponent === 128) {
        return sign * ((significand) ? Number.NaN : Number.POSITIVE_INFINITY);
    }

    if (exponent === -127) {
        if (significand === 0) {
            return sign * 0.0;
        }

        exponent = -126;
        significand /= (1 << 22);
    } else {
        significand = (significand | (1 << 23)) / (1 << 23);
    }

    return sign * significand * Math.pow(2, exponent);
};
},{}],418:[function(require,module,exports){
var a2a = require(412).argumentsToArray;

function defaultCacheKey(args) {
    return args;
}

function memoize(f, cache, matcher) {
    matcher = matcher || defaultCacheKey;
    cache = cache || {};

    var c = function() {
        var args = a2a(arguments);
        var a = matcher(args);
        var r = cache[a];

                if (r) {
            return r;
        } else {
            r = f.apply(f, args);
        }

                cache[a] = r;
        return r;
    };

    c._uncached = f;
    return c;
}

module.exports = memoize;

},{"412":412}],419:[function(require,module,exports){
var MESSAGE_TYPE_MASK = 0xA7D8C0;
var ZLIB_TYPE_FLAGS = 0x80;

function MessageEncoding(encodingByte, messageTypeFlags) {
    return {
        encodingByte : encodingByte,
        messageTypeFlags : messageTypeFlags
    };
}

function extractMessageType(typeAndEncoding) {
    return typeAndEncoding & ~MESSAGE_TYPE_MASK;
}
function extractMessageEncoding(typeAndEncoding) {
    var encoding = typeAndEncoding & MESSAGE_TYPE_MASK;
    if (encoding === ZLIB_TYPE_FLAGS) {
        return EncodingType.ZLIB;
    }
    else {
        return EncodingType.NO_ENCODING;
    }
}

var EncodingType = {
    NO_ENCODING : MessageEncoding(0x00, 0x00),
    ZLIB : MessageEncoding(0x12, 0x01)
};

module.exports = {
    extractMessageEncoding : extractMessageEncoding,
    extractMessageType : extractMessageType,
    EncodingType : EncodingType
};
},{}],420:[function(require,module,exports){
function Queue() {
    var messages = [];

    this.length = function () {
        return messages.length;
    };

    this.add = function add(message) {
        messages.push(message);
    };

    this.drain = function drain() {
        return messages.splice(0, messages.length);
    };

    this.isEmpty = function isEmpty() {
        return messages.length === 0;
    };

}

module.exports = {
    create: function create() {
        return new Queue();
    }
};
},{}],421:[function(require,module,exports){
function regex(s, context) {
    context = context || "";

        if (s === "") {
        throw new Error("Empty regular expression [" + context + "]");
    }
    try {
        var r = new RegExp(s);

                return function(test) {
            var m = r.exec(test);
            return m && m[0] === test;
        };
    } catch (e) {
        throw new Error("Bad regular expression [" + e.message + ", " + context + "]");
    }
}

module.exports = regex;
},{}],422:[function(require,module,exports){
function requireNonNull(value, what) {
    if (typeof value === 'undefined' || value === null) {
        throw new Error(what + " is null");
    }

        return value;
}

module.exports = requireNonNull;
},{}],423:[function(require,module,exports){
function split(str, delim) {
    if (str === "") {
        return [];
    }

        var parts = [], l = delim.length, i = str.lastIndexOf(delim);

        while (i > -1) {
        parts.push(str.substring(i + l, str.length));
        str = str.substring(0, i);
        i = str.lastIndexOf(delim);
    }

        parts.push(str);

        return parts.reverse();
}

module.exports = {
    split : split
};
},{}],424:[function(require,module,exports){

function AliasMap() {
    var aliases = {};

    this.establish  = function establish(topic) {
        var bang = topic.indexOf('!');

        if (bang !== -1) {
            if (bang === 0) {
                return aliases[topic.substring(1)];
            } else {
                var alias = topic.substring(bang + 1);
                topic = topic.substring(0, bang);

                                aliases[alias] = topic;

                return topic;
            }
        } else {
            return topic;
        }
    };
}

AliasMap.create = function() {
    return new AliasMap();
};

module.exports = AliasMap;

},{}],425:[function(require,module,exports){
var Connection = require(426);

module.exports.create = function(aliases, transports, reconnectTimeout, recoveryBufferSize) {
    return new Connection(aliases, transports, reconnectTimeout, recoveryBufferSize);
};

},{"426":426}],426:[function(require,module,exports){
(function (global){
var RecoveryBuffer = require(204);
var BufferOutputStream = require(202);
var ResponseCode = require(216);
var CloseReason = require(67);
var Message = require(429);
var Emitter = require(173);

var logger = require(416).create('Connection');
var FSM = require(413);

var CLOSE_TIMEOUT = global.DIFFUSION_CLOSE_TIMEOUT || 1000;

var CONNECT_TIMEOUT = global.DIFFUSION_CONNECT_TIMEOUT || 10000;

var RECOVERY_BUFFER_INDEX_SIZE = global.DIFFUSION_RECOVERY_BUFFER_INDEX_SIZE || 8;

function Connection(aliases, transports, reconnectTimeout, recoveryBufferSize) {
    var emitter = Emitter.assign(this);

    var fsm = FSM.create('disconnected', {
        connecting      : ['connected', 'disconnected', 'closed'],
        connected       : ['disconnecting', 'disconnected', 'closed'],
        disconnecting   : ['disconnected'],
        disconnected    : ['connecting', 'closed'],
        closed          : []
    });

    var lastSentSequence = 0;
    this.lastReceivedSequence = 0;

    var recoveryBuffer = new RecoveryBuffer(recoveryBufferSize, RECOVERY_BUFFER_INDEX_SIZE);

    var scheduledRecoveryBufferTrim;
    var scheduledClose;
    var closeReason;

    var transport = null;
    var self = this;

    fsm.on('change', function(previous, current) {
        logger.debug('State changed: ' + previous + ' -> ' + current);
    });

    function onData(data) {
        logger.debug('Received message from transport', data);

        Message.parse(data, function(err, message) {
            if (!err) {
                if (message.type === Message.types.ABORT_NOTIFICATION) {
                    transport.close();
                    return;
                }

                if (message.topic) {
                    message.topic = aliases.establish(message.topic);
                }

                self.lastReceivedSequence++;

                emitter.emit('data', message);
            }
            else {
                logger.warn('Unable to parse message', err);

                if (fsm.change('closed')) {
                    closeReason = CloseReason.CONNECTION_ERROR;
                    transport.close();
                }
            }
        });
    }

    function onClose() {
        if (fsm.change('disconnected') || fsm.change('closed')) {
            clearInterval(scheduledRecoveryBufferTrim);
            clearTimeout(scheduledClose);

            logger.trace('Transport closed: ', closeReason);

            if (fsm.state === 'disconnected') {
                emitter.emit('disconnect', closeReason);
            } else {
                emitter.close(closeReason);
            }
        } else {
            logger.debug('Unable to disconnect, current state: ', fsm.state);
        }
    }

    function onError(err) {
        logger.error("Error from transport", err);

        if (fsm.change('closed')) {
            closeReason = CloseReason.TRANSPORT_ERROR;
            transport.close();
        }
    }

    function onHandshakeSuccess(response) {
        if (response.response === ResponseCode.RECONNECTED) {
            var messageDelta = lastSentSequence - response.sequence + 1;

            if (recoveryBuffer.recover(messageDelta, dispatch)) {
                recoveryBuffer.clear();
                lastSentSequence = response.sequence - 1;
            } else {
                var outboundLoss = lastSentSequence - recoveryBuffer.size() - response.sequence + 1;

                logger.warn("Unable to reconnect due to lost messages (" + outboundLoss + ")");

                if (fsm.change('disconnected')) {
                    closeReason = CloseReason.LOST_MESSAGES;
                    transport.close();
                }

                return response;
            }
        }

        if (response.success && fsm.change('connected')) {
            logger.trace('Connection response: ', response.response);
            closeReason = CloseReason.TRANSPORT_ERROR;
            emitter.emit('connect', response);
        } else {
            logger.error('Connection failed: ', response.response);

            if (response.response === ResponseCode.AUTHENTICATION_FAILED) {
                closeReason = CloseReason.ACCESS_DENIED;
            } else {
                closeReason = CloseReason.HANDSHAKE_REJECTED;
            }

            transport.close();
        }

        clearTimeout(scheduledClose);
        return response;
    }

    function onHandshakeError(error) {
        closeReason = CloseReason.HANDSHAKE_ERROR;

        clearTimeout(scheduledClose);

        logger.trace('Unable to deserialise handshake response', error);

        return;
    }

    this.connect = function connect(request, opts, timeout) {
        if (fsm.state !== 'disconnected') {
            logger.warn('Cannot connect, current state: ', fsm.state);
            return;
        }

        if (fsm.change('connecting')) {
            timeout = timeout === undefined ? CONNECT_TIMEOUT : timeout;

            closeReason = CloseReason.CONNECTION_ERROR;

            transport = transports.get(opts);

            transport.on('data', onData);
            transport.on('close', onClose);
            transport.on('error', onError);

            transport.connect(request, onHandshakeSuccess, onHandshakeError);

            scheduledClose = setTimeout(function() {
                logger.debug('Timed out connection attempt after ' + timeout);

                closeReason = CloseReason.CONNECTION_TIMEOUT;
                transport.close();
            }, timeout);

            scheduledRecoveryBufferTrim = setInterval(function() {
                if (fsm.state === 'connected') {
                    recoveryBuffer.flush(Date.now() - reconnectTimeout);
                }
            }, reconnectTimeout);
        }
    };

    this.resetSequences = function() {
        recoveryBuffer.clear();

        lastSentSequence = 0;
        this.lastReceivedSequence = 0;
    };

    this.getAvailableSequence = function() {
        return lastSentSequence + 1 - recoveryBuffer.size();
    };

    function dispatch(message) {
        var bos = new BufferOutputStream();
        Message.writeToBuffer(message, bos);

        lastSentSequence += 1;

        recoveryBuffer.put(message);
        recoveryBuffer.markTime(Date.now());

        transport.dispatch(bos.getBuffer());
    }

    this.send = function(message) {
        if (fsm.state === 'connected') {
            return dispatch(message);   
        }

        return false;
    };

    this.close = function(reason) {
        closeReason = reason;

        if (fsm.state === 'disconnected') {
            onClose();
        } else if (fsm.change('disconnecting')) {
            dispatch(Message.create(Message.types.CLOSE_REQUEST));

            scheduledClose = setTimeout(transport.close, CLOSE_TIMEOUT);
        }
    };

    this.closeIdleConnection = function() {
        if (fsm.change('disconnecting')) {
            logger.debug('Connection detected as idle');
            closeReason = CloseReason.IDLE_CONNECTION;
            transport.close();
        }
    };

    this.getState = function() {
        return fsm.state;
    };
}

module.exports = Connection;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"173":173,"202":202,"204":204,"216":216,"413":413,"416":416,"429":429,"67":67}],427:[function(require,module,exports){
(function (Buffer){
var BufferOutputStream = require(202);
var BufferInputStream = require(201);

var serialiser = require(234);

function encodeAsString(password) {
    var bos = new BufferOutputStream();

        serialiser.write(bos, password);

        return bos.getBase64();
}

function decodeAsString(s) {
    if (s) {
        var bis = new BufferInputStream(new Buffer(s, 'base64'));
        return serialiser.read(bis);
    } else {
        return null;
    }
}

module.exports = {
    encodeAsString : encodeAsString,
    decodeAsString : decodeAsString
};
}).call(this,require(17).Buffer)
},{"17":17,"201":201,"202":202,"234":234}],428:[function(require,module,exports){
var ConversationId = require(136);

module.exports = {
    cidFromHeaders : function(headers) {
        if (!headers || headers.length === 0) {
            throw new Error("Empty headers, expected at least 1");
        }

        return ConversationId.fromString(headers[headers.length - 1]);
    }
};
},{"136":136}],429:[function(require,module,exports){
(function (Buffer){
var inherits = require(44);
var BufferOutputStream = require(202);
var BufferInputStream = require(201);
var MessageEncoding = require(419);
var EncodingType = require(419).EncodingType;
var Zlib = require(16);
var when = require(64);

var systemHeaders = {
    0: [],
    1: [],
    2: [],
    4: [],
    5: [],
    28: [],
    29: [],
    34: ['topic'],
};

var types = {
    SERVICE_REQUEST: 0,
    SERVICE_RESPONSE: 1,
    SERVICE_ERROR: 2,
    TOPIC_VALUE: 4,
    TOPIC_DELTA: 5,
    ABORT_NOTIFICATION: 28,
    CLOSE_REQUEST: 29,
    FETCH_REPLY: 34,
};

var encoding = {
    NONE: 0,
    ENCRYPTION_REQUESTED: 1,
    COMPRESSION_REQUESTED: 2,
    BASE64_REQUESTED: 3,
    ENCRYPTED: 17,
    COMPRESSED: 18,
    BASE64: 19
};

var parse = function (buffer, callback) {
    var bis = new BufferInputStream(buffer);

    var typeAndEncoding = bis.read();
    var type = MessageEncoding.extractMessageType(typeAndEncoding);
    var encodingType = MessageEncoding.extractMessageEncoding(typeAndEncoding);

    if (systemHeaders[type] === undefined) {
        callback(new Error('Invalid message type: ' + type), null);
    }

    if (type === types.SERVICE_REQUEST || type === types.SERVICE_RESPONSE || type === types.SERVICE_ERROR) {
        callback(null, parseServiceMessage(type, bis));
    }
    else if (type === types.TOPIC_VALUE || type === types.TOPIC_DELTA) {
        parseClientTopicMesage(type, encodingType, bis, callback);
    }
    else {
        var headers = bis.readUntil(0x01).toString().split('\u0002');
        var body = bis.readMany(bis.count);
        var fields = {};
        if (systemHeaders[type]) {
            systemHeaders[type].forEach(function (key) {
                fields[key] = headers.shift();
            });
        }
        callback(null, new Message(type, encoding.NONE, fields, body, headers));
    }
};

function parseServiceMessage(type, bis) {
    var headers = [];
    var body = bis.readMany(bis.count);
    var fields = {};

    return new Message(type, encoding.NONE, fields, body, headers);
}

function parseClientTopicMesage(type, encodingType, bis, callback) {
    var headers = [];
    var fields = {'id' : bis.readInt32()};
    if (encodingType === EncodingType.ZLIB) {
        decompress(bis.readMany(bis.count)).then(function(result) {
            var message =  new Message(type, encoding.COMPRESSED, fields, result, headers);
            callback(null, message);
        }, function(error) {
            callback(error, null);
        });
    }
    else {
        var message =  new Message(type, encoding.NONE, fields, bis.readMany(bis.count), headers);
        callback(null, message);
    }
}

function decompress(input) {
    return when.promise(function (resolve, reject) {
        Zlib.inflate(input, function (err, buffer) {
            if (!err) {
                resolve(buffer);
            }
            else {
                reject(err);
            }
        });
    });
}

var create = function (type) {
    var fields;

    if (type === undefined || (typeof type === "number" && systemHeaders[type] === undefined)) {
        throw "Invalid message type: " + type;
    } else if (type instanceof Object) {
        fields = type;
    } else {
        fields = {type: type};
    }

    fields.encoding = fields.encoding || 0;
    fields.headers = fields.headers || [];
    fields.buffer = fields.buffer || new Buffer(0);

    return new WriteableMessage(fields);
};

var getHeaderString = function (message) {
    var sh = [];
    var headers = '';

    systemHeaders[message.type].forEach(function (h) {
        sh.push(message[h]);
    });

    message.headers.forEach(function (h) {
        sh.push(h);
    });

    if (systemHeaders[message.type].length === 0) {
        return headers;
    }

    if (sh.length > 0) {
        headers = sh.join('\u0002');
        headers += '\u0001';
    }

    return headers;
};

function writeToBuffer(message, bos) {
    bos.write(message.type);
    bos.writeString(getHeaderString(message));
    bos.writeMany(message.getBuffer());
}

function WriteableMessage(fields) {
    Message.call(this, fields.type, fields.encoding, fields, fields.buffer, fields.headers);
    BufferOutputStream.call(this, fields.buffer);
}

inherits(WriteableMessage, BufferOutputStream);

function Message(type, encoding, fields, data, headers) {
    this.type = type;
    this.encoding = encoding;

    for (var f in fields) {
        this[f] = fields[f];
    }

    this.data = data;
    this.headers = headers;
}

Message.prototype.getInputStream = function () {
    return new BufferInputStream(this.data);
};

module.exports = {
    types: types,
    parse: parse,
    create: create,
    encoding: encoding,
    getHeaderString: getHeaderString,
    writeToBuffer: writeToBuffer
};

}).call(this,require(17).Buffer)
},{"16":16,"17":17,"201":201,"202":202,"419":419,"44":44,"64":64}],430:[function(require,module,exports){
var DEFAULT_HOST = 'localhost';

var DEFAULT_PORT = 80;

var DEFAULT_SECURE_PORT = 443;

var DEFAULT_SECURE = true;

var DEFAULT_PATH = '/diffusion';

if (typeof window !== 'undefined') {
    if (window.location.hostname) {
        DEFAULT_HOST = window.location.hostname;
    }

    if (window.location.protocol === "http:") {
        DEFAULT_SECURE = false;

        if (window.location.port) {
            DEFAULT_PORT = parseInt(window.location.port);
        }
    }

    if (window.location.protocol === "https:") {
        DEFAULT_SECURE = true;

        if (window.location.port) {
            DEFAULT_SECURE_PORT = parseInt(window.location.port);
        }
    }
}

var DEFAULT_RECONNECT_TIMEOUT = 60000;
var DEFAULT_RECONNECT_STRATEGY = function(start) {
    setTimeout(start, 5000);
};

var DEFAULT_ABORT_STRATEGY = function(start, abort) {
    abort();
};

var DEFAULT_PRINCIPAL = "";
var DEFAULT_PASSWORD = "";

var DEFAULT_ACTIVITY_MONITOR = true;

var DEFAULT_TRANSPORTS = ['WEBSOCKET'];

var DEFAULT_MAX_MESSAGE_SIZE = 2147483647;
var MIN_MAX_MESSAGE_SIZE = 1024;


function Options(options) {
    options = options || {};

    if (options.host === undefined) {
        options.host = DEFAULT_HOST;
    } else if (options.host.indexOf(':') > -1) {
        var parts = options.host.split(':');

        if (options.port === undefined) {
            options.port = parseInt(parts[1]);
        }

        options.host = parts[0];
    }

    if (options.path === undefined) {
        options.path = DEFAULT_PATH;
    } else {
        if (options.path[0] !== '/') {
            options.path = '/' + options.path;
        }

        if (options.path.substring(options.path.length - DEFAULT_PATH.length) !== DEFAULT_PATH) {
            if (options.path[options.path.length - 1] === '/') {
                options.path = options.path.substring(0, options.path.length - 1);
            }

            options.path = options.path + DEFAULT_PATH;
        }
    }

    if (isNaN(parseInt(options.port, 10))) {
        options.port = undefined;
    } else {
        options.port = parseInt(options.port, 10);
    }

    if (options.secure === undefined) {
        if (options.port === undefined) { 
            options.secure = DEFAULT_SECURE;
        } else { 
            options.secure = options.port === DEFAULT_SECURE_PORT ? true : false;
        }
    }

    if (options.port === undefined) {
        options.port = options.secure ? DEFAULT_SECURE_PORT : DEFAULT_PORT;
    }

    this.host = options.host;
    this.port = options.port;
    this.path = options.path;
    this.secure = options.secure;

    if (options.reconnect === undefined || (typeof options.reconnect === 'boolean') && options.reconnect) {
        this.reconnect = {
            timeout : DEFAULT_RECONNECT_TIMEOUT,
            strategy : DEFAULT_RECONNECT_STRATEGY
        };
    } else if (typeof options.reconnect === 'number') {
        this.reconnect = {
            timeout : options.reconnect,
            strategy : DEFAULT_RECONNECT_STRATEGY
        };
    } else if (typeof options.reconnect === 'function') {
        this.reconnect = {
            timeout : DEFAULT_RECONNECT_TIMEOUT,
            strategy : options.reconnect
        };    
    } else if (typeof options.reconnect === 'object') {
        this.reconnect = {
            timeout : options.reconnect.timeout === undefined ? DEFAULT_RECONNECT_TIMEOUT : options.reconnect.timeout,
            strategy : options.reconnect.strategy || DEFAULT_RECONNECT_STRATEGY
        };    
    } else {
        this.reconnect = {
            timeout : 0,
            strategy : DEFAULT_ABORT_STRATEGY
        }; 
    }

    if (options.principal !== undefined) {
        this.principal = options.principal || DEFAULT_PRINCIPAL;
        this.credentials = options.credentials || DEFAULT_PASSWORD;
    }

    if (typeof options.transports === 'string') {
        this.transports = [options.transports];
    } else if (typeof options.transports === 'object' &&
            options.transports instanceof Array &&
            options.transports.length > 0) {
        this.transports = options.transports.slice();
    } else {
        this.transports = DEFAULT_TRANSPORTS.slice();
    }

    this.transports = this.transports.slice().map(function(t) {
        return t.toUpperCase();
    });

    var mms;

    if (options.maxMessageSize && options.maxMessageSize > MIN_MAX_MESSAGE_SIZE) {
        mms = options.maxMessageSize;
    } else {
        mms = DEFAULT_MAX_MESSAGE_SIZE;
    }

    this.maxMessageSize = mms;

    this.activityMonitor = (options.activityMonitor !== undefined) ? options.activityMonitor : DEFAULT_ACTIVITY_MONITOR;
}

Options.prototype.with = function(options) {
    var o = {};
    var k;

    for (k in this) {
        o[k] = this[k];
    }

    for (k in options) {
        o[k] = options[k];
    }

    return new Options(o);
};

module.exports = Options;

},{}],431:[function(require,module,exports){
var util = require(402);

function TopicSelector(type, prefix, expression) {
    this.type = type;

    this.prefix = prefix;

    this.expression = expression;
}

TopicSelector.Prefix = {
    PATH : '>',
    SPLIT_PATH_PATTERN : '?',
    FULL_PATH_PATTERN : '*',
    SELECTOR_SET : '#'
};

TopicSelector.Type = {
    PATH : TopicSelector.Prefix.PATH,
    SPLIT_PATH_PATTERN : TopicSelector.Prefix.SPLIT_PATH_PATTERN,
    FULL_PATH_PATTERN : TopicSelector.Prefix.FULL_PATH_PATTERN,
    SELECTOR_SET : TopicSelector.Prefix.SELECTOR_SET
};

TopicSelector.prototype.selects = function(topicPath) {
    var canonical = util.canonicalise(topicPath);
    return canonical.indexOf(this.prefix) === 0 && this.doSelects(canonical);
};

TopicSelector.prototype.toString = function() {
    return this.expression;
};

module.exports = TopicSelector;

},{"402":402}],432:[function(require,module,exports){
var parser = require(403);

var topicSelectors = {
    parse: parser
};

module.exports = topicSelectors;

},{"403":403}],433:[function(require,module,exports){
var _interface = require(415)._interface;

module.exports = _interface('Session', [
    'sessionID',

    'options',

    'close',

    'isClosed',

    'isConnected',

    'getPrincipal'





]);

},{"415":415}],434:[function(require,module,exports){
var keys = [
    "PUBLISH_VALUES_ONLY",
    "SCHEMA",
    "VALIDATE_VALUES",
    "SLAVE_MASTER_TOPIC",
    "TIDY_ON_UNSUBSCRIBE",
    "TIME_SERIES_EVENT_VALUE_TYPE",
    "TIME_SERIES_RETAINED_RANGE",
    "TIME_SERIES_SUBSCRIPTION_RANGE",
    "DONT_RETAIN_VALUE"
];

module.exports = function TopicSpecification(type, properties) {
    this.type = type;

    this.properties = properties || {};

    this.withProperty = function(key, value) {
        if (typeof key !== 'string' || keys.indexOf(key.toUpperCase()) < 0) {
            throw new Error("Invalid key: " + key);
        }

        if (typeof value !== 'string') {
            throw new Error('Invalid value, must be string: ' + value);
        }

        var newProperties = {};
        newProperties[key] = value;

        for (var k in this.properties) {
            if (k !== key) {
                newProperties[k] = this.properties[k];
            }
        }

        return new TopicSpecification(this.type, newProperties);
    };

    this.equals = function(other) {
        if (other && type === other.type && Object.keys(properties).length === Object.keys(other.properties).length) {
            for (var k in properties) {
                if (other.properties[k] !== properties[k]) {
                    return false;
                }
            }

            return true;
        }

        return false;
    };
};

module.exports.PUBLISH_VALUES_ONLY = "PUBLISH_VALUES_ONLY";

module.exports.VALIDATE_VALUES = "VALIDATE_VALUES";

module.exports.SLAVE_MASTER_TOPIC = "SLAVE_MASTER_TOPIC";

module.exports.TIDY_ON_UNSUBSCRIBE = "TIDY_ON_UNSUBSCRIBE";

module.exports.TIME_SERIES_EVENT_VALUE_TYPE = "TIME_SERIES_EVENT_VALUE_TYPE";

module.exports.TIME_SERIES_RETAINED_RANGE = "TIME_SERIES_RETAINED_RANGE";

module.exports.TIME_SERIES_SUBSCRIPTION_RANGE = "TIME_SERIES_SUBSCRIPTION_RANGE";

module.exports.SCHEMA = "SCHEMA";

module.exports.DONT_RETAIN_VALUE = "DONT_RETAIN_VALUE";

},{}],435:[function(require,module,exports){
var TopicSpecification = require(434);


function TopicType(id, stateful, functional) {
    this.id = id;
    this.stateful = stateful;
    this.functional = functional;
}

module.exports.TopicSpecification = TopicSpecification;

module.exports.TopicType = {
    STATELESS : new TopicType(1, false, false),

    SINGLE_VALUE : new TopicType(3, true, false),

    RECORD : new TopicType(4, true, false),

    SLAVE : new TopicType(7, true, false),

    ROUTING : new TopicType(12, false, true),

    BINARY : new TopicType(14, true, false),
    JSON : new TopicType(15, true, false),

    STRING : new TopicType(17, true, false),

    INT64 : new TopicType(18, true, false),

    DOUBLE : new TopicType(19, true, false),

    TIME_SERIES : new TopicType(16, true, false),

    RECORD_V2 : new TopicType(20, true, false)
};

function UnsubscribeReason(id, reason) {
    this.id = id;
    this.reason = reason;
}

module.exports.UnsubscribeReason = {
    SUBSCRIPTION_REFRESH : new UnsubscribeReason(undefined, "The server has re-subscribed this session"),

    STREAM_CHANGE : new UnsubscribeReason(undefined, "A more specific stream has been registered to the same path"),

    REQUESTED : new UnsubscribeReason(0, "The unsubscription was requested by this client"),
    CONTROL : new UnsubscribeReason(1, "The server or another client unsubscribed this client"),
    REMOVED : new UnsubscribeReason(2, "The topic was removed"),
    AUTHORIZATION : new UnsubscribeReason(3, "Not authorized to subscribe to this topic")
};

function UpdateFailReason(id, reason) {
    this.id = id;
    this.reason = reason;
}

module.exports.UpdateFailReason = {
    INCOMPATIBLE_UPDATE: new UpdateFailReason(1, "Update type is incompatible with topic type"),
    UPDATE_FAILED: new UpdateFailReason(2, "Update failed - possible content incompatibility"),
    INVALID_UPDATER: new UpdateFailReason(3, "Updater is invalid for updating"),
    MISSING_TOPIC: new UpdateFailReason(4, "Topic does not exist"),
    INVALID_ADDRESS: new UpdateFailReason(5, "Key or index value is invalid for topic data"),
    DUPLICATES: new UpdateFailReason(6, "Duplicates violation"),
    EXCLUSIVE_UPDATER_CONFLICT:
        new UpdateFailReason(7, "An exclusive update source is already registered for the topic branch"),
    DELTA_WITHOUT_VALUE:
        new UpdateFailReason(8, "An attempt has been made to apply a delta to a topic that does not yet have a value"),
    CLUSTER_REPARTITION: new UpdateFailReason(
        9, "When trying to update the topic the cluster was migrating the partition that owns the topic")
};

function TopicAddFailReason(id, reason) {
    this.id = id;
    this.reason = reason;
}

module.exports.TopicAddFailReason = {
    EXISTS: new TopicAddFailReason(1, "The topic already exists with the same details"),
    EXISTS_MISMATCH: new TopicAddFailReason(2, "The topic already exists, with different details"),
    INVALID_PATH: new TopicAddFailReason(3, "The topic path is invalid"),
    INVALID_DETAILS: new TopicAddFailReason(4, "The topic details are invalid"),
    USER_CODE_ERROR: new TopicAddFailReason(5, "A user supplied class could not be found or instantiated"),
    TOPIC_NOT_FOUND: new TopicAddFailReason(6, "A referenced topic could not be found"),
    PERMISSIONS_FAILURE: new TopicAddFailReason(7, "Invalid permissions to add a topic at the specified path"),
    INITIALISE_ERROR:
        new TopicAddFailReason(8, "The topic could not be initialised, supplied value may be of the wrong format"),
    UNEXPECTED_ERROR: new TopicAddFailReason(9, "An unexpected error occured while creating the topic"),
    CLUSTER_REPARTITION: new TopicAddFailReason(
        10, "When trying to create the topic the cluster was migrating the partition that owns the topic")
};

},{"434":434}]},{},[85])(85)
});