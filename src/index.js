import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const diffusion = window.diffusion;

class DiffusionReactDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hostname     : 'localhost',
            counterValue : ''
        };
    }

    connect(url) {
        var self = this;
        diffusion.connect({
            host : url,
            port : 443,
            secure : true,
            principal : 'admin',
            credentials : 'password'
        }).then(function(session) {

            console.log('Connected');

            session.stream('>foo/counter')
                .asType(diffusion.datatypes.json())
                .on('value', function(topicPath, specification, newValue, oldValue) {
                    self.setState({counterValue: JSON.stringify(newValue.get(), null, 2)});
                });
            session.subscribe('>foo/counter');
        });
    }
    
    render() {
        return (
            <div>
              <h1>Diffusion React Demo</h1>
              <ConnectionPanel hostname={this.state.hostname} onClick={(url) => this.connect(url)}/>
                <Counter counterValue={this.state.counterValue}/>
            </div>
        );
    }
}

class ConnectionPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hostname: this.props.hostname
        };
    }
    
    render() {
        return (
            <div>
              <input type="text" value={this.state.hostname} onChange={(evt) => this.setState({hostname: evt.target.value})}></input>
              <button onClick={() => this.props.onClick(this.state.hostname)}>Connect</button>
            </div>
        );
    }
}

class Counter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            time  : '',
            name  : '',
            count : ''
        };
    };
    
    componentWillReceiveProps(newProps) {
        var json = JSON.parse(newProps.counterValue);
        this.setState({
            time  : json.time,
            name  : json.name,
            count : json.count
        });
    }
    
    render() {
        return (
            <div className="CounterInfo">
              <div>Time: {this.state.time}</div>
              <div>Name: {this.state.name}</div>
              <div>Count: {this.state.count}</div>
            </div>
        )
    }
}

ReactDOM.render(
    <DiffusionReactDemo/>,
    document.getElementById('root')
);
